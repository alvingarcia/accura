<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('load_ci')) {

    function load_ci() {
        return get_instance();
    }

}

if (!function_exists('get_label')) {

    function get_label($table,$field) {
        $ci = load_ci();		
		$label = $ci->db->get_where('su-tb_sys_validation',array('strTable'=>$table,'strField'=>$field))->result_array();				
		if(empty($label))
			return $field;
		else
		{			
			$return = $label[0]['strLabel'];
			
			if($label[0]['strRequired'])
				$return.=" *";
			
			return $return;
		}
    }

}

if (!function_exists('get_flag')) {

    function get_flag($results,$from,$to) {
        $flag = "";
                    
        if($results < $from)
            $flag = "L";
        elseif($results > $to)
            $flag = "H";
        
        return $flag;
    }

}

if (!function_exists('get_priority')) {

    function get_priority($arr) {
        $ci = load_ci();
        $min = 9999;
        $prio = 0;
        foreach($arr as $key=>$val)
        {
            if($val < $min){
                $min = $val;
                $prio = $key;
            }
            elseif($val == $min)
            {
                //determine how to prioritize jobs
            }
        }
        return $prio;
    }

}


if (!function_exists('pw_hash')) {

    function pw_hash($string) {
        $ci = load_ci();
		$ci->load->library("salting");

		$key = date('YmdHis');
		$key.= $ci->config->item("encryption_key");
				
		$ci->salting->set_hash(md5($key));
		//$ci->salting->set_hash(md5($ci->config->item("encryption_key")));
		$ci->salting->set_string($string);
		return $ci->salting->hash_string();
    }
}
if (!function_exists('pw_unhash')) {

    function pw_unhash($string) {
        $ci = load_ci();
		$ci->load->library("salting");

/* 		$key = date('YmdHis');
		$key.= $ci->config->item("encryption_key");
		
		$ci->salting->set_hash(md5($key)); */
		//$ci->salting->set_hash(md5($ci->config->item("encryption_key")));
		$ci->salting->set_string($string);
		return $ci->salting->unhash_string();;
    }
}

if (!function_exists('simplify_title')) {
	
	function simplify_title ($title) {

	    $title = strtolower($title);
	    $title = strip_tags($title);
	    $title = stripslashes($title);
	    $title = html_entity_decode($title);

	    $title = trim(str_replace('\'', '', $title));
		$title = trim(str_replace(' ','_', $title));
	    $title = trim(preg_replace('/[^A-Za-z0-9_]/', '_', $title));
	
		return $title;
	
	}
}

if(!function_exists('get_day'))
{
    function get_day($day)
    {
        switch($day)
            {
                case '1':
                    $day = "Mon";
                    break;
                case '2':
                    $day = "Tue";
                    break;
                case '3':
                    $day = "Wed";
                    break;
                case '4':
                    $day = "Thu";
                    break;
                case '5':
                    $day = "Fri";
                    break;
                case '6':
                    $day = "Sat";
                    break;
            }
        
            return $day;
    }
}

if(!function_exists('switch_num'))
{
    function switch_num($num)
    {
        switch($num)
            {
                case '1':
                    $num = "1st";
                    break;
                case '2':
                    $num = "2nd";
                    break;
                case '3':
                    $num = "3rd";
                    break;
                case '4':
                    $num = "4th";
                    break;
                case '5':
                    $num = "5th";
                    break;
                case '6':
                    $num = "6th";
                    break;
            }
        
            return $num;
    }
}

if(!function_exists('switch_num_rev'))
{
    function switch_num_rev($num)
    {
        switch($num)
            {
                case '1st':
                    $num = "1";
                    break;
                case '2nd':
                    $num = "2";
                    break;
                case '3rd':
                    $num = "3";
                    break;
                case '4th':
                    $num = "4";
                    break;
                case '5th':
                    $num = "5";
                    break;
                case '6th':
                    $num = "6";
                    break;
            }
        
            return $num;
    }
}

if(!function_exists('switch_day_schema'))
{
    function switch_day_schema($num)
    {
        switch($num)
            {
                case '1 3 5':
                    $num = "M W F";
                    break;
                case '1 3':
                    $num = "M W";
                    break;
                case '2 4 6':
                    $num = "T TH S";
                    break;
                case '2 4':
                    $num = "T TH";
                    break;
                case '1 2 3 4 5 6':
                    $num = "ALL";
                    break;
                case '3 5':
                    $num = "W F";
                    break;
            }
        
            return $num;
    }
}

if(!function_exists('switch_user_level'))
{
    function switch_user_level($num)
    {
        switch($num)
            {
                case "lcc_admin":
                    $num = 0;
                    break;
                case "doctor":
                    $num = 1;
                    break;
                case "admin":
                    $num = 2;
                    break;
                case "extraction_admin":
                    $num = 3;
                    break;
                case "secretary":
                    $num = 4;
                    break;
                case "company hr":
                    $num = 5;
                    break;
                case "clinic staff":
                    $num = 6;
                    break;
                case "receptionist":
                    $num = 7;
                    break;
                case "nurse":
                    $num = 8;
                    break;
                default:
                    $num = 0;
            }
        
            return $num;
    }
}


if(!function_exists('convert_to'))
{
    function convert_to($val,$factor)
    {
       if($factor == 0)
           $factor = 1;
       return round($val/$factor,2);
    }
}

if(!function_exists('mmol_to_mgdl'))
{
    function mmol_to_mgdl($val)
    {
       return round($val/0.111,2);
    }
}

if(!function_exists('calculate_age'))
{
    function calculate_age($birthDate,$dte = null)
    {
        $birthDate = date("m/d/Y",strtotime($birthDate));
        //explode the date to get month, day and year
        $birthDate = explode("/", $birthDate);
        //get age from date or birthdate
        if($dte != null)
        {
            $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md",strtotime($dte))
            ? ((date("Y",strtotime($dte)) - $birthDate[2]) - 1)
            : (date("Y",strtotime($dte)) - $birthDate[2]));
        }
        else
        {
          $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
            ? ((date("Y") - $birthDate[2]) - 1)
            : (date("Y") - $birthDate[2]));
        }
      return $age;
    }
}