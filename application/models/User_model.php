<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class User_model extends CI_Model {

		function __construct() 
		{   
            //$this->load->model('customers');       
		}
		
		public function authenticate($username,$password,$table)
		{
            echo $username." ".$password." ".$table;
            die();
			$user = $this->db->get_where($table,array('strUsername'=>$username))->result_array();
			
			$user = current($user);
            
			if(empty($user))
			{
                
				return false;
			}
			else
			{			
				$auth_data = $this->db->get_where($table, array('strUsername'=>$username))->first_row();
				//if($user['strCMSUserPassword'] == md5($password))
				if(pw_unhash($user['strPass']) == $password)
				{											
					foreach($auth_data as $key => $value):
						$this->session->set_userdata($key, $value);			
					endforeach;
                    					
					$this->session->set_userdata('user_logged', true);		
					
					return true;									
				}
				else
				{
                    
					return false;
				}
			}
			
			
		
		}

		function register_user($data)
		{
			$email_check = array('strEmail'=>$data['strEmail']);
			$auth_data = $this->db->get_where('tb_mas_users', $email_check, 1)->first_row();			 
			if(isset($auth_data->strEmail))
			{
				return 0;
			}
			else
			{
				$uname_check = array('strUsername'=>$data['strUsername']);
				$auth_data = $this->db->get_where('tb_mas_users', $uname_check, 1)->first_row();				 
				if(isset($auth_data->strUsername))
				{
					return 2;
				}
				else
				{

					$data['dteCreated'] = date("Y-m-d"); //set date created					 
					$data['strConfirmed'] = $this->generate_hash();
					if(isset($data['strPass'])){
						$data['strPass'] = pw_hash($data['strPass']); //encrypt password
					}

					if($this->send_confirmation_email($data['strEmail'],$data['strConfirmed']))
                    {
						
						$this->db->insert('tb_mas_users', $data);
                        return 1;
                    }
					else
                    {
                        return 3;
                    }	
					

				}
			}
		}
		
		function register_user_fb($data)
		{
			$email_check = array('strEmail'=>$data['strEmail']);
			$query = $this->db->get_where('tb_mas_users', $email_check, 1);
			$auth_data = $query->result_array();
			if(isset($auth_data['strEmail']))
			{
				return 0;
			}
			else
			{

					$data['dteCreated'] = date("Y-m-d"); //set date created					 
					$data['strConfirmed'] = $this->generate_hash();
					if(isset($data['password'])){
						$data['password'] = pw_hash($data['password']); //encrypt password
					}

					
						$this->db->insert('tb_mas_users', $data);
						
					return 1;

			}
		}
		

        function fetch_customer_data($user_id)
        {   
            //Check if customer data exists
            $customer = $this->customers->fetch_customer($user_id);
            if(isset($customer[0]['user_id'])){
                foreach($customer[0] as $key => $value)
                {   
                    $this->session->set_userdata($key, $value);
                }
                //$this->session->set_userdata('customer', $customer_array);
            }           

            return;
            
        }
				
	function generate_hash()
        {
	        $result = "";
	        $charPool = '0123456789abcdefghijklmnopqrstuvwxyz';
	        for($p = 0; $p<15; $p++)
		    $result .= $charPool[mt_rand(0,strlen($charPool)-1)];
	        return sha1(md5(sha1($result)));
        }
	function generate_simple_hash()
        {
	        $result = "";
	        $charPool = '0123456789';
	        for($p = 0; $p<8; $p++)
		    $result .= $charPool[mt_rand(0,strlen($charPool)-1)];
		
	        return $result;
        }
        
        function validate_hash($hash)
        {
            $where = array('strConfirmed'=>$hash);
            $auth_data = $this->db->get_where('tb_mas_users',$where,1)->first_row();
			
	        if(isset($auth_data->strUsername))
	        {
	            $data = array('strConfirmed'=>'1');
	            $this->db->where('strConfirmed',$hash);
	            $this->db->update('tb_mas_users',$data);
                foreach($auth_data as $key => $value):
					$this->session->set_userdata($key, $value);			
				endforeach;
				$this->session->set_userdata('user_logged', true);
				header('location:'.base_url());
	        }
	        else
	        {
    			header('location:'.base_url());
	        }		
        
        }
		
		function reset_password($data)
		{
			$where = array(
				'strReset' => $data['hash']
			);
			
			$update = array(
				'strReset' => '',
				'strPass' => pw_hash($data['password'])
			);
		
			$this->db->where($where);
			$this->db->update('tb_mas_users', $update);
			
			return;
			
		}

        function update_user($email, $data)
		{
			$this->db->where(array('strEmail' => $email));
			$this->db->update('tb_mas_users', $data);
			
			return;
			
		}
		
		function create_reset_request($data)
		{
			// Check if user exists
			$email_check = array('strEmail'=>$data['strEmail']);
			$query = $this->db->get_where('tb_mas_users', $email_check, 1);
			$auth_data = $query->result_array();
			if(isset($auth_data[0]['strEmail']))
			{
				
				// Create Reset Request Hash
				$hash = $this->generate_hash();
				
				// Update Database
				$this->db->where('strEmail',$data['strEmail']);
	            $this->db->update('tb_mas_users',array('strReset' => $hash));
			
				// Send Email Reset
				$result = $this->send_reset_email($data['strEmail'], $hash);
				
				return $result;
				
			}
			else
			{
				return 0;
			}
		
		}
        function set_email_data($email, $subject, $content, $path=null)
		{
		  
            if(base_url() == "http://localhost/accuraMain/"){
			//init configuration for email------------------------------------------------
				
				$config['protocol'] = 'smtp';
				$config['smtp_host'] = "ssl://smtp.googlemail.com";
				$config['smtp_port'] = '465';
				$config['smtp_user'] = 'josephedmundcastillo@gmail.com';
				$config['smtp_pass'] = 'Unearthfellowes!';
				$config['mailtype'] = 'html';
				$config['charset'] = 'utf-8';
				$config['newline'] = "\r\n";
			     
				$this->email->initialize($config);
                

			//-------------------------------------------------------------------------
                if($path!=null)
                    $this->email->attach($path);
			    $this->email->from("josephedmundcastillo@gmail.com","Accura Express");
	            $this->email->to($email);
                //$this->email->bcc("inquire@iACADEMY.edu.ph");
                
			    $this->email->subject($subject);
			    //$mail_content = $this->load->view("templates/email/confirmation", $data);
			    $this->email->message($content);
            }
            else
            {
                //init configuration for email------------------------------------------------
				$config['mailtype'] = 'html';
				$config['charset'] = 'utf-8';
				$config['newline'] = "\r\n";

			     
				$this->load->library('email');
                $this->email->initialize($config);
                $this->email->set_newline("\r\n"); 
                

			//-------------------------------------------------------------------------
                if($path!=null)
                    $this->email->attach($path);
			    $this->email->from("josephedmundcastillo@gmail.com","Accura Express");
	            $this->email->to($email);
                //$this->email->bcc("inquire@iACADEMY.edu.ph");
			    $this->email->subject($subject);
			    //$mail_content = $this->load->view("templates/email/confirmation", $data);
			    $this->email->message($content);
            
            }
		}
        
        function set_email_data_admin($email, $subject, $content)
		{
		
            if(base_url() == "http://localhost:8888/iacademy/"){
			//init configuration for email------------------------------------------------
				
				$config['protocol'] = 'smtp';
				$config['smtp_host'] = "ssl://smtp.googlemail.com";
				$config['smtp_port'] = '465';
				$config['smtp_user'] = 'inquire@iacademy.edu.ph';
				$config['smtp_pass'] = 'parasabayan0217';
				$config['mailtype'] = 'html';
				$config['charset'] = 'utf-8';
				$config['newline'] = "\r\n";
			     
				$this->email->initialize($config);
                

			//-------------------------------------------------------------------------
			    $this->email->from("inquire@iacademy.edu.ph","Employee Portal");
	            $this->email->to($email);
                $this->email->bcc("rms@iacademy.edu.ph");
			    $this->email->subject($subject);
			    //$mail_content = $this->load->view("templates/email/confirmation", $data);
			    $this->email->message($content);
            }
            else
            {
                //init configuration for email------------------------------------------------
				
				$config = Array(
                  'mailtype' =>'html'
                 );

			     
				$this->load->library('email');
                $this->email->initialize($config);
                $this->email->set_newline("\r\n"); 
                 

			//-------------------------------------------------------------------------
			    $this->email->from("rms@iacademy.edu.ph","iACADEMY Employee Portal");
	            $this->email->to($email);
                $this->email->subject($subject);
			    //$mail_content = $this->load->view("templates/email/confirmation", $data);
			    $this->email->message($content);
            
            }
		}
		
		function send_confirmation_email($email, $hash)
		{
			
			//load library email
			$this->load->library('email');
			
			$data['hash'] = $hash;
			$subject = 'TV5 March Madness Account Registration Confirmation';
			$content =  "<div style='padding:10px;background:#002857;'><img src='".base_url()."assets/themes/default/images/mml.png'></div><br>";			
			$content .= '<div style="padding:25px;height:300px;"><h3>To Confirm registration in NCAA TV5 March Madness, click the link below. </h3><br /><a href="'.base_url().'users/confirm_email/'.$hash.'">'.base_url().'users/confirm_email/'.$data['hash'] .'</a></div>';
			$content.=  "<div style='font-weight:bold;padding:10px;background:#002857;color:#ccc;'>Contact Us:<br>Email:GMGatbonton@tv5.com.ph <br></div>";		    
		    
			$this->set_email_data($email,$subject,$content);		    
			 if($this->email->send())
		    {
				$this->email->clear();
				return true;
		    }
		    else
		    {
				$this->email->clear();
		        return false;
		    }
		
		}
        
        function send_leave_notification_email($email, $user,$leave)
		{
			
			//load library email
			$this->load->library('email');
			
			$subject = 'Leave Requested Please Check Messages in RMS';

            $content = $user['strFirstname']." ".$user['strLastname']." with employee ID ".$user['strEmployeeID']." requested a leave of abscence.";
            $content .= "<br /> Reason: ".$leave['strReason'];
            $content .= "<br /> from ".date("M j, Y",strtotime($leave['dteToLeave']))." to ".date("M j, Y",strtotime($leave['dteToReturn']));
            $content .= "<br /> Nature of Leave: ".$leave['strNatureLeave'];
            $content .= "<br /> <a href='".base_url()."cms/approve_leave/".$leave['intID']."/approved'>Approve</a>";
            $content .= "<br /> <a href='".base_url()."cms/approve_leave/".$leave['intID']."/disapproved'>Disapprove</a>";	    
		    
			$this->set_email_data($email,$subject,$content);		    
            if($this->email->send())
		    {
				$this->email->clear();
				return true;
		    }
		    else
		    {
				$this->email->clear();
		        return false;
		    }
		
		}
        
        function send_room_notification_email($email, $user,$room,$room_slots,$hash)
		{
			
			//load library email
			$this->load->library('email');
			
			$subject = 'Room Reservation Requested '.$room['intID'];
            
            $content = $user['strFirstname']." ".$user['strLastname']." with employee ID ".$user['strEmployeeID']." requested to Reserve a room.";
            $content .= "<br /> Date ".date("M j, Y",strtotime($room['dteDay']));
            $content .= "<br /> Schedule:";
            foreach($room_slots as $rs)
            {
                $content .= "<br />".date('h:i a',strtotime($rs['timeStart']))." - ".date('h:i a',strtotime($rs['timeEnd']));
            }
            $content .= "<br /> Room To Reserve: ".$room['intRoomNumber']." ".$room['strDescription'];
            $content .="<br /> Purpose for reservation: ".$room['strMessage'];
            $content .="<br /> Number of Users: ".$room['intNumPax'];
            if($room['strStudentNames']!=""){
               $content .="<br /> Student Names: ".$room['strStudentNames'];
            }
            $content .= "<br /> <a href='".base_url()."cms/approve_room_reserve/".$room['intID']."/submitted/".$hash."'>Approve</a>";
            $content .= "<br /> <a href='".base_url()."cms/approve_room_reserve/".$room['intID']."/cancelled/".$hash."'>Disapprove</a>";    
		    
			$this->set_email_data($email,$subject,$content);		    
            if($this->email->send())
		    {
				$this->email->clear();
				return true;
		    }
		    else
		    {
				$this->email->clear();
		        return false;
		    }
		
		}
        
        function send_vehicle_notification_email($email, $user,$room,$room_slots,$hash)
		{
			
			//load library email
			$this->load->library('email');
			
			$subject = 'Vehicle Reservation Requested '.$room['intID'];
            
            $content = $user['strFirstname']." ".$user['strLastname']." with employee ID ".$user['strEmployeeID']." requested to Reserve a vehicle.";
            $content .= "<br /> Date ".date("M j, Y",strtotime($room['dteDay']));
            $content .= "<br /> Schedule:";
            foreach($room_slots as $rs)
            {
                $content .= "<br />".date('h:i a',strtotime($rs['timeStart']))." - ".date('h:i a',strtotime($rs['timeEnd']));
            }
            $content .= "<br /> Vehicle To Reserve: ".$room['strVehicleLicense']." ".$room['strVehicleModel'];
            $content .= "<br /> Driver: ".$room['strVehicleDriver'];
            $content .= "<br /> Destination: ".$room['strVehicleDestination'];
            $content .="<br /> Purpose for reservation: ".$room['strMessage'];
            $content .= "<br /> <a href='".base_url()."cms/approve_vehicle_reserve/".$room['intID']."/submitted/".$hash."'>Approve</a>";
            $content .= "<br /> <a href='".base_url()."cms/approve_vehicle_reserve/".$room['intID']."/cancelled/".$hash."'>Disapprove</a>";    
		    
			$this->set_email_data($email,$subject,$content);		    
            if($this->email->send())
		    {
				$this->email->clear();
				return true;
		    }
		    else
		    {
				$this->email->clear();
		        return false;
		    }
		
		}
        
        function send_notification_email_client($message,$email)
        {
            $this->email->clear();
            
            $subject = $message['strSubject'];
            $content = $message['strMessage'];
            
            $this->set_email_data($email,$subject,$content);		    
            if($this->email->send())
		    {
				$this->email->clear();
				return true;
		    }
		    else
		    {
             	$this->email->clear();
		        return false;
		    }

        }
        function send_comms_notification_email($email,$message)
        {
            
            $subject = $message['strSubject'];
            $content = $message['strMessage'];
            
            $this->set_email_data($email,$subject,$content);		    
            if($this->email->send())
		    {
				$this->email->clear();
				return true;
		    }
		    else
		    {
				$this->email->clear();
		        return false;
		    }
        }
        function send_room_notification_email_submitted($email, $user,$room,$room_slots,$hash)
		{
			
			//load library email
			$this->load->library('email');
			
			$subject = 'Room Reservation Requested Noted by Div/Dept Head '.$room['intID'];
            
            $content = $user['strFirstname']." ".$user['strLastname']." with employee ID ".$user['strEmployeeID']." requested to Reserve a room.";
            $content .= "<br /> Date ".date("M j, Y",strtotime($room['dteDay']));
            $content .= "<br /> Schedule:";
            foreach($room_slots as $rs)
            {
                $content .= "<br />".date('h:i a',strtotime($rs['timeStart']))." - ".date('h:i a',strtotime($rs['timeEnd']));
            }
            $content .= "<br /> Room To Reserve: ".$room['intRoomNumber']." ".$room['strDescription'];
            $content .="<br /> Purpose of reservation: ".$room['strMessage'];
            $content .="<br /> Number of Users: ".$room['intNumPax'];
            if($room['strStudentNames']!=""){
               $content .="<br /> Student Names: ".$room['strStudentNames'];
            }
            $content .= "<br /> <a href='".base_url()."cms/approve_submitted_room_reserve/".$room['intID']."/approved/".$hash."'>Approve</a>";
            $content .= "<br /> <a href='".base_url()."cms/approve_submitted_room_reserve/".$room['intID']."/cancelled/".$hash."'>Disapprove</a>";    
            
			$this->set_email_data($email,$subject,$content);		    
            if($this->email->send())
		    {
				$this->email->clear();
				return true;
		    }
		    else
		    {
				$this->email->clear();
		        return false;
		    }
		
		}
        
        function send_vehicle_notification_email_submitted($email, $user,$room,$room_slots,$hash)
		{
			
			//load library email
			$this->load->library('email');
			
			$subject = 'Vehicle Reservation Requested Noted by Div/Dept Head'.$room['intID'];
            
            $content = $user['strFirstname']." ".$user['strLastname']." with employee ID ".$user['strEmployeeID']." requested to Reserve a vehicle.";
            $content .= "<br /> Date ".date("M j, Y",strtotime($room['dteDay']));
            $content .= "<br /> Schedule:";
            foreach($room_slots as $rs)
            {
                $content .= "<br />".date('h:i a',strtotime($rs['timeStart']))." - ".date('h:i a',strtotime($rs['timeEnd']));
            }
            $content .= "<br /> Vehicle To Reserve: ".$room['strVehicleLicense']." ".$room['strVehicleModel'];
            $content .= "<br /> Driver: ".$room['strVehicleDriver'];
            $content .= "<br /> Destination: ".$room['strVehicleDestination'];
            $content .="<br /> Purpose for reservation: ".$room['strMessage'];
            $content .= "<br /> <a href='".base_url()."cms/approve_submitted_vehicle_reserve/".$room['intID']."/approved/".$hash."'>Approve</a>";
            $content .= "<br /> <a href='".base_url()."cms/approve_submitted_vehicle_reserve/".$room['intID']."/cancelled/".$hash."'>Disapprove</a>";    
		    
			$this->set_email_data($email,$subject,$content);		    
            if($this->email->send())
		    {
				$this->email->clear();
				return true;
		    }
		    else
		    {
				$this->email->clear();
		        return false;
		    }
		
		}
        
        function send_borrow_notification_email($email, $user,$borrow,$hash)
		{
			
			//load library email
			$this->load->library('email');
			
			$subject = 'Borrow Item Request '.$borrow['intID'];
            
            $items = $this->data_fetcher->fetch_table('tb_mas_borrow_request_item',array('tb_mas_borrow_request_item.intID','asc'),null,array('tb_mas_borrow_request_item.intRequestID'=>$borrow['intID']),array('table'=>'tb_mas_products','tbl1'=>'intItemID','tbl2'=>'tb_mas_products.intID'));
        
        $item_table = '<table class="table table-hover">
            <tbody><tr>
              <th>Item Code</th>
              <th>Name</th>
              <th>Description</th>
              <th>Amount</th>
            </tr>';
        
            foreach($items as $i){
            $item_table .= '<tr>
              <td>'.$i['strCode'].'</td>
              <td>'.$i['strName'].'</td>
              <td>'.$i['strDescription'].'</td>
              <td>'.$i['intAmount'].'</td>
            </tr>';
            }
            $item_table .=  '</tbody></table>';
            
            
            $content = $user['strFirstname']." ".$user['strLastname']." with employee ID ".$user['strEmployeeID']." requested to borrow item.";
            $content .= "<br /> Items: ".$item_table;
            $content .= "<br /> Reason for Borrowing: ".$borrow['strMessage'];
            $content .= "<br /> from ".date("M j, Y",strtotime($borrow['dteToBorrow']))." to ".date("M j, Y",strtotime($borrow['dteToReturn']));
            $content .= "<br /><br /> <a href='".base_url()."cms/approve_borrow/".$borrow['intID']."/submitted/".$hash."'>Approve</a>";
            $content .= "<br /><br /> <a href='".base_url()."cms/approve_borrow/".$borrow['intID']."/disapproved/".$hash."'>Disapprove</a>";	    
		    
			$this->set_email_data($email,$subject,$content);		    
            if($this->email->send())
		    {
				$this->email->clear();
				return true;
		    }
		    else
		    {
				$this->email->clear();
		        return false;
		    }
		
		}
        
        function send_borrow_notification_email_submitted($email, $user,$borrow,$hash)
		{
			
			//load library email
			$this->load->library('email');
			
			$subject = 'Borrow Item Requested '.$borrow['intID'];
            
            $items = $this->data_fetcher->fetch_table('tb_mas_borrow_request_item',array('tb_mas_borrow_request_item.intID','asc'),null,array('tb_mas_borrow_request_item.intRequestID'=>$borrow['intID']),array('table'=>'tb_mas_products','tbl1'=>'intItemID','tbl2'=>'tb_mas_products.intID'));
        
        $item_table = '<table class="table table-hover">
            <tbody><tr>
              <th>Item Code</th>
              <th>Name</th>
              <th>Description</th>
              <th>Amount</th>
            </tr>';
        
            foreach($items as $i){
            $item_table .= '<tr>
              <td>'.$i['strCode'].'</td>
              <td>'.$i['strName'].'</td>
              <td>'.$i['strDescription'].'</td>
              <td>'.$i['intAmount'].'</td>
            </tr>';
            }
            $item_table .=  '</tbody></table>';
            
            
            $content = $user['strFirstname']." ".$user['strLastname']." with employee ID ".$user['strEmployeeID']." requested to borrow item.";
           
            $content .= "<br /> Items: ".$item_table;
            $content .= "<br /> Reason for Borrowing: ".$borrow['strMessage'];
            $content .= "<br /> from ".date("M j, Y",strtotime($borrow['dteToBorrow']))." to ".date("M j, Y",strtotime($borrow['dteToReturn']));
            $content .= "<br /><br /> <a href='".base_url()."cms/approve_borrow_submitted/".$borrow['intID']."/approved/".$hash."'>Approve</a>";
            $content .= "<br /><br /> <a href='".base_url()."cms/approve_borrow_submitted/".$borrow['intID']."/disapproved/".$hash."'>Disapprove</a>";	    
		    
			$this->set_email_data($email,$subject,$content);		    
            if($this->email->send())
		    {
				$this->email->clear();
				return true;
		    }
		    else
		    {
				$this->email->clear();
		        return false;
		    }
		
		}
		

		function send_reset_email($email, $hash)
		{
			//load library email
			$this->load->library('email');
			
			$data['hash'] = $hash;
			$subject='PT-Store Password Reset';
			$content =  "<div style='padding:10px;background:#333;'><img src='".base_url()."images/pinoytuner-logo.png'></div><br>";
			$content .= '<div style="padding:25px;height:300px;">To Reset your password click the link. <br /><a href="'.base_url().'users/password_reset/'.$hash.'">'.base_url().'users/password_reset/'.$hash.'</a></div>';
			$content.=  "<div style='font-weight:bold;padding:10px;background:#333;color:#ccc;'>Contact Us:<br>Email:tech@stratuscast.com<br>Address: 7615 Guijo St. R&D Bldg. San Antonio Village, Makati City Philippines<br>Telephone: (02)728-6242</div>";
			$this->set_email_data($email,$subject,$content);		    		    		    
			//$mail_content = $this->load->view("templates/email/confirmation", $data);
			
		    
			 if($this->email->send())
		    {
				$this->email->clear();
                return true;
		    }
		    else
		    {
				$this->email->clear();
		        return false;
		    }
			
		}
		
		function get_user_info($userid)
		{
			$query = $this->db->get_where('users',array('user_id'=>$userid));
			return current($query->result_array());
		}

		function get_user_image($userid)
		{
			$query = $this->db->get_where('user_images',array('user_id'=>$userid));
			return current($query->result_array());
		}
		
		function get_user_info_field($userid,$field)
		{
			$this->db->select($field);
			$this->db->where(array('user_id'=>$userid));
			$query = $this->db->get('users');
			return current($query->result_array());
		}
		
	}
	

?>
