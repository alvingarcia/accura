<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_poster extends CI_Model {

	function post_data($table,$post,$update=null,$id="intID",$where=null)
	{
		if($update==null)
			$this->db->insert($table,$post);
		else
		{
          
            $this->db
				 ->where($id,$update);
			if($where!=null)
                $this->db
                    ->where($where);
            
            $this->db->update($table,$post);
            
		}
        
        
        
        
	}
    
    function post_data_area($id,$area,$status,$specimenid=0)
	{
        $dte = date("Y-m-d h:i:s");
        
        if($status == "in progress")
        $this->db
             ->query('UPDATE appointment_test a JOIN procedures p ON (a.procedureid = p.procedureId)
SET a.appointmentTestStatus = \''.$status.'\', a.appointmentTestTimeServed = \''.$dte.'\' WHERE a.aid = '.$id.' AND p.procedureSection = '.$area.'  AND a.intOnHold != 1 AND a.appointmentTestStatus = \'in queue\'');
        elseif($status == "processing")
        $this->db
             ->query('UPDATE appointment_test a JOIN procedures p ON (a.procedureid = p.procedureId)
SET a.appointmentTestStatus = \''.$status.'\', a.testSpecimenId = \''.$specimenid.'\', a.appointmentTestTimeFinished = \''.$dte.'\' WHERE a.aid = '.$id.' AND p.procedureSection = '.$area.' AND a.intOnHold != 1 AND a.appointmentTestStatus = \'in progress\'');
        else
        $this->db
             ->query('UPDATE appointment_test a JOIN procedures p ON (a.procedureid = p.procedureId)
SET a.appointmentTestStatus = \''.$status.'\', a.appointmentTestTimeFinished = \''.$dte.'\' WHERE a.aid = '.$id.' AND p.procedureSection = '.$area.' AND a.intOnHold != 1');
            
	}
    
    function sendMessage($message)
    {
        $post['strSubject'] = $message['strSubject'];
        $post['strMessage'] = $message['strMessage'];
        $post['dteDate'] = $message['dteDate'];
        $this->db->insert('tb_mas_system_message',$post);
        
        $messageId = $this->db->insert_id();
        
        $d['intTrash'] = 0;
        $d['intRead'] = 0;
        $d['intFacultyIDSender'] = $message['intFacultyIDSender'];
        
        foreach($message['intFacultyID'] as $employee)
        {
             $d['intMessageID'] = $messageId;
             $d['intFacultyID'] = $employee;
             $this->db->insert('tb_mas_message_user',$d);
        }
       
    }
    
    function updateQueue($appointment,$area)
    {
        $procs = $this->db->get_where('procedures',array('procedureSection'=>$area))->result_array();
        
        foreach($procs as $proc)
        {
            $this->db
                 ->where(array('procedureid'=>$proc['procedureId'],'aid'=>$appointment))
                 ->update('appointment_test',array('appointmentTestStatus'=>'in queue'));
        }
    }
    
    function setMessageRead($id)
	{
		
        $post['intRead'] = 1;
        $this->db
             ->where('intMessageUserID',$id)
             ->update('tb_mas_message_user',$post);

    }
    
    function setMessages($ids,$read)
	{
		
        $post['intRead'] = $read;
        for($i = 0;$i<count($ids);$i++){
            if($i == 0)
                $this->db->where('intMessageUserID',$ids[$i]);
            else
                $this->db->or_where('intMessageUserID',$ids[$i]);
        }
        $this->db
             ->update('tb_mas_message_user',$post);

    }
    
    function deleteMessages($ids)
	{
		
        for($i = 0;$i<count($ids);$i++){
            if($i == 0)
                $this->db->where('intMessageUserID',$ids[$i]);
            else
                $this->db->or_where('intMessageUserID',$ids[$i]);
        }
        $this->db
             ->delete('tb_mas_message_user');

    }
    
    function trashMessages($ids,$recover=1)
	{
		$post['intTrash'] = $recover;
        for($i = 0;$i<count($ids);$i++){
            if($i == 0)
                $this->db->where('intMessageUserID',$ids[$i]);
            else
                $this->db->or_where('intMessageUserID',$ids[$i]);
        }
        $this->db
             ->update('tb_mas_message_user',$post);

    }
    
    function trashMessage($id,$recover=1)
	{
		
        $post['intTrash'] = $recover;
        $this->db
             ->where('intMessageUserID',$id)
             ->update('tb_mas_message_user',$post);

    }
    
    function deleteItem($table,$id,$idLabel="intID")
    {
       
        $this->db
        ->where($idLabel,$id)
        ->delete($table);

        return true;
    }
    
    function deleteItems($table,$where)
    {
       
        $this->db
        ->where($where)
        ->delete($table);

        return true;
    }
    
    function removeAdvisedSubjects($sid,$ay)
    {
       
        $sql = "DELETE tb_mas_advised_subjects FROM tb_mas_advised_subjects JOIN tb_mas_advised ON tb_mas_advised_subjects.intAdvisedID = tb_mas_advised.intAdvisedID WHERE tb_mas_advised.intStudentID = ".$sid." AND tb_mas_advised.intSYID = ".$ay;
        $this->db->query($sql);
        
    }
    
    function updateIncompleteSubjects($year,$sem)
    {
        $semID = $this->db
                      ->get_where('tb_mas_sy',array('strYearStart'=>$year,'enumSem'=>$sem))
                      ->first_row();
                      
        if(!empty($semID))
        {
            $update['floatFinalGrade'] = 5;
            $update['strRemarks'] = "Failed";
            $sql = "UPDATE tb_mas_classlist cl JOIN tb_mas_classlist_student cs ON cs.intClassListID = cl.intID SET cs.floatFinalGrade = 5, cs.strRemarks = 'failed' WHERE cl.strAcademicYear = ".$semID->intID." AND cs.floatFinalGrade = 3.5";
            /*
            $this->db
                 ->where(array('strAcademicYear'=>$semID->intID,'floatFinalGrade'=>3.5))
                 ->join('tb_mas_classlist_student','tb_mas_classlist_student.intClassListID = tb_mas_classlist.intID')
                 ->update('tb_mas_classlist',$update);
                 */
            
            $this->db->query($sql);
                
        }
                    
        
        
    }
    
    
    
    
    function setMessageUnRead($id)
	{
		
        $post['intRead'] = 0;
        $this->db
             ->where('intMessageUserID',$id)
             ->update('tb_mas_message_user',$post);

    }
    
    function setMessageUnReadByMID($id)
    {
        $post['intRead'] = 0;
        $this->db
             ->where('intMessageID',$id)
             ->update('tb_mas_message_user',$post);
    }
    
    
    function log_action($category,$action,$color)
    {
        $data = array('strCategory'=>$category,'strAction'=>$action,'dteLogDate'=>date("Y-m-d H:i:s"),'intFacultyID'=>$this->session->userdata('intID'),'strColor'=>$color);
        $this->post_data('tb_mas_logs',$data);
        
    }
    
    function addStudentClasslist($post)
    {
        $this->db->insert('tb_mas_classlist_student',$post);
        
    }
    
    function update_classlist($table,$post,$update=null)
	{
		if($update==null)
			$this->db->insert($table,$post);
		else
		{
			$this->db
				 ->where('intCSID',$update)
				 ->update($table,$post);
		}
	}
    
    
    function queueNextTest($id,$id2=0)
    {
        $apt = $this->db->get_where('appointment_test',array('aid'=>$id,'appointmentTestStatus'=>'waiting','appointmentTestId !='=>$id2,'intOnHold !='=>1))->first_row();
        
        if(!empty($apt)){
            $post['appointmentTestStatus'] = "in queue";
            $post['testQueueCode'] = date('ymdHis');

            $this->db
                 ->where('appointmentTestId',$apt->appointmentTestId)
                 ->update('appointment_test',$post);
        }
        
    }
    
	function update_image($post,$update=null,$field = 'strFileName')
	{
		if($update!=null)
		{
			$this->db
				 ->where($field,$update)
				 ->update('tb_sys_media',$post);
			
			return 1;
		}
		return 0;
	}
		
	function delete_data($table,$id)
	{
		$this->db
			->where('intID',$id)
			->delete($table);
	}
    
    function delete_room_subject($subject)
	{
		$this->db
			->where('intSubjectID',$subject)
			->delete('tb_mas_room_subject');
	}
   
    function delete_prereq_subject($subject)
	{
		$this->db
			->where('intSubjectID',$subject)
			->delete('tb_mas_prerequisites');
	}
    
    function delete_days_subject($subject)
	{
		$this->db
			->where('intSubjectID',$subject)
			->delete('tb_mas_days');
	}
    
    function deleteFromSchedule($scode,$sem)
	{
		$this->db
			->where(array('strScheduleCode'=>$scode,'intSem'=>$sem))
			->delete('tb_mas_room_schedule');
	}
    
    function deleteStudentCS($table,$id)
	{
		$this->db
			->where('intCSID',$id)
			->delete($table);
	}
    
    function deleteClassroom($id)
	{
		$this->db
			->where('intID',$id)
			->delete('tb_mas_classrooms');
        
        $this->db
			->where('intRoomID',$id)
			->delete('tb_mas_room_schedule');
	}
    
    function deleteStudent($id)
	{
		$this->db
			->where('intID',$id)
			->delete('tb_mas_users');
        
        $this->db
			->where('intStudentID',$id)
			->delete('tb_mas_classlist_student');
        
        $registration = current($this->db->get_where('tb_mas_registration',array('intStudentID'=>$id))->result_array());
        
        $this->db
			->where('intStudentID',$id)
			->delete('tb_mas_registration');
        
        $this->db
			->where('intRegistrationID',$registration['intRegistrationID'])
			->delete('tb_mas_transactions');
        
        $advised = current($this->db->get_where('tb_mas_advised',array('intStudentID'=>$id))->result_array());
        
        $this->db
			->where('intStudentID',$id)
			->delete('tb_mas_advised');
        
        $this->db
			->where('intAdvisedID',$advised['intAdvisedID'])
			->delete('tb_mas_advised_subjects');
        
        
	}
    
    function deleteUser($id)
	{
        
        $this->db
			->where('id',$id)
			->delete('user');
        
        $this->db
			->where('userid',$id)
			->delete('doctors');
        
	}
    
    function deleteClassList($id)
    {
        $this->db
			->where('intClassListID',$id)
			->delete('tb_mas_classlist_student');
        
        $this->db
			->where('intID',$id)
			->delete('tb_mas_classlist');
    }
    
    function deleteFromClassList($id)
    {
        $this->db
			->where('intClassListID',$id)
			->delete('tb_mas_classlist_student');
    }
    
    function deleteSchedule($id,$sc = "strScheduleCode")
    {
        $this->db
			->where($sc,$id)
			->delete('tb_mas_room_schedule');
        
     
    }
    
    
    function deleteSubject($id)
    {
        
        $cl =$this->db
			->where('intSubjectID',$id)
            ->get('tb_mas_classlist')
            ->result_array();
        
        if(empty($cl)){
        
            $this->db
			->where('intID',$id)
			->delete('tb_mas_subjects');
        
            return true;
        }
        else
            return false;
    
    }
    
    function deleteCurriculum($id)
    {
        
        $cl =$this->db
			->where('intCurriculumID',$id)
            ->get('tb_mas_users')
            ->result_array();
        
        if(empty($cl)){
        
            $this->db
			->where('intID',$id)
			->delete('tb_mas_curriculum');
        
            return true;
        }
        else
            return false;
    
    }
    
    function deleteCS($id)
	{
		$this->db
			->where('intClassListID',$id)
			->delete('tb_mas_classlist_student');
	}
    
    function deleteProgram($id)
	{
		$this->db
			->where('intProgramID',$id)
			->delete('tb_mas_programs');
	}
    
    function delete_from($table,$where)
	{
		$this->db
			->where($where)
			->delete($table);
	}
	
	
}
