<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_fetcher extends CI_Model {
	
	
	function fetch_table($table,$order=null,$limit=null,$where=null)
	{				
		
		if($order!=null)
			$this->db->order_by($order[0],$order[1]);
		elseif($table == 'tb_mas_content')
			$this->db->order_by('dteStart','desc');
		
		if($limit!=null)
			$this->db->limit($limit);
			
		if($where!=null)
			$this->db->where($where);
		
		$data =  $this->db
						->get($table)
						->result_array();
						
		return $data;
						
	}
    
    function fetch_content($cats,$limit=10,$offset=0,$search=null)
    {
        $s = array();
        $all= array();
        foreach($cats as $k=>$v)
        {
            $all = array_merge($this->db
                    ->get_where('tb_mas_category',array('intParent'=>$v))
                    ->result_array(),$all);
        }
        
        
        foreach($all as $a)
        {
            $cats[] = $a['intID'];
        }
        
             $this->db
             ->select('tb_mas_content.*')
             ->from('tb_mas_content_category')
             ->join('tb_mas_content','tb_mas_content.intID = tb_mas_content_category.intContentID')
             ->order_by('strTitle','ASC')
             ->group_by('tb_mas_content.intID')
             ->limit($limit)
             ->offset($offset);
            
            if($search!=null){
                $s = "";
                $ss = explode(" ",$search);
                for($i=0;$i<count($ss);$i++){
                    if($i == 0)
                        $s.= "(";
                
                    $s .='(strTitle LIKE "%'.$ss[$i].'%" OR strSecondary LIKE "%'.$ss[$i].'%" OR strContent LIKE "%'.$ss[$i].'%" OR strAuthor LIKE "%'.$ss[$i].'%")';
                    
                    if($i == count($ss)-1)
                        $s .= ")";
                    else
                        $s.=" && ";
                }
                $this->db->where($s);
                }
        
            $where = "(";
            for($i=0;$i<count($cats);$i++)
            {    
                $where.="intCategoryID = ".$cats[$i];
                if((count($cats)-1) == $i)
                    $where.= ")";
                else
                    $where.= " OR ";
            }
        
           $this->db->where($where);
             
            $s = $this->db
                 ->get()
                 ->result_array();  

       return $s;
       
    }
    
    function getDropdown($table,$id,$param,$order)
    {
        $users = array();
            
        $use = $this->data_fetcher->fetch_table($table,$order);
        
        foreach($use as $u)
        {
            $users[$u[$id]] = "";
            foreach($param as $p)
                $users[$u[$id]] .= $u[$p]." "; 
        }
        
        return $users;
    }
    
    function is_specific($min = 0)
    {
        $logged = $this->session->userdata('admin_logged');
        if($logged!=""){
        $admin = $this->session->userdata('roles');
        
        $admin = switch_user_level($admin);
        
        if(!is_array($min))
            $min = array($min);
        if(in_array($admin,$min))
            return true;
        else
            return false;
        }
        else
            return false;
    }
    
    function getFacultyOnlineUsername()
    {
        $ret = array();
        
        $users = $this->db
                    ->select('intID,strFirstname,strLastname,intIsOnline')
                    ->from('tb_mas_faculty')
                    ->where(array('intIsOnline !='=>'0000-00-00 00:00:00'))
                    ->get()
                    ->result_array();
        
        foreach($users as $user)
        {
            $datetime1 = strtotime($user['intIsOnline']);
            $datetime2 = strtotime(date("Y-m-d H:i:s"));
            $interval  = abs($datetime2 - $datetime1);
            $minutes   = round($interval / 60);
            
            if($minutes < 1)
                $ret[] = $user;
        }
        
        return $ret;
        
    }   
    
    function count_classlist($submitted=1)
    {
        $sem  = $this->get_active_sem();
        return $this->db
                    ->get_where('tb_mas_classlist',array('strAcademicYear'=>$sem['intID'],'intFinalized'=>$submitted))
                    ->num_rows();
    }   
    
    function fetch_table_fields($fields,$table)
    {
        return $this->db
                    ->select($fields)
                    ->from($table)
                    ->get()
                    ->result_array();
    }
    
    function messageExists($messageID,$userID)
    {
        $array = $this->db
             ->get_where('tb_mas_message_user',array('intFacultyID'=>$userID,'intMessageID'=>$messageID))
             ->result_array();
        
        if(empty($array))
            return false;
        else
            return true;
    }
    
    function getMessage($id)
    {
      $data =  $this->db
             ->select('tb_mas_system_message.intID as intID, strMessage, strSubject, dteDate,intFacultyIDSender, strFirstname, strLastname, intMessageID')
            ->join('tb_mas_system_message','tb_mas_message_user.intMessageID = tb_mas_system_message.intID')
             ->join('tb_mas_faculty','tb_mas_faculty.intID = tb_mas_message_user.intFacultyIDSender')
             ->where(array('intMessageUserID'=>$id))
             ->get('tb_mas_message_user')
             ->result_array();
        
        return current($data);
                     
    }
    
    function getMessages($id)
    {
      $data =  $this->db
             ->select('tb_mas_system_message.intID as intID, strMessage, strSubject, dteDate,intFacultyIDSender, strFirstname, strLastname, intMessageID,intMessageUserID')
             ->join('tb_mas_system_message','tb_mas_message_user.intMessageID = tb_mas_system_message.intID')
             ->join('tb_mas_faculty','tb_mas_faculty.intID = tb_mas_message_user.intFacultyIDSender')
             ->where(array('intFacultyID'=>$id,'intRead'=>0,'intTrash'=>0))
             ->order_by('dteDate','desc ')
             ->limit(8)
             ->get('tb_mas_message_user')
             ->result_array();
        
        return $data;
                     
    }
    
    function getReplyThread($id)
    {
         $reply = $this->db
                     ->select('strReplyMessage, dteReplied, strFirstname, strLastname , tb_mas_faculty.intID as intFacultyID, intReplyThreadID')
                     ->join('tb_mas_faculty','tb_mas_faculty.intID = tb_mas_reply_thread.intFacultyID')
                     ->where(array('intMessageID'=>$id))
                     ->order_by('dteReplied','asc')
                     ->get('tb_mas_reply_thread')
                     ->result_array();
        return $reply;
    }
    
    function getItem($table,$id,$key)
    {
        return  current($this->db->get_where($table,array($key=>$id))->result_array());
                     
    }
    
    function getItems($table,$where=null,$sel="*",$join=null,$order=null)
    {
        //Join array structure array("table","comp","table.id");
         $this->db
              ->select($sel)
              ->from($table);
        if($where != null)
            $this->db->where($where);
        if($join != null)
            $this->db->join($join[0],$table.".".$join[1]." = ".$join[3]);
        if($order != null)
            $this->db->order($order[0],$order[1]);
        
        return $this->db->get()->result_array();
                     
    }
    
    function getTestsByArea($id,$area)
    {
        $ret = array();
        if($area!=0)
         $results = $this->db
                 ->select('appointment_test.*,testTypeDescription,testTypeName,areaName,procedureCode,procedureName')
                 ->join('procedures','procedures.procedureId = appointment_test.procedureid')
                 ->join('area','area.areaId = procedures.procedureSection')
                 ->join('test_type','area.areaType = test_type.testTypeId')
                 ->where(array('aid'=>$id,'procedures.procedureSection'=>$area))
                 ->get('appointment_test')
                 ->result_array(); 
        else
        $results = $this->db
                 ->select('appointment_test.*')
                 ->join('procedures','procedures.procedureId = appointment_test.procedureid')
                 ->where(array('aid'=>$id))
                 ->get('appointment_test')
                 ->result_array(); 
        
        foreach($results as $r)
        {
            $t = $this->db->get_where('appointment_test_results',array('appointmenttestid'=>$r['appointmentTestId']))->result_array();
            $r['results'] = $t;
            $ret[] = $r;
        }
        
        return $ret;
    }
    
    
    
    
    
    function getTestsWaiting()
    {
        $results = $this->db
                 ->select('appointment_test.*,procedureCode,procedureName')
                 ->join('procedures','procedures.procedureId = appointment_test.procedureid')
                 ->join('area','area.areaId = procedures.procedureSection')
                 ->join('test_type','area.areaType = test_type.testTypeId')
                 ->where(array('appointmentTestStatus'=>'waiting results','testTypeName !='=>'Imaging'))
                 ->get('appointment_test')
                 ->result_array(); 
        
        return $results;
    }
    
    function getTestsWaitingPerTransaction($aid)
    {
        $results = $this->db
                 ->select('appointment_test.*,procedureCode,procedureName')
                 ->join('procedures','procedures.procedureId = appointment_test.procedureid')
                 ->join('area','area.areaId = procedures.procedureSection')
                 ->join('test_type','area.areaType = test_type.testTypeId')
                 ->where(array('appointmentTestStatus'=>'waiting results','testTypeName !='=>'Imaging','aid'=>$aid))
                 ->get('appointment_test')
                 ->result_array(); 
        
        return $results;
    }
    
    
    function getTestsById($id)
    {
         return $this->db
                 ->select('appointment_test.*')
                 ->join('procedures','procedures.procedureId = appointment_test.procedureid')
                 ->where(array('aid'=>$id))
                 ->get('appointment_test')
                 ->result_array(); 
    }
    function getTestById($id)
    {
         $test = $this->db
                 ->select('appointment_test.*,procedures.*')
                 ->join('procedures','procedures.procedureId = appointment_test.procedureid')
                 ->where(array('appointmentTestId'=>$id))
                 ->get('appointment_test')
                 ->result_array(); 
        
        return current($test);
    }
    
    function getTestConsultationById($id)
    {
        $test = $this->db
                 ->select('appointment_test.appointmentTestId,appointment_test.aid,procedureSection,appointment_test.testRequestCode,procedures.procedureShortName,procedures.procedureName,patient.patientLastname,patient.patientFirstname,patient.birthday,patient.patientid,firstname,lastname,testConsultationId,findings,perscription,testRecommendations,specialization,prcid,pid')
                 ->from('appointment_test')
                 ->join('procedures','appointment_test.procedureid = procedures.procedureId') 
                 ->join('appointment','appointment_test.aid = appointment.appointmentId')
                 ->join('patient','appointment.pid = patient.id')
                 ->join('test_consultation','appointment_test.appointmentTestId = test_consultation.consultationAtestId')
                 ->join('doctors','test_consultation.consultationDoctorId = doctors.doctorId')
                 ->join('user','user.id = doctors.userid')
                 ->where(array('appointmentTestId'=>$id))
                 ->get()
                 ->result_array(); 
        
        return current($test);
    }
    function getTestConsultationPatient($id,$current)
    {
        $test = $this->db
                 ->select('test_consultation.*,appointment.appointmentDate,firstname,lastname')
                 ->from('appointment_test')
                  ->join('appointment','appointment_test.aid = appointment.appointmentId')
                 ->join('test_consultation','appointment_test.appointmentTestId = test_consultation.consultationAtestId')
                 ->join('doctors','test_consultation.consultationDoctorId = doctors.doctorId')
                 ->join('user','user.id = doctors.userid')
                 ->where(array('pid'=>$id,'consultationAtestId !='=>$current))
                 ->order_by('appointmentDate','desc')
                 ->limit(3)
                 ->get()
                 ->result_array(); 
        
        return $test;
    }
    function getResultsTransaction($tid,$rname)
    {
         $res = $this->db
                 ->get_where('test_results',array("appointmentTestIdfk"=>$tid,"rname"=>$rname))
                 ->result_array(); 
        
        return current($res);
    }
    function getResultsById($tid)
    {
         $res = $this->db
                 ->get_where('test_results',array("appointmentTestIdfk"=>$tid))
                 ->result_array(); 
        
        return $res;
    }
    
    function getProcedureJoinLab($id,$labid)
    {
        $proc = $this->db
                     ->select('procedures.*,laboratory_procedure.labCcPrice')
                     ->join('laboratory_procedure','laboratory_procedure.procedureid = procedures.procedureId')
                     ->where(array('laboratory_procedure.procedureid'=>$id,'laboratory_procedure.laboratoryid'=>$labid))
                     ->get('procedures')
                     ->result_array();
        return current($proc);
                     
    }
    
    function getMedTech($procedure)
    {
        $medtech = $this->db
                     ->select('user.*,doctors.doctorId')
                     ->join('doctors','doctors.userid = user.id')
                     ->join('doctor_procedure','doctors.doctorId = doctor_procedure.doctorid')
                     ->where(array('qualification'=>'med tech','doctor_procedure.procedureid'=>$procedure))
                     ->order_by('lastname','asc')
                     ->get('user')
                     ->result_array();
        return $medtech;
    }
    
    function getEstimatedTime($area=null)
    {
        if($area!=null)
           $wt = $this->db
                 ->select("sum(procedures.procedureWeight) as procWeight")
                 ->from('procedures')
                 ->join('appointment_test','appointment_test.procedureid = procedures.procedureId')
                 ->where(array('procedures.procedureSection'=>$area,'appointment_test.appointmentTestStatus'=>'in queue'))
                 ->group_by('procedures.procedureSection')
                 ->get()
                 ->first_row();
        else
            $wt = $this->db
                 ->select("sum(procedures.procedureWeight) as procWeight")
                 ->from('procedures')
                 ->join('appointment_test','appointment_test.procedureid = procedures.procedureId')
                 ->where(array('appointment_test.appointmentTestStatus'=>'in queue'))
                 ->group_by('procedures.procedureSection')
                 ->get()
                 ->first_row();
            
        if(!empty($wt))
            return $wt->procWeight;
        else
            return 0;
    }
    
    function getEstimatedTimePatient($area,$patient)
    {
        return $this->db
             ->select("procedures.*")
             ->from('procedures')
             ->join('appointment_test','appointment_test.procedureid = procedures.procedureId')
             ->where(array('procedures.procedureSection'=>$area,'appointment_test.aid'=>$patient,'appointment_test.intOnHold'=>'0'))
             ->get()
             ->result_array();
            
    }
    
    function getDoctor($procedure)
    {
        $medtech = $this->db
                     ->select('user.*,doctors.doctorId')
                     ->join('doctors','doctors.userid = user.id')
                     ->join('doctor_procedure','doctors.doctorId = doctor_procedure.doctorid')
                     ->where(array('qualification'=>'doctor','doctor_procedure.procedureid'=>$procedure))
                     ->order_by('lastname','asc')
                     ->get('user')
                     ->result_array();
        return $medtech;
    }
    
    function isDoctor($id)
    {
        $medtech = $this->db
                     ->select('user.*,doctors.doctorId')
                     ->join('doctors','doctors.userid = user.id')
                     ->where(array('qualification'=>'doctor','user.id'=>$id))
                     ->get('user')
                     ->result_array();
        if(!empty($medtech))
            return true;
        else
            return false;
    }
    
    function getDoctorsDropdown()
    {
        $doctors = $this->db
                     ->select('user.*,doctors.doctorId')
                     ->join('doctors','doctors.userid = user.id')
                     ->where(array('qualification'=>'doctor'))
                     ->order_by('lastname','asc')
                     ->get('user')
                     ->result_array();
        $ret = array();
        
        foreach($doctors as $doctor)
        {
            $ret[$doctor['doctorId']] = $doctor['lastname']." ".$doctor['firstname'];
        }
        
        return $ret;
    }
    
    function getAvailableDoctorsDropdown($date)
    {
        $day = date('D',strtotime($date));
        $time = date('H:i:s',strtotime($date));
        
        $doctors = $this->db
                     ->select('user.*,doctors.doctorId')
                     ->join('doctors','doctors.userid = user.id')
                     ->where(array('qualification'=>'doctor'))
                     ->order_by('lastname','asc')
                     ->get('user')
                     ->result_array();
        $ret = array();
        
        foreach($doctors as $doctor)
        {
            $schedule = $this->db->get_where('doctor_schedule',array('dsDay'=>$day,'dsStart <='=>$time,'dsEnd >'=>$time,'dsDoctorID'=>$doctor['doctorId']))->result_array();
            if(!empty($schedule))
                $ret[$doctor['doctorId']] = $doctor['lastname']." ".$doctor['firstname'];
        }
        
        return $ret;
    }
    
    function getAreas()
    {
        $areas = $this->db
                     ->select('area.*')
                     ->join('area_admin','area.areaId = area_admin.areaid')
                     ->where(array('area_admin.userid'=>$this->session->userdata('id')))
                     ->order_by('area.areaName','asc')
                     ->get('area')
                     ->result_array();
        return $areas;
    }
    
    function getArea($id)
    {
        $areas = $this->db
                     ->select('area.*')
                     ->join('area_admin','area.areaId = area_admin.areaid')
                     ->where(array('area_admin.userid'=>$this->session->userdata('id'),'area.areaId'=>$id))
                     ->order_by('area.areaName','asc')
                     ->get('area')
                     ->result_array();
        return current($areas);
    }
    
    function getAreaType($id)
    {
        $areas = $this->db
                     ->select('*')
                     ->from('test_type')
                     ->where(array('testTypeId'=>$id))
                     ->get()
                     ->result_array();
        return current($areas);
    }
    
    function getAreaName($pid)
    {
        $area = $this->db
                     ->select('test_type.testTypeName')
                     ->from('procedures')
                     ->join('area','area.areaId = procedures.procedureSection')
                     ->join('test_type','test_type.testTypeId = area.areaType')
                     ->where(array('procedures.procedureId'=>$pid))
                     ->get()
                     ->first_row();
        
        return $area->testTypeName;
    }
    
    function getAreaByProcedure($procid)
    {
         $area = $this->db
                     ->select('procedures.procedureSection')
                     ->where(array('procedureId'=>$procid))
                     ->get('procedures')
                     ->first_row();
        return $area->procedureSection;
    }
    
    function getProceduresLab($lab,$type="laboratory")
    {
         if($lab != 0)
             $procs = $this->db
                     ->select('procedures.*,test_type.testTypeName')
                     ->join('laboratory_procedure','laboratory_procedure.procedureid = procedures.procedureId')
                     ->join('test_type','test_type.testTypeId = procedures.procedureType')
                     ->where(array('laboratoryid'=>$lab,'procedureType'=>$type))
                     ->order_by('procedures.procedureName','asc')
                     ->get('procedures')
                     ->result_array();
          else
              $procs = $this->db
                     ->select('procedures.*,test_type.testTypeName')
                     ->join('test_type','test_type.testTypeId = procedures.procedureType')
                     ->order_by('procedureName','asc')
                     ->where(array('procedureType'=>$type))
                     ->get('procedures')
                     ->result_array();
        
        return $procs;
    }
    
    function getProceduresLabNotSelected($lab,$type,$id)
    {
        if($lab != 0)
            $query = "SELECT procedures.*,testTypeName from procedures JOIN laboratory_procedure ON laboratory_procedure.procedureid = procedures.procedureId JOIN test_type ON test_type.testTypeId = procedures.procedureType WHERE procedures.procedureId NOT IN (SELECT procedureid FROM appointment_test WHERE aid = ".$id.") AND laboratoryid = ".$lab." AND procedureType = '".$type."'";
        else
        $query = "SELECT procedures.*,testTypeName from procedures JOIN test_type ON test_type.testTypeId = procedures.procedureType WHERE procedureId NOT IN (SELECT procedureid FROM appointment_test WHERE aid = ".$id.") AND procedureType = '".$type."'";
        return $this->db
                ->query($query)
                ->result_array();
    }
    
    function getProceduresAppointment($id)
    {
         
         $procs = $this->db
                 ->select('appointment_test.*,procedureName,procedureDescription')
                 ->join('procedures','procedures.procedureId = appointment_test.procedureid')
                 ->where(array('aid'=>$id))
                 ->order_by('procedures.procedureName','asc')
                 ->get('appointment_test')
                 ->result_array();

        return $procs;
    }
    
    function getAlert($lab)
    {
         $user =  $this->getItem('doctors',$this->session->userdata('id'),'userid');
         if(empty($user))
             $rows = array();
         if($user['qualification'] == "laboratory admin")
         $rows = $this->db
                 ->select('appointment_test.*,procedureName,procedureDescription,patientLastname,patientFirstname,appointmentDate')
                 ->join('procedures','procedures.procedureId = appointment_test.procedureid')
                 ->join('appointment','appointment.appointmentId = appointment_test.aid')
                 ->join('patient','appointment.pid = patient.id')
                 ->join('area','area.areaId = procedures.procedureSection')
                 ->join('area_admin','area.areaId = area_admin.areaid')
                 ->where(array('appointmentTestStatus'=>'in queue','appointment.laboratoryid'=>$lab,'area_admin.userid'=>$this->session->userdata('id')))
                 ->order_by('appointment.pid asc, appointment.appointmentDate asc')
                 ->get('appointment_test')
                 ->result_array(); 
        else
            $rows = array();
        

        return $rows;
    }
    
    function getDoctorById($id)
    {
        $medtech = $this->db
                     ->select('user.*,doctors.doctorId')
                     ->join('doctors','doctors.userid = user.id')
                     ->where(array('doctorId'=>$id))
                     ->order_by('lastname','asc')
                     ->get('user')
                     ->result_array();
        return current($medtech);
    }
    
    function getDoctorByUserId($id)
    {
        $medtech = $this->db
                     ->select('user.*,doctors.doctorId')
                     ->join('doctors','doctors.userid = user.id')
                     ->where(array('userid'=>$id))
                     ->order_by('lastname','asc')
                     ->get('user')
                     ->result_array();
        return current($medtech);
    }
    
    function getUsersNotInDoctor()
    {
        $query = "SELECT * from user WHERE id NOT IN (SELECT userid FROM doctors)";
        return $this->db
                ->query($query)
                ->result_array();
    }
    
    function getLaboratoryNotSelected($id)
    {
        $query = "SELECT * from laboratories WHERE laboratoryId NOT IN (SELECT laboratoryid FROM laboratory_procedure WHERE procedureid = ".$id.")";
        return $this->db
                ->query($query)
                ->result_array();
    }
    
    function getLaboratorySelected($id)
    {
        $query = "SELECT * from laboratories WHERE laboratoryId IN (SELECT laboratoryid FROM laboratory_procedure WHERE procedureid = ".$id.")";
        return $this->db
                ->query($query)
                ->result_array();
    }
    
    function getProcedureNotSelected($id)
    {
        $query = "SELECT * from procedures WHERE procedureId NOT IN (SELECT procedureid FROM laboratory_procedure WHERE laboratoryid = ".$id.")";
        return $this->db
                ->query($query)
                ->result_array();
    }
    
    function getProcedureSelected($id)
    {
        $query = "SELECT procedures.*,labCcPrice,labprocId from procedures JOIN laboratory_procedure ON laboratory_procedure.procedureid = procedures.procedureId  WHERE laboratoryid = ".$id."";
        return $this->db
                ->query($query)
                ->result_array();
    }
    
    function getProcedureNotSelectedDoctor($id)
    {
        $query = "SELECT * from procedures WHERE procedureId NOT IN (SELECT procedureid FROM doctor_procedure WHERE doctorid = ".$id.")";
        return $this->db
                ->query($query)
                ->result_array();
    }
    
    function getUsersNotSelectedArea($id,$labid)
    {
        $query = "SELECT * from user WHERE id NOT IN (SELECT userid FROM area_admin WHERE areaid = ".$id.") AND laboratoryid = ".$labid;
        return $this->db
                ->query($query)
                ->result_array();
    }
    
    function getUsersSelectedArea($id,$labid)
    {
        $query = "SELECT * from user WHERE id IN (SELECT userid FROM area_admin WHERE areaid = ".$id.") AND laboratoryid = ".$labid;
        return $this->db
                ->query($query)
                ->result_array();
    }
    
    function getProcedureSelectedDoctor($id)
    {
        $query = "SELECT * from procedures WHERE procedureId IN (SELECT procedureid FROM doctor_procedure WHERE doctorid = ".$id.")";
        return $this->db
                ->query($query)
                ->result_array();
    }
    
    
    function fetchDoctors()
    {
        return 
            $this->db
             ->select('doctorId,firstname,lastname,specialization,subspecialization,qualification,prcid')
             ->from('user')
             ->join('doctors','doctors.userid = user.id')
             ->order_by('lastname','asc')
             ->get()
             ->result_array();    
    }
    
    function getSubjectsNotInCurriculum($id)
    {
        $bucket = "SELECT intID,strCode,strDescription FROM tb_mas_subjects WHERE intID NOT IN (SELECT intSubjectID from tb_mas_curriculum_subject WHERE intCurriculumID = ".$id.") ORDER BY strCode ASC"; 
        
        $subjects = $this->db
             ->query($bucket)
             ->result_array();
        
        //echo $this->db->last_query();
        return $subjects;
    }
    
    function getRoomsNotSelected($id)
    {
        $bucket = "SELECT tb_mas_classrooms.* FROM tb_mas_classrooms WHERE intID NOT IN (SELECT intRoomID from tb_mas_room_subject WHERE intSubjectID = ".$id.") ORDER BY strRoomCode ASC"; 
        
        $subjects = $this->db
             ->query($bucket)
             ->result_array();
        
        //echo $this->db->last_query();
        return $subjects;
    }
    
    function getRoomsSelected($id,$type=null)
    {
        $bucket = "SELECT tb_mas_classrooms.* FROM tb_mas_classrooms WHERE intID IN (SELECT intRoomID from tb_mas_room_subject WHERE intSubjectID = ".$id.")";  
        
        if($type!=null)
            $bucket .= " AND enumType = '".$type."' ";
        
        $bucket .= "ORDER BY strRoomCode ASC"; 
        
        $subjects = $this->db
             ->query($bucket)
             ->result_array();
        
        //echo $this->db->last_query();
        return $subjects;
    }
    
    function getSelectedDays($id)
    {
        return $this->db
                    ->select('strDays')
                    ->from('tb_mas_days')
                    ->where(array('intSubjectID'=>$id))
                    ->get()
                    ->result_array();
    }   
    
    function getPrereq($id,$type=null)
    {
        $bucket = "SELECT tb_mas_subjects.* FROM tb_mas_subjects WHERE intID IN (SELECT intPrerequisiteID from tb_mas_prerequisites WHERE intSubjectID = ".$id.")";  
        
        if($type!=null)
            $bucket .= " AND enumType = '".$type."' ";
        
        $bucket .= "ORDER BY strCode ASC"; 
        
        $subjects = $this->db
             ->query($bucket)
             ->result_array();
        
        //echo $this->db->last_query();
        return $subjects;
    }
    
    function getSubjectsNotSelected($id,$type=null)
    {
        $bucket = "SELECT tb_mas_subjects.* FROM tb_mas_subjects WHERE intID NOT IN (SELECT intPrerequisiteID from tb_mas_prerequisites WHERE intSubjectID = ".$id.")";  
        
        if($type!=null)
            $bucket .= " AND enumType = '".$type."' ";
        
        $bucket .= "ORDER BY strCode ASC"; 
        
        $subjects = $this->db
             ->query($bucket)
             ->result_array();
        
        //echo $this->db->last_query();
        return $subjects;
    }
    
    function getRequiredSubjects($studentID,$curriculumID,$sem=null,$year=null)
    {
        $bucket = "SELECT tb_mas_subjects.intID,strCode,strDescription FROM tb_mas_subjects JOIN tb_mas_curriculum_subject ON tb_mas_curriculum_subject.intSubjectID = tb_mas_subjects.intID JOIN tb_mas_curriculum on tb_mas_curriculum.intID = tb_mas_curriculum_subject.intCurriculumID WHERE tb_mas_subjects.intID NOT IN (SELECT intSubjectID from tb_mas_classlist_student  JOIN tb_mas_classlist ON intClassListID = tb_mas_classlist.intID WHERE intStudentID = ".$studentID." AND strRemarks = 'Passed') AND tb_mas_subjects.intID NOT IN (SELECT intSubjectID from tb_mas_credited_grades WHERE intStudentID =".$studentID.") AND  tb_mas_curriculum.intID = '".$curriculumID."' ";
            
    //
        
        
        if($sem!=null && $year!=null)
            $bucket .= "AND tb_mas_curriculum_subject.intYearLevel = ".$year." AND tb_mas_curriculum_subject.intSem = ".$sem." ";
        
        
        
        $bucket .= "ORDER BY tb_mas_curriculum_subject.intYearLevel ASC, tb_mas_curriculum_subject.intSem ASC"; 
        
        $subjects = $this->db
             ->query($bucket)
             ->result_array();
        
        //echo $this->db->last_query();
        //print_r($subjects);
        $ret = array();
        //PREREQUISITES CODE----------------------------------------------------------------------------
        /*
        foreach($subjects as $subj)
        {
            $add = true;
            
            $r = $this->db
                      ->get_where('tb_mas_prerequisites',array('intSubjectID'=>$subj['intID']))
                      ->result_array();
            
            if(!empty($r))
            {
                foreach($r as $res){
                    $s = $this->db
                      ->select('tb_mas_classlist_student.intCSID')
                      ->from('tb_mas_classlist_student')
                      ->join('tb_mas_classlist','tb_mas_classlist.intID = tb_mas_classlist_student.intClassListID')
                      ->where(array('intSubjectID'=>$res['intPrerequisiteID'],'strRemarks'=>'Passed','intStudentID'=>$studentID))
                      ->get()
                      ->result_array();
                    
                    if(empty($s))
                    {
                        $add = false;
                        break;
                    }
                    
                }
                
                
            }
            
            if($add)
                $ret[] = $subj;
                    
        }
        return $ret;
        */
        return $subjects;
    }
    
    function getSubjectsInCurriculum($id)
    {
        $subjects = $this->db
                         ->select( 'tb_mas_curriculum_subject.intID,tb_mas_curriculum_subject.intYearLevel,tb_mas_curriculum_subject.intSem,tb_mas_subjects.strCode,tb_mas_subjects.strUnits,tb_mas_subjects.intID as intSubjectID,tb_mas_subjects.strDescription')
                         ->from('tb_mas_subjects')
                         ->join('tb_mas_curriculum_subject','tb_mas_curriculum_subject.intSubjectID = tb_mas_subjects.intID')
                         ->where('tb_mas_curriculum_subject.intCurriculumID',$id)
                         ->order_by('intYearLevel asc,intSem asc, strCode asc')
                         ->get()
                         ->result_array();
        
        return $subjects;
    }
    
    function getSubjectsInCurriculumWithSections($curriculumID,$sem,$studentID)
    {
        
        $bucket = "SELECT tb_mas_curriculum_subject.intID,tb_mas_curriculum_subject.intYearLevel,tb_mas_curriculum_subject.intSem,tb_mas_subjects.strCode,tb_mas_subjects.strUnits,tb_mas_subjects.intID as intSubjectID,tb_mas_subjects.strDescription FROM tb_mas_subjects JOIN tb_mas_curriculum_subject ON tb_mas_curriculum_subject.intSubjectID = tb_mas_subjects.intID JOIN tb_mas_curriculum on tb_mas_curriculum.intID = tb_mas_curriculum_subject.intCurriculumID JOIN tb_mas_classlist ON tb_mas_subjects.intID = tb_mas_classlist.intSubjectID WHERE tb_mas_subjects.intID NOT IN (SELECT intSubjectID from tb_mas_classlist_student  JOIN tb_mas_classlist ON intClassListID = tb_mas_classlist.intID WHERE intStudentID = ".$studentID." AND strRemarks = 'Passed') AND tb_mas_subjects.intID NOT IN (SELECT intSubjectID from tb_mas_credited_grades WHERE intStudentID =".$studentID.") AND  tb_mas_curriculum.intID = '".$curriculumID."' AND (tb_mas_subjects.intPrerequisiteID IN (SELECT `intSubjectID` from tb_mas_classlist_student  JOIN tb_mas_classlist ON intClassListID = tb_mas_classlist.intID WHERE intStudentID = ".$studentID." AND strRemarks = 'Passed') OR tb_mas_subjects.intPrerequisiteID = 0 ) AND tb_mas_classlist.strAcademicYear = ".$sem." ";
            
        
        $bucket .= "GROUP BY tb_mas_classlist.intSubjectID ORDER BY tb_mas_curriculum_subject.intYearLevel ASC, tb_mas_curriculum_subject.intSem ASC"; 
        
        $subjects = $this->db
             ->query($bucket)
             ->result_array();
        
        //echo $this->db->last_query();
        //print_r($subjects);
        return $subjects;
    }
    
    function countUnitsInCurriculum($id)
    {
        $subjects = $this->db
                         ->select( 'SUM(tb_mas_subjects.strUnits) as totalUnits')
                         ->from('tb_mas_subjects')
                         ->join('tb_mas_curriculum_subject','tb_mas_curriculum_subject.intSubjectID = tb_mas_subjects.intID')
                         ->where('tb_mas_curriculum_subject.intCurriculumID',$id)
                         ->group_by('tb_mas_curriculum_subject.intCurriculumID')
                         ->get()
                         ->result_array();
        
        return $subjects[0]['totalUnits'];
    }
    
    function count_table_contents($table,$category = null,$where=null,$group=null)
    {
            if($category!=null)
                $this->db
				     ->where('enumCat',$category);		
            if($where!=null)
                $this->db
				     ->where($where);		
            if($group!=null)
                $this->db->group_by($group); 
        
            return $this->db
                        ->count_all_results($table);
        
        
        
    }
    
    function count_sent_items($user)
    {
            	
            $this->db->select("intMessageUserID")
                     ->where(array("intTrash"=>"0","intFacultyIDSender"=>$user))
                      ->group_by("intMessageID"); 
        
            $result = $this->db
                  ->get("tb_mas_message_user")->result_array();
        
            return count($result);
        
        
        
    }
    
    function fetch_student_data($table,$order=null,$limit=null,$where=null)
	{				
		
        $this->db->select('intID,strFirstname,strMiddlename,strLastname,strCourse,dteCreated,strSection');
		if($order!=null)
			$this->db->order_by($order[0],$order[1]);
		elseif($table == 'tb_mas_content')
			$this->db->order_by('dteStart','desc');
		
		if($limit!=null)
			$this->db->limit($limit);
			
		if($where!=null)
			$this->db->where($where);
		
		$data =  $this->db
						->get($table)
						->result_array();
						
		return $data;
						
	}
    function search_for_students($search_string)
    {
        $this->db->where('strFirstname',$search_string)
                 ->or_where('strLastname',$search_string)
                 ->get('tb_mas_users');
        
        return $this->db->result_array();
        
        
    }
    
    function fetch_students($table,$order=null,$limit=20,$where=null,$offset)
	{				
		
		if($order!=null)
			$this->db->order_by($order[0],$order[1]);
		elseif($table == 'tb_mas_content')
			$this->db->order_by('dteStart','desc');
		
		if($limit!=null)
			$this->db->limit($limit,$offset);
			
		if($where!=null)
			$this->db->where($where);
		
		$data =  $this->db
						->get($table)
						->result_array();
						
		return $data;
						
	}
    
    
    function fetch_logs($start,$end)
    {
        $this->db
             ->select('strFirstname,strLastname,strAction,strCategory,dteLogDate,strColor')
             ->from('tb_mas_logs')
             ->join('tb_mas_faculty','tb_mas_faculty.intID = tb_mas_logs.intFacultyID');
           
        if($start == null)    
            $this->db->limit(20);
        else{
            $end .=" 23:59:59";
           $this->db->where(array('dteLogDate >='=>$start,'dteLogDate <='=>$end));
        }
        return    $this->db
                ->order_by('dteLogDate','desc')
                ->get()
                ->result_array();
    }
    
    function fetch_transactions($start,$end)
    {
        if($start != null)
        {
            $this->db
                 ->select('tb_mas_transactions.*,tb_mas_users.strFirstname,tb_mas_users.strLastname,tb_mas_users.intID as studentID')
                 ->from('tb_mas_transactions')
                 ->join('tb_mas_registration','tb_mas_registration.intRegistrationID = tb_mas_transactions.intRegistrationID')
                 ->join('tb_mas_users','tb_mas_users.intID = tb_mas_registration.intStudentID');
            
            
            
                $end .=" 23:59:59";
               $this->db->where(array('dtePaid >='=>$start,'dtePaid <='=>$end));
            
            return    $this->db
                    ->order_by('dtePaid desc,intORNumber asc')
                    ->get()
                    ->result_array();
        }
        else
            return array();
    }
    
    function get_active_sem()
    {
        return current($this->db->get_where('tb_mas_sy',array('enumStatus'=>'active'))->result_array());
        
    }
    
    function get_prev_sem($sem = null)
    {
        if($sem == null)
            $active = current($this->db->get_where('tb_mas_sy',array('enumStatus'=>'active'))->result_array());
        else     
            $active = current($this->db->get_where('tb_mas_sy',array('intID'=>$sem))->result_array());
        
        if($active['enumSem'] != "1st")
        {
            $sem = switch_num(switch_num_rev($active['enumSem']) - 1);
            $yearStart = $active['strYearStart'];
            $yearEnd = $active['strYearEnd'];
        }
        else
        {
            $sem = "2nd";
            $yearStart = $active['strYearStart'] - 1;
            $yearEnd = $active['strYearEnd'] - 1;
        }
        
        return current($this->db->get_where('tb_mas_sy',array('enumSem'=>$sem,'strYearStart'=>$yearStart,'strYearEnd'=>$yearEnd))->result_array());
        
        
    }
    
    function getClassroom($id)
    {
        return current($this->db->get_where('tb_mas_classrooms',array('intID'=>$id))->result_array());
        
    }
    
     function get_sem_by_id($id)
    {
        return current($this->db->get_where('tb_mas_sy',array('intID'=>$id))->result_array());
        
    }
    
    function fetch_classlists($limit=null,$sem = false,$sem_sel=null)
    {
        $faculty_id = $this->session->userdata("intID");
                    $this->db
                     ->select("tb_mas_classlist.intID as intID, strSection, intFacultyID,intSubjectID,strClassName,strCode,intFinalized,strAcademicYear,enumSem,strYearStart,strYearEnd,count(tb_mas_classlist_student.intCSID) as numStudents")
                     ->from("tb_mas_classlist")
                     ->where(array("intFacultyID"=>$faculty_id));
                    
                    if($sem_sel!=null)
                        $this->db->where(array('strAcademicYear'=>$sem_sel));
        
                     $this->db->join('tb_mas_subjects', 'tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                     ->join('tb_mas_sy', 'tb_mas_sy.intID = tb_mas_classlist.strAcademicYear')
                    ->join('tb_mas_classlist_student','tb_mas_classlist_student.intClassListID = tb_mas_classlist.intID','left outer');
                if($limit != null)
                    $this->db->limit($limit);
                 return $this->db
                        ->group_by('tb_mas_classlist.intID')
                        ->get()
                        ->result_array();
        
    }
    
    
    
    function fetch_classlists_all($limit=null,$sem_sel=null)
    {
                    $this->db
                     ->select("tb_mas_classlist.intID as intID, strSection, intFacultyID,intSubjectID,strClassName,strCode,intFinalized,strAcademicYear,strFirstname,strLastname,strYearStart,strYearEnd,enumSem")
                     ->from("tb_mas_classlist")
                     ->join('tb_mas_faculty', 'tb_mas_faculty.intID = tb_mas_classlist.intFacultyID')
                     ->join('tb_mas_subjects', 'tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                     ->join('tb_mas_sy', 'tb_mas_sy.intID = tb_mas_classlist.strAcademicYear');
                if($sem_sel!=null)
                        $this->db->where(array('strAcademicYear'=>$sem_sel));
                if($limit != null)
                    $this->db->limit($limit);
                 
                return $this->db 
                        ->get()
                        ->result_array();
    }
    
    function fetch_classlists_unassigned($sem_sel=null,$limit=null)
    {
                    $this->db
                     ->select("tb_mas_classlist.intID as intID, strSection, intFacultyID,intSubjectID,strClassName,strCode,intFinalized,strAcademicYear,strFirstname,strLastname,strYearStart,strYearEnd,enumSem")
                     ->from("tb_mas_classlist")
                     ->join('tb_mas_faculty', 'tb_mas_faculty.intID = tb_mas_classlist.intFacultyID')
                     ->join('tb_mas_subjects', 'tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                     ->join('tb_mas_sy', 'tb_mas_sy.intID = tb_mas_classlist.strAcademicYear');
               
                    $this->db->where(array('strAcademicYear'=>$sem_sel,'intFacultyID'=>999));
                
            if($limit != null)
                    $this->db->limit($limit);
                 
                return $this->db 
                        ->get()
                        ->result_array();
    }
    
    function fetch_classlist_by_id($limit=null,$id)
    {
        $faculty_id = $this->session->userdata("intID");
                    $this->db
                     ->select("tb_mas_classlist.intID as intID, strSection, intFacultyID,intSubjectID,strClassName,strCode,intFinalized,strAcademicYear,strFirstname,strLastname,strYearStart,strYearEnd,enumSem,tb_mas_classlist.strUnits,strSignatory1Name,strSignatory2Name,strSignatory1Title,strSignatory2Title")
                     ->from("tb_mas_classlist")
                     ->join('tb_mas_faculty', 'tb_mas_faculty.intID = tb_mas_classlist.intFacultyID')
                     ->join('tb_mas_subjects', 'tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                     ->join('tb_mas_sy', 'tb_mas_sy.intID = tb_mas_classlist.strAcademicYear');
                
                $this->db->where(array('tb_mas_classlist.intID'=>$id));
                
                if($limit != null)
                    $this->db->limit($limit);
                 return current($this->db 
                        ->get()
                        ->result_array());
    }
    
    function fetch_classlist_by_subject($subject_id,$sem)
    {
        return $this->db
                    ->select('tb_mas_classlist.*,count(tb_mas_classlist_student.intCSID) as numStudents')
                    ->from('tb_mas_classlist')
                    ->join('tb_mas_classlist_student','tb_mas_classlist_student.intClassListID = tb_mas_classlist.intID')
                    ->where(array('intSubjectID'=>$subject_id,'strSection !='=>'','strAcademicYear'=>$sem))
                    ->group_by('tb_mas_classlist_student.intClasslistID')
                    ->get()
                    ->result_array();
    }
    
    
    function fetch_classlist_id($id)
    {
        return current($this->db
                    ->get_where('tb_mas_classlist',array('intID'=>$id))
                    ->result_array());
    }
    
    
    function getAverageGrade($id)
    {
        $score = $this->db->get_where('tb_mas_classlist_student',array('intCSID'=>$id))->first_row();
        $average = ($score->floatPrelimGrade+$score->floatMidtermGrade+$score->floatFinalGrade)/3;
        return $average;
    }
    
    function getCS($studentId,$classListId)
    {
        return  $this->db->get_where('tb_mas_classlist_student',array('intStudentID'=>$studentId,'intClassListID'=>$classListId))->result_array();
                     
    }   
    
    function getStudentSection($course,$year,$section)
    {
        return  $this->db->get_where('tb_mas_users',array('intProgramID'=>$course,'dteCreated'=>$year."-01-01",'strSection'=>$section))->result_array();
                     
    }
    
    function getStudents($course = 0,$regular= 0, $year=0,$gender = 0,$graduate=0,$scholarship=0,$registered=0,$sem=0)
    {
        
        $this->db
            ->select('tb_mas_users.*,strProgramCode')
            ->from('tb_mas_users')
            ->join('tb_mas_programs','tb_mas_users.intProgramID = tb_mas_programs.intProgramID');
        
        if($registered!=0 && $sem!=0){
            $this->db
                 ->join('tb_mas_registration','tb_mas_registration.intStudentID = tb_mas_users.intID')
                 ->where('tb_mas_registration.intAYID',$sem);
        }
        
         if($course!=0)
            $this->db->where('tb_mas_users.intProgramID',$course);
        if($regular!=0)
           if($regular == 1)
                $this->db->where('strAcademicStanding','regular');
            else
                $this->db->where('strAcademicStanding','irregular');
        
        if($gender!=0)
           if($gender == 1)
                $this->db->where('enumGender','male');
            else
                $this->db->where('enumGender','female');
        
        if($year!=0)
            $this->db->where('intStudentYear',$year);
        
        if($graduate!=0)
            if($graduate == 1)
                    $this->db->where('isGraduate',1);
                else
                    $this->db->where('isGraduate',0);
        
        
        
        if($scholarship!=0)
            if($scholarship == 1)
                $this->db->where('enumScholarship','paying');
            elseif($scholarship == 2)
                $this->db->where('enumScholarship','resident scholar');
            elseif($scholarship == 3)
                    $this->db->where('enumScholarship','7th district scholar');
            elseif($scholarship == 4)
                    $this->db->where('enumScholarship','DILG scholar');
        
        return $this->db
             ->get()
             ->result_array();
            
            
                     
    }
    
    function getRegisteredStudents($ay)
    {
       
        return
        $this->db
             ->select('tb_mas_users.*,strProgramCode')
             ->from('tb_mas_users')
             ->join('tb_mas_programs','tb_mas_users.intProgramID = tb_mas_programs.intProgramID')
             ->join('tb_mas_registration','tb_mas_registration.intStudentID = tb_mas_users.intID')
             ->where(array('intAYID'=>$ay))
             ->get()
             ->result_array();
            
            
                     
    }
    
    function countStudentsByCourse($course)
    {
        return  count($this->db->get_where('tb_mas_users',array('intProgramID'=>$course,'isGraduate'=>0))->result_array());   
    }
    
    function getScholars($programID,$type,$ay)
    {
        return 
            count($this->db
             ->select('intRegistrationID')
             ->from('tb_mas_registration')
             ->where('intAYID = '.$ay.' AND tb_mas_users.enumScholarship = \''.$type.'\' AND tb_mas_users.intProgramID = '.$programID)
             ->join('tb_mas_users','tb_mas_registration.intStudentID = tb_mas_users.intID')
             ->get()
             ->result_array());
    }
    
    function getSubjects($post = null)
    {
        if($post!=null){
            $courses =
                $this->db
                     ->select( 'tb_mas_subjects.intID,intAthleticFee,strCode,strDescription,strUnits,intLab,intAthleticFee,tb_mas_curriculum_subject.intYearLevel,tb_mas_curriculum_subject.intSem')
                     ->from('tb_mas_subjects')
                     ->join('tb_mas_curriculum_subject','tb_mas_curriculum_subject.intSubjectID = tb_mas_subjects.intID')
                     ->join('tb_mas_curriculum','tb_mas_curriculum.intID = tb_mas_curriculum_subject.intCurriculumID')
                     ->where(array('tb_mas_curriculum.intID'=>$post['intCurriculumID'],'tb_mas_curriculum_subject.intYearLevel'=>$post['intYearLevel'],'tb_mas_curriculum_subject.intSem'=>$post['intSem']))
                     ->get()
                     ->result_array();
                
                
               // $this->db->get_where('tb_mas_subjects',array('intYearLevel'=>$post['intYearLevel'],'intProgramID'=>$post['strCourse'],'intSem'=>$post['intSem']))->result_array();

            
        }
        else
        {
            $courses =  $this->db
                             ->get('tb_mas_subjects')
                             ->result_array();
        }
        return $courses;
        
    }
    
    
    
    function getSubjectsCurriculum($post = null)
    {
        if($post!=null){
            $courses =
                $this->db
                     ->select( 'tb_mas_subjects.intID,intAthleticFee,strCode,strDescription,strUnits,intLab,intAthleticFee,tb_mas_curriculum_subject.intYearLevel,tb_mas_curriculum_subject.intSem')
                     ->from('tb_mas_subjects')
                     ->join('tb_mas_curriculum_subject','tb_mas_curriculum_subject.intSubjectID = tb_mas_subjects.intID')
                     ->join('tb_mas_curriculum','tb_mas_curriculum.intID = tb_mas_curriculum_subject.intCurriculumID')
                     ->where(array('tb_mas_curriculum.intID'=>$post['intCurriculumID']))
                     ->get()
                     ->result_array();
                
                
               // $this->db->get_where('tb_mas_subjects',array('intYearLevel'=>$post['intYearLevel'],'intProgramID'=>$post['strCourse'],'intSem'=>$post['intSem']))->result_array();

            
        }
        return $courses;
        
    }
    
    function getFromSchedule($scode,$sem,$lab='lect',$day=null)
	{
        $this->db
		 	 ->where(array('strScheduleCode'=>$scode,'intSem'=>$sem,'enumClassType'=>$lab));
        
            if($day!=null)
                $this->db->where('strDay',$day);
        $d =
            $this->db
                 ->get('tb_mas_room_schedule')
                 ->first_row();
        
        if(empty($d))
            return 0;
        else
            return $d->intRoomSchedID;
	}
    
    function getSelectedSubjects($id)
    {
        
        $subjects =  $this->db
                         ->select('intSubjectID')
                         ->from('tb_mas_subjects_faculty')
                         ->where(array('intFacultyID'=>$id))   
                         ->get()
                         ->result_array();
        
        return $subjects;
        
    }
    
    function getStudent($id)
    {
        return  current(
                $this->db
                     ->select('tb_mas_users.*,tb_mas_programs.*,tb_mas_curriculum.strName')
                     ->from('tb_mas_users')
                     ->join('tb_mas_programs','tb_mas_programs.intProgramID = tb_mas_users.intProgramID')   
                     ->join('tb_mas_curriculum','tb_mas_curriculum.intID = tb_mas_users.intCurriculumID')
                     ->where(array('tb_mas_users.intID'=>$id))
                     ->get()
                     ->result_array());
                     
    }
    
    function assessCurriculum($studentID,$curriculumID)
    {
        $subjects =  $this->db
                     ->select('tb_mas_subjects.strCode,floatFinalGrade,strRemarks,tb_mas_curriculum_subject.intID,tb_mas_curriculum_subject.intYearLevel,tb_mas_curriculum_subject.intSem,tb_mas_subjects.strUnits,tb_mas_sy.enumSem,tb_mas_sy.strYearStart,tb_mas_sy.strYearEnd,tb_mas_sy.intID as syID')
                     ->from('tb_mas_curriculum_subject')
                     ->join('tb_mas_subjects','tb_mas_subjects.intID = tb_mas_curriculum_subject.intSubjectID OR tb_mas_subjects.intEquivalentID1 = tb_mas_curriculum_subject.intSubjectID OR tb_mas_subjects.intEquivalentID2 = tb_mas_curriculum_subject.intSubjectID')
                     ->join('tb_mas_classlist','tb_mas_subjects.intID = tb_mas_classlist.intSubjectID','inner')
                     ->join('tb_mas_classlist_student','tb_mas_classlist.intID = tb_mas_classlist_student.intClasslistID','inner')
                    ->join('tb_mas_sy','tb_mas_sy.intID = tb_mas_classlist.strAcademicYear')
                     ->where(array('tb_mas_curriculum_subject.intCurriculumID'=>$curriculumID,'intStudentID'=>$studentID))
                     ->order_by('strYearStart asc,enumSem asc, strCode asc')
                     ->get()
                     ->result_array();
        
        $credited = $this->db
                     ->select('tb_mas_subjects.strCode,tb_mas_credited_grades.floatFinalGrade,tb_mas_credited_grades.strRemarks,tb_mas_curriculum_subject.intID,tb_mas_curriculum_subject.intYearLevel,tb_mas_curriculum_subject.intSem,tb_mas_subjects.strUnits,tb_mas_credited_grades.intSYID as enumSem,tb_mas_credited_grades.intSYID as strYearStart,tb_mas_credited_grades.intSYID as strYearEnd,tb_mas_credited_grades.intSYID  as syID')
                     ->from('tb_mas_credited_grades')
                     ->join('tb_mas_subjects','tb_mas_subjects.intID = tb_mas_credited_grades.intSubjectID')
                     ->join('tb_mas_curriculum_subject','tb_mas_curriculum_subject.intCurriculumID = tb_mas_credited_grades.intCurriculumID','inner')
                     ->where(array('tb_mas_credited_grades.intCurriculumID'=>$curriculumID,'intStudentID'=>$studentID))
                     ->group_by('tb_mas_credited_grades.intID')
                     ->order_by('strYearStart asc,enumSem asc')
                     ->get()
                     ->result_array();
                     
        return array_merge($subjects,$credited);
    }
    
    function getCreditedSubjects($studentID,$curriculumID)
    {
        return $this->db
                     ->select('tb_mas_credited_grades.intID,tb_mas_subjects.strCode,tb_mas_credited_grades.floatFinalGrade,tb_mas_credited_grades.strRemarks,tb_mas_subjects.strUnits')
                     ->from('tb_mas_credited_grades')
                     ->join('tb_mas_subjects','tb_mas_subjects.intID = tb_mas_credited_grades.intSubjectID')
                     ->where(array('tb_mas_credited_grades.intCurriculumID'=>$curriculumID,'intStudentID'=>$studentID))
                     ->order_by('intYearLevel asc,intSem asc')
                     ->get()
                     ->result_array();
    }
    
    
    function unitsEarned($studentID,$curriculumID)
    {
        $ret =  $this->db
                     ->select('SUM(tb_mas_subjects.strUnits) AS TotalUnitsEarned')
                     ->from('tb_mas_curriculum_subject')
                     ->join('tb_mas_subjects','tb_mas_subjects.intID = tb_mas_curriculum_subject.intSubjectID OR tb_mas_subjects.intEquivalentID1 = tb_mas_curriculum_subject.intSubjectID OR tb_mas_subjects.intEquivalentID2 = tb_mas_curriculum_subject.intSubjectID')
                     ->join('tb_mas_classlist','tb_mas_subjects.intID = tb_mas_classlist.intSubjectID','inner')
                     ->join('tb_mas_classlist_student','tb_mas_classlist.intID = tb_mas_classlist_student.intClasslistID','inner')
                     ->where(array('tb_mas_curriculum_subject.intCurriculumID'=>$curriculumID,'intStudentID'=>$studentID,'strRemarks'=>'Passed','floatFinalGrade !='=>'5'))
                     ->group_by('tb_mas_curriculum_subject.intCurriculumID')
                     ->get()
                     ->result_array();
        
        $credited = $this->db
                     ->select('SUM(tb_mas_subjects.strUnits) AS TotalUnitsEarned')
                     ->from('tb_mas_curriculum_subject')
                     ->join('tb_mas_subjects','tb_mas_subjects.intID = tb_mas_curriculum_subject.intSubjectID')
                     ->join('tb_mas_credited_grades','tb_mas_credited_grades.intSubjectID = tb_mas_subjects.intID','inner')
                     ->where(array('tb_mas_curriculum_subject.intCurriculumID'=>$curriculumID,'intStudentID'=>$studentID,'strRemarks'=>'Passed','floatFinalGrade !='=>'5'))
                     ->group_by('tb_mas_curriculum_subject.intCurriculumID')
                     ->get()
                     ->result_array();
        
        $return = 0;
        if(!empty($ret))
            $return += $ret[0]['TotalUnitsEarned'];
        if(!empty($credited))
            $return +=$credited[0]['TotalUnitsEarned'];
       
        return $return;
    }
    
    function getGPA($studentID,$curriculumID)
    {
        $st =  $this->db
                     ->select('SUM(floatFinalGrade * tb_mas_subjects.strUnits) as gpa, SUM(tb_mas_subjects.strUnits) as num')
                     ->from('tb_mas_curriculum_subject')
                     ->join('tb_mas_subjects','tb_mas_subjects.intID = tb_mas_curriculum_subject.intSubjectID OR tb_mas_subjects.intEquivalentID1 = tb_mas_curriculum_subject.intSubjectID OR tb_mas_subjects.intEquivalentID2 = tb_mas_curriculum_subject.intSubjectID')
                     ->join('tb_mas_classlist','tb_mas_subjects.intID = tb_mas_classlist.intSubjectID','inner')
                     ->join('tb_mas_classlist_student','tb_mas_classlist.intID = tb_mas_classlist_student.intClasslistID','inner')
                     ->where(array('tb_mas_curriculum_subject.intCurriculumID'=>$curriculumID,'intStudentID'=>$studentID,'floatFinalGrade !='=>'0','floatFinalGrade !='=>'3.5'))
                     ->group_by('tb_mas_curriculum_subject.intCurriculumID')
                     ->get()
                     ->first_row();
        
        $credited =  $this->db
                     ->select('SUM(floatFinalGrade * tb_mas_subjects.strUnits) as gpa, SUM(tb_mas_subjects.strUnits) as num')
                     ->from('tb_mas_curriculum_subject')
                     ->join('tb_mas_subjects','tb_mas_subjects.intID = tb_mas_curriculum_subject.intSubjectID')
                     ->join('tb_mas_credited_grades','tb_mas_credited_grades.intSubjectID = tb_mas_subjects.intID','inner')
                     ->where(array('tb_mas_curriculum_subject.intCurriculumID'=>$curriculumID,'intStudentID'=>$studentID,'floatFinalGrade !='=>'0','floatFinalGrade !='=>'3.5'))
                     ->group_by('tb_mas_curriculum_subject.intCurriculumID')
                     ->get()
                     ->first_row();
        
        
        $return = 0;
        $div = 0;
        if(!empty($st)){
            $return += $st->gpa;
            $div += $st->num;
        }
     
        
        if(!empty($credited))
        {
            $return += $credited->gpa;
            $div += $credited->num;
        }
        
        
        
        if($div!=0)
            $return  = $return/$div;
        
        return $return;
    }
    
    function getUserData($id)
    {
        return $this->db
             ->select('user.*,companyName,companyAddress,companyContactNumber')
             ->from('user')
             ->join('companies','companies.companyId = user.companyAssoc')
             ->where('id',$id)
             ->get()
             ->first_row();
    }
    
    
    function getUnitsPerYear($id)
    {
        $subjects = $this->db
                         ->select( 'SUM(tb_mas_subjects.strUnits) as totalUnits, tb_mas_curriculum_subject.intYearLevel')
                         ->from('tb_mas_subjects')
                         ->join('tb_mas_curriculum_subject','tb_mas_curriculum_subject.intSubjectID = tb_mas_subjects.intID')
                         ->where('tb_mas_curriculum_subject.intCurriculumID',$id)
                         ->group_by('tb_mas_curriculum_subject.intYearLevel, tb_mas_curriculum_subject.intSem')
                         ->get()
                         ->result_array();
        
        return $subjects;
    }
    
    function executeAcademicSync()
    {
        $stud = $this->db
             ->where(array('isGraduate !='=>'1'))
             ->get('tb_mas_users')
             ->result_array();
        
        foreach($stud as $s)
        {
            $standing = $this->getAcademicStanding($s['intID'],$s['intCurriculumID']);
            $data['intStudentYear'] = $standing['year'];
            $data['strAcademicStanding'] = $standing['status'];
            $this->db
                 ->where('intID',$s['intID'])
                 ->update('tb_mas_users',$data);
            
        }
             
    }
    
    function getFailedSubject($studentID)
    {
        return $this->db
             ->select('intCSID')
             ->from('tb_mas_classlist_student')
             ->where(array('intStudentID'=>$studentID,'strRemarks != '=>'Passed','floatFinalGrade != '=>0))
             ->get()
             ->result_array();
    }
    
    function getAcademicStanding($studentID,$curriculumID)
    {
        $units = $this->unitsEarned($studentID,$curriculumID);
        $standing['year'] = 1;
        $standing['status'] = "regular";
        $t = 0;
        $i = 1;
        foreach($this->getUnitsPerYear($curriculumID) as $year_level)
        {
            $t += $year_level['totalUnits'];
            if($units >= $t && ($i % 2) == 0)
                $standing['year'] = $year_level['intYearLevel']+1;
            
            $i++;
        }
        
        if(!empty($this->getFailedSubject($studentID)))
            $standing['status'] = "irregular";
        elseif($units == 0)
            $standing['status'] = "new";
        
        
        return $standing;
    }
    
    
    function getStudentStudentNumber($id)
    {
        return  current(
                $this->db
                     ->select('*')
                     ->from('tb_mas_users')
                     ->where(array('strStudentNumber'=>$id))
                     ->join('tb_mas_programs','tb_mas_programs.intProgramID = tb_mas_users.intProgramID')    
                     ->get()
                     ->result_array());
                     
    }
    function getStudentByName($lname,$fname,$code)
    {
        return  current(
                $this->db
                     ->select('*')
                     ->from('tb_mas_users')
                     ->where(array('strFirstname LIKE'=>$fname,'strLastname LIKE'=>$lname,'strProgramCode LIKE'=>$code))
                     ->join('tb_mas_programs','tb_mas_programs.intProgramID = tb_mas_users.intProgramID')    
                     ->get()
                     ->result_array());
                     
    }
    function getUser($id)
    {
        return  current($this->db->get_where('user',array('id'=>$id))->result_array());
                     
    }
    function getPatient($id)
    {
        return  current($this->db->get_where('patient',array('id'=>$id))->result_array());
                     
    }
    function getVitals($id)
    {
        return  $this->db
                     ->order_by('vitalsDate','DESC')
                     ->limit(5)
                     ->get_where('vitals',array('vitalsPatientID'=>$id))
                     ->result_array();
    }
    function get_next_queue($area,$lab)
    {
         
         $this->db
                 ->select('appointment_test.aid')
                 ->from('appointment_test')
                 ->join('procedures','appointment_test.procedureid = procedures.procedureId') 
                 ->join('appointment','appointment_test.aid = appointment.appointmentId')
                 ->join('patient','appointment.pid = patient.id')
                 ->where(array('appointmentTestStatus'=>'in queue','appointment.laboratoryid'=>$lab,'procedureSection'=>$area,'appointmentStatus'=>'active'))
                 ->order_by('appointment_test.testQueueCode','asc');
        
         $this->db->where(array('appointmentTestStatus'=>'in progress','appointment.laboratoryid'=>$lab,'procedureSection'=>$area,'appointmentDate'=>date("Y-m-d"),'appointmentTime <='=>date("H:i:s")));
        
             $d =    $this->db->get()
                 ->first_row();
        if(!empty($d))
            return $d->aid;
        else
            return 0;
    }
    
    function fetch_categories($id)
    {
        return $this->db->get_where('tb_mas_content_category',array('intContentID'=>$id))->result_array();
    }
    
    function fetch_gallery_images($id)
    {
        return $this->db->get_where('tb_mas_gallery',array('intArticleID'=>$id))->result_array();
    }
    
    function getQueue($lab,$inprogress=false,$area=0)
    {
        
            $this->db
                 ->select('appointment_test.appointmentTestId,appointment_test.aid,procedureSection,appointment_test.testRequestCode,procedures.procedureShortName,procedures.procedureName,patient.patientLastname,patient.patientFirstname,patient.birthday,patient.patientid')
                 ->from('appointment_test')
                 ->join('procedures','appointment_test.procedureid = procedures.procedureId') 
                 ->join('appointment','appointment_test.aid = appointment.appointmentId')
                 ->join('patient','appointment.pid = patient.id');
                 
        
            if(!$inprogress)
                    $this->db->where(array('appointmentTestStatus'=>'in queue','appointment.laboratoryid'=>$lab,'procedureSection'=>$area));
            else
                $this->db->where(array('appointmentTestStatus'=>'in progress','appointment.laboratoryid'=>$lab,'procedureSection'=>$area));
        
             $this->db->where(array('intOnHold'=>'0','appointmentStatus'=>'active','appointmentDate'=>date("Y-m-d"),'appointmentTime <='=>date("H:i:s")));
        
             $ret =
                 $this->db
                     ->order_by('appointment_test.testRequestCode','asc')
                     ->group_by('appointment_test.aid')
                     ->get()
                     ->result_array();
        
        
            return $ret;
        
    }
    
    
    
    function getQueueConsultation($lab,$inprogress=false,$area=0)
    {
        
             
            if($inprogress)
                $select = "appointment_test.appointmentTestId,appointment_test.aid,procedureSection,appointment_test.testRequestCode,procedures.procedureShortName,procedures.procedureName,patient.patientLastname,patient.patientFirstname,patient.birthday,patient.patientid,firstname,lastname,consultationDoctorId,appointmentDate,appointmentTime";
            else
                $select = "appointment_test.appointmentTestId,appointment_test.aid,procedureSection,appointment_test.testRequestCode,procedures.procedureShortName,procedures.procedureName,patient.patientLastname,patient.patientFirstname,patient.birthday,patient.patientid";
                
        $this->db
                 ->select($select)
                 ->from('appointment_test')
                 ->join('procedures','appointment_test.procedureid = procedures.procedureId') 
                 ->join('appointment','appointment_test.aid = appointment.appointmentId')
                 ->join('patient','appointment.pid = patient.id');
          if($inprogress)
                $this->db
                    ->join('test_consultation','appointment_test.appointmentTestId = test_consultation.consultationAtestId')   
                    ->join('doctors','doctors.doctorId = test_consultation.consultationDoctorId')
                    ->join('user','user.id = doctors.userid');;    
            
            if(!$inprogress)
                    $this->db->where(array('appointmentTestStatus'=>'in queue','appointment.laboratoryid'=>$lab,'procedureSection'=>$area));
            else
                $this->db->where(array('appointmentTestStatus'=>'in progress','appointment.laboratoryid'=>$lab,'procedureSection'=>$area,'appointmentDate'=>date("Y-m-d"),'appointmentTime <='=>date("H:i:s")));
        
             $this->db->where(array('intOnHold'=>'0','appointmentStatus'=>'active'));
        
             $ret =
                 $this->db
                     ->order_by('appointment_test.testRequestCode','asc')
                     ->group_by('appointment_test.aid')
                     ->get()
                     ->result_array();
        
        
            return $ret;
        
    }
    
    function getDoctorSchedule($id)
    {
        return $this->db
                    ->get_where('doctor_schedule',array('dsDoctorID'=>$id))
                    ->result_array();
    }
    function getDoctorCalendar($start,$end,$id)
    {
        
        $this->db
                 ->select('appointment_test.appointmentTestId,appointment_test.aid,procedureSection,appointment_test.testRequestCode,procedures.procedureShortName,procedures.procedureName,patient.patientLastname,patient.patientFirstname,patient.birthday,patient.patientid,firstname,lastname,consultationDoctorId,appointmentDate,appointmentTime')
                 ->from('appointment_test')
                 ->join('procedures','appointment_test.procedureid = procedures.procedureId') 
                 ->join('appointment','appointment_test.aid = appointment.appointmentId')
                 ->join('patient','appointment.pid = patient.id')
                 ->join('test_consultation','appointment_test.appointmentTestId = test_consultation.consultationAtestId')
                 ->join('doctors','test_consultation.consultationDoctorId = doctors.doctorId')
                 ->join('user','user.id = doctors.userid');
                 
        
           
                $this->db->where(array('consultationDoctorId'=>$id,'appointmentStatus'=>'active',"appointmentDate >="=>$start,"appointmentDate <"=>$end));
        
                $this->db->where(array('intOnHold'=>'0'));
        
             $ret =
                 $this->db
                     ->order_by('appointment_test.testRequestCode','asc')
                     ->group_by('appointment_test.aid')
                     ->get()
                     ->result_array();
        
        
            return $ret;
        
    }
    function countConsultationDoctor($id)
    {
         $this->db->select('test_consultation.consultationAtestId')
                          ->from('test_consultation')
                          ->join('appointment_test','appointment_test.appointmentTestId = test_consultation.consultationAtestId')
                          ->join('appointment','appointment_test.aid = appointment.appointmentId');
        
            $this->db->where(array('appointmentTestStatus'=>'in progress','consultationDoctorId'=>$id,'appointmentStatus'=>'active','appointmentDate'=>date("Y-m-d"),'appointmentTime <='=>date("H:i:s")));
        
                $this->db->where(array('intOnHold'=>'0'));             
            $query = $this->db
                         ->order_by('appointment_test.testRequestCode','asc')
                         ->group_by('appointment_test.aid')
                         ->get();
        return $query->num_rows();
    }
    function getQueueConsultationDoctor($id,$status='in progress')
    {
        
            $this->db
                 ->select('appointment_test.appointmentTestId,appointment_test.aid,procedureSection,appointment_test.testRequestCode,procedures.procedureShortName,procedures.procedureName,patient.patientLastname,patient.patientFirstname,patient.birthday,patient.patientid,firstname,lastname','consultationDoctorId')
                 ->from('appointment_test')
                 ->join('procedures','appointment_test.procedureid = procedures.procedureId') 
                 ->join('appointment','appointment_test.aid = appointment.appointmentId')
                 ->join('patient','appointment.pid = patient.id')
                 ->join('test_consultation','appointment_test.appointmentTestId = test_consultation.consultationAtestId')
                 ->join('doctors','test_consultation.consultationDoctorId = doctors.doctorId')
                 ->join('user','user.id = doctors.userid');
                 
        
           
                $this->db->where(array('appointmentTestStatus'=>$status,'consultationDoctorId'=>$id,'appointmentStatus'=>'active','appointmentDate'=>date("Y-m-d"),'appointmentTime <='=>date("H:i:s")));
        
                $this->db->where(array('intOnHold'=>'0'));
        
             $ret =
                 $this->db
                     ->order_by('appointment_test.testRequestCode','asc')
                     ->group_by('appointment_test.aid')
                     ->get()
                     ->result_array();
        
        
            return $ret;
        
    }
    
   
    
    function getQueueAll($lab,$inprogress=false,$limit=null)
    {
        
            $this->db
                 ->select('appointment_test.appointmentTestId,appointment.appointmentId,appointment_test.aid,procedureSection,areaName,appointment_test.testRequestCode,procedures.procedureShortName,procedures.procedureName,patient.patientLastname,patient.patientFirstname,patient.birthday,patient.patientid')
                 ->from('appointment_test')
                 ->join('procedures','appointment_test.procedureid = procedures.procedureId')
                 ->join('area','area.areaId = procedures.procedureSection')
                 ->join('appointment','appointment_test.aid = appointment.appointmentId')
                 ->join('patient','appointment.pid = patient.id');
                 
        
            if(!$inprogress)
                    $this->db->where(array('appointmentTestStatus'=>'in queue','appointment.laboratoryid'=>$lab));
            else
                $this->db->where(array('appointmentTestStatus'=>'in progress','appointment.laboratoryid'=>$lab));
        
             $this->db->where(array('intOnHold'=>'0','appointmentStatus'=>'active','appointmentDate'=>date("Y-m-d"),'appointmentTime <='=>date("H:i:s")));
            if($limit!=null)
                $this->db->limit($limit);
            $ret =
                 $this->db
                     ->order_by('appointment_test.testRequestCode','asc')
                     ->group_by('appointment_test.aid')
                     ->get()
                     ->result_array();
        
        
            return $ret;
        
    }
    
    function getProcessing($lab,$area=0)
    {
        
            $this->db
                 ->select('appointment_test.appointmentTestId,appointment_test.aid,procedureSection,appointment_test.testRequestCode,procedures.procedureShortName,procedures.procedureName,patient.patientLastname,patient.patientFirstname,patient.birthday')
                 ->from('appointment_test')
                 ->join('procedures','appointment_test.procedureid = procedures.procedureId') 
                 ->join('appointment','appointment_test.aid = appointment.appointmentId')
                 ->join('patient','appointment.pid = patient.id');
                 
                $this->db->where(array('appointmentTestStatus'=>'processing','appointment.laboratoryid'=>$lab,'procedureSection'=>$area));
        
        $this->db->where(array('intOnHold'=>'0','appointmentStatus'=>'active','appointmentDate'=>date("Y-m-d"),'appointmentTime <='=>date("H:i:s")));
        
             $ret =
                 $this->db
                     ->order_by('appointment_test.testRequestCode','asc')
                     ->group_by('appointment_test.aid')
                     ->get()
                     ->result_array();
        
        
            return $ret;
        
    }
    
    function getQueueUnseen($lab,$area=0)
    {
        
            $this->db
                 ->select('appointment_test.appointmentTestId,procedureSection,appointment_test.testRequestCode,procedures.procedureShortName,procedures.procedureName,patient.patientLastname,patient.patientFirstname')
                 ->from('appointment_test')
                 ->join('procedures','appointment_test.procedureid = procedures.procedureId') 
                 ->join('appointment','appointment_test.aid = appointment.appointmentId')
                 ->join('patient','appointment.pid = patient.id');
                 
                    $this->db->where(array('appointmentTestStatus'=>'in queue','appointment.laboratoryid'=>$lab,'procedureSection'=>$area,'seenExtraction'=>0));
        
             $ret =
                 $this->db
                     ->order_by('appointment_test.testRequestCode','asc')
                     ->get()
                     ->result_array();
        
            return $ret;
        
    }
    
    
    function generatePatientID($year)
    {
        $st = true;
        
        while($st){       
            $snum = $year.rand(1000,9999);
            
            $array = $this->db->get_where('patient',array('patientid'=>$snum))->result_array();
            if(empty($array))
                $st = false;
            
        }
        
        return $snum;
    }
    
    function generateSpecimenID()
    {
        $st = true;
        
        
        $snum = date("dmyhis").rand(10,99);
        
        return $snum;
    }
    
    function generatePatientPassword()
    {
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                 .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                 .'0123456789!@#$%^&*'); // and any other characters
        shuffle($seed); // probably optional since array_is randomized; this may be redundant
        $rand = '';
        foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
        
        $snum = rand(1000,9999).$rand;
        
        return $snum;
    }
    
    
    function generateTestRequestCode()
    {
        $st = true;
        
        while($st){       
            $snum = strtotime(date("ymd")).rand(1000,9999);
            
            $array = $this->db->get_where('appointment_test',array('testRequestCode'=>$snum))->result_array();
            if(empty($array))
                $st = false;
            
        }
        
        return $snum;
    }
    
    function generatePassword()
    {
        $snum = substr(pw_hash(date("hisY")),5,5).rand(1000,9999);
        return $snum;
    }
    
    function getProgramDetails($id)
    {
        /*
        return  $this->db
                     ->select("*")
                     ->from("tb_mas_programs")
                     //->group_by("intSubjectID")
                     ->where(array("intProgramID"=>$id))
                     ->join('tb_mas_users', 'tb_mas_users.intID = tb_mas_classlist_student.intStudentID')
                     ->order_by('strLastName','asc')
                     ->get()
                     ->result_array();
        */
    }
    
    function generateOR()
    {
        $st = true;
        $date = date("njy");
        while($st){
            $or = $date.rand(1000,9999);
            $array = $this->db->get_where('tb_mas_transactions',array('intORNumber'=>$or))->result_array();
            if(empty($array))
                $st = false;
            
        }
        
        return $or;
    }
    
    function getClassListStudents($id)
    {
        $faculty_id = $this->session->userdata("intID");
        return  $this->db
                     ->select("tb_mas_classlist_student.intCSID,intID, strFirstname,strMiddlename,strLastname,strStudentNumber, tb_mas_classlist_student.floatFinalGrade,enumStatus,strRemarks, strUnits,strProgramCode")
                     ->from("tb_mas_classlist_student")
                     //->group_by("intSubjectID")
                     ->where(array("intClassListID"=>$id))
                     ->join('tb_mas_users', 'tb_mas_users.intID = tb_mas_classlist_student.intStudentID')
                     ->join('tb_mas_programs','tb_mas_programs.intProgramID = tb_mas_users.intProgramID')
                     ->order_by('strLastName asc, strFirstname asc')
                     ->get()
                     ->result_array();
    }
    function getClassListStudentsSt($id,$classlist) 
    {
               
        return  $this->db
                     ->select("tb_mas_classlist_student.intCSID,strCode,strSection , intLab, tb_mas_subjects.strDescription,tb_mas_classlist_student.floatFinalGrade as v3,intFinalized,enumStatus,strRemarks,tb_mas_faculty.strFirstname,tb_mas_faculty.strLastname, tb_mas_subjects.strUnits, tb_mas_classlist.intID as classlistID, tb_mas_subjects.intID as subjectID")
                     ->from("tb_mas_classlist_student")
            
                    ->where(array("intStudentID"=>$id,"strAcademicYear"=>$classlist))
                        
                        
                     ->join('tb_mas_classlist', 'tb_mas_classlist.intID = tb_mas_classlist_student.intClasslistID')
                     ->join('tb_mas_subjects', 'tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                     ->join('tb_mas_faculty', 'tb_mas_faculty.intID = tb_mas_classlist.intFacultyID')
                     ->order_by('strCode','asc')   
                     ->get()
                     ->result_array();
        
    }
    
    function getClasslistDetails($id)
    {
        $d = $this->db
             ->select('strSection,intLab,intSubjectID,intLectHours,intFacultyID')
             ->from('tb_mas_classlist')
             ->join('tb_mas_subjects','intSubjectID = tb_mas_subjects.intID')
             ->where('tb_mas_classlist.intID',$id)
             ->get()
             ->first_row();
        
        return $d;
    }
    
    function getAllClasslist($sem)
    {
        $d = $this->db
             ->select('tb_mas_classlist.*,tb_mas_subjects.strCode')
             ->from('tb_mas_classlist')
             ->join('tb_mas_subjects','intSubjectID = tb_mas_subjects.intID')
             ->where('tb_mas_classlist.strAcademicYear',$sem)
             ->get()
             ->result_array();
        
        return $d;
    }
    
    
    function getScheduleByCode($schedCode)
    {
       $ret = array();
       $sched = $this->db
                        ->select('intRoomSchedID,strDay, dteStart, dteEnd, strRoomCode,strSection,strCode')
                        ->from('tb_mas_room_schedule')
                        ->where(array('strScheduleCode'=>$schedCode))
                        ->join('tb_mas_classrooms','tb_mas_room_schedule.intRoomID = tb_mas_classrooms.intID')
                        ->join('tb_mas_classlist','tb_mas_classlist.intID = tb_mas_room_schedule.strScheduleCode')
                        ->join('tb_mas_subjects','tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                        ->get()
                        ->result_array();
       foreach($sched as $s)
        {
            $s['strDay'] = get_day($s['strDay']);
            $ret[] = $s;
        }
        
        return $ret;
        
        
        
    }
    
    function getScheduleByRoomID($id,$sem)
    {
         $ret = array();
       $sched = $this->db
                        ->select('tb_mas_room_schedule.*,strSection,strCode')
                        ->from('tb_mas_room_schedule')
                        ->where(array('intRoomID'=>$id,'tb_mas_room_schedule.intSem'=>$sem))
                        ->join('tb_mas_classrooms','tb_mas_room_schedule.intRoomID = tb_mas_classrooms.intID')
                        ->join('tb_mas_classlist','tb_mas_classlist.intID = tb_mas_room_schedule.strScheduleCode')
                        ->join('tb_mas_subjects','tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                        ->order_by('strDay ASC, dteStart ASC')
                        ->get()
                        ->result_array();
       foreach($sched as $s)
        {
            $s['strDay'] = get_day($s['strDay']);
            $ret[] = $s;
        }
        
        return $ret;
    }
    
    function getSchedule($id)
    {
        
       $sched = current($this->db
                        ->select('tb_mas_room_schedule.*,strSection,strCode')
                        ->from('tb_mas_room_schedule')
                        ->where(array('intRoomSchedID'=>$id))
                        ->join('tb_mas_classrooms','tb_mas_room_schedule.intRoomID = tb_mas_classrooms.intID')
                        ->join('tb_mas_classlist','tb_mas_classlist.intID = tb_mas_room_schedule.strScheduleCode')
                        ->join('tb_mas_subjects','tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                        ->get()
                        ->result_array());
         
        $sched['strDay'] = get_day($sched['strDay']);
        return $sched;
        
        
        
    }
    
    function get_subjects_by_course($curriculum)
    {
       $subjects = $this->db
                         ->select( 'tb_mas_subjects.intID,tb_mas_curriculum_subject.intYearLevel,tb_mas_curriculum_subject.intSem,tb_mas_subjects.strCode, tb_mas_subjects.strDescription')
                         ->from('tb_mas_subjects')
                         ->join('tb_mas_curriculum_subject','tb_mas_curriculum_subject.intSubjectID = tb_mas_subjects.intID')
                         ->where('tb_mas_curriculum_subject.intCurriculumID',$curriculum)
                         ->order_by('intYearLevel asc,intSem asc')
                         ->get()
                         ->result_array();
        
        return $subjects;
    }
    
    function get_curriculum_by_course($course)
    {
        $query = "SELECT * from tb_mas_curriculum WHERE intProgramID = ".$course;
        
        return $this->db
                    ->query($query)
                    ->result_array();
    }
   
    function fetch_classlist($id)
    {
        $faculty_id = $this->session->userdata("intID");
        return  $this->db
                     ->select("tb_mas_classlist.intID as intID, strSection, intFacultyID,intSubjectID, strClassName,strCode,intFinalized,strAcademicYear,enumSem,strYearStart,strYearEnd, tb_mas_classlist.strUnits as strUnits")
                     ->from("tb_mas_classlist")
                     //->group_by("intSubjectID")
                     ->where(array("intFacultyID"=>$faculty_id,"tb_mas_classlist.intID"=>$id))
                     ->join('tb_mas_subjects', 'tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                     ->join('tb_mas_sy', 'tb_mas_sy.intID = tb_mas_classlist.strAcademicYear')
                     ->get()
                     ->result_array();
    }
    
    function fetch_classlist_delete($id)
    {
        return  $this->db
                     ->select("intFinalized,intFacultyID")
                     ->from("tb_mas_classlist")
                     //->group_by("intSubjectID")
                     ->where(array("tb_mas_classlist.intID"=>$id))
                     ->get()
                     ->result_array();
    }
    
  
    /*
    newly added function - 10/23/2015 -
        fetching classlists by faculty
    */
    function fetch_classlist_by_faculty($id, $classlist)
    {
        $faculty_id = $id;
                    $this->db
                     ->select("tb_mas_classlist.intID as intID, intFacultyID,intSubjectID, strClassName,strCode,strDescription, strSection, intFinalized,strAcademicYear,enumSem,strYearStart,strYearEnd, tb_mas_subjects.strUnits")
                     ->from("tb_mas_classlist")
                     ->where(array("intFacultyID"=>$faculty_id, "strAcademicYear"=>$classlist));

                     $this->db->join('tb_mas_subjects', 'tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                     ->join('tb_mas_sy', 'tb_mas_sy.intID = tb_mas_classlist.strAcademicYear');
                    
                 return $this->db 
                        ->get()
                        ->result_array();
    }
    
    function fetch_classlist_by_sujects($id,$sem=null)
    {
        $subject_id = $id;
                    $this->db
                     ->select("tb_mas_classlist.intID as intID, intFacultyID,intSubjectID,strClassName,strCode,strSection,intFinalized,strAcademicYear,enumSem,strYearStart,strYearEnd, tb_mas_faculty.strLastname, tb_mas_faculty.strFirstname")
                     ->from("tb_mas_classlist")
                     ->where(array("intSubjectID"=>$subject_id,'strAcademicYear'=>$sem));
//                    if($sem)
//                        $this->db->where(array('enumStatus'=>'active'));
//                    
//                    if($sem_sel!=null)
//                        $this->db->where(array('strAcademicYear'=>$sem_sel));
                     $this->db->join('tb_mas_subjects', 'tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                     ->join('tb_mas_sy', 'tb_mas_sy.intID = tb_mas_classlist.strAcademicYear')
                     ->join('tb_mas_faculty', 'tb_mas_faculty.intID = tb_mas_classlist.intFacultyID')
                    ->order_by('strClassName','asc');
                   
//                if($limit != null)
//                    $this->db->limit($limit);
                 return $this->db 
                        ->get()
                        ->result_array();
    }
     /**********************************************
     newly added function -11-04-2014 ^______^
    *********************************************/
    function getTotalUnits($id) 
    {
                    return $this->db->select_sum('tb_mas_classlist_student.strUnits')
                     ->from("tb_mas_classlist_student")
                     ->where(array("intStudentID"=>$id))
                     ->join('tb_mas_classlist', 'tb_mas_classlist.intID = tb_mas_classlist_student.intClasslistID')
                     ->join('tb_mas_subjects', 'tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                     ->join('tb_mas_faculty', 'tb_mas_faculty.intID = tb_mas_classlist.intFacultyID')
                     ->get()
                     ->result_array();

    }

    function fetch_classlist_all($id)
    {
        //$faculty_id = $this->session->userdata("intID");
        return  $this->db
                     ->select("tb_mas_classlist.intID as intID, intFacultyID,intSubjectID,strClassName,strCode,intFinalized,strAcademicYear,enumSem,strYearStart,strYearEnd")
                     ->from("tb_mas_classlist")
                     //->group_by("intSubjectID")
                     ->where(array("tb_mas_classlist.intID"=>$id))
                     ->join('tb_mas_subjects', 'tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                     ->join('tb_mas_sy', 'tb_mas_sy.intID = tb_mas_classlist.strAcademicYear')
                     ->get();
                    
    }
    
    function fetch_classlist_section($subject,$section)
    {
        //$faculty_id = $this->session->userdata("intID");
        return  current($this->db
                     ->select("tb_mas_classlist.intID as intID, intFacultyID,intSubjectID,strClassName,strCode,intFinalized,strAcademicYear,enumSem,strYearStart,strYearEnd")
                     ->from("tb_mas_classlist")
                     //->group_by("intSubjectID")
                     ->where(array("tb_mas_classlist.strClassName"=>$subject,"tb_mas_classlist.intSubjectID"=>$section))
                     ->join('tb_mas_subjects', 'tb_mas_subjects.intID = tb_mas_classlist.intSubjectID')
                     ->join('tb_mas_sy', 'tb_mas_sy.intID = tb_mas_classlist.strAcademicYear')
                     ->get()
                     ->result_array()
                       );
                    
    }
    
    //Room Conflict
    function schedule_conflict($post,$id=null,$sem,$d=null)
    {
        $query ="SELECT intRoomSchedID,strCode,strSection
                FROM tb_mas_room_schedule
                JOIN tb_mas_classlist ON tb_mas_classlist.intID = tb_mas_room_schedule.strScheduleCode
                JOIN tb_mas_subjects ON tb_mas_classlist.intSubjectID = tb_mas_subjects.intID
                WHERE
                (
                (dteStart >= '".$post['dteStart']."' AND dteEnd <= '".$post['dteEnd']."') OR
                (dteStart < '".$post['dteEnd']."' AND dteEnd >= '".$post['dteEnd']."') OR 
                (dteStart <= '".$post['dteStart']."' AND dteEnd > '".$post['dteStart']."') 
                )";
        if($id!=null)
        {
            $query .= " AND intRoomSchedID != ".$id;
        }
        
        if($d == null)
            $query .=" AND strDay = '".$post['strDay']."' AND intRoomID = '".$post['intRoomID']."' AND tb_mas_room_schedule.intSem = ".$sem."
                ";
        else
        {
            $query .=" AND ( ";
            for($i=0;$i<count($d);$i++)
            {
                if($i == count($d)-1)
                    $query .="strDay = ".$d[$i]." ) ";
                else
                    $query .="strDay = ".$d[$i]." OR ";
            }
            
            $query .= "AND intRoomID = '".$post['intRoomID']."' AND tb_mas_room_schedule.intSem = ".$sem." ";
        }
        
        // echo $query."<br />";
        //print_r($this->db->query($query)->result_array());
        //die();
        return $this->db->query($query)->result_array();
    }
    
    function section_conflict($post,$id=null,$section,$sem,$d=null)
    {
        $query ="SELECT intRoomSchedID,strCode,strSection
                FROM tb_mas_room_schedule
                JOIN tb_mas_classlist ON tb_mas_classlist.intID = tb_mas_room_schedule.strScheduleCode
                JOIN tb_mas_subjects ON tb_mas_classlist.intSubjectID = tb_mas_subjects.intID
                WHERE
                (
                (dteStart >= '".$post['dteStart']."' AND dteEnd <= '".$post['dteEnd']."') OR
                (dteStart < '".$post['dteEnd']."' AND dteEnd >= '".$post['dteEnd']."') OR 
                (dteStart <= '".$post['dteStart']."' AND dteEnd > '".$post['dteStart']."')
                )";
        if($id!=null)
        {
            $query .= " AND intRoomSchedID != ".$id;
        }
        if($d == null)
            $query .=" AND strDay = ".$post['strDay']." AND strSection = '".$section."' AND tb_mas_room_schedule.intSem = ".$sem;
        else
        {
            $query .=" AND ( ";
            for($i=0;$i<count($d);$i++)
            {
                if($i == count($d)-1)
                    $query .="strDay = ".$d[$i]." ) ";
                else
                    $query .="strDay = ".$d[$i]." OR ";
            }
            $query .= "AND strSection = '".$section."' AND tb_mas_room_schedule.intSem = ".$sem;
        }
        // echo $query."<br />";
        //print_r($this->db->query($query)->result_array());
        //die();
        return $this->db->query($query)->result_array();
    }
    
    function faculty_conflict($post,$id=null,$faculty_id,$sem,$d=null)
    {
        $query ="SELECT intRoomSchedID,strCode,strSection
                FROM tb_mas_room_schedule
                JOIN tb_mas_classlist ON tb_mas_classlist.intID = tb_mas_room_schedule.strScheduleCode
                JOIN tb_mas_subjects ON tb_mas_classlist.intSubjectID = tb_mas_subjects.intID
                WHERE
                (
                (dteStart >= '".$post['dteStart']."' AND dteEnd <= '".$post['dteEnd']."') OR
                (dteStart < '".$post['dteEnd']."' AND dteEnd >= '".$post['dteEnd']."') OR 
                (dteStart <= '".$post['dteStart']."' AND dteEnd > '".$post['dteStart']."')
                )";
        if($id!=null)
        {
            $query .= " AND intRoomSchedID != ".$id;
        }
        if($d == null)
            $query .=" AND strDay = '".$post['strDay']."' AND `intFacultyID` = ".$faculty_id." AND tb_mas_room_schedule.intSem = ".$sem." ";
        else
        {
            $query .=" AND ( ";
            for($i=0;$i<count($d);$i++)
            {
                if($i == count($d)-1)
                    $query .="strDay = ".$d[$i]." ) ";
                else
                    $query .="strDay = ".$d[$i]." OR ";
            }
            
            $query .= "AND `intFacultyID` = ".$faculty_id." AND tb_mas_room_schedule.intSem = ".$sem." ";
        }
        // echo $query."<br />";
        //print_r($this->db->query($query)->result_array());
        //die();
        return $this->db->query($query)->result_array();
    }
}