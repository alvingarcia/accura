<?php

/* Form Configuration */

$config['test_types'] = 
         array(
            'consultation'=>'consultation',
            'imaging'=>'imaging',
            'hematology'=>'hematology',
            'microscopy'=>'microscopy',
            'chemistry'=>'chemistry',
            'chemistry panels'=>'chemistry panels',
            '24 hour urine test'=>'24 hour urine test',
            'endocrine test'=>'endocrine test',
            'drug assay'=>'drug assay',
            'drug test'=>'drug test',
            'immuno serology'=>'immuno serology',
            'tumor markers'=>'tumor markers',
            'hepatitis markers'=>'hepatitis markers',
            'bacteriology'=>'bacteriology'
          );

$config['roles'] = 
         array(
            'admin'=>'Admin',
            'lcc_admin'=>'Collection Center Admin',
            'doctor'=>'Doctor',
            'extraction_admin'=>'Extraction Admin',
            'secretary'=>'Secretary',
            'company hr'=>'Company HR',
            'clinic staff'=>'Clinic Staff',
            'receptionist'=>'Receptionist',
            'nurse'=>'Nurse'
          );

$config['cstatus'] = 
         array(
            'active'=>'active',
            'inactive'=>'inactive'
          );

$config['si_units'] =
    array(
            'mmol/L'=>'mmol/L'
        );

$config['conv_units'] =
    array(
            'mg/dL'=>'mg/dL'
        );






/* Form Configuration */

?>