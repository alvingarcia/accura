<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doctor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');		
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");	
		$this->load->model("user_model");
        $this->config->load('courses');
        $this->data['user'] = $this->session->all_userdata();
        $this->data['page'] = "adminusers";
        
        if($this->logged_in())
            $this->data['isDoctor'] = $this->data_fetcher->isDoctor($this->data['user']['id']);
    }
    
    public function doctor_calendar_admin($id)
    {
        if($this->is_admin())
        {
            $this->data['doctor_id'] = $id;
            $this->data['page'] = "doctor_calendar";
            $this->data['opentree'] = "doctor";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/doctor_calendar",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("common/doctor_calendar_conf",$this->data); 
        }
    }
    
    public function doctor_calendar_json($id)
    {
        $post = $this->input->post();
        
        $events = $this->data_fetcher->getDoctorCalendar($post['start'],$post['end'],$id);
        
        $ret = array();
        
        foreach($events as $e)
        {
            //do{
                $temp['start'] = $e['appointmentDate']." ".$e['appointmentTime'];
                $temp['end'] = $e['appointmentDate']." ".$e['appointmentTime'];
                $temp['title'] = $e['patientLastname'].", ".$e['patientFirstname'];
                $ret[] = $temp;
                /*$temp['description'] = $e['strSecondary'];
                $temp['url'] = base_url()."home/schedule/".pw_hash($e['intID']).'/'.simplify_title($e['strTitle']);
                $ret[] = $temp;*/
                //$e['dteStart'] = date ("Y-m-d H:i:s", strtotime("+1 day", strtotime($e['dteStart'])));
            /*}
            while(strtotime($e['dteStart']) <= strtotime($e['dteEnd']));*/
        }
        
        echo json_encode($ret);
        
    
    }
    public function add_doctor()
    {
        if($this->is_admin())
        {
            $users = array();
            
            $use = $this->data_fetcher->getUsersNotInDoctor();
            foreach($use as $u)
            {
                $users[$u['id']] = $u['lastname']." ".$u['firstname']; 
            }
          
            $this->data['users'] = $users;
            
            $this->data['page'] = "add_doctor";
            $this->data['opentree'] = "doctor";
            $this->load->view("common/header",$this->data);
            if(empty($use))
                $this->load->view("admin/add_doctor_full",$this->data);
            else
                $this->load->view("admin/add_doctor",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("doctor_validation_js",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function submit_schedule()
    {
        $post = $this->input->post();
        $this->data_poster->post_data('doctor_schedule',$post);
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        redirect(base_url()."doctor/edit_doctor/".$post['dsDoctorID']);
    }
    
    public function edit_doctor($id)
    {
        
        if($this->is_admin())
        {
           
            $this->data['proc'] = $this->data_fetcher->getProcedureNotSelectedDoctor($id);
            $this->data['proc_selected'] = $this->data_fetcher->getProcedureSelectedDoctor($id);
            $this->data['id'] = $id;
            $this->data['doctor_schedule'] = $this->data_fetcher->getDoctorSchedule($id);
            $this->data['opentree'] = "doctor";
            $this->data['item'] = $this->data_fetcher->getItem('doctors',$id,'doctorId');
            $this->data['profile'] = $this->data_fetcher->getItem('user',$this->data['item']['userid'],'id');
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/edit_doctor",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("doctor_validation_js",$this->data); 
            $this->load->view("edit_doctor_conf",$this->data); 
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url());    
        
        
    }
    
    public function submit_procedures()
    {
        $post = $this->input->post();
        $did = $post['doctorid'];
        //print_r($post);
        $this->data_poster->deleteItems('doctor_procedure',array('doctorid'=>$did));
        if(isset($post['procedures']))
        {
            //check per subject

            foreach($post['procedures'] as $proc)
            {
                    
                $data_subject['procedureid'] = $proc;
                $data_subject['doctorid'] = $did;
                $this->data_poster->post_data('doctor_procedure',$data_subject);  

            }
        }
        
        $data['message'] = "Success";
        
        echo json_encode($data);
    }
    
   
    
    public function submit_doctor()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            //print_r($post);
           // $this->data_poster->log_action('User','Added a new User: '.$post['firstname']." ".$post['lastname'],'aqua');
            $this->data_poster->post_data('doctors',$post);
        }
        redirect(base_url()."doctor/view_all_doctors");
            
    }
    
    public function edit_submit_doctor()
    {
        $post = $this->input->post();
        //print_r($post);
        $this->data_poster->post_data('doctors',$post,$post['doctorId'],'doctorId');
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        
        redirect(base_url()."doctor/edit_doctor/".$post['doctorId']);
            
    }
    
   
    public function view_all_doctors()
    {
        if($this->is_admin())
        {
            $this->data['page'] = "view_all_doctors";
            $this->data['opentree'] = "doctor";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/doctor_view",$this->data);
            $this->load->view("common/footer_datatables",$this->data); 
            $this->load->view("common/doctor_conf",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url());  
    }
    
    
   function generate_patient_id()
    {
        
        $post = $this->input->post();
        $data['patientID'] = $this->data_fetcher->generatePatientID($post['year']);
        
        echo json_encode($data);   
    }
    
    
    public function delete_doctor()
    {
        $data['message'] = "failed";
        
        if($this->is_admin()){
            $post = $this->input->post();
                $this->data_poster->deleteItem('doctors',$post['id'],'doctorId');
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            
        }
        echo json_encode($data);
    }
    
    public function delete_schedule()
    {
        $data['message'] = "failed";
        
        if($this->is_admin()){
            $post = $this->input->post();
                $this->data_poster->deleteItem('doctor_schedule',$post['id'],'doctorScheduleID');
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            
        }
        echo json_encode($data);
    }
    
    public function patient_viewer($id, $sem = null)
    {
        if($this->is_admin())
        { 

            $this->load->view("common/header",$this->data);
            $this->load->view("admin/faculty_viewer",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("common/faculty_viewer_conf",$this->data); 
        }
        else
            redirect(base_url()); 
    }
    
    
    
    public function logged_in()
    {
        if($this->session->userdata('admin_logged'))
            return true;
        else
            return false;
    }
    
    public function is_admin()
    {
        if($this->session->userdata('roles')=="admin")
            return true;
        else
            return false;
    }


}