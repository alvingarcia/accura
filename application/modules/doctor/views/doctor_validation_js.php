<script type="text/javascript">
    $(document).ready(function(){
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='name']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='name']").html("<i class='fa fa-times-circle-o'></i> name field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='name']").html("name*");
            }
        
        });
        
        $("#validate-faculty").submit(function(e){
            
            
            if($("input[name='name']").val() == ""){
                $("input[name='name']").parent().addClass('has-error');
                $("label[for='name']").html("<i class='fa fa-times-circle-o'></i> name field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='name']").parent().removeClass('has-error');
                $("label[for='name']").html("name*");
            }
            
            
            return;
        });
        
        
        $("#generate-doctor-id").click(function(){
           
            $.ajax({
                'url':'<?php echo base_url(); ?>doctor/generate_doctor_id',
                'method':'post',
                'data':{'year':$(this).attr("rel")},
                'dataType':'json',
                'success':function(ret){
                    $("#patientid").val(ret.patientID);
                }
            
            });
        });
    });
</script>