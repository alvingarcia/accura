<aside class="right-side">
<section class="content-header">
                    <h1>
                        Doctor/Staff
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Doctor/Staff</a></li>
                        <li class="active">Edit Doctor Doctor/Staff</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Edit Doctor/Staff</h3>
        </div>
       
            
            
                 <div class="box-body">
                     <h4><?php echo $profile['lastname'].", ".$profile['firstname']; ?></h4>
                     <hr />
                     <div class="row">
                         <form id="validate-faculty" action="<?php echo base_url(); ?>doctor/edit_submit_doctor" method="post" role="form">
                <?php 
                    echo cms_hidden('doctorId',$item['doctorId']);
                    echo cms_input('prcid','PRC ID','Enter ID','col-sm-6',$item['prcid']);
                     echo cms_dropdown('qualification','Role',array("doctor"=>"Doctor","med tech"=>"Med Tech","extraction admin"=>"Extraction Admin","laboratory admin"=>"Laboratory Admin"),'col-xs-6',$item['qualification']); 
                    echo cms_input('specialization','Specialization','Enter Specialization','col-sm-6',$item['specialization']);
                    
                    ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="update" class="btn btn-default  btn-flat">
                </div>
                         </form>
                </div>
                <hr />
                <div class="row">
                <form id="validate-faculty" action="<?php echo base_url(); ?>doctor/submit_schedule" method="post" role="form">
                     <?php
                    echo cms_hidden('dsDoctorID',$item['doctorId']);
                     echo cms_dropdown('dsDay','Day',array("Mon"=>"Monday","Tue"=>"Tuesday","Wed"=>"Wednesday","Thu"=>"Thursday","Fri"=>"Friday","Sat"=>"Saturday","Sun"=>"Sunday"),'col-sm-4'); 
                     
                        $time = array(  
                                        "07:00:00"=>"07:00AM",
                                        "07:30:00"=>"07:30AM",
                                        "08:00:00"=>"08:00AM",
                                        "08:30:00"=>"08:30AM",
                                        "09:00:00"=>"09:00AM",
                                        "09:30:00"=>"09:30AM",
                                        "10:00:00"=>"10:00AM",
                                        "10:30:00"=>"10:30AM",
                                        "11:00:00"=>"11:00AM",
                                        "11:30:00"=>"11:30AM",
                                        "12:00:00"=>"12:00NN",
                                        "12:00:00"=>"12:30PM",
                                        "13:00:00"=>"1:00PM",
                                        "13:30:00"=>"1:30PM",
                                        "14:00:00"=>"2:00PM",
                                        "14:30:00"=>"2:30PM",
                                        "15:00:00"=>"3:00PM",
                                        "15:30:00"=>"3:30PM",
                                        "16:00:00"=>"4:00PM",
                                        "16:30:00"=>"4:30PM",
                                        "17:00:00"=>"5:00PM",
                                        "17:30:00"=>"5:30PM",
                                        "18:00:00"=>"6:00PM",
                                        "18:30:00"=>"6:30PM",
                                        "19:00:00"=>"7:00PM",
                                     );
                         echo cms_dropdown('dsStart','Start Time',$time,'col-sm-3'); 
                        echo cms_dropdown('dsEnd','End Time',$time,'col-sm-3'); 
                     ?>
                     <div class="col-sm-2">
                            <label>&nbsp;</label><br />
                            <input type="submit" class="btn btn-primary" value="add" />
                        </div>
                     </form>
                </div>
                <hr />
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Day</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($doctor_schedule as $sched): ?>
                            <tr>
                                <th><?php echo $sched['dsDay']; ?></th>
                                <th><?php echo date("h:i a",strtotime($sched['dsStart'])); ?></th>
                                <th><?php echo date("h:i a",strtotime($sched['dsEnd'])); ?></th>
                                <th><a href="#" class="delete-schedule" rel="<?php echo $sched['doctorScheduleID']; ?>">Delete</a></th>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                    <hr />
                <div class="row">
                <div class="col-md-5">
                    <h4>Tests</h4>
                    <select style="height:300px" class="form-control" id="branches-selector" multiple>
                        <?php foreach($proc as $l): ?>
                        <option value="<?php echo $l['procedureId']; ?>"><?php echo $l['procedureName']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-2">
                    <br /><br />
                    <a href="#" id="load-branches" class="btn btn-default  btn-flat btn-block">Load <i class="ion ion-arrow-right-c"></i> </a>
                    <a href="#" id="unload-branches" class="btn btn-default  btn-flat btn-block"><i class="ion ion-arrow-left-c"></i> Remove</a>
                    <a href="#" id="save-branches" class="btn btn-default  btn-flat btn-block">Save</a>

                </div>
                <div class="col-md-5">
                    <h4>Tests Performed</h4>
                    <select style="height:300px" class="form-control" id="branches-selected" multiple>
                       <?php foreach($proc_selected as $l): ?>
                        <option value="<?php echo $l['procedureId']; ?>"><?php echo $l['procedureName']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                </div>
            
       
        </div>
</aside>