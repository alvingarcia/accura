<aside class="right-side">
<section class="content-header">
                    <h1>
                        Doctor
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Doctor</a></li>
                        <li class="active">Add Doctor</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New Doctor/Medical Staff</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>doctor/submit_doctor" method="post" role="form">
                 <div class="box-body row">
                <?php  
                    echo cms_dropdown('userid','Select User',$users,'col-xs-6'); 
                    echo cms_input('prcid','PRC ID','Enter ID','col-sm-6');
                    echo cms_dropdown('qualification','Role',array("doctor"=>"Doctor","med tech"=>"Med Tech","extraction admin"=>"Extraction Admin","laboratory admin"=>"Laboratory Admin"),'col-xs-6'); 
                    echo cms_input('specialization','Specialization','Enter Specialization','col-sm-6');
                    echo cms_input('schedule','Schedule','Schedule ex. MWF 1:00PM-4:00PM','col-sm-6');
                    ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="add" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>