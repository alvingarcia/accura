<aside class="right-side">
<section class="content-header">
                    <h1>
                        Doctor
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Doctor</a></li>
                        <li class="active">Add Doctor</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New Doctor</h3>
        </div>
       
        <div class="box-body">
            <h3>Can't add new doctor.</h3>
            <p>First add a <a href="<?php echo base_url(); ?>adminusers/add_user">new user</a></p>
       </div>
    </div>
</aside>