<aside class="right-side">
<section class="content-header">
                    <h1>
                        Doctor
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Doctor</a></li>
                        <li class="active">Doctor's Calendar</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Doctor's Calendar</h3>
        </div>
        <div class="box-body">
            <div id="calendar">
                
                </div>
        </div>
            
        </div>
</aside>