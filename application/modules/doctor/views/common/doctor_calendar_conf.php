<script src="<?php echo $js_dir; ?>moment.js"></script>
<script src="<?php echo $js_dir; ?>full_calendar.js"></script>

<script>

$(document).ready(function(){
    
    /*$.ajax({
    url: '<?php echo base_url(); ?>homev3/academic_calendar_json',
    dataType: 'json',
        success: function(response) {
            //just example
            $('#calendar').fullCalendar({
                refetchResourcesOnNavigate: true,
                events: response.events
            });    
        }    
    });*/
    
    $('#calendar').fullCalendar({
        defaultView: 'listWeek',
        header:{
        left:   'title',
        center: '',
        right:  'month listWeek today prev,next'
        },
        eventSources: [

            // your event source
            {
              url: '<?php echo base_url(); ?>doctor/doctor_calendar_json/<?php echo $doctor_id; ?>',
              type: 'POST',
              error: function() {
                alert('there was an error while fetching events!');
              },
              color: '#26ade3',   // a non-ajax option
              textColor: 'black' // a non-ajax option
            }

            // any other sources...

        ]

    });
});
    
    
    
    
</script>