<script type="text/javascript">
    $(document).ready(function(){
        
        $("#save-branches").click(function(e){
            e.preventDefault();
            var procedures = Array();
            $('#branches-selected option').each(function(i, selected){ 
                procedures[i] = $(selected).val();
            });
            
            
            data = {'procedures':procedures,'doctorid':<?php echo $id; ?>};
            
            
            $.ajax({
                'url':'<?php echo base_url(); ?>doctor/submit_procedures/',
                'method':'post',
                'data':data,
                'dataType':'json',
                'success':function(ret){
                   alert("saved"); 
                }
            });
            
        });
        
        $("#load-branches").click(function(e){ 
             e.preventDefault();
                $('#branches-selector :selected').each(function(i, selected){ 
                    itemVal = $(selected).val(); 
                    itemText = $(selected).text();
                    $("#branches-selected").append($('<option>', { 
                                    value: itemVal,
                                    text : itemText
                                }));
                        $("#branches-selector option[value='"+itemVal+"']").remove();
                    
                });
                
                
        });
        
        $("#unload-branches").click(function(e){ 
             e.preventDefault();
                $('#branches-selected :selected').each(function(i, selected){ 
                    itemVal = $(selected).val(); 
                    itemText = $(selected).text();
                    $("#branches-selector").prepend($('<option>', { 
                                    value: itemVal,
                                    text : itemText
                                }));
                        $("#branches-selected option[value='"+itemVal+"']").remove();
                    
                });
                
                
        });
    
        $(".delete-schedule").click(function(e){
            e.preventDefault();
            conf = confirm("Are you sure you want to delete?");
            if(conf)
            {
                $(".loading-img").show();
                $(".overlay").show();
                var id = $(this).attr('rel');
                var parent = $(this).parent().parent();
                data = {'id':id};
                $.ajax({
                        'url':'<?php echo base_url(); ?>index.php/doctor/delete_schedule',
                        'method':'post',
                        'data':data,
                        'dataType':'json',
                        'success':function(ret){
                            if(ret.message == "failed"){
                               alert("failed");
                            }
                            else
                                parent.hide();

                            $(".loading-img").hide();
                            $(".overlay").hide();
                    }
                });
            }
        });
        
        
    });
    
</script>