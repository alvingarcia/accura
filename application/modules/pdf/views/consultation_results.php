<table>
    <tbody>
        <tr>
            <td width="30%" style="font-size:7px;line-height:10px;"></td>
            <td width="40%" style="font-size:10px;line-height:10px;text-align:center"><img src="<?php echo $img_dir; ?>ax-logo.png" /></td>
            <td width="30%">&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
            <td style="font-size:10px;line-height:15px;text-align:center"></td>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tr style="line-height:12px;font-size:9px;">
        <td colspan="2" style="text-align:right;"><?php echo date("M j, Y",strtotime($test['appointment']['appointmentDate'])); ?></td>
    </tr>
    <tr>
        <td style="font-size:7px;line-height:20px;">&nbsp;</td>
    </tr>
    <tr style="line-height:12px;font-size:9px;">
        <td style="width:15%;">Name:</td>
        <td style="width:40%;"><?php echo $patient['patientLastname'].", ".$patient['patientFirstname']." ".$patient['patientMiddlename']; ?></td>
    </tr>
    <!--tr style="line-height:12px;font-size:9px;">
        <td style="width:15%;">Nickname:</td>
        <td style="width:40%;"><?php echo $patient['alias']; ?></td>
    </tr-->
    <tr style="line-height:12px;font-size:9px;">
        <td style="width:15%;">Birthdate: </td>
        <td style="width:12%;"><?php echo date("M j,Y",strtotime($patient['birthday'])); ?></td>
    </tr>
    <!--tr style="line-height:12px;font-size:9px;">
        <td style="width:15%;">Source:</td>
        <td style="width:45%;"><?php echo isset($lcc['laboratoryBranch'])?$lcc['laboratoryBranch']:'Main'; ?></td>
    </tr-->
</table>
<table>
    <tbody>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
            <td style="font-size:10px;line-height:15px;text-align:center"></td>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
            <td style="font-size:10px;line-height:15px;text-align:center"></td>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
<table style="font-size:.7em;">
<?php
        $presc = $test['rx'];
?>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
            <td style="font-size:10px;line-height:15px;text-align:center"></td>
        </tr>
        <tr>
            <td width="40%" style="font-size:20px;"><strong>Rx:</strong></td>
        </tr>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
            <td style="font-size:10px;line-height:15px;text-align:center"></td>
        </tr>
        <?php 
        $i = 1;
        foreach($presc as $pres): ?>
        <tr>
            <td width="70%"><?php echo $i.". ".$pres['prescription']; ?></td>
        </tr>
        <?php 
        $i++;
        endforeach; 
        ?>
</table>
<table width="100%">
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size:10px" width="100%">Assessment:</td>
        </tr>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
        </tr>
        <tr>
            
            <td style="font-size:10px"><?php echo $test['results']['findings']; ?></td>
        </tr>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size:10px">Recommended Tests:</td>
        </tr>
        <tr>
            <td style="font-size:10px;"><?php echo $test['results']['testRecommendations']; ?></td>
        </tr>
        
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
        </tr>
</table>
<table>
        <tr>
            <td style="font-size:7px;line-height:30px;">&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size:7px;"><strong>THIS IS CONSIDERED A LEGAL PRESCRIPTION IF SIGNED AND STAMPED BY THE PHYSICIAN.</strong></td>
        </tr>
        <tr>
            <td style="font-size:7px;">
You may reach the clinic for concerns, through the following numbers: Clinic Landline/Telefax: +632 625 0100; Globe Mobile Phone: +63 917 813 9724;
Mobile Landline +632 904 1675; Smart Mobile Phone: +63 929 217 5011; Sun Mobile Phone +63 932 612 8777 or you may directly SMS me at +63 917
813 9724 or call me via my Mobile Landline at +632 505 8607. You may also email me via jvbornillamd@gmail.com. I would be more than willing to
accommodate your concerns.</td>
        </tr>
        <tr>
            <td style="font-size:7px;line-height:40px;">&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size:7px;">Thank you.</td>
        </tr>
</table>
<table>
    <tr>
        <td style="font-size:7px;line-height:50px;">&nbsp;</td>
    </tr>
    <tr>
        <td style="font-size:10px;line-height:15px;"><strong><?php echo $test['results']['firstname']." ".$test['results']['lastname']; ?>, M.D.</strong></td>
    </tr>
    <tr>
        <td style="font-size:10px;line-height:15px;">License No. <?php echo $test['results']['prcid']; ?></td>
    </tr>
    <tr>
        <td style="font-size:10px;line-height:15px;"><i><?php echo $test['results']['specialization']; ?></i></td>
    </tr>
    <tr>
        <td style="font-size:10px;line-height:15px;"><strong>AccuraXpress Diagnostics</strong></td>
    </tr>
    <tr>
        <td style="font-size:10px;line-height:15px;"><i>Address Goes Here</i></td>
    </tr>
    <tr>
        <td style="font-size:10px;line-height:15px;"><i>Contact Number Goes Here</i></td>
    </tr>
    <tr>
        <td style="font-size:10px;line-height:15px;"><i>info@accuraxpress.com</i></td>
    </tr>
    <tr>
        <td style="font-size:10px;line-height:15px;"><i>www.accuraxpress.com</i></td>
    </tr>
</table>