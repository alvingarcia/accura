<table>
    <tbody>
        <tr>
            <td width="30%" style="font-size:7px;line-height:10px;"></td>
            <td width="40%" style="font-size:10px;line-height:10px;text-align:center"><img src="<?php echo $img_dir; ?>ax-logo.png" /></td>
            <td width="30%">&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
            <td style="font-size:10px;line-height:15px;text-align:center"></td>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tr style="line-height:12px;font-size:9px;">
        <td style="width:10%;">Patient ID:</td>
        <td style="width:40%;"><?php echo $patient['patientid']; ?></td>
        <td style="width:10%;">Trans ID:</td>
        <td style="width:40%"><?php echo $appointment['appointmentId']; ?></td>
    </tr>
    <tr style="line-height:12px;font-size:9px;">
        <td style="width:10%;">Patient Name:</td>
        <td style="width:40%;"><?php echo $patient['patientLastname'].", ".$patient['patientFirstname']." ".$patient['patientMiddlename']; ?></td>
        <td style="width:10%;">Trans Date:</td>
        <td style="width:40%"><?php echo $appointment['appointmentDate']; ?></td>
        
    </tr>
    <tr style="line-height:12px;font-size:9px;">
        <td style="width:15%;">Date of Birth: </td>
        <td style="width:12%;"><?php echo date("M j,Y",strtotime($patient['birthday'])); ?></td>
        <!--td style="width:5%;">Age:</td>
        <td style="width:5%;"><?php echo $patient['age']; ?></td-->
        <td style="width:5%;">Sex: </td>
        <td style="width:13%;"><?php echo $patient['sex']; ?></td>
        <!--td style="width:15%;">Collection Date:</td>
        <td style="width:30%;"><?php // echo date("M j, Y",strtotime($appointment['appointmentTestTimeFinished']))." ".date("h:i a",strtotime($appointment['appointmentTime'])); ?></td-->
    </tr>
    <!--tr style="line-height:12px;font-size:9px;">
        <td style="width:15%;">Source:</td>
        <td style="width:45%;"><?php echo isset($lcc['laboratoryBranch'])?$lcc['laboratoryBranch']:'Main'; ?></td>
    </tr-->
</table>
<table>
    <tbody>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
            <td style="font-size:10px;line-height:15px;text-align:center"></td>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
<table style="font-size:.6em;">
    <tr style="line-height:15px;">
        <td style="width:40%;border-bottom:1px dashed #d2d2d2;border-top:1px dashed #d2d2d2;">TEST</td>
        <td style="width:5%;border-bottom:1px dashed #d2d2d2;border-top:1px dashed #d2d2d2;"></td>
        <td style="width:15%;border-bottom:1px dashed #d2d2d2;border-top:1px dashed #d2d2d2;">Result<br /><span style="font-size:.9em">Reference Interval</span></td>
        <td style="width:10%;border-bottom:1px dashed #d2d2d2;border-top:1px dashed #d2d2d2;">(SI Unit)</td>
        <td style="width:5%;border-bottom:1px dashed #d2d2d2;border-top:1px dashed #d2d2d2;"></td>
        <td style="width:15%;border-bottom:1px dashed #d2d2d2;border-top:1px dashed #d2d2d2;">Result<br /><span style="font-size:.9em">Reference Interval</span></td>
        <td style="width:10%;border-bottom:1px dashed #d2d2d2;border-top:1px dashed #d2d2d2;">(Conv. Unit)</td>
    </tr>
</table>
<table>
    <tbody>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
            <td style="font-size:10px;line-height:15px;text-align:center"></td>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
<table style="font-size:.7em;">
<?php
foreach($tests as $test): 
    ?>
        <tr>
            <td colspan="6" style="line-height:30px;"><?php echo $test['procedure']['procedureName']; ?><br />
            <span style="font-size:.7em">
            <?php  echo ($test['procedure']['resultComments']!="")?$test['procedure']['resultComments']:''; ?>
            </span>
            </td>
        </tr> 
    <?php $i=1; foreach($test['results'] as $results): 
    
        $last = (count($test['results']) == $i)?true:false;
    
        if($results['flag'] == "H" || $results['flag'] == "CH")
        {
            $icon = "&uarr;";
        }
        elseif($results['flag'] == "L" || $results['flag'] == "CL")
        {
            $icon = "&darr;";
        }
        else
        {
            $icon = "";
        }
        $i++; 
    ?>
        <tr style="line-height:15px;">
                <td style="width:40%;font-size:.9em;<?php echo $last?'border-bottom:1px solid #d2d2d2':''; ?>;"><?php echo $results['rname']; ?><br />
            
            </td>
            <td style="width:5%;<?php echo $last?'border-bottom:1px solid #d2d2d2':''; ?>;"><span style="<?php echo $style; ?>"><?php echo $results['flag']; ?> <?php echo $icon ?></span></td>
                <td style="width:15%;font-size:.8em;<?php echo $last?'border-bottom:1px solid #d2d2d2':''; ?>;"> <?php echo $results['rvaluesi']; ?><br />
                   <?php  echo ($results['rr_si']!="")?"[".$results['rr_si']."]":''; ?></td>
                <td style="width:10%;<?php echo $last?'border-bottom:1px solid #d2d2d2':''; ?>;"><?php echo $results['si_unit']; ?></td>
                <td style="width:5%;<?php echo $last?'border-bottom:1px solid #d2d2d2':''; ?>;"><span style="<?php echo $style; ?>"><?php echo $results['flag']; ?> <?php echo $icon ?></span></td>
                <td style="width:15%;font-size:.8em;<?php echo $last?'border-bottom:1px solid #d2d2d2':''; ?>;"><?php  echo ($results['rvalueconv']!="")?$results['rvalueconv']:''; ?><br />
                    <?php  echo ($results['rr_conv']!="")?"[".$results['rr_conv']."]":''; ?></td>
                <td style="width:10%;<?php echo $last?'border-bottom:1px solid #d2d2d2':''; ?>;"><?php echo $results['conv_unit']; ?></td>
            </tr>
    
        <?php 
            endforeach;
        ?>


<?php endforeach; ?>
</table>