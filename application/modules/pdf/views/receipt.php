<table>
    <tbody>
        <tr>
            <td width="30%" style="font-size:7px;line-height:10px;"></td>
            <td width="40%" style="font-size:10px;line-height:10px;text-align:center"><img src="<?php echo $img_dir; ?>ax-logo.png" /></td>
            <td width="30%">&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
            <td style="font-size:10px;line-height:15px;text-align:center"></td>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td style="font-size:7px;line-height:10px;border-top:1px solid #333;">&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tr style="line-height:12px;font-size:9px;">
            <td style="width:15%;">Patient ID:</td>
            <td style="width:40%;"><?php echo $patient['patientid']; ?></td>
            <td style="width:15%;">Order Number:</td>
            <td style="width:30%;"><?php echo sprintf('%08d',$appointment['appointmentId']); ?></td>
        </tr>
        <tr style="line-height:12px;font-size:9px;">
            <td style="width:15%;">Patient Name:</td>
            <td style="width:40%;"><?php echo $patient['patientLastname'].", ".$patient['patientFirstname']." ".$patient['patientMiddlename']; ?></td>
            <td style="width:15%;">Order Date:</td>
            <td style="width:30%;"><?php echo date("M j, Y",strtotime($appointment['appointmentDate']))." ".date("h:i a",strtotime($appointment['appointmentTime'])); ?></td>
        </tr>
        <!--tr style="line-height:12px;font-size:9px;">
            <td style="width:15%;">Source:</td>
            <td style="width:45%;"><?php echo isset($lcc['laboratoryBranch'])?$lcc['laboratoryBranch']:'Main'; ?></td>
        </tr-->
</table>
<table>
    <tbody>
        <tr>
            <td style="font-size:7px;line-height:20px;">&nbsp;</td>
        </tr>
    </tbody>
</table>
<table cellpadding="1" border="1">
    <tr style="line-height:12px;font-size:9px;">
        <td style="width:70%;text-align:center;">Test</td>
        <td style="width:30%;text-align:center;">Price</td>
    </tr>
    <?php 
    $total = 0;
    foreach($tests as $test): 
    $total += $proc[$test['appointmentTestId']]['price'];
    ?>
    <tr style="line-height:12px;font-size:9px;">
        <td><?php echo $test['procedureName']; ?></td>
        <td>P <?php echo $proc[$test['appointmentTestId']]['price']; ?></td>
    </tr>
    <?php endforeach; ?>
    <tr style="line-height:12px;font-size:9px;">
        <td style="text-align:right;"><strong>TOTAL</strong></td>
        <td><strong>P <?php echo $total; ?></strong></td>
    </tr>
</table>