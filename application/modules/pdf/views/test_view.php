<table>
    <tbody>
        <tr>
            <td width="20%" style="font-size:7px;line-height:10px;"></td>
            <td width="60%" style="font-size:10px;line-height:10px;text-align:center"><!--Company Name--><br />TEST</td>
            <td width="20%">&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td style="font-size:7px;line-height:10px;">&nbsp;</td>
            <td style="font-size:10px;line-height:15px;text-align:center"></td>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr style="line-height:25px;">
            <td style="font-size:10px;"></td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr style="line-height:25px;">
            <td style="font-size:9px;"><?php echo $patient['patientLastname'].", ".$patient['patientFirstname']; ?></td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr style="line-height:25px;">
            <td style="font-size:9px;"><?php echo date("M j, Y",strtotime($appointment['appointmentDate'])); ?></td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr style="line-height:25px;">
            <td style="font-size:10px;"></td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr style="line-height:25px;">
            <td style="font-size:10px;">Procedure: <?php echo $procedure['procedureName']; ?></td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr style="line-height:25px;">
            <td style="font-size:10px;"></td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr style="line-height:25px;">
            <td style="font-size:10px;"><?php echo $procedure['procedureDescription']; ?></td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr style="line-height:25px;">
            <td style="font-size:10px;"></td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr style="line-height:35px;">
            <td style="font-size:9px;text-align:center;">
                <?php echo $generated_barcode; ?>
            </td>
        </tr>
    </tbody>
</table>
