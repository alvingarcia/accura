<tr style="<?php echo ($sal['isHoliday'] || $sal['intpaidLeave'])?'background:#33dd33':''; ?>">
    <td><?php echo $sal['date']; ?></td>
    <?php if($sal['isHoliday']): ?>
    <td colspan="6"><?php echo $sal['isHoliday']; ?></td>
    <?php elseif($sal['intpaidLeave']): ?>
        <td colspan="5">Paid Leave</td>
        <td>P<?php echo $sal['salary']; ?></td>
    <?php else: ?>
    <td><?php echo $sal['timeIn']; ?></td>
    <td><?php echo $sal['timeOut']; ?></td>
    <td style="<?php echo($sal['late']!=0)?'background:#dd7777;color:#fff':'' ?>"><?php echo $sal['late']; ?></td>
    <td style="<?php echo($sal['undertime']!=0)?'background:#dd5555;color:#fff':''; ?>"><?php echo $sal['undertime']; ?></td>
    <td><?php echo $sal['total_hours']; ?></td>
    <td>P<?php echo $sal['salary']; ?></td>
    <?php endif; ?>
</tr>