<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include('src/BarcodeGenerator.php');
include('src/BarcodeGeneratorPNG.php');
include('src/BarcodeGeneratorSVG.php');
include('src/BarcodeGeneratorJPG.php');
include('src/BarcodeGeneratorHTML.php');


class Pdf extends CI_Controller {

	public function __construct()
	{
		
        parent::__construct();
		
        $this->config->load('themes');		
		$theme = $this->config->item('cms');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        
        $this->data["user"] = $this->session->all_userdata();
        $this->load->helper('pdf');
        
		
	}
    
    function print_results($id)
    {
        $post = $this->input->post();
       
        $ret = array();
        $appointments = array();
        
        foreach($post['test'] as $t){
            $test = $this->data_fetcher->getItem('appointment_test',$t,'appointmentTestId');
            $proc = $this->data_fetcher->getItem('procedures',$test['procedureid'],'procedureId');
            $results = $this->data_fetcher->getItems('test_results',array('appointmentTestIdfk'=>$test['appointmentTestId']));
            $test['procedure'] = $proc;
            $test['results'] = $results;
            
            $ret[$test['aid']][] = $test;
        }
        

        //$this->data['appointment'] = $this->data_fetcher->getItem('appointment',$post[''],'appointmentId');

        $this->data['patient'] = $this->data_fetcher->getItem('patient',$id,'id');
        //$this->data['laboratory'] = $this->data_fetcher->getItem('laboratories',$this->data['appointment']['laboratoryid'],'laboratoryId');
        
        $birthDate = date('m/d/Y',strtotime($this->data['patient']['birthday']));
        //explode the date to get month, day and year
        $birthDate = explode("/", $birthDate);
        
        //$this->data['patient']['age'] = calculate_age($birthDate,$appointmentDate)
        
        //print_r($this->data['spouse']);
        tcpdf();
        // create new PDF document
        //$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //$pdf = new TCPDF("P", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
       // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array('A4'), true, 'UTF-8', false, true);
        //$pdf = new TCPDF("P", PDF_UNIT, array(8.5, 13), false, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle("Test Results");
        
        // set margins
        $pdf->SetMargins('6', '7', '6');
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //$pdf->SetAutoPageBreak(TRUE, 6);

       //font setting
        //$pdf->SetFont('calibril_0', '', 15, '', 'false');
        // set default font subsetting mode
        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        
        $pdf->SetFont('dejavusans', '', 15,'','false');
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_FOOTER);
        
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);
        // Add a page
        // This method has several options, check the source code documentation for more information.
        foreach($ret as $r){
            $this->data['appointment'] = $this->data_fetcher->getItem('appointment',$r[0]['aid'],'appointmentId');
            $this->data['tests'] = $r;
            $pdf->AddPage();
            $html = $this->load->view('test_results',$this->data,true);
            //$html = $pdf->unhtmlentities($html);
            $pdf->writeHTML($html, true, false, true, false, '');
        }
       
        $pdf->Output("test-results.pdf", 'I');
    }
    
    function print_consultation($id)
    {
        $post = $this->input->post();
       
        
        $ret = array();
        
        $this->data['patient'] = $this->data_fetcher->getItem('patient',$id,'id');
        //$this->data['laboratory'] = $this->data_fetcher->getItem('laboratories',$this->data['appointment']['laboratoryid'],'laboratoryId');
        
        $birthDate = date('m/d/Y',strtotime($this->data['patient']['birthday']));
        //explode the date to get month, day and year
        //$birthDate = explode("/", $birthDate);
       

        //$this->data['appointment'] = $this->data_fetcher->getItem('appointment',$post[''],'appointmentId');

        
        
        
        
        //print_r($this->data['spouse']);
        tcpdf();
        // create new PDF document
        //$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //$pdf = new TCPDF("P", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
       // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array('A4'), true, 'UTF-8', false, true);
        //$pdf = new TCPDF("P", PDF_UNIT, array(8.5, 13), false, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle("Test Results");
        
        // set margins
        $pdf->SetMargins('6', '7', '6');
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //$pdf->SetAutoPageBreak(TRUE, 6);

       //font setting
        //$pdf->SetFont('calibril_0', '', 15, '', 'false');
        // set default font subsetting mode
        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        
        $pdf->SetFont('dejavusans', '', 15,'','false');
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_FOOTER);
        
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
         
        foreach($post['test'] as $t){
            $test = $this->data_fetcher->getItem('appointment_test',$t,'appointmentTestId');
            $proc = $this->data_fetcher->getItem('procedures',$test['procedureid'],'procedureId');
            $results = $this->data_fetcher->getTestConsultationById($t);
            $test['rx'] = $this->data_fetcher->getItems('rx',array('tcId'=>$results['testConsultationId']));
            $test['procedure'] = $proc;
            $test['results'] = $results;
            $test['atest'] = $test;
            $test['appointment'] = $this->data_fetcher->getItem('appointment',$test['aid'],'appointmentId');
            $test['age'] = calculate_age($birthDate,$test['appointment']['appointmentDate']);
            $this->data['test'] = $test;
            
            $pdf->AddPage();
        
            $html = $this->load->view('consultation_results',$this->data,true);
            //$html = $pdf->unhtmlentities($html);
        
            $pdf->writeHTML($html, true, false, true, false, '');
            
        }
        
       
        $pdf->Output("test-results.pdf", 'I');
    }
    
    function prescription($id)
    {
       
        $ret = array();
        $test = $this->data_fetcher->getTestConsultationById($id);
        
       

        $this->data['presc'] = explode(';',$test['perscription']);

        $this->data['appointment'] = $this->data_fetcher->getItem('appointment',$id,'appointmentId');

        $this->data['patient'] = $this->data_fetcher->getItem('patient',$this->data['appointment']['pid'],'id');
        $this->data['laboratory'] = $this->data_fetcher->getItem('laboratories',$this->data['appointment']['laboratoryid'],'laboratoryId');
        
        $birthDate = date('m/d/Y',strtotime($this->data['patient']['birthday']));
        //explode the date to get month, day and year
        $birthDate = explode("/", $birthDate);
        
        $this->data['patient']['age'] = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
    ? ((date("Y") - $birthDate[2]) - 1)
    : (date("Y") - $birthDate[2]));
        
        //print_r($this->data['spouse']);
        tcpdf();
        // create new PDF document
        //$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //$pdf = new TCPDF("P", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
       // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT,'A5', true, 'UTF-8', false, true);
        //$pdf = new TCPDF("P", PDF_UNIT, array(215.9,279.4), false, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle("Prescription");
        
        // set margins
        $pdf->SetMargins('6', '7', '6');
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //$pdf->SetAutoPageBreak(TRUE, 6);

       //font setting
        //$pdf->SetFont('calibril_0', '', 15, '', 'false');
        // set default font subsetting mode
        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        
        $pdf->SetFont('dejavusans', '', 15,'','false');
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_FOOTER);
        
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        // Add a page
        // This method has several options, check the source code documentation for more information.
        
        $pdf->AddPage();
        
        $html = $this->load->view('prescription',$this->data,true);
        //$html = $pdf->unhtmlentities($html);
        
        $pdf->writeHTML($html, true, false, true, false, '');
        
       
        $pdf->Output("prescription-print.pdf", 'I');
    }
    
    function print_receipt($id)
    {
       
        $this->data['appointment'] = $this->data_fetcher->getItem('appointment',$id,'appointmentId');
            $ret_proc = array();
            $this->data['tests'] = $this->data_fetcher->getProceduresAppointment($id);
            
            foreach($this->data['tests'] as $t)
            {
                if($this->data['appointment']['laboratoryid'] == 0)
                {
                    $r = $this->data_fetcher->getItem('procedures',$t['procedureid'],'procedureId');
                    $r['price'] = $r['procedurePrice'];
                    $ret_proc[$t['appointmentTestId']] = $r;
                }
                     
                else{
                    $r = $this->data_fetcher->getProcedureJoinLab($t['procedureid'],$this->data['appointment']['laboratoryid']);
                    $r['price'] = $r['labCcPrice'];
                    $ret_proc[$t['appointmentTestId']] = $r; 
                }
            }
            
            $this->config->load('courses');		
            $this->data['types'] = $this->config->item('test_types');
            foreach($this->data['types'] as $type){
                $this->data['procedure_types'][] = $this->data_fetcher->getProceduresLabNotSelected($this->data['appointment']['laboratoryid'],$type,$id);
           }
            
            
            
            $this->data['proc'] = $ret_proc;
            
            $this->data['patient'] = $this->data_fetcher->getItem('patient',$this->data['appointment']['pid'],'id');
            $this->data['laboratory'] = $this->data_fetcher->getItem('laboratories',$this->data['appointment']['laboratoryid'],'laboratoryId');
        
        //print_r($this->data['spouse']);
        tcpdf();
        // create new PDF document
        //$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //$pdf = new TCPDF("P", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
       // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array('A4'), true, 'UTF-8', false, true);
        //$pdf = new TCPDF("P", PDF_UNIT, array(8.5, 13), false, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle("Test Results");
        
        // set margins
        $pdf->SetMargins('6', '7', '6');
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //$pdf->SetAutoPageBreak(TRUE, 6);

       //font setting
        //$pdf->SetFont('calibril_0', '', 15, '', 'false');
        // set default font subsetting mode
        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        
        $pdf->SetFont('helvetica', '', 15,'','false');
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_FOOTER);
        
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        // Add a page
        // This method has several options, check the source code documentation for more information.
        
        $pdf->AddPage();
        
        $html = $this->load->view('receipt',$this->data,true);
        //$html = $pdf->unhtmlentities($html);
        
        $pdf->writeHTML($html, true, false, true, false, '');
        
       
        $pdf->Output("transaction-receipt.pdf", 'I');
    }
    
    function test($id)
    {
        
        //print_r($this->data['spouse']);
        tcpdf();
        // create new PDF document
        //$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //$pdf = new TCPDF("P", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
       // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array('A4'), true, 'UTF-8', false, true);
        //$pdf = new TCPDF("P", PDF_UNIT, array(8.5, 13), false, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle("Test Form");
        
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //$pdf->SetAutoPageBreak(TRUE, 6);

       //font setting
        //$pdf->SetFont('calibril_0', '', 15, '', 'false');
        // set default font subsetting mode
        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        
        $pdf->SetFont('dejavusans', '', 15,'','false');
        $pdf->SetAutoPageBreak(false, PDF_MARGIN_FOOTER);
        
        $test = $this->data_fetcher->getItem('appointment_test',$id,'appointmentTestId');
        $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
        
        $this->data['code'] = $test['testRequestCode'];
        $this->data['test'] = $test;
        $this->data['appointment'] = $this->data_fetcher->getItem('appointment',$test['aid'],'appointmentId');
        $this->data['patient'] = $this->data_fetcher->getItem('patient',$this->data['appointment']['pid'],'id');
        $this->data['procedure'] = $this->data_fetcher->getItem('procedures',$test['procedureid'],'procedureId');
        
        $this->data['generated_barcode'] =  '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode('081231723897', $generator::TYPE_CODE_128)) . '">';
        
        
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        // Add a page
        // This method has several options, check the source code documentation for more information.
        
        $pdf->AddPage();
        
        $html = $this->load->view('test_view',$this->data,true);
        //$html = $pdf->unhtmlentities($html);
        
        $pdf->writeHTML($html, true, false, true, false, '');
        
       
        $pdf->Output("request-form.pdf", 'I');
    }
    
    
    


}