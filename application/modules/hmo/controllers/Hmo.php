<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hmo extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');		
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");	
		$this->load->model("user_model");
        $this->config->load('courses');
        $this->data['user'] = $this->session->all_userdata();
        $this->data['page'] = "adminusers";
    }
    
    
    public function add_hmo()
    {
        if($this->is_admin())
        {
            
            $this->data['page'] = "add_hmo";
            $this->data['opentree'] = "hmo";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/add_hmo",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("hmo_validation_js",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function edit_hmo($id)
    {
        
        if($this->is_admin())
        {
           
          
            $this->data['item'] = $this->data_fetcher->getItem('hmo',$id,'hmoId');
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/edit_hmo",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("hmo_validation_js",$this->data); 
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url());    
        
        
    }
    
   
    
    public function submit_hmo()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            //print_r($post);
           // $this->data_poster->log_action('User','Added a new User: '.$post['firstname']." ".$post['lastname'],'aqua');
            $this->data_poster->post_data('hmo',$post);
        }
        redirect(base_url()."hmo/view_all_hmos");
            
    }
    
    public function edit_submit_hmo()
    {
        $post = $this->input->post();
        //print_r($post);
        $this->data_poster->post_data('hmo',$post,$post['id'],'hmoId');
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        
        redirect(base_url()."patient/view_all_patients");
            
    }
    
   
    public function view_all_hmos()
    {
        if($this->is_admin())
        {
            $this->data['page'] = "view_all_hmos";
            $this->data['opentree'] = "hmo";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/hmo_view",$this->data);
            $this->load->view("common/footer_datatables",$this->data); 
            $this->load->view("common/hmo_conf",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url());  
    }
    
    
   function generate_patient_id()
    {
        
        $post = $this->input->post();
        $data['patientID'] = $this->data_fetcher->generatePatientID($post['year']);
        
        echo json_encode($data);   
    }
    
    
    public function delete_hmo()
    {
        $data['message'] = "failed";
        
        if($this->is_admin()){
            $post = $this->input->post();
               
            $items = $this->data_fetcher->fetch_table('patient_hmo',null,null,array('hmoid'=>$post['id']));
            
            if(empty($items))
            {
                 $this->data_poster->deleteItem('hmo',$post['id'],'hmoId');
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            }
            else
            {
                $data['message'] = "Cannot delete HMO, it is in use by patients";
            }
        }
        echo json_encode($data);
    }
    
    public function patient_viewer($id, $sem = null)
    {
        if($this->is_admin())
        { 

            $this->load->view("common/header",$this->data);
            $this->load->view("admin/faculty_viewer",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("common/faculty_viewer_conf",$this->data); 
        }
        else
            redirect(base_url()); 
    }
    
    
    
    public function logged_in()
    {
        if($this->session->userdata('admin_logged'))
            return true;
        else
            return false;
    }
    
    public function is_admin()
    {
        if($this->session->userdata('roles')=="admin")
            return true;
        else
            return false;
    }


}