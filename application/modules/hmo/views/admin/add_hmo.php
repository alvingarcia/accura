<aside class="right-side">
<section class="content-header">
                    <h1>
                        HMO
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> HMO</a></li>
                        <li class="active">Add HMO</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New HMO</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>hmo/submit_hmo" method="post" role="form">
                 <div class="box-body row">
                <?php  
                    echo cms_input('hmoName','HMO','Enter Name','col-sm-6');
                    echo cms_input('hmoCode','HMO Code','Enter Code','col-sm-6');
                    echo cms_input('insuranceProvider','Insurance Provider','Enter Insurance Provider','col-sm-6');
                    ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="add" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>