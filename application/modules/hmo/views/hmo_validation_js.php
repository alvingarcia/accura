<script type="text/javascript">
    $(document).ready(function(){
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='hmoName']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='hmoName']").html("<i class='fa fa-times-circle-o'></i> name field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='hmoName']").html("HMO*");
            }
        
        });
        
        $("input[name='hmoCode']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='hmoCode']").html("<i class='fa fa-times-circle-o'></i> code field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='hmoCode']").html("HMO Code*");
            }
        
        });
        
        $("#validate-faculty").submit(function(e){
            
            
            if($("input[name='hmoName']").val() == ""){
                $("input[name='hmoName']").parent().addClass('has-error');
                $("label[for='hmoName']").html("<i class='fa fa-times-circle-o'></i> name field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='hmoName']").parent().removeClass('has-error');
                $("label[for='hmoName']").html("HMO*");
            }
            if($("input[name='hmoCode']").val() == ""){
                $("input[name='hmoCode']").parent().addClass('has-error');
                $("label[for='hmoCode']").html("<i class='fa fa-times-circle-o'></i> code field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='hmoCode']").parent().removeClass('has-error');
                $("label[for='hmoCode']").html("HMO Code*");
            }
            
            
            return;
        });
        
        
       
    });
</script>