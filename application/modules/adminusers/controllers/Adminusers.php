<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminusers extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');
        $this->config->load('courses');
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");	
		$this->load->model("user_model");
        $this->config->load('courses');
        $this->data['user'] = $this->session->all_userdata();
        $this->data['page'] = "adminusers";
    }
    
    
    public function add_user()
    {
        if($this->is_admin())
        {
            $lb = array('0'=>'none');
            $lab = $this->data_fetcher->fetch_table('laboratories',array('laboratoryBranch','asc'));
            foreach($lab as $l)
            {
                $lb[$l['laboratoryId']] = $l['laboratoryBranch']; 
            }
            
            $c2 = $this->data_fetcher->getDropdown('companies','companyId',array('companyName'),array('companyName','asc'));
            $c2[0] = "n/a";
            $this->data['companies'] =$c2;
            
            $this->data['lcc'] = $lb;
            $this->data['roles'] = $this->config->item('roles');
            
            $this->data['page'] = "add_user";
            $this->data['opentree'] = "admin";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/add_user",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("user_validation_js",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function edit_user($id)
    {
        
        if($this->is_admin())
        {
            $lb = array('0'=>'none');
            $lab = $this->data_fetcher->fetch_table('laboratories',array('laboratoryBranch','asc'));
            foreach($lab as $l)
            {
                $lb[$l['laboratoryId']] = $l['laboratoryBranch']; 
            }
            
            $c2 = $this->data_fetcher->getDropdown('companies','companyId',array('companyName'),array('companyName','asc'));
            $c2[0] = "n/a";
            $this->data['companies'] =$c2;
            
            $this->data['roles'] = $this->config->item('roles');
            $this->data['lcc'] = $lb;
            $this->data['item'] = $this->data_fetcher->getUser($id);
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/edit_user",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("user_validation_js",$this->data); 
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url());    
        
        
    }
    
    public function submit_user()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            //print_r($post);
           // $this->data_poster->log_action('User','Added a new User: '.$post['firstname']." ".$post['lastname'],'aqua');
            $post['password'] = pw_hash($post['password'],'aqua');
            $this->data_poster->post_data('user',$post);
        }
        redirect(base_url()."adminusers/view_all_users");
            
    }
    
    public function edit_submit_user()
    {
        $post = $this->input->post();
        //print_r($post);
        
        $post['password'] = pw_hash($post['password']);
        $this->data_poster->post_data('user',$post,$post['id'],'id');
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        
        redirect(base_url()."adminusers/edit_user/".$post['id']);
            
    }
    
    public function view_all_users()
    {
        if($this->is_admin())
        {
            $this->data['page'] = "view_all_users";
            $this->data['opentree'] = "admin";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/user_view",$this->data);
            $this->load->view("common/footer_datatables",$this->data); 
            $this->load->view("common/user_conf",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url());  
    }
    
    
    public function get_online_users()
    {
        $data['users'] = $this->data_fetcher->getFacultyOnlineUsername();
        echo json_encode($data);
        
    }
    
    public function faculty_logged_in()
    {
        if($this->session->userdata('faculty_logged'))
            return true;
        else
            return false;
    }
    
    public function delete_user()
    {
        $data['message'] = "failed";
        
        if($this->is_admin()){
            $post = $this->input->post();
            if($this->session->userdata('id') != $post['id']){
                $this->data_poster->deleteUser($post['id']);
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            }
        }
        echo json_encode($data);
    }
    
    public function faculty_viewer($id, $sem = null)
    {
        if($this->is_admin())
        { 
           $this->data['sy'] = $this->data_fetcher->fetch_table('tb_mas_sy');
           //$this->data['active_sem'] = $this->data_fetcher->get_active_sem();
            $active_sem = $this->data_fetcher->get_active_sem();
                if($sem!=null)
                    $this->data['selected_ay'] = $sem;
                else
                    $this->data['selected_ay'] = $active_sem['intID'];

            $this->data['active_sem'] = $this->data_fetcher->get_sem_by_id($this->data['selected_ay']);
            $this->data['faculty'] = $this->data_fetcher->getFaculty($id);
            //$this->data['classlists'] = $this->data_fetcher->fetch_classlists_all(null,$this->data['selected_ay']);
            $records = $this->data_fetcher->fetch_classlist_by_faculty($id,$this->data['active_sem']['intID']);

            foreach($records as $record)
            {
                $record['schedule'] = $this->data_fetcher->getScheduleByCode($record['intID']);
                //print_r($record['schedule']);
                $this->data['classlist'][] = $record;
            }

            $this->data['total_units'] = $this->data_fetcher->getTotalUnits($id);

            $this->load->view("common/header",$this->data);
            $this->load->view("admin/faculty_viewer",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("common/faculty_viewer_conf",$this->data); 
        }
        else
            redirect(base_url()); 
    }
    
    public function edit_profile()
    {
        if($this->faculty_logged_in())
        {
           // print_r($this->data['records']);
            $this->data['faculty'] = $this->data_fetcher->getFaculty($this->session->userdata('intID'));
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/edit_profile",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("faculty_validation_js",$this->data); 
            
        }
        else
            redirect(base_url());    
    }
    
    public function my_profile($sem = null)
    {
        $this->data['page'] = "my profile";
        if($this->faculty_logged_in())
        {
           $id = $this->session->userdata('intID');
           $this->data['sy'] = $this->data_fetcher->fetch_table('tb_mas_sy');
           //$this->data['active_sem'] = $this->data_fetcher->get_active_sem();
            $active_sem = $this->data_fetcher->get_active_sem();
                if($sem!=null)
                    $this->data['selected_ay'] = $sem;
                else
                    $this->data['selected_ay'] = $active_sem['intID'];

            $this->data['active_sem'] = $this->data_fetcher->get_sem_by_id($this->data['selected_ay']);
            $this->data['faculty'] = $this->data_fetcher->getFaculty($id);
            //$this->data['classlists'] = $this->data_fetcher->fetch_classlists_all(null,$this->data['selected_ay']);
            $records = $this->data_fetcher->fetch_classlist_by_faculty($id,$this->data['active_sem']['intID']);

            foreach($records as $record)
            {
                $record['schedule'] = $this->data_fetcher->getScheduleByCode($record['intID']);
                //print_r($record['schedule']);
                $this->data['classlist'][] = $record;
            }

            $this->data['total_units'] = $this->data_fetcher->getTotalUnits($id);

            $this->load->view("common/header",$this->data);
            $this->load->view("admin/my_profile",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("common/faculty_viewer_conf",$this->data); 
            }
            else
                redirect(base_url());   
        
    }
    
    
    public function edit_submit_profile()
    {
        $post = $this->input->post();
        //print_r($post);
        $post['strPass'] = pw_hash($post['strPass']);
        $this->data_poster->post_data('tb_mas_faculty',$post,$post['intID']);

        $auth_data = $this->db->get_where('tb_mas_faculty', array('intID'=>$post['intID']), 1)->first_row();
        foreach($auth_data as $key=>$value)
        {
            $this->session->set_userdata($key,$value);
        }
        
        redirect(base_url().'faculty/my_profile');
            
    }
    
    public function logged_in()
    {
        if($this->session->userdata('admin_logged'))
            return true;
        else
            return false;
    }
    
    public function is_admin()
    {
        if($this->session->userdata('roles')=="admin")
            return true;
        else
            return false;
    }


}