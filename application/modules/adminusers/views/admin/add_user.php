<aside class="right-side">
<section class="content-header">
                    <h1>
                        User
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> User</a></li>
                        <li class="active">Add User</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New User</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>adminusers/submit_user" method="post" role="form">
                 <div class="box-body">
                     <div class="form-group col-xs-6">
                        <label for="firstname">First Name*</label>
                        <input type="text" name="firstname" class="form-control" id="firstname" placeholder="Enter First Name">
                    </div>
                    <div class="form-group col-xs-6">
                        <label for="lastname">Last Name*</label>
                        <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Enter Last Name">
                    </div>
                 
                <div class="form-group col-xs-6">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email Address">
                    </div>
                <div class="form-group col-xs-6">
                        <label for="username">Username</label>
                        <input type="text" name="username" class="form-control" id="username" placeholder="Enter Username">
                    </div>
                
                <div class="form-group col-xs-6">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password">
                    </div>
              
                
                <?php 
                     echo cms_dropdown('companyAssoc','Company',$companies,'col-xs-6');
                     echo cms_dropdown('roles','Roles',$roles,'col-xs-6');
                     echo cms_dropdown('laboratoryid','Select Collection Center (for LCC Admin)',$lcc,'col-xs-6');  ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="add" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>