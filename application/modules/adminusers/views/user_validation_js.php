<script type="text/javascript">
    $(document).ready(function(){
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='firstname']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='firstname']").html("<i class='fa fa-times-circle-o'></i> Firstname field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='firstname']").html("Firstname*");
            }
        
        });
        $("input[name='lastname']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='lastname']").html("<i class='fa fa-times-circle-o'></i> Lastname field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='lastname']").html("Lastname*");
            }
        });
        
        $("input[name='password']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='password']").html("<i class='fa fa-times-circle-o'></i> Password field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='password']").html("Password*");
            }
        });
        
        $("input[name='username']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='username']").html("<i class='fa fa-times-circle-o'></i> Username field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='username']").html("Username*");
            }
        });
        $("#validate-faculty").submit(function(e){
            
            
            if($("input[name='firstname']").val() == ""){
                $("input[name='firstname']").parent().addClass('has-error');
                $("label[for='firstname']").html("<i class='fa fa-times-circle-o'></i> Firstname field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='firstname']").parent().removeClass('has-error');
                $("label[for='firstname']").html("Firstname*");
            }
            if($("input[name='lastname']").val() == ""){
                $("input[name='lastname']").parent().addClass('has-error');
                 $("label[for='lastname']").html("<i class='fa fa-times-circle-o'></i> Lastname field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='lastname']").parent().removeClass('has-error');
                $("label[for='lastname']").html("Lastname*");
                
            }
            if($("input[name='username']").val() == ""){
                $("input[name='username']").parent().addClass('has-error');
                 $("label[for='username']").html("<i class='fa fa-times-circle-o'></i> Username field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='username']").parent().removeClass('has-error');
                $("label[for='username']").html("Username*");
                
            }
            
            if($("input[name='password']").val() == ""){
                $("input[name='password']").parent().addClass('has-error');
                 $("label[for='password']").html("<i class='fa fa-times-circle-o'></i> Password field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='password']").parent().removeClass('has-error');
                $("label[for='password']").html("Password*");
                
            }
            
            return;
        });
    });
</script>