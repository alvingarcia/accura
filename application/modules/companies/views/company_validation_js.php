<script type="text/javascript">
    $(document).ready(function(){
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='hmoName']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='companyName']").html("<i class='fa fa-times-circle-o'></i> name field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='companyName']").html("Name*");
            }
        
        });
        
        
        $("#validate-faculty").submit(function(e){
            
            
            if($("input[name='companyName']").val() == ""){
                $("input[name='companyName']").parent().addClass('has-error');
                $("label[for='companyName']").html("<i class='fa fa-times-circle-o'></i> name field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='companyName']").parent().removeClass('has-error');
                $("label[for='companyName']").html("Name*");
            }
            
            
            return;
        });
        
        
       
    });
</script>