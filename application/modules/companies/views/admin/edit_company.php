<aside class="right-side">
<section class="content-header">
                    <h1>
                        Companies
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Companies</a></li>
                        <li class="active">Edit Company</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Edit Company</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>companies/edit_submit_company" method="post" role="form">
                 <div class="box-body row">
                <?php  
                    echo cms_hidden('companyId',$item['companyId']);
                    echo cms_input('companyName','Name','Enter Name','col-sm-6',$item['companyName']);
                    echo cms_input('companyContactNumber','Contact Number','Enter Contact Number','col-sm-6',$item['companyContactNumber']);
                    echo cms_textarea('companyAddress','Address','Enter Address','col-sm-6',$item['companyAddress']);
                    echo cms_dropdown('companyStatus','Company Status',$cstatus,'col-xs-6',$item['companyStatus']);
                    ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="update" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>