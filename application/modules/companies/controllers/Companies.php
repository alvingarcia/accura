<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companies extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');		
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");	
		$this->load->model("user_model");
        $this->config->load('courses');
        $this->data['user'] = $this->session->all_userdata();
        $this->data['page'] = "adminusers";
    }
    
    
    public function add_company()
    {
        if($this->data_fetcher->is_specific(array(2)))
        {
            $this->data['cstatus'] = $this->config->item('cstatus');
            $this->data['page'] = "add_company";
            $this->data['opentree'] = "companies";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/add_company",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("company_validation_js",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function edit_company($id)
    {
        
        if($this->data_fetcher->is_specific(array(2)))
        {
           
            $this->data['cstatus'] = $this->config->item('cstatus');
            $this->data['item'] = $this->data_fetcher->getItem('companies',$id,'companyId');
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/edit_company",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("company_validation_js",$this->data); 
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url());    
        
        
    }
    
   
    
    public function submit_company()
    {
        if($this->data_fetcher->is_specific(array(2)))
        {
            $post = $this->input->post();
            //print_r($post);
           // $this->data_poster->log_action('User','Added a new User: '.$post['firstname']." ".$post['lastname'],'aqua');
            $this->data_poster->post_data('companies',$post);
        }
        redirect(base_url()."companies/edit_company/".$this->db->insert_id());
            
    }
    
    public function edit_submit_company()
    {
        $post = $this->input->post();
        //print_r($post);
        $this->data_poster->post_data('companies',$post,$post['companyId'],'companyId');
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        
        redirect(base_url()."companies/edit_company/".$post['companyId']);
            
    }
    
   
    public function view_all_companies()
    {
        if($this->data_fetcher->is_specific(array(2)))
        {
            $this->data['page'] = "view_all_companies";
            $this->data['opentree'] = "companies";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/company_view",$this->data);
            $this->load->view("common/footer_datatables",$this->data); 
            $this->load->view("common/company_conf",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url());  
    }
    
    
   function generate_patient_id()
    {
        
        $post = $this->input->post();
        $data['patientID'] = $this->data_fetcher->generatePatientID($post['year']);
        
        echo json_encode($data);   
    }
    
    
    public function delete_company()
    {
        $data['message'] = "failed";
        
        if($this->data_fetcher->is_specific(array(2)))
        {
            $post = $this->input->post();
               
            $items = $this->data_fetcher->fetch_table('patient',null,null,array('companyid'=>$post['id']));
            
            if(empty($items))
            {
                 $this->data_poster->deleteItem('companies',$post['id'],'companyId');
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            }
            else
            {
                $data['message'] = "Cannot delete Company, it is in use by patients";
            }
        }
        echo json_encode($data);
    }
    
    
    

}