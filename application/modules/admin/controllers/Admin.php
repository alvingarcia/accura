<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');		
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
       
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");
		$this->load->model("google_login");	
		$this->load->model("facebook_login");	
		$this->load->model("user_model");
        $this->config->load('courses');
        
        $this->data['user'] = $this->session->all_userdata();
        if($this->logged_in())
            $this->data['isDoctor'] = $this->data_fetcher->isDoctor($this->data['user']['id']);
        
        $this->data["page"] = "default";
        
	}
    
    public function index()
	{	
        
        
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7)))
            redirect(base_url()."admin/dashboard");

        elseif($this->data_fetcher->is_specific(array(5)))
            redirect(base_url()."admin/hr_dashboard");
        
        elseif($this->data_fetcher->is_specific(array(8)))
            redirect(base_url()."admin/nurse_dashboard");

        else
            redirect(base_url()."users/login");
        
        
    }
    
    public function add_content()
    {
        if($this->data_fetcher->is_specific(array(1,2,3)))
        {
            $this->data['page'] = "Add Content";
            $this->data['opentree'] = "content";
            $categories = $this->data_fetcher->fetch_table('tb_mas_category',array('strName','asc'));
            $cats = array();
            $cats[0] = "none";
            foreach($categories as $category)
            {
                $cats[$category['intID']] = $category['strName'];
            }
            $this->data['type_options'] = array('post'=>'Post','video'=>'Video','gallery'=>'Gallery');
            $this->data['category_items'] = $cats;
            $this->load->view("common/header",$this->data);
            $this->load->view("pages/add_content",$this->data);
            $this->load->view("common/footer",$this->data);
            $this->load->view("product_validation_js",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url());  
    }
    
    public function file_upload($table=null,$id=null,$field_name=null)
	{	
		if($this->logged_in())
		{
			$data['table'] = $table;
			$data['id'] = $id;
			$data['field_name'] = $field_name;
			$this->load->view('/file_uploader_head',$data);
			$this->load->view('/file_uploader',$data);
		}
		else
		{
			echo "system error";
		}
	
	}
    
    public function file_upload_gallery($table=null,$id=null,$field_name=null)
	{	
		if($this->logged_in())
		{
			$data['table'] = $table;
			$data['id'] = $id;
			$data['field_name'] = $field_name;
			$this->load->view('/file_uploader_head',$data);
			$this->load->view('/file_uploader_gallery',$data);
		}
		else
		{
			echo "system error";
		}
	
	}
    
    public function view_all_content()
    {
        if($this->logged_in())
        {
            $this->data['page'] = "View Content";
            $this->data['opentree'] = "content";
            
            $this->data['categories'] = $this->data_fetcher->fetch_table('tb_mas_category',array('strName','asc'));
            $this->load->view("common/header",$this->data);
            $this->load->view("pages/content_view",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("content_conf",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url()."cms");  
    }
    
    public function edit_content($id)
    {
        
        if($this->logged_in())
        {
          
            $this->data['item'] = $this->data_fetcher->getItem('tb_mas_content',$id,'intID');
            $categories = $this->data_fetcher->fetch_table('tb_mas_category',array('strName','asc'));
            $this->data['cat_vals'] = array();
            $cts = $this->data_fetcher->fetch_categories($id);
            foreach($cts as $c)
            {
                $this->data['cat_vals'][] = $c['intCategoryID'];
            }
            $cats = array();
            $cats[0] = "none";
            foreach($categories as $category)
            {
                $cats[$category['intID']] = $category['strName'];
            }  
            $this->data['type_options'] = array('post'=>'Post','video'=>'Video','gallery'=>'Gallery');
            
            $this->data['category_items'] = $cats;
            $this->data['gallery_images'] = $this->data_fetcher->fetch_gallery_images($id);
            $this->load->view("common/header",$this->data);
            $this->load->view("pages/edit_content",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("product_validation_js",$this->data); 
            $this->load->view("form_gallery_js",$this->data); 
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url()."cms");    
        
        
    }
    
    public function submit_content()
    {
        if($this->logged_in())
        {
            $post = $this->input->post();



            if(isset($post['intContentCategory']))
                $cats = $post['intContentCategory'];
            else
                $cats = array();

            $post['dteStart'] = date("Y-m-d H:i:s",strtotime($post['dteStart']));
            $post['dteEnd'] = date("Y-m-d H:i:s",strtotime($post['dteEnd']));

            unset($post['intContentCategory']);

            if(isset($post['intID']))
            {
                $this->data_poster->post_data('tb_mas_content',$post,$post['intID']);
                $id = $post['intID'];
            }
            else
            {
                $this->data_poster->post_data('tb_mas_content',$post);
                $id = $this->db->insert_id();
            }

            $this->data_poster->delete_from('tb_mas_content_category',array('intContentID'=>$id));

            foreach($cats as $cat)
            {
                $this->data_poster->post_data('tb_mas_content_category',array('intContentID'=>$id,'intCategoryID'=>$cat));
            }

            if(isset($post['intContentCategory']))
                $post['intContentCategory'];

            redirect(base_url()."admin/edit_content/".$id);
        }
        else
            redirect(base_url());  
            
    }
    
    public function deleteItem()
    {
        if($this->logged_in())
        {
        $post = $this->input->post();
        $table = $post['table'];
        $message = "";
        
        if($table =="tb_mas_admin" && ($this->session->userdata("intID") == $post['id']))
        {
            $deleted = false;
            $message = "Cannot delete while logged in";
        }
        elseif($table == "tb_mas_category")
        {
            $category = $this->data_fetcher->getItem('tb_mas_category',$post['id'],'intID');
            $cats = $this->db->get_where('tb_mas_content_category',array('intCategoryID'=>$post['id']))->result_array();
            
            $is_parent = $this->db->get_where('tb_mas_category',array('intParent'=>$post['id']))->result_array();
            
            //print_r($cats);
            if(!empty($cats))
            {
                $deleted = false;
                $message = "Can't delete while content is connected";
            }
            elseif($category['intLocked'])
            {
                $deleted = false;
                $message = "Can't delete locked categories";
            }
            elseif(!empty($is_parent))
            {
                $deleted = false;
                $message = "Delete parent and all child categories? <a href='".base_url()."cms/delete_category_all/".$post['id']."'>Delete</a>";
            }
            else
            {
                $deleted = $this->data_poster->deleteItem($table,$post['id']);
                
            }
                
        }
        else
            $deleted = $this->data_poster->deleteItem($table,$post['id']);
        
        }
        else
        {
            $deleted = "false";
             $message = "Must be logged in to delete";
        }
        if($deleted)
            $data['message'] = "success";
        elseif($message=="")
            $data['message'] = "failed";
        else
            $data['message'] = $message;
        
        echo json_encode($data);
        
    }
    
    public function hr_dashboard($sel=null)
    {
        if($this->data_fetcher->is_specific(array(2,5)))
        {
            $this->data["adm"] = $this->data_fetcher->getUserData($this->session->userdata("id"));
            $this->load->view('common/header',$this->data);
            $this->load->view('pages/hr_dashboard',$this->data);
            $this->load->view('common/footer',$this->data);
            $this->load->view('common/dconf_hr.php',$this->data);
        }
        else
            redirect(base_url()."admin");
    }
    
    public function get_test_reccomendations()
    {
        $ret = array();
        $all = 
        $this->db
             ->select('procedureName')
             ->group_by('procedureName')
             ->get('procedures')
             ->result_array();
        
        foreach($all as $a)
        {
            $r['value'] = $r['text'] = $a['procedureName'];
            $ret[] = $r;
        }
        
        $ret = json_encode($ret);
        
        $fp = fopen('tests.json', 'w');
        fwrite($fp, $ret);
        fclose($fp);
    }
    
    public function get_rx_templates()
    {
        $ret = array();
        $all = 
        $this->db
             ->select('rxId,templateName')
             ->get('rx_template')
             ->result_array();
        
        foreach($all as $a)
        {
            $r['value'] = $a['rxId'];
            $r['text'] = $a['templateName'];
            $ret[] = $r;
        }
        
        $ret = json_encode($ret);
        
        $fp = fopen('rx.json', 'w');
        fwrite($fp, $ret);
        fclose($fp);
    }
    
    public function nurse_dashboard($sel=null)
    {
        if($this->data_fetcher->is_specific(array(2,8)))
        {
            //$this->data["adm"] = $this->data_fetcher->getUserData($this->session->userdata("id"));
            $this->load->view('common/header',$this->data);
            $this->load->view('pages/nurse_dashboard',$this->data);
            $this->load->view('common/footer',$this->data);
            $this->load->view('common/dconf_hr.php',$this->data);
        }
        else
            redirect(base_url()."admin");
    }
    
    public function dashboard($sel=null)
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7)))
        {
            $ar = $this->data_fetcher->getAreas(); 
            $queue = array();
            for($i=0;$i<count($ar);$i++){
                $queue[$i]['name'] = $ar[$i]['areaName'];
                $queue[$i]['areaid'] = $ar[$i]['areaId'];
                $queue[$i]['procedures'] = $this->data_fetcher->getEstimatedTime($ar[$i]['areaId']);
                $queue[$i]['queue'] = $this->data_fetcher->getQueue($this->session->userdata('laboratoryid'),false,$ar[$i]['areaId']);
                 $queue[$i]['progress'] = $this->data_fetcher->getQueue($this->session->userdata('laboratoryid'),true,$ar[$i]['areaId']);
                $queue[$i]['processing'] = $this->data_fetcher->getProcessing($this->session->userdata('laboratoryid'),$ar[$i]['areaId']);
            }
            
            $q['queue'] = $this->data_fetcher->getQueueAll($this->session->userdata('laboratoryid'),false);
            $q['progress'] = $this->data_fetcher->getQueueAll($this->session->userdata('laboratoryid'),true);
            
            $this->data['areas'] = $queue;
            $this->data['p_data'] = $q;
            $this->data['sel'] = $sel;
            
            $this->load->view('common/header',$this->data);
            $this->load->view('pages/dashboard',$this->data);
            $this->load->view('common/footer',$this->data);
            $this->load->view('common/dconf.php',$this->data);
        }
        else
            redirect(base_url()."admin");
    }
    
    public function check_patient_doctor($docid,$num)
    {
        $cons = $this->data_fetcher->countConsultationDoctor($docid);
        if($cons > $num)
            $data["message"] = "yes";
        else
            $data["message"] = "no";
        
        echo json_encode($data);
    }
    
    public function doctor_consultation_page()
    {
        $doctor = $this->data_fetcher->getDoctorByUserId($this->session->userdata('id'));
        $this->data['cons'] = $this->data_fetcher->getQueueConsultationDoctor($doctor['doctorId']);
        $this->data['doctorId'] = $doctor['doctorId'];
        $this->load->view('common/header',$this->data);
        $this->load->view('pages/doctor_consultation',$this->data);
        $this->load->view('common/footer',$this->data);
        $this->load->view('common/doctor_consultation_conf',$this->data);
    }
    
    public function doctor_patient($id)
    {
        $doctor = $this->data_fetcher->getDoctorByUserId($this->session->userdata('id'));
        $this->data['templates'] = array("0"=>"blank");
        
        $this->data['test'] = $this->data_fetcher->getTestConsultationById($id);
        //$this->get_test_reccomendations(); //remove comment to update tests
        $this->get_rx_templates();
        
        $this->data['previousConsultations'] = $this->data_fetcher->getTestConsultationPatient($this->data['test']['pid'],$id);
        
        
        $this->data['allergies'] = $this->db
                ->get_where('patient_history',array('pHpid'=>$this->data['test']['pid'],'histType'=>'allergy'))
                ->result_array();
        
        $this->data['illnesses'] = $this->db
                ->get_where('patient_history',array('pHpid'=>$this->data['test']['pid'],'histType'=>'illness'))
                ->result_array();
        
        $this->data['doctorId'] = $doctor['doctorId'];
        $this->load->view('common/header',$this->data);
        $this->load->view('pages/doctor_patient',$this->data);
        $this->load->view('common/footer',$this->data);
        $this->load->view('common/doctor_patient_conf.php');
    }
    
    public function submit_consultation_result()
    {
        $post = $this->input->post();
        $atestid = $post['appointmentTestId'];
        $appointment_id = $post['aid'];
        for($i=0;$i<count($post['medicine']);$i++)
        {
            $prescription = $post['medicine'][$i]." ";
            if($post['dosage'][$i]!="")
                $prescription .= $post['dosage'][$i];
            if($post['unit'][$i]!="")
                $prescription .= $post['unit'][$i]." ";
            if($post['times'][$i]!="")
                $prescription .= $post['times'][$i]."X a day ";
            if($post['days'][$i]!="")
                $prescription .= "for ".$post['days'][$i]." days ";
            if($post['schedule'][$i]!="")
                $prescription .= $post['schedule'][$i];
            
            $d = array(
                        "tcId" =>$post['testConsultationId'],
                        "prescription" =>$prescription
                     );
            
             $this->data_poster->post_data('rx',$d);
        }
        unset($post['medicine']);
        unset($post['dosage']);
        unset($post['unit']);
        unset($post['times']);
        unset($post['days']);
        unset($post['schedule']);
        unset($post['appointmentTestId']);
        unset($post['aid']);
        $this->data_poster->post_data('test_consultation',$post,$post['testConsultationId'],'testConsultationId');
        $data['appointmentTestStatus'] = "consultation finished";
        $this->data_poster->post_data('appointment_test',$data,$atestid,'appointmentTestId');
       
        
        $this->data_poster->queueNextTest($appointment_id);
        redirect(base_url().'admin/doctor_consultation_page');
        
    }
    
    public function area($areaid,$section=1)
    {
        if($this->logged_in())
        {
            $ar = $this->data_fetcher->getArea($areaid); 
            $artype = $this->data_fetcher->getAreaType($ar['areaType']);
            
            switch($artype['testTypeName'])
            {
                case 'Laboratory':
                $section_names = array("Reception","Extraction","Laboratory");
                $page = 'area';
                break;
                case 'Imaging':
                $section_names = array("Reception","Imaging","Finished");
                $page = 'imaging';
                break;
                case 'Consultation':
                $this->data['doctors'] = $this->data_fetcher->getDoctorsDropdown();
                $section_names = array("Reception","Consulting","Finished");
                $page = 'consultation';
                break;
                default:
                $page = 'area';
                $section_names = array("Reception","Extraction","Laboratory");
            }
            
            $this->data['section_names'] = $section_names;
           
            $queue['name'] = $ar['areaName'];
            $queue['areaid'] = $ar['areaId'];
            $this->data['spidgen'] = $this->data_fetcher->generateSpecimenID();
            $queue['procedures'] = $this->data_fetcher->getEstimatedTime($ar['areaId']);
            $this->data['section'] = $section;
            $conf = 'dconf';
            switch($section){
                case 1:
                    if($artype['testTypeName'] == "Consultation")
                        $queue['queue'] = $this->data_fetcher->getQueueConsultation($this->session->userdata('laboratoryid'),false,$ar['areaId']);
                    else
            $queue['queue'] = $this->data_fetcher->getQueue($this->session->userdata('laboratoryid'),false,$ar['areaId']);
                $view_f = "common/area_ajax_none";
                break;
                case 2:
                if($artype['testTypeName'] == "Consultation"){
                    $queue['queue'] = $this->data_fetcher->getQueueConsultation($this->session->userdata('laboratoryid'),true,$ar['areaId']);
                    $conf = "dconf_cons";
                    $view_f = "common/area_ajax_none";
                }
                else{
                    $queue['queue'] = $this->data_fetcher->getQueue($this->session->userdata('laboratoryid'),true,$ar['areaId']);
                $view_f = "common/area_ajax";
                }
                break;
                case 3:
            $queue['queue'] = $this->data_fetcher->getProcessing($this->session->userdata('laboratoryid'),$ar['areaId']);
                $view_f = "common/area_ajax2";
                break;
                default:
            $queue['queue'] = $this->data_fetcher->getQueue($this->session->userdata('laboratoryid'),false,$ar['areaId']);
                $view_f = "common/area_ajax_none";
            }
            $this->data['item_count'] = count($queue['queue']);
            
            $this->data['area'] = $queue;
            $this->load->view('common/header',$this->data);
            $this->load->view('pages/'.$page,$this->data);
            $this->load->view('common/footer',$this->data);
            $this->load->view('common/'.$conf,$this->data);
            $this->load->view($view_f,$this->data);
        }
        else
            redirect(base_url()."admin");
    }
    
    public function serve_item()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            $id = $post['id'];
            $area = $post['area'];
            $this->data_poster->post_data_area($id,$area,'in progress'); 
            echo json_encode(array("success"=>"success"));
        }
        
    }
    
    public function serve_consultation()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            $id = $post['id'];
            $area = $post['area'];
            $atid = $post['atid'];
            $this->data_poster->post_data_area($id,$area,'in progress'); 
            $data['consultationDoctorId'] = $post['doctorid'];
            $data['consultationAtestId'] = $atid;
            $this->data_poster->post_data('test_consultation',$data);
            echo json_encode(array("success"=>"success"));
        }
    }
    
    public function getAvailableDoctorsAjax()
    {
        $post = $this->input->post();
        $item = $this->data_fetcher->getItem('appointment',$post['id'],'appointmentId');
        $items = $this->data_fetcher->getAvailableDoctorsDropdown($item['appointmentDate']." ".$item['appointmentTime']);
        $dropdown = cms_dropdown2('doctorid','Select Available Doctor',$items,'col-xs-12');
        echo json_encode(array("dropdown"=>$dropdown));
    }
    
    public function load_rx()
    {
        $post = $this->input->post();
        $templates = explode(",",$post['templates']);
        $html = "";
        if(!empty($templates)){
            foreach($templates as $t)
            {
                $data['item'] = $this->data_fetcher->getItem('rx_template',$t,'rxId');
                $html .= $this->load->view('pages/rx_template',$data,true);
            }
        }
        else
        {
           $html .= $this->load->view('pages/rx_template_blank',null,true);
        }
        
        echo json_encode(array("html"=>$html));
    }
    
    public function getSectionItemsAjax($areaid)
    {
        $items = $this->data_fetcher->getQueue($this->session->userdata('laboratoryid'),true,$areaid);
        //echo count($items);
        if(count($items) == 0){
            $id = $this->data_fetcher->get_next_queue($areaid,$this->session->userdata('laboratoryid'));
            $cons = $this->data_fetcher->getTestConsultationById($id);
            if(empty($cons)){
                $data['appointmentTestStatus'] = "in progress";
                $this->data_poster->post_data_area($id,$areaid,'in progress'); 
            }
            echo json_encode(array("item"=>$id));
        }
        else
            echo json_encode(array("item"=>0));
    }
    
    public function getSectionItemsAjaxLab($areaid)
    {
        $items = $this->data_fetcher->getProcessing($this->session->userdata('laboratoryid'),$areaid);
        echo json_encode(array("items"=>count($items)));
        
    }
    
    
    public function check_estimated()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            $id = $post['id'];
            $area = $post['area'];
            $procs = $this->data_fetcher->getEstimatedTimePatient($area,$id); 
            echo json_encode(array("procs"=>$procs));
        }
        
    }
    
    public function get_queue_unseen()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            $area = $post['area'];
            $lab = $this->session->userdata('laboratoryid');
            $data['queue'] = $this->data_fetcher->getQueue($lab,false,$area);
            

            echo json_encode($data);
        }
    }
    
    public function serve_finish()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            $id = $post['id'];
            $area = $post['area'];
            $data['appointmentTestStatus'] = "processing";
            
            //Add specimen ID then send to API
            if(isset($post['with_specimen']))
                $specimenid = $post['specimenid'];
            else
                $specimenid = 0;
            
            $this->data_poster->post_data_area($id,$area,'processing',$specimenid); 
            $this->data_poster->queueNextTest($id);
            
            $id = $this->data_fetcher->get_next_queue($area,$this->session->userdata('laboratoryid'));
            
            $data['appointmentTestStatus'] = "in progress";
            $this->data_poster->post_data_area($id,$area,'in progress'); 
            
            echo json_encode(array("success"=>"success"));
        }
        
    }
    
    public function next_item()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            $area = $post['area'];
            
            $id = $this->data_fetcher->get_next_queue($area,$this->session->userdata('laboratoryid'));
            
            $data['appointmentTestStatus'] = "in progress";
            $this->data_poster->post_data_area($id,$area,'in progress'); 
            
            echo json_encode(array("success"=>"success"));
        }
        
    }

    
    
    
    public function logged_in()
    {
        if($this->session->userdata('admin_logged'))
            return true;
        else
            return false;
    }
    
    public function is_admin()
    {
        if($this->session->userdata('roles')=="admin")
            return true;
        else
            return false;
    }
    
    
    
    
}