<script type="text/javascript">
    $(document).ready(function(){ 
        
        $('#selectDoctor').on('show.bs.modal', function (e) {
            var $invoker = $(e.relatedTarget);
            id = $invoker.attr('rel');
            data = {'id':id}
            $.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>admin/getAvailableDoctorsAjax/",
               data:data,
               dataType:'json',
               success: function(data){
                  $("#doctor-dropdown").html(data.dropdown);
                }

            });
        });
    });
</script>