<script type="text/javascript">
    $(document).ready(function(){
        
        $(".show-estimated").click(
            function(e){
                e.preventDefault();
                container = $(this).parent();
                elem = $(this);
                if(elem.attr('data-clicked') == 0)
                {
                    id = $(this).attr('rel');
                    area = $(this).attr('data-area');
                    data = {'id':id,'area':area};
                    $.ajax({
                        'url':'<?php echo base_url(); ?>index.php/admin/check_estimated',
                        'method':'post',
                        'data':data,
                        'dataType':'json',
                        'success':function(ret){
                            elem.attr('data-clicked',1);
                            txt ="<table class='table'>";
                            for(i in ret.procs)
                            {
                                txt+="<tr><td>"+ret.procs[i].procedureName+"</td><!--td>"+ret.procs[i].procedureWeight+" min</td--></tr>";
                            }
                            txt+="</table>";
                            container.append("<div class='info-pop'>"+txt+"</div>");   
                        }
                    });
                }
                else
                {
                    container.find(".info-pop").remove();
                    elem.attr('data-clicked',0);
                }
                
            }
            
        );
        
        $(".serve-test").click(function(e){
            e.preventDefault();
            id = $(this).attr('rel');
            area = $(this).attr('data-area');
            data = {'id':id,'area':area};
            $.ajax({
                'url':'<?php echo base_url(); ?>index.php/admin/serve_item',
                'method':'post',
                'data':data,
                'dataType':'json',
                'success':function(ret){
                    document.location="<?php echo base_url(); ?>admin/area/"+area+"/1";   
                }
            });
        });
        
        $(".serve-consultation").click(function(e){
            e.preventDefault();
            id = $(this).attr('rel');
            area = $(this).attr('data-area');
            doctor = $("select[name=doctorid]").val();
            atid = $(this).attr('data-atid');
            data = {'id':id,'area':area,'doctorid':doctor,'atid':atid};
            $.ajax({
                'url':'<?php echo base_url(); ?>index.php/admin/serve_consultation',
                'method':'post',
                'data':data,
                'dataType':'json',
                'success':function(ret){
                    document.location="<?php echo base_url(); ?>admin/area/"+area+"/1";   
                }
            });
        });
        
        $("#selectDoctor").on('show.bs.modal',function(e){
            var $invoker = $(e.relatedTarget);
            $("#serve-consultaton-button").attr('rel',$invoker.attr('rel'));
             $("#serve-consultaton-button").attr('data-atid',$invoker.attr('data-atid'));
            
        });
        
        
        
        
        $(".serve-finish").click(function(e){
            e.preventDefault();
            id = $(this).attr('rel');
            area = $(this).attr('data-area');
            specimen = $("#specimenid").val();
            
            data = {'id':id,'area':area,'specimenid':specimen};
            
            $.ajax({
                'url':'<?php echo base_url(); ?>index.php/admin/serve_finish',
                'method':'post',
                'data':data,
                'dataType':'json',
                'success':function(ret){
                   document.location="<?php echo base_url(); ?>admin/area/"+area+"/2";   
                }
            });
        });
        
        $(".call-next").click(function(e){
            e.preventDefault();
            area = $(this).attr('rel');
            type = $(this).attr('href');
            data = {'area':area,'type':type};
            
            $.ajax({
                'url':'<?php echo base_url(); ?>index.php/admin/next_item',
                'method':'post',
                'data':data,
                'dataType':'json',
                'success':function(ret){
                    document.location="<?php echo base_url(); ?>admin/area/"+area+"/2";    
                }
            });
        });
        
    });
</script>