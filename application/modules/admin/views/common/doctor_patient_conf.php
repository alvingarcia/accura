<script type="text/javascript">
    $(document).ready(function(){
       $('.testRecommendations').fastselect();
        $('.rxTemplates').fastselect();
        
        $(".add-rx").click(function(e){
           e.preventDefault();
           data = {'id':$(this).attr('rel'),'templates':$("#rxTemplate").val()}
           $.ajax({
               type: "POST",
               url: "<?php echo base_url(); ?>admin/load_rx",
               data:data,
               dataType:'json',
               success: function(data){
                  $("#rx").append(data.html);
                   $('#addRx').modal('toggle');
                   
                   $(".delete-rx").click(function(e){
                     e.preventDefault();
                     $(this).parent().parent().remove();
                   });
                }

            });
        });
        
        $("#submit-consultation-button").click(function(e){
           e.preventDefault();
            var conf = confirm("Finalize and submit results?");
            if(conf)
            {
                $("#consultation-form").submit();
            }
        });
        
        
    });
 
</script>