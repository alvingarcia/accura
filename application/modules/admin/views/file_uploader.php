
<body style="padding-top:0;">
    <div class="page-header" style="position: fixed;
background: #000;
width: 100%;
margin-top: 0;
color: #fff;
padding-left: 1rem;">
			<h1>Files</h1>
		</div>
	<div class="container" style="padding-top:100px;">
		
		<br>
		<!-- The file upload form used as target for the file upload widget -->
		<form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
			<!-- Redirect browsers with JavaScript disabled to the origin page -->
			<noscript><input type="hidden" name="redirect" value="http://blueimp.github.com/jQuery-File-Upload/"></noscript>
			<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->		
				<div id="dropzone">Drag and Drop Files here</div>
			<hr>
            <p>
                <h4>Crop X-coordinate</h4>
                <input type="text" name="crop_x" value="0" />
            </p>
            <p>
                <h4>Crop Y-coordinate</h4>
                <input type="text" name="crop_y" value="0" />
            </p>
            <hr>
			<div class="row fileupload-buttonbar">
				<div class="span9">
					<!-- The fileinput-button span is used to style the file input field as button -->					
					<span class="btn fileinput-button">
						<i class="icon-plus"></i>
						<span>Add files...</span>
						<input type="file" name="files[]" multiple>
					</span>
					<button type="submit" class="btn btn-primary start">
						<i class="icon-upload icon-white"></i>
						<span>Start upload</span>
					</button>
					<button type="reset" class="btn btn-primary cancel">
						<i class="icon-ban-circle icon-white"></i>
						<span>Cancel upload</span>
					</button>			
					<input type="checkbox" class="toggle">
					<div style="clear:both"></div>
				</div>			
				<!-- The global progress information -->
				<div class="span4 fileupload-progress fade">
					<!-- The global progress bar -->
					<div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
						<div class="bar" style="width:0%;"></div>
					</div>
					<!-- The extended global progress information -->
					<div class="progress-extended">&nbsp;</div>
				</div>			
			</div>
			<!-- The loading indicator is shown during file processing -->
			<div class="fileupload-loading"></div>
			<br>
			<!-- The table listing the files available for upload/download -->
			<table role="presentation" class="table">
                <tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery">
                </tbody>
            </table>
		</form>
	</div>
	<!-- modal-gallery is the modal dialog used for the image gallery -->
	<div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd" tabindex="-1">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">&times;</a>
			<h3 class="modal-title"></h3>
		</div>
		<div class="modal-body"><div class="modal-image"></div></div>
		<div class="modal-footer">
			<a class="btn modal-download" target="_blank">
				<i class="icon-download"></i>
				<span>Download</span>
			</a>
			<a class="btn btn-success modal-play modal-slideshow" data-slideshow="5000">
				<i class="icon-play icon-white"></i>
				<span>Slideshow</span>
			</a>
			<a class="btn btn-info modal-prev">
				<i class="icon-arrow-left icon-white"></i>
				<span>Previous</span>
			</a>
			<a class="btn btn-primary modal-next">
				<span>Next</span>
				<i class="icon-arrow-right icon-white"></i>
			</a>
		</div>
	</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
            <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
            <td>
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
            </td>
            <td class="start">{% if (!o.options.autoUpload) { %}
                <button class="btn btn-primary">
                    <i class="icon-upload icon-white"></i>
                    <span>Start</span>
                </button>
            {% } %}</td>
        {% } else { %}
            <td colspan="2"></td>
        {% } %}
        <td class="cancel">{% if (!i) { %}
            <button class="btn btn-warning">
                <i class="icon-ban-circle icon-white"></i>
                <span>Cancel</span>
            </button>
        {% } %}</td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
            <td></td>            
			<td>{%=file.name%}</td>
            <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else { %}
            <td class="preview">
                <span><div class="image" style="background:url({%=file.url%});"></div></span>    
                <td>{%=file.name%}</td>
                <a href="{%=file.url%}" title="{%=file.name%}" data-gallery="gallery" download="{%=file.name%}"><img src="{%=file.url%}"></a>				
           </td>
            
            <td colspan="2"></td>
        {% } %}
        <td class="del">
            <button class="btn btn-danger tiny delete" style="margin-bottom:5px;" rel="{%=file.name%}">                
                <span>Delete</span>
            </button>
			<a href="#" rel="{%=file.name%}" class="btn tiny btn-primary attach-to-table" >Select</a>
           
        </td>
    </tr>
{% } %}
</script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.min.js';?>"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo base_url() ?>assets/lib/uploader/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?php echo base_url() ?>assets/lib/uploader/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?php echo base_url() ?>assets/lib/uploader/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?php echo base_url() ?>assets/lib/uploader/js/canvas-to-blob.min.js"></script>    
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo base_url() ?>assets/lib/uploader/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo base_url() ?>assets/lib/uploader/js/jquery.fileupload.js"></script>
<!-- The File Upload file processing plugin -->
<script src="<?php echo base_url() ?>assets/lib/uploader/js/jquery.fileupload-fp.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo base_url() ?>assets/lib/uploader/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script type="text/javascript">
var loader = "<img src='<?php echo base_url() ?>assets/img/ajax-loader.gif' >";
var base_url = "<?php echo base_url() ?>";

$(function () {

		$('.attach-to-table').live('click',function(e){
			 e.preventDefault();
                parent.$("#strPicture").val($(this).attr("rel"));
			    parent.$("#imageThumb").attr("src","<?php echo base_url().IMAGE_UPLOAD_DIR; ?>"+$(this).attr("rel"));
                parent.$("#fileUpload").modal('hide');
			});
		
        $('.delete').live('click',function(e){
            remove = $(this).attr('rel');
            conf = confirm("are you sure you want to delete this image?");
            if(conf){
            $.ajax({
				type:'post',
				url:base_url+'cms/cleanup/',
                data:{'remove':remove},
				success:function(ret)
				{
					//window.parent.location=window.parent.location;
				}
			});
            }
        
        });
    
		$(document).bind('dragover', function (e) {
			var dropZone = $('#dropzone'),
				timeout = window.dropZoneTimeout;
			if (!timeout) {
				dropZone.addClass('in');
			} else {
				clearTimeout(timeout);
			}
			if (e.target === dropZone[0]) {
				dropZone.addClass('hover');
			} else {
				dropZone.removeClass('hover');
			}
			window.dropZoneTimeout = setTimeout(function () {
				window.dropZoneTimeout = null;
				dropZone.removeClass('in hover');
			}, 100);
		});
		'use strict';

		// Initialize the jQuery File Upload widget:
		$('#fileupload').fileupload({
			// Uncomment the following to send cross-domain cookies:
			//xhrFields: {withCredentials: true},
			url: '<?php echo base_url(); ?>assets/lib/uploader/server/php/',
			dropZone: $('#dropzone'),
            autoUpload:true
		});

		// Enable iframe cross-domain access via redirect option:
		$('#fileupload').fileupload(
			'option',
			'redirect',
			window.location.href.replace(
				/\/[^\/]*$/,
				'<?php echo base_url(); ?>assets/lib/uploader/cors/result.html?%s'
			)
		);

		if (window.location.hostname === 'blueimp.github.com') {
			// Demo settings:
			$('#fileupload').fileupload('option', {
				url: '//jquery-file-upload.appspot.com/',
				maxFileSize: 5000000,
				acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
				process: [
					{
						action: 'load',
						fileTypes: /^image\/(gif|jpeg|png)$/,
						maxFileSize: 20000000 // 20MB
					},
					{
						action: 'resize',
						maxWidth: 1440,
						maxHeight: 900
					},
					{
						action: 'save'
					}
				]
			});
			// Upload server status check for browsers with CORS support:
			if ($.support.cors) {
				$.ajax({
					url: '//jquery-file-upload.appspot.com/',
					type: 'HEAD'
				}).fail(function () {
					$('<span class="alert alert-error"/>')
						.text('Upload server currently unavailable - ' +
								new Date())
						.appendTo('#fileupload');
				});
			}
		} else {
			// Load existing files:
			$.ajax({
				// Uncomment the following to send cross-domain cookies:
				//xhrFields: {withCredentials: true},
				url: $('#fileupload').fileupload('option', 'url'),
				dataType: 'json',
				context: $('#fileupload')[0]
			}).done(function (result) {
				$(this).fileupload('option', 'done')
					.call(this, null, {result: result});
			});
		}

	});
</script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
<!--[if gte IE 8]><script src="js/cors/jquery.xdr-transport.js"></script><![endif]-->
</body> 
</html>
