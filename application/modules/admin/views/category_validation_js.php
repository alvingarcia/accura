<script type="text/javascript">
    $(document).ready(function(){
        
        //FORM VALIDATION FOR SUBJECT
        $("input[name='strName']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='strName']").html("<i class='fa fa-times-circle-o'></i> Name is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='strName']").html("Name*");
            }
        
        });
        
        
        $("#validate-category").submit(function(e){
            
            
            if($("input[name='strName']").val() == ""){
                $("input[name='strName']").parent().addClass('has-error');
                $("label[for='strName']").html("<i class='fa fa-times-circle-o'></i> Name is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='strName']").parent().removeClass('has-error');
                $("label[for='strName']").html("Name*");
            }
            
            return;
        });
    });
</script>