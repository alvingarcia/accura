<script type="text/javascript">
    
    $(document).ready(function(){
    $("#category-select").change(function(){
        
        if($(this).val() == "all")
            document.location = "<?php echo base_url(); ?>cms/view_all_content/"; 
        else
            document.location = "<?php echo base_url(); ?>cms/view_content_by_category/"+$(this).val(); 
    });
    $('#employees_table').dataTable( {
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url(); ?>index.php/datatables/data_tables_ajax/tb_mas_content_category/<?php echo $cat; ?>",
            "aoColumnDefs":[
                {
                    "aTargets":[4],
                    "mData": null,
                    "bSortable":false,
                    "mRender": function (data,type,row,meta) { return '<?php echo dropdown_menu_open(); ?><li><a href="<?php echo base_url(); ?>cms/edit_content/'+row[0]+'">Edit</a><li><li><a href="#" rel="'+row[0]+'" class="trash-content">Delete</a><li><?php echo dropdown_menu_close(); ?>'; }
                },
                {
                    "aTargets":[0],
                    "bVisible": false 
                }
            ],
            
            "fnDrawCallback": function () {
                
                $(".trash-content").click(function(e){
                    conf = confirm("Are you sure you want to delete?");
                    if(conf)
                    {
                        $(".loading-img").show();
                        $(".overlay").show();
                        var id = $(this).attr('rel');
                        var parent = $(this).parent().parent().parent().parent().parent();
                        var data = {'table':'tb_mas_content','id':id};
                        $.ajax({
                            'url':'<?php echo base_url(); ?>index.php/cms/deleteItem/intID',
                            'method':'post',
                            'data':data,
                            'dataType':'json',
                            'success':function(ret){
                                if(ret.message == "failed"){
                                    $(".alert").show();
                                    setTimeout(function() {
                                        $(".alert").hide('fade', {}, 500)
                                    }, 3000);
                                }
                                else{
                                    parent.hide();
                                }
                                $(".loading-img").hide();
                                $(".overlay").hide();
                        }
                    });
                    }
                });
            
            }
        } );
        
    });

</script>