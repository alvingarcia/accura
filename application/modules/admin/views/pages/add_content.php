<aside class="right-side">
<section class="content-header">
                    <h1>
                        Content
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Content</a></li>
                        <li class="active">New Content</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New Content</h3>
        </div>
                <div class="box-body">
                    <div class="col-xs-8">
                        <?php 
                            echo cms_form_open('validate-content',base_url().'admin/submit_content','post');
                            echo cms_input('strTitle','Title','Enter Title','col-xs-6'); 
                            echo cms_input('strAuthor','Author','Enter Author\'s Name','col-xs-6'); 
                            echo cms_hidden('dteStart',date("Y-m-d H:i:s")); 
                         ?>
                        <hr style="clear:both" />
                        <?php
                            echo cms_wysiwyg('strContent','Content','Content Goes Here','col-xs-12'); 
                        ?>
                        <hr style="clear:both" />
                        <?php
                            echo cms_textarea('strSecondary','Embed Content','Secondary Content Goes Here','col-xs-12'); 
                            $choices = array("enabled"=>"Enabled","disabled"=>"Disabled","hold"=>"Hold");
                            $types = array("post"=>"post","video"=>"video","music"=>"music","web design"=>"web design","graphics"=>"graphics","branding"=>"branding","identity"=>"identity");
                            $featured = array("0"=>"0","1"=>"1","2"=>"2","3"=>"3","4"=>"4","5"=>"5","6"=>"6","7"=>"7","8"=>"8","9"=>"9","10"=>"10");
                            echo cms_dropdown('enumEnabled','Enabled',$choices,'col-xs-4');
                            echo cms_dropdown('enumType','Type',$types,'col-xs-4');
                            echo cms_dropdown('intFeatured','Featured',$featured,'col-xs-4');
                            echo cms_date('dteStart','Start Date',"enter date","col-xs-6",date("m/d/Y H:i:s"));
                            echo cms_date('dteEnd','End Date',"enter date","col-xs-6",date("m/d/Y H:i:s"));
                            
                    ?>
                        </div>
                        <div class="col-xs-4">
                    <?php 
                         echo cms_group_checkbox('intContentCategory','Category',$category_items,'col-xs-6','#newCategory');        
                    ?>
                        </div>
                        <hr style="clear:both" />
                    <?php
                            echo cms_submit('add');
                            echo cms_form_close();
                        ?>
                </div>
            
</aside>
    
<div class="modal" id="newCategory">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Add New Category</h4>
        </div>
        <div class="modal-body">
            <div class="box-body">
                     <div class="form-group col-xs-12">
                        <label for="strCode">Category Name</label>
                        <input type="text" name="strCategoryName" class="form-control" id="strCategoryName" placeholder="Enter Category Name">
                    </div>
                    <?php echo cms_dropdown('intParentID','Parent',$category_items,'col-xs-12'); ?>

                    

                <div style="clear:both"></div>
            </div>
        </div>
        <div class="modal-footer">
          <a href="#" data-dismiss="modal" class="btn">Close</a>
          <a href="#" id="submit-category" class="btn btn-primary">Add Category</a>
        </div>
      </div>
    </div>
</div>
