<div class="content-wrapper">
    <section class="content-header">
                    <h1>
                        Dashboard 
                        <small>Welcome <?php echo ($isDoctor)?"Doctor":""; ?> <?php echo $user['lastname']; ?> </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Patients</h3>    
            </div>
            <div class="box-body">
                <table class="table table-bordered" id="doctor-table">
                    <tr>
                        <th>Patient</th>
                        <th>Age</th>
                        <th>Action</th>
                    </tr>
                    <?php foreach($cons as $c): ?>
                    <tr id="test-<?php echo $c['appointmentTestId']; ?>">
                        <td><?php echo $c['patientLastname'].", ".$c['patientFirstname']; ?></td>
                        <td><?php echo calculate_age($c['birthday']); ?></td>
                        <td><a class="btn btn-primary" href="<?php echo base_url()."admin/doctor_patient/".$c['appointmentTestId']; ?>">Serve</a></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </section>
</div>


