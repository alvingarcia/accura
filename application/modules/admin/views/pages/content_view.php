<aside class="right-side">
<section class="content-header">
                    <h1>
                        Content
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Content</a></li>
                        <li class="active">View All Content</li>
                    </ol>
                </section>
    <div class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Content</h3>
            </div><!-- /.box-header -->
            
            <div class="box-body table-responsive">
                <!--h4>Filter By Category:</h4>
                <select class="form-control" id="category-select">
                    <option value="all">All</option>
                    <?php foreach($categories as $category): ?>
                        <option value="<?php echo pw_hash($category['intID']); ?>"><?php echo $category['strName']; ?></option>
                    <?php endforeach; ?>
                </select>
                <hr /-->
               <table id="employees_table" class="table table-striped table-bordered table-hover">
                   <thead><tr><th>ID</th><th>Title</th><th>Description</th><th>actions</th></tr></thead>
                    <tbody></tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</aside>