<aside class="right-side">
<section class="content-header">
                    <h1>
                        Categories
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Categories</a></li>
                        <li class="active">View All Categories</li>
                    </ol>
                </section>
    <div class="content">
        <div class="alert alert-danger" style="display:none;">
            <i class="fa fa-ban"></i>
            <span id="alert-text"></span>
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Categories</h3>

            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
               <table id="employees_table" class="table table-striped table-bordered table-hover">
                    <thead><tr><th>ID Hash</th><th>ID</th><th>Name</th><th>actions</th></tr></thead>
                    <tbody></tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</aside>