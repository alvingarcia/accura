<style>
    .info-pop
    {
        position:absolute;
        background: #d2d2d2;
        z-index: 999;
    }
</style> 
<div class="content-wrapper">
     <section class="content-header">
                    <h1>
                        <?php echo $area['name']; ?>
                        <small>Welcome</small>
                        <a href="<?php echo base_url().'admin/dashboard'; ?>" class="btn btn-app">
                            <i class="fa fa-arrow-left"></i>
                            Back
                        </a>
                        <a <?php echo $section==1?'disabled':''; ?> href="<?php echo base_url().'admin/area/'.$area['areaid'].'/1'; ?>" class="btn btn-app">
                            
                            <?php echo $section_names[0]; ?>
                        </a>
                        <a <?php echo $section==2?'disabled':''; ?> href="<?php echo base_url().'admin/area/'.$area['areaid'].'/2'; ?>" class="btn btn-app">
                            
                            <?php echo $section_names[1]; ?>
                        </a>
                        <a <?php echo $section==3?'disabled':''; ?> href="<?php echo base_url().'admin/area/'.$area['areaid'].'/3'; ?>" class="btn btn-app">
                            
                            <?php echo $section_names[2]; ?>
                        </a>
                        <a target="_blank" href="<?php echo base_url().'monitor/queue/'.$area['areaid']; ?>" class="btn btn-app">Open Queue Monitor
                        </a>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Area</li>
                    </ol>
                </section>
    <section class="content">

    <div style="background:#fff;padding:1rem;" id="pane<?php echo $area['areaid']; ?>">
        <div class="row">
            <?php if($section == 1): ?>
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title" style="font-size:1em;"><?php echo $section_names[0]; ?></h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered">
                                <tbody>
                                    
                        <?php foreach($area['queue'] as $test): ?>
                                    <tr>
                                        <td><div class="btn-group"><button type="button" class="btn btn-default btn-sm">Actions</button><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="serve-test" data-area="<?php echo $area['areaid']; ?>" rel="<?php echo $test['aid']; ?>">Serve</a></li><li><a href="<?php echo base_url()."appointment/view_appointment/".$test['aid']; ?>">Details</a></li><li><a href="#" >Cancel</a></li></ul></div></td>
                                        <td> <a href="#" data-clicked=0 data-area="<?php echo $area['areaid']; ?>" rel="<?php echo $test['aid']; ?>" class="show-estimated product-title" data-placement="vertical"><?php echo "Name: ".$test['patientLastname'].", ".$test['patientFirstname']; ?></a></td>
                                        <td><?php echo "Age: ".calculate_age($test['birthday']); ?></td>
                                    
                                </tr>
                          <?php endforeach; ?>
                                    
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>
            <?php elseif($section == 2): ?>
                <div class="col-md-8 col-md-offset-2">
                  <div class="box box-warning box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title" style="font-size:1em;"><?php echo $section_names[1]; ?></h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                       
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <table class="table table-bordered">
                                <tbody>
                                    
                        <?php foreach($area['queue'] as $test): ?>
                             <tr>
                                        <td><div class="btn-group"><button type="button" class="btn btn-default btn-sm">Actions</button><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#"  data-toggle="modal" data-target="#addSpecimen"  >Finished</a></li><li><a href="#" >Cancel</a></li></ul></div></td>
                                        <td> <a href="#" data-clicked=0 data-area="<?php echo $area['areaid']; ?>" rel="<?php echo $test['aid']; ?>" class="show-estimated product-title" data-placement="vertical"><?php echo "Name: ".$test['patientLastname'].", ".$test['patientFirstname']; ?></a></td>
                                        <td><?php echo "Age: ".calculate_age($test['birthday']); ?></td>
                                    </tr>
                              
                            
                          <?php endforeach; ?>
                            
                       </tbody>
                    </table>
                    </div>
                    
                    <!-- /.box-body -->
                  </div>
                </div>
            <?php elseif($section == 3): ?>
                <div class="col-md-8 col-md-offset-2">
                  <div class="box box-success box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title" style="font-size:1em;"><?php echo $section_names[2]; ?></h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                       
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <table class="table table-bordered">
                                <tbody>
                                    
                        <?php foreach($area['queue'] as $test): ?>
                          <tr>
                                        <td><div class="btn-group"><button type="button" class="btn btn-default btn-sm">Actions</button><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="<?php echo base_url()."appointment/submit_order/".$test['aid']."/".$area['areaid']; ?>" class="">Send Order</a></li></ul></div></td>
                                        <td> <a href="#" data-clicked=0 data-area="<?php echo $area['areaid']; ?>" rel="<?php echo $test['aid']; ?>" class="show-estimated product-title" data-placement="vertical"><?php echo "Name: ".$test['patientLastname'].", ".$test['patientFirstname']; ?></a></td>
                                        <td><?php echo "Age: ".calculate_age($test['birthday']); ?></td>
                                    </tr>
                              
                         
                         <?php endforeach; ?>
                       </tbody>
                    </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>
            <?php endif; ?>
        </div>
    </div>        
        
    </section>
</div>
    
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="addSpecimen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Proceed with Test</h4>
      </div>
      <div class="modal-body">
        <h4>Proceed?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" data-area="<?php echo $area['areaid']; ?>" rel="<?php echo $area['queue'][0]['aid']; ?>" class="serve-finish btn btn-primary">Proceed <i class="fa fa-arrow-right"></i></button>
      </div>
    </div>
  </div>
</div>


