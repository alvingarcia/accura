<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard 
            <small>Welcome <?php echo ($isDoctor)?"Doctor":""; ?> <?php echo $user['lastname']; ?> </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo $test['patientFirstname']." ".$test['patientLastname']; ?> AGE: <?php echo calculate_age($test['birthday']); ?></h3>    
            </div>
            <div class="box-body">
                <div class="row"></div>
                <form id="consultation-form" method="post" action="<?php echo base_url(); ?>admin/submit_consultation_result">
                <?php 
                echo cms_hidden('testConsultationId',$test['testConsultationId']);
                echo cms_hidden('appointmentTestId',$test['appointmentTestId']);
                echo cms_hidden('aid',$test['aid']);
                echo cms_textarea('findings','Assessment','Enter Text','col-sm-12');
                    ?>
                    <div class="col-sm-12">
                        <label>Test Recommendations</label><br />
                     <input type="text" multiple class="testRecommendations" data-url="<?php echo base_url(); ?>tests.json"  value="" data-initial-value='' data-load-once="true" id="testRecommendations" name="testRecommendations" />
                    </div>
                    <?php
                //echo cms_textarea('testRecommendations','Test Recommendations','eg. CBC,Blood Test','col-sm-12');
                //echo cms_textarea('perscription','Rx (separate items with semi colon)','eg. Amoxicillin 250 mg tablets 1 tablet per day for 7 days; Robitussin Cough Syrup 1 tbsp per day for 12 days','col-sm-12');
                
                ?>
                <div class="col-sm-12" id="rx">
                    
                    
                </div>
                <div class="col-sm-2">                           
                            <a data-toggle="modal" data-target="#addRx" class="btn btn-primary btn-block" href="#"><i class="fas fa-plus"></i> Perscription</a>
                        </div>
                <div class="col-sm-2">
                    <a id="submit-consultation-button" href="#" class="btn btn-primary btn-block">Submit Results <i class="fa fa-arrow-right"></i></a>    
                </div>
                </form>
                </div>
            </div>
            <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Previous Consultations</h3>    
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Assessment</th>
                            <th>Test Recommendations</th>
                            <th>Rx</th>
                            <th>Doctor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if(is_array($previousConsultations)):
                        foreach($previousConsultations as $pc): ?>
                        <tr>
                            <td><?php echo $pc['appointmentDate']; ?></td>
                            <td><?php echo $pc['findings']; ?></td>
                            <td><?php echo $pc['testRecommendations']; ?></td>
                            <td><?php echo $pc['perscription']; ?></td>
                            <td><?php echo $pc['lastname'].", ".$pc['firstname']; ?></td>
                        </tr>
                        <?php endforeach;
                            endif;
                        ?>
                    </tbody>
                </table>
                
                
                
            </div>
            </div>
            <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Medical History</h3>    
            </div>
            <div class="box-body">
                <h4>Allergies</h4>
                <?php for($i=0;$i<count($allergies);$i++): 
                    echo $allergies[$i]['desc'];
                    if($i < count($allergies) - 1)
                    echo ", ";
                    endfor; ?>
                <hr />
                <h4>Illnesses</h4>
                <?php for($i=0;$i<count($illnesses);$i++): 
                    echo $illnesses[$i]['desc'];
                    if($i < count($illnesses) - 1)
                    echo ", ";
                    endfor; ?>
            </div>
            </div>
    </section>
</div>


<!-- Modal -->
<div class="modal fade" id="addRx" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Template</h4>
      </div>
      <div class="modal-body">
        <div class="row">
             <input type="text" multiple class="rxTemplates" data-url="<?php echo base_url(); ?>rx.json"  value="" data-initial-value='' data-load-once="true" id="rxTemplate" name="rxTemplate" />
            
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" rel="<?php echo $test['appointmentTestId']; ?>" class="add-rx btn btn-primary">Add <i class="fa fa-arrow-right"></i></button>
      </div>
    </div>
  </div>
</div>
