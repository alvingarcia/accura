<style>
    .info-pop
    {
        position:absolute;
        background: #d2d2d2;
        z-index: 999;
    }
</style> 
<div class="content-wrapper">
     <section class="content-header">
                    <h1>
                        Dashboard 
                        <small>Welcome <?php echo ($isDoctor)?"Doctor":""; ?></small>
                        <a href="<?php echo base_url(); ?>appointment/new_appointment" class="btn btn-app">
                            <i class="fa fa-plus"></i>
                            New Trasnsaction
                        </a>
                        <a href="<?php echo base_url(); ?>patient/add_patient" class="btn btn-app">
                            <i class="fa fa-plus"></i>
                            Add Patient
                        </a>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
    <section class="content">
        <div class="row">
            
             <?php $i=1; foreach($areas as $area): 
                if(!$isDoctor || $area['name'] == "Consultation"):
             ?>
            <?php if($isDoctor): ?>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>
                            &nbsp;
                        </h3>
                        <p>
                            <?php echo $area['name']; ?>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-clipboard-list"></i>
                    </div>
                    <a href="<?php echo base_url().'admin/doctor_consultation_page/'; ?>" class="small-box-footer">
                        View <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <?php else: ?>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>
                            &nbsp;
                        </h3>
                        <p>
                            <?php echo $area['name']; ?>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-clipboard-list"></i>
                    </div>
                    <a href="<?php echo base_url().'admin/area/'.$area['areaid']; ?>" class="small-box-footer">
                        View <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <?php endif; ?>
        <?php $i++; endif; endforeach; ?>
        </div>
       <div class="box">
           <div class="box-body">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th colspan="3">Patient Queue</th>
                </tr>
                <tr>
                    <th>Name</th>
                    <th>Area</th>
                    <th>Status</th>
                </tr>
            </thead>   
            <tbody>
                <?php foreach($p_data['queue'] as $d): ?>
                    <tr>
                        <td><?php echo $d['patientLastname']." ".$d['patientFirstname']; ?></td>
                        <td><?php echo $d['areaName']; ?></td>
                        <td>Waiting</td>
                    </tr>
                <?php endforeach; ?>
                <?php foreach($p_data['progress'] as $d): ?>
                    <tr>
                        <td><?php echo $d['patientLastname']." ".$d['patientFirstname']; ?></td>
                        <td><?php echo $d['areaName']; ?></td>
                        <td>In Progress</td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        </div>
        </div>
<?php /*
  <!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <?php $i=1; foreach($areas as $area): ?>
    <li role="presentation" class="<?php echo ($sel==$area['areaid'])?'active':''; ?>"><a href="#pane<?php echo $area['areaid']; ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $area['name']; ?></a></li>
    <?php $i++; endforeach; ?>
</ul>

  <!-- Tab panes -->
<div class="tab-content">
    <?php $i=1; foreach($areas as $area): ?>
    <div style="background:#fff;padding:1rem;" role="tabpanel" class="tab-pane <?php echo ($sel==$area['areaid'])?'active':''; ?>" id="pane<?php echo $area['areaid']; ?>">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title" style="font-size:1em;">Queue for <?php echo $area['name']; ?></h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <ul class="products-list product-list-in-box">
                        <?php foreach($area['queue'] as $test): ?>
                        <li class="item">
                            <div class="product-img" style="margin-right:1rem;">
                                <div class="btn-group"><button type="button" class="btn btn-default btn-sm">Actions</button><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="serve-test" data-area="<?php echo $area['areaid']; ?>" rel="<?php echo $test['aid']; ?>">Serve</a></li><li><a href="<?php echo base_url()."appointment/view_appointment/".$test['aid']; ?>">Details</a></li><li><a href="#" >Cancel</a></li></ul></div>
                            </div>
                            <div class="product-info">

                            <a href="#" data-clicked=0 data-area="<?php echo $area['areaid']; ?>" rel="<?php echo $test['aid']; ?>" class="show-estimated product-title" data-placement="vertical"><i class="fa fa-clock-o"></i>
                             </a>
                               <?php echo $test['patientLastname'].", ".$test['patientFirstname'];  ?>
                            </div>
                           
                        </li>
                          
                          <?php endforeach; ?>
                          
                      </ul>
                       
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="box box-warning box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title" style="font-size:1em;">Now Serving in <?php echo $area['name']; ?></h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                       
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <ul class="products-list product-list-in-box">
                        <?php foreach($area['progress'] as $test): ?>
                        <li class="item">
                            <div class="product-img" style="margin-right:1rem;">
                                <div class="btn-group"><button type="button" class="btn btn-default btn-sm">Actions</button><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="#" class="serve-finish" data-area="<?php echo $area['areaid']; ?>"  rel="<?php echo $test['aid']; ?>">Finished</a></li><li><a href="#" >Cancel</a></li></ul></div>
                            </div>
                          <div class="product-info">
                            <a href="javascript:void(0)" class="product-title"> <?php echo $test['patientLastname'].", ".$test['patientFirstname'];  ?></a>
                                <span class="product-description">
                                 
                                </span>
                          </div>
                        </li>
                          <?php endforeach; ?>
                      </ul>
                    </div>
                    <div class="box-footer text-center">
                    <?php if(empty($area['progress'])): ?>
                      <a href="#" rel="<?php echo $area['areaid']; ?>" class="uppercase call-next"><i class="fa fa-arrow-right"></i> Next</a>
                    <?php endif; ?>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="box box-success box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title" style="font-size:1em;">Waiting for Results in <?php echo $area['name']; ?></h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                       
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <ul class="products-list product-list-in-box">
                        <?php foreach($area['processing'] as $test): ?>
                        <li class="item">
                            <div class="product-img" style="margin-right:1rem;">
                                <div class="btn-group"><button type="button" class="btn btn-default btn-sm">Actions</button><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu"><li><a href="<?php echo base_url()."appointment/aproc_view/".$test['aid']."/".$area['areaid']; ?>" class="">Update Results</a></li></ul></div>
                            </div>
                          <div class="product-info">
                            <a href="javascript:void(0)" class="product-title"><?php echo $test['patientLastname'].", ".$test['patientFirstname'];  ?></a>
                                <span class="product-description">
                                  
                                </span>
                          </div>
                        </li>
                          <?php endforeach; ?>
                      </ul>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>
        </div>
    </div>
    <?php $i++; endforeach; ?>
</div>
*/ ?>
        
        
    </section>
</div>

