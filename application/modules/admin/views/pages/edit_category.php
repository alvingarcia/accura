<aside class="right-side">
<section class="content-header">
                    <h1>
                        Category
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Categories</a></li>
                        <li class="active">Edit Category</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Edit Category</h3>
        </div>
       
                 <div class="box-body">
                     <?php 
                            echo cms_form_open('validate-category',base_url().'cms/submit_category','post');
                            echo cms_hidden('intID',$item['intID']); 
                            echo cms_input('strName','Category Name','Enter Category Name','col-xs-6',$item['strName']); 
                            echo cms_dropdown('intParent','Parent',$categories,'col-xs-6',$item['intParent']);
                            echo cms_dropdown('intLocked','Locked',array("0"=>"No","1"=>"yes"),'col-xs-6',$item['intLocked']);
                            echo cms_submit('update');
                            echo cms_form_close();
                        ?>
       
                </div>
            </div>
    </div>
</aside>