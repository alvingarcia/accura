<div class="row rx-item" >
        
        <div class="col-sm-4">
            <label>Medicine</label>
            <input type="text" name="medicine[]" value="<?php echo $item['rxMedicine']; ?>" class="form-control" />
        </div>
        <div class="col-sm-2">
            <label>Dosage</label>
            <input type="number" value="<?php echo $item['rxDosage']; ?>" name="dosage[]" class="form-control" />
        </div>
        <div class="col-sm-2">
            <label>Unit</label>
            <input type="text" value="<?php echo $item['rxUnit']; ?>" name="unit[]" class="form-control" />
        </div>
        <div class="col-sm-2">
            <label>Freq</label>
            <input type="number" value="<?php echo $item['rxTimes']; ?>" name="times[]" class="form-control" />
        </div>
        <div class="col-sm-2">
            <label>Days</label>
            <input type="number" value="<?php echo $item['rxDays']; ?>" name="days[]" class="form-control" />
        </div>
        <div class="col-sm-6">
            <label>Schedule</label>
            <input placeholder="Before Meal, After Meal, 10:00am 1:00pm, etc..." value="<?php echo $item['rxSchedule']; ?>" type="text" name="schedule[]" class="form-control" />
        </div>
        <div class="col-sm-2">
            <label>Delete</label>
            <a class="btn btn-danger btn-block delete-rx">&times;</a>
        </div>
    <div class="col-sm-12">
        <hr />
    </div>
    </div>  
