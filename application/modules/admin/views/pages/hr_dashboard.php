<style>
    .info-pop
    {
        position:absolute;
        background: #d2d2d2;
        z-index: 999;
    }
</style> 
<div class="content-wrapper">
     <section class="content-header">
                    <h1>
                        <?php echo $adm->companyName; ?>
                        <small>Welcome <?php echo $adm->firstname." ".$adm->lastname; ?> </small>
                        <a href="<?php echo base_url(); ?>patient/add_patient_hr" class="btn btn-app">
                            <i class="fa fa-plus"></i>
                            Add Patient
                        </a>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">HR Dashboard</li>
                    </ol>
                </section>
    <section class="content">
        <div class="box box-solid box-success">
                <div class="box-header">
                    <h3 class="box-title">List of Employees</h3>
                    <div class="box-tools">

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="faculty-table" class="table">
                        <thead><tr>
                            <th>id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Gender</th>
                            <th>Birthday</th>
                            <th>Select Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                    </tbody></table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        
        
    </section>
</div>

