<!-- Include JS file. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/lib/fwysiwig/js/froala_editor.pkgd.min.js'></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.wysiwyg').froalaEditor({
        toolbarButtons:	['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineClass', 'inlineStyle', 'paragraphStyle', 'lineHeight', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertTable', '|','specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'spellChecker', 'help', 'html', '|', 'undo', 'redo']
      });

        
        //FORM VALIDATION FOR SUBJECT
        $("input[name='strCode']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='strTitle']").html("<i class='fa fa-times-circle-o'></i> Title is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='strTitle']").html("Title*");
            }
        
        });
        
        $("#validate-content").submit(function(e){
          
            
            
            if($("input[name='strTitle']").val() == ""){
                $("input[name='strTitle']").parent().addClass('has-error');
                $("label[for='strTitle']").html("<i class='fa fa-times-circle-o'></i> Title is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='strTitle']").parent().removeClass('has-error');
                $("label[for='strTitle']").html("Title*");
            }
            
            
            return;
        });
    });
</script>