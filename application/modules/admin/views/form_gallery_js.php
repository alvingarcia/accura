<script type="text/javascript">
	$(document).ready(function(){			

			
		
		
		$(".various").fancybox({
			maxWidth	: 960,
			maxHeight	: 600,
			fitToView	: false,
			width		: '100%',
			height		: '70%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none'
		});		
		
		$(".delete-gallery").on('click',function(e){
            e.preventDefault();
            conf = confirm("are you sure you want to remove this image from Gallery?");
            if(conf){
                container = $(this);
                imageId = $(this).attr('rel');
                $.ajax({
                    url:'<?php echo base_url() ?>cms/delete_from_gallery',
                    type:'post',
                    dataType:'json',
                    data:{'intID':imageId},
                    success:function(response)
                    {
                        //alert(response);
                        container.parent().remove();
                    }
                });
            }
        });

	});
	
	
</script>