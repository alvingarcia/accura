<script type="text/javascript">
    $(document).ready(function(){
        
        $("#save-branches").click(function(e){
            e.preventDefault();
            var procedures = Array();
            $('#branches-selected option').each(function(i, selected){ 
                procedures[i] = $(selected).val();
            });
            
            
            data = {'users':procedures,'areaid':<?php echo $item['areaId']; ?>};
            
            
            $.ajax({
                'url':'<?php echo base_url(); ?>area/submit_users/',
                'method':'post',
                'data':data,
                'dataType':'json',
                'success':function(ret){
                   alert("saved"); 
                }
            });
            
        });
        
        $("#load-branches").click(function(e){ 
             e.preventDefault();
                $('#branches-selector :selected').each(function(i, selected){ 
                    itemVal = $(selected).val(); 
                    itemText = $(selected).text();
                    $("#branches-selected").append($('<option>', { 
                                    value: itemVal,
                                    text : itemText
                                }));
                        $("#branches-selector option[value='"+itemVal+"']").remove();
                    
                });
                
                
        });
        
        $("#unload-branches").click(function(e){ 
             e.preventDefault();
                $('#branches-selected :selected').each(function(i, selected){ 
                    itemVal = $(selected).val(); 
                    itemText = $(selected).text();
                    $("#branches-selector").prepend($('<option>', { 
                                    value: itemVal,
                                    text : itemText
                                }));
                        $("#branches-selected option[value='"+itemVal+"']").remove();
                    
                });
                
                
        });
        
        
    });
    
</script>