<script type="text/javascript">
    $(document).ready(function(){
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='areaName']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='areaName']").html("<i class='fa fa-times-circle-o'></i> area field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='areaName']").html("area*");
            }
        
        });
        
        
        $("#validate-faculty").submit(function(e){
            
            
            if($("input[name='areaName']").val() == ""){
                $("input[name='areaName']").parent().addClass('has-error');
                $("label[for='areaName']").html("<i class='fa fa-times-circle-o'></i> area field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='areaName']").parent().removeClass('has-error');
                $("label[for='areaName']").html("area*");
            }
            
            
            return;
        });
        
        
       
    });
</script>