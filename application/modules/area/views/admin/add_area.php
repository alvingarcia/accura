<aside class="right-side">
<section class="content-header">
                    <h1>
                        Area
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Area</a></li>
                        <li class="active">Add Area</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New Area</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>area/submit_area" method="post" role="form">
                 <div class="box-body row">
                <?php  
                    echo cms_input('areaName','area','Enter Name','col-sm-6');
                    echo cms_input('roomNumber','Room Number','Enter Room Number','col-sm-6');
                    echo cms_hidden('laboratoryid',$this->session->userdata('laboratoryid')); 
                    echo cms_dropdown('areaType','Section',$types,'col-xs-6'); 
                    ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="add" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>