<aside class="right-side">
<section class="content-header">
                    <h1>
                        Area
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Area</a></li>
                        <li class="active">Add Area</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New Area</h3>
        </div>
       
            
            
                 <div class="box-body">
                     <div class="row">
                         <form id="validate-faculty" action="<?php echo base_url(); ?>area/edit_submit_area" method="post" role="form">
                        <?php  
                            echo cms_hidden('areaId',$item['areaId']);
                            echo cms_input('areaName','area','Enter Name','col-sm-6',$item['areaName']);
                            echo cms_input('roomNumber','Room Number','Enter Room Number','col-sm-6',$item['roomNumber']);
                            echo cms_dropdown('areaType','Section',$types,'col-xs-6',$item['areaType']); 
                            echo cms_hidden('laboratoryid',$item['laboratoryid']); 
                            ?>
                        <div class="form-group col-xs-12">
                            <input type="submit" value="update" class="btn btn-default  btn-flat">
                        </div>
                        <div style="clear:both"></div>
                    </form>
                </div>
            <hr />
            <div class="row">
                <div class="col-md-5">
                    <h4>Select Admin</h4>
                    <select style="height:300px" class="form-control" id="branches-selector" multiple>
                        <?php foreach($users as $l): ?>
                        <option value="<?php echo $l['id']; ?>"><?php echo $l['lastname']." ".$l['firstname']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-2">
                    <br /><br />
                    <a href="#" id="load-branches" class="btn btn-default  btn-flat btn-block">Load <i class="ion ion-arrow-right-c"></i> </a>
                    <a href="#" id="unload-branches" class="btn btn-default  btn-flat btn-block"><i class="ion ion-arrow-left-c"></i> Remove</a>
                    <a href="#" id="save-branches" class="btn btn-default  btn-flat btn-block">Save</a>

                </div>
                <div class="col-md-5">
                    <h4>Admins for Area</h4>
                    <select style="height:300px" class="form-control" id="branches-selected" multiple>
                       <?php foreach($users_selected as $l): ?>
                       <option value="<?php echo $l['id']; ?>"><?php echo $l['lastname']." ".$l['firstname']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                </div>
        </div>
</aside>