<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');		
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");	
		$this->load->model("user_model");
        $this->config->load('courses');
        $this->data['user'] = $this->session->all_userdata();
        $this->data['page'] = "adminusers";
    }
    
    
    public function add_area()
    {
        if($this->is_admin())
        {
            
            $this->data['page'] = "add_area";
            $this->data['opentree'] = "area";
            $test_types = $this->data_fetcher->fetch_table('test_type');
            $types = array();
            foreach($test_types as $tt)
            {
                $types[$tt['testTypeId']] = $tt['testTypeName'];
            }
            $this->data['types'] = $types;
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/add_area",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("area_validation_js",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function submit_users()
    {
        $post = $this->input->post();
        $aid = $post['areaid'];
        //print_r($post);
        $this->data_poster->deleteItems('area_admin',array('areaid'=>$aid));
        if(isset($post['users']))
        {
            //check per subject

            foreach($post['users'] as $usr)
            {
                    
                $data_subject['userid'] = $usr;
                $data_subject['areaid'] = $aid;
                $this->data_poster->post_data('area_admin',$data_subject);  

            }
        }
        
        $data['message'] = "Success";
        
        echo json_encode($data);
    }
    
    public function edit_area($id)
    {
        
        if($this->is_admin())
        {
           
            $this->data['users'] = $this->data_fetcher->getUsersNotSelectedArea($id,$this->session->userdata('laboratoryid'));
            $this->data['users_selected'] = $this->data_fetcher->getUsersSelectedArea($id,$this->session->userdata('laboratoryid'));
            $test_types = $this->data_fetcher->fetch_table('test_type');
            $types = array();
            foreach($test_types as $tt)
            {
                $types[$tt['testTypeId']] = $tt['testTypeName'];
            }
            $this->data['types'] = $types;
            
            $this->data['item'] = $this->data_fetcher->getItem('area',$id,'areaId');
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/edit_area",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("area_validation_js",$this->data); 
            $this->load->view("edit_area_conf",$this->data); 
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url());    
        
        
    }
    
   
    
    public function submit_area()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            //print_r($post);
           // $this->data_poster->log_action('User','Added a new User: '.$post['firstname']." ".$post['lastname'],'aqua');
            $this->data_poster->post_data('area',$post);
        }
        redirect(base_url()."area/view_all_areas");
            
    }
    
    public function edit_submit_area()
    {
        $post = $this->input->post();
        //print_r($post);
        $this->data_poster->post_data('area',$post,$post['areaId'],'areaId');
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        
        redirect(base_url()."area/view_all_areas");
            
    }
    
   
    public function view_all_areas()
    {
        if($this->is_admin())
        {
            $this->data['page'] = "view_all_areas";
            $this->data['opentree'] = "area";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/area_view",$this->data);
            $this->load->view("common/footer_datatables",$this->data); 
            $this->load->view("common/area_conf",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url());  
    }
    
    
   function generate_patient_id()
    {
        
        $post = $this->input->post();
        $data['patientID'] = $this->data_fetcher->generatePatientID($post['year']);
        
        echo json_encode($data);   
    }
    
    
    public function delete_area()
    {
        $data['message'] = "failed";
        
        if($this->is_admin()){
            
            $post = $this->input->post();
            $items = $this->data_fetcher->fetch_table('procedures',null,null,array('procedureSection'=>$post['id']));
            
            if(empty($items))
            {
                $this->data_poster->deleteItem('area',$post['id'],'areaId');
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            }
            else
            {
                $data['message'] = "Cannot delete area, it is in use by a test";
            }
            
        }
        echo json_encode($data);
    }
    
    public function patient_viewer($id, $sem = null)
    {
        if($this->is_admin())
        { 

            $this->load->view("common/header",$this->data);
            $this->load->view("admin/faculty_viewer",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("common/faculty_viewer_conf",$this->data); 
        }
        else
            redirect(base_url()); 
    }
    
    
    
    public function logged_in()
    {
        if($this->session->userdata('admin_logged'))
            return true;
        else
            return false;
    }
    
    public function is_admin()
    {
        if($this->session->userdata('roles')=="admin")
            return true;
        else
            return false;
    }


}