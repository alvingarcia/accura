<div class="container">
    <div class="box">
        <div class="box-header">
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center;font-size:2em;" colspan="2">QUEUE</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($queue as $test): ?>
                                 <tr>
                                    <td style="text-align:right;font-size:2em;width:30%;"><?php echo $test['appointmentId']; ?></td>
                                    <td style="font-size:2em;width:70%;"><?php echo $test['areaName']; ?></td>
                                 </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="text-align:center;font-size:2em;" colspan="2">NOW SERVING</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($serving as $test): ?>
                                 <tr>
                                    <td style="text-align:right;font-size:2em;width:30%;"><?php echo $test['appointmentId']; ?></td>
                                    <td style="font-size:2em;width:70%;"><?php echo $test['areaName']; ?></td>
                                 </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>