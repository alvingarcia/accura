<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Monitor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');		
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");	
		$this->load->model("user_model");
        $this->config->load('courses');
        $this->data['user'] = $this->session->all_userdata();
        $this->data['page'] = "adminusers";
    }
    
    
    public function queue()
    {
        //$this->data['area'] = $this->data_fetcher->getArea($areaid); 
        $this->data['queue'] = $this->data_fetcher->getQueueAll($this->session->userdata('laboratoryid'),false,10);
        $this->data['serving'] = $this->data_fetcher->getQueueAll($this->session->userdata('laboratoryid'),true,10);
        $this->load->view("common/header",$this->data);
        $this->load->view("admin/queue",$this->data);
        $this->load->view("common/footer",$this->data);

    }
    
    public function changeServed($serv,$areaid)
    {
        $s = current($this->data_fetcher->getQueue($this->session->userdata('laboratoryid'),true,$areaid));
        if($serv != $s['patientid'])
        {
            $data["message"] = "yes";
        }
        else
            $data["message"] = "no";
        
        echo json_encode($data);
    }
    
    
    
    
    
    public function logged_in()
    {
        if($this->session->userdata('admin_logged'))
            return true;
        else
            return false;
    }
    
    public function is_admin()
    {
        if($this->session->userdata('roles')=="admin")
            return true;
        else
            return false;
    }


}