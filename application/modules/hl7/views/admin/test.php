<?php 
    // Create a Message object from a HL7 string
    $msg = new Message("MSH|^~\\&|1|\rPID|||abcd|\r"); // Either \n or \r can be used as segment endings
    $pid = $msg->getSegmentByIndex(1);
    echo $pid->getField(3); // prints 'abcd'
    echo $msg->toString(true); // Prints entire HL7 string
?>