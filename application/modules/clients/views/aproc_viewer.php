<aside style="background-color: #ecf0f5;">
<section class="content-header">
                    <h1>
                        <img src="<?php echo $img_dir; ?>ax-logo.png" style="max-width:300px;" class="img-responsive" />
                        <small>Welcome <?php echo $user['patientFirstname']." ".$user['patientLastname']; ?>
                        </small>
                    </h1>
                    <h3>Test Details</h3>
                    
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Patient Portal</a></li>
                        <li class="active">View Test</li>
                    </ol>
                </section>
    <div class="content">
            <div class="alert alert-danger" style="display:none;">
                <i class="fa fa-ban"></i>
                <b>Alert!</b> Can not delete that user
            </div>
            <div class="box box-primary">
                <div class="box-body">
                    <p><strong>Date:</strong> <?php echo date("D M j,Y",strtotime($appointment['appointmentDate'])); ?></p>
            <h3><?php echo $patient['patientLastname'].", ".$patient['patientFirstname']; ?></h3>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Test</th>
                                <th>Specimen ID</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th><?php echo $test['procedureName']; ?></th>
                            <th><?php echo $test['testSpecimenId']; ?></th>
                        </tr>
                        </tbody>
                    </table>
                    <hr />
                    <?php if(!empty($results)): ?>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Result</th>
                                <th>conv</th>
                                <th>SI</th>
                                <th>Flag</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($results as $result): ?>
                        <tr>
                            <th><?php echo $result['rname']; ?></th>
                            <th><?php echo $result['rvalueconv']." ".$result['rr_conv']; ?></th>
                            <th><?php echo $result['rvaluesi']." ".$result['rr_si']; ?></th>
                            <th><?php echo $result['flag']; ?></th>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php else: ?>
                    <div class="alert alert-warning">Waiting for Results</div>
                    <?php endif; ?>
                    <hr />
                    <a class="btn btn-primary" href="<?php echo base_url()."clients/transactions/".$from; ?>">
                                <i class="fa fa-arrow-left"></i>Back</a> 
                </div>
        </div>
    </div>
</aside>

