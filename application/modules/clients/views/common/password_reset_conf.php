<script type="text/javascript">
    
    $(document).ready(function(){
        
        $("#reset-form").submit(function(e){		
            if($("#password").val()!=""){
                if($("#password").val().length >= 6){
                    if($("#password").val() != $("#confirm-password").val())
                    {
                        $(".status").html("Passwords do not match");
                        $(".status").css("display","block");
                        e.preventDefault();
                    }
                }
                else{
                    $(".status").html("Password must be at least 6 characters");
                    $(".status").css("display","block");
                    e.preventDefault();
                }
            }
            else{
                $(".status").html("Password cannot be empty");
                $(".status").css("display","block");
                e.preventDefault();
            }
        });
        
    });
</script>