<?php $d_open = '<div class="btn-group"><button type="button" class="btn btn-default">Actions</button><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button><ul class="dropdown-menu" role="menu">';
?>
<script type="text/javascript">
    
    $(document).ready(function(){
        //<i class="fa fa-file-pdf"></i>
    $('#faculty-table').dataTable( {
            "aLengthMenu":  [10, 20,50,100, 250, 500, 750, 1000],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url()."index.php/datatables/data_tables_transactions/".$user['id']."/".$transaction_type; ?>",
            "aoColumnDefs":[
                /*{
                    "aTargets":[8],
                    "mData": null,
                    "bSortable":false,
                    "mRender": function (data,type,row,meta) { return '<a href="<?php echo base_url(); ?>clients/aproc_view/'+row[0]+'/<?php echo $type; ?>">View Results</a>'; }
                },*/
                {
                    "aTargets":[0],
                    "bVisible": false 
                }
            ],
            "aaSorting": [[1,'desc']],
            "fnDrawCallback": function () {  
                $("#view-pdf").click(function(e){
                    e.preventDefault();
                    var checked = []
                    $("input[name='test[]']:checked").each(function ()
                    {
                        checked.push(parseInt($(this).val()));
                    });
                    if(checked.length == 0)
                    {
                        alert("select at least one test");
                    }
                    else{

                        $("#form-pdf").submit();
                    }

                });
            
            },
        } );
        
        
        
    });

</script>