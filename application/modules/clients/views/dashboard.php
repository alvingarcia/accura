<aside style="background-color: #ecf0f5;">
<section class="content-header">
                    <h1>
                        <img src="<?php echo $img_dir; ?>ax-logo.png" style="max-width:300px;" class="img-responsive" />
                        <small>Welcome <?php echo $user['patientFirstname']." ".$user['patientLastname']; ?></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
    <section class="content">
        <div class="box">
            <div class="box-header">
                <ul class="nav nav-tabs" role="tablist">
                         <?php foreach($test_type as $tt): ?>
                        <li class="<?php echo (isset($page) && $page==$tt['testTypeName']." Results")?'active':''; ?>"><a href="<?php echo base_url() ?>clients/transactions/<?php echo $tt['testTypeId'] ?>"><span><?php echo $tt['testTypeName']; ?> Results</span></a></li>
                        <?php endforeach; ?>
                    </ul>
            </div>
        <div class="box-body">
        <div class="jumbotron" style="background:#fff;">
            Welcome to the <strong>AccuraXpress</strong> Patient's Portal.
            <p style="font-size:.7em;">select type of test on the menu to view/check the status of your test results.</p>
        </div>
         </div>
        </div>

        
        
    </section>
</div>

