<aside style="background-color: #ecf0f5;">
<section class="content-header">
</section>
<section class="content">
        <div class="box">	
            <div class="box-header">
                <h4>Change Password</h4>
            </div>
			<div class="box-body">
				<form id="reset-form" action="<?php echo base_url(); ?>clients/submit_password_change/" method="POST">
					<div style="display:none;" class="status alert alert-danger"></div>
					<div class="row">
						<div class="col-sm-6 col-sm-offset-3">
                            <input type="text" placeholder="Password" id="password" name="password" class="form-control" minlength="6"/>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <input type="text" placeholder="Confirm Password" id="confirm-password" name="confirm-password" class="form-control" minlength="6"/>
                        </div>
						
					</div>
					<br />
                    <div class="text-center">
					<input type="submit" id="change-password" class="btn btn-primary" value="Update Password" />
                        </div>
				</form>
				
				
				
	   </div>
    </div>
    </section>
</aside>