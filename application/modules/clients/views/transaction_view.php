<aside style="background-color: #ecf0f5;">
<section class="content-header">
                     <h1>
                        <img src="<?php echo $img_dir; ?>ax-logo.png" style="max-width:300px;" class="img-responsive" />
                        <small>Welcome <?php echo $user['patientFirstname']." ".$user['patientLastname']; ?></small>
                    </h1>   
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Transactions</a></li>
                        <li class="active"><?php echo $ptest['testTypeName']." Results"; ?></li>
                    </ol>
                </section>
    <div class="content">
            <div class="alert alert-danger" style="display:none;">
                <i class="fa fa-ban"></i>
                <b>Alert!</b> Can not delete that user
            </div>
            <div class="box">
                <div class="box-header">
                    <ul class="nav nav-tabs" role="tablist">
                         <?php foreach($test_type as $tt): ?>
                        <li class="<?php echo (isset($page) && $page==$tt['testTypeName']." Results")?'active':''; ?>"><a href="<?php echo base_url() ?>clients/transactions/<?php echo $tt['testTypeId'] ?>"><span><?php echo $tt['testTypeName']; ?> Results</span></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="box-body">
                    <form id="form-pdf" method="post" action="<?php echo base_url().$pdf_action."/".$user['id']; ?>">
                    <input type="hidden" name="type" value="<?php echo $type; ?>" />
                    <div class="table-responsive">
                    <table id="faculty-table" class="table">
                        <thead><tr>
                            <th>id</th>
                            <th>Order Date</th>
                            <th>Patient ID</th>
                            <th>Patient Name</th>
                            <th>Gender</th>
                            <th>Birthdate/Age</th>
                            <th>Test</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>

                    </tbody></table>
                    </div>
                    </form>
                    <hr />
                    <a href="#" id="view-pdf" class="btn btn-primary">Print Results</a>
                </div><!-- /.box-body -->
                
                
            </div><!-- /.box -->
    </div>
</aside>