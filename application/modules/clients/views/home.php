<aside style="background-color: #ecf0f5;">
        <section class="content-header">
                    <h1>
                        <img src="<?php echo $img_dir; ?>ax-logo.png" style="max-width:300px;" class="img-responsive" />
                        <small>Welcome</small>
                    </h1>
                </section>
    <section class="content">
        <div class="box">
            <div class="box-header">
            </div>
        <div class="box-body">
            <div class="jumbotron" style="background:#fff;">
                Welcome to the <strong>AccuraXpress</strong> Patient's Portal.
                <p style="font-size:.7em;">There <?php echo ($number_patients != 1)?'are':'is'; ?> currently <?php echo $number_patients; ?> <?php echo ($number_patients != 1)?'patients':'patient'; ?> being served.</p>
            </div>
         </div>
        </div>
        <div class="box">
            <div  class="box-header">
                <h3>Promos &amp; Events</h3>
            </div>
            <div class="box-body">
                <?php foreach($content as $c): ?>
                    <div class="promo row">
                        <div class="col-sm-6">
                            <img src="<?php echo base_url().IMAGE_UPLOAD_DIR.$c['strPicture']; ?>" class="img-responsive" />
                       </div>
                        <div class="col-sm-6">
                            <h4><?php echo $c['strTitle']; ?></h4>
                            <p><?php echo $c['strContent']; ?></p>
                        </div>
                    </div>
                    <hr />
                <?php endforeach; ?>
            </div>

        
        
    </section>


