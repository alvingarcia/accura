<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Patient extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');		
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");	
		$this->load->model("user_model");
        $this->config->load('courses');
        $this->data['user'] = $this->session->all_userdata();
        $this->data['page'] = "adminusers";
    }
    
    
    public function add_patient()
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7)))
        {
            $docs = array();
            $doctors = $this->data_fetcher->fetchDoctors();
            foreach($doctors as $d)
            {
                $docs[$d['doctorId']] = $d['lastname']." ".$d['firstname']; 
            }
            
           $c2 = $this->data_fetcher->getDropdown('companies','companyId',array('companyName'),array('companyName','asc'));
            $c2[0] = "n/a";
            $this->data['companies'] =$c2;
            $this->data['gPid'] = $this->data_fetcher->generatePatientID(date('ymd'));
            $this->data['gPwd'] = $this->data_fetcher->generatePatientPassword();
            
            $hmo = array('0'=>'none');
            $hmos = $this->data_fetcher->fetch_table('hmo',array('hmoCode','asc'));
            foreach($hmos as $h)
            {
                $hmo[$h['hmoId']] = $h['hmoCode']; 
            }
         
            $this->data['hmo_list'] = $hmo;
            
            $this->data['docs'] = $docs;
            
            $this->data['page'] = "add_patient";
            $this->data['opentree'] = "patient";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/add_patient",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("patient_validation_js",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function add_patient_hr()
    {
        if($this->data_fetcher->is_specific(array(2,5)))
        {
            $docs = array();
            $doctors = $this->data_fetcher->fetchDoctors();
            foreach($doctors as $d)
            {
                $docs[$d['doctorId']] = $d['lastname']." ".$d['firstname']; 
            }
           
            $this->data['company'] =$this->session->userdata('companyAssoc');
            $this->data['gPid'] = $this->data_fetcher->generatePatientID(date('ymd'));
            $this->data['gPwd'] = $this->data_fetcher->generatePatientPassword();
            
            $hmo = array('0'=>'none');
            $hmos = $this->data_fetcher->fetch_table('hmo',array('hmoCode','asc'));
            foreach($hmos as $h)
            {
                $hmo[$h['hmoId']] = $h['hmoCode']; 
            }
         
            $this->data['hmo_list'] = $hmo;
            
            $this->data['docs'] = $docs;
            
            $this->data['page'] = "add_patient";
            $this->data['opentree'] = "patient";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/add_patient_hr",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("patient_validation_js",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function get_allergies_json()
    {
        $ret = array();
        $all = 
        $this->db
             ->group_by('desc')
             ->where('histType','allergy')
             ->get('patient_history')
             ->result_array();
        
        foreach($all as $a)
        {
            $r['value'] = $r['text'] = $a['desc'];
            $ret[] = $r;
        }
        
        $ret = json_encode($ret);
        $fp = fopen('allergies.json', 'w');
        fwrite($fp, $ret);
        fclose($fp);
    }
    
    public function get_illnesses_json()
    {
        $ret = array();
        $all = 
        $this->db
             ->group_by('desc')
             ->where('histType','illness')
             ->get('patient_history')
             ->result_array();
        
        foreach($all as $a)
        {
            $r['value'] = $r['text'] = $a['desc'];
            $ret[] = $r;
        }
        
        $ret = json_encode($ret);
        
        $fp = fopen('illnesses.json', 'w');
        fwrite($fp, $ret);
        fclose($fp);
    }
    
    public function edit_patient($id)
    {
        
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,5)))
        {
            $this->data['item'] = $this->data_fetcher->getItem('patient',$id,'id');
            if($this->session->userdata('roles') == "company hr" && $this->data['item']['companyid'] != $this->session->userdata('companyAssoc'))
                redirect(base_url());
            
            $this->get_allergies_json();
            $this->get_illnesses_json();
            
            $allergy = $this->db
                ->get_where('patient_history',array('pHpid'=>$id,'histType'=>'allergy'))
                ->result_array();
            
            $ret_all_arr = array();
            $ret_all_val = "";
            $i = 1;
            foreach($allergy as $all)
            {
                $r['text']  = $r['value'] = $all['desc'];
                $ret_all_arr[] = $r;
                $ret_all_val.=$all['desc'];
                if($i < count($allergy))
                    $ret_all_val.=",";
                $i++;
                    
            }
            
            $illness = $this->db
                ->get_where('patient_history',array('pHpid'=>$id,'histType'=>'illness'))
                ->result_array();
            
            $ret_ill_arr = array();
            $ret_ill_val = "";
            $i = 1;
            foreach($illness as $all)
            {
                $r['text']  = $r['value'] = $all['desc'];
                $ret_ill_arr[] = $r;
                $ret_ill_val.=$all['desc'];
                if($i < count($illness))
                    $ret_ill_val.=",";
                $i++;
            }
            
            $this->data['ill_arr'] = json_encode($ret_ill_arr);
            $this->data['ill_val'] = $ret_ill_val;
            $this->data['all_arr'] = json_encode($ret_all_arr);
            $this->data['all_val'] = $ret_all_val;
            
    
            $this->data['role'] = $this->session->userdata('roles');
            $docs = array();
            $doctors = $this->data_fetcher->fetchDoctors();
            foreach($doctors as $d)
            {
                $docs[$d['doctorId']] = $d['lastname']." ".$d['firstname']; 
            }
            $this->data['docs'] = $docs;
            
            $c2 = $this->data_fetcher->getDropdown('companies','companyId',array('companyName'),array('companyName','asc'));
            $c2[0] = "n/a";
            $this->data['companies'] =$c2;
            
            
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/edit_patient",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("patient_validation_js",$this->data); 
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url());    
        
        
    }
    
    public function transactions($id,$type=1)
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,5,8)))
        {
            $this->data['test_type'] = $this->data_fetcher->fetch_table('test_type');
            $this->data['vitals'] = $this->data_fetcher->getVitals($id);
            $this->data['patient'] = $this->data_fetcher->getItem('patient',$id,'id');
            $this->data['pid'] = $id;
            $this->data['type'] = $type;
            $ptest = $this->data['ptest'] = $this->data_fetcher->getItem('test_type',$type,'testTypeId');
            if($ptest['testTypeName'] == "Consultation")
            {
                $this->data['pdf_action'] = "pdf/print_consultation";
            }
            elseif($ptest['testTypeName'] == "Imaging")
            {
                $this->data['pdf_action'] = "clients/imaging_results";
            }
            else
            {
                $this->data['pdf_action'] = "pdf/print_results";
            }
            $this->data['transaction_type'] = $type;
            $this->data["page"] = $this->data['ptest']['testTypeName']." Results";
            $this->load->view('common/header',$this->data);
            $this->load->view('transaction_view',$this->data);
            $this->load->view('common/footer',$this->data);
            $this->load->view('common/transaction_view_conf',$this->data);
        }
        else
            redirect(base_url()."clients");
    }
    
    public function edit_hmo($id)
    {
        
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,5)))
        {
            $this->data['item'] = $this->data_fetcher->getItem('patient',$id,'id');
            if($this->session->userdata('roles') == "company hr" && $this->data['item']['companyid'] != $this->session->userdata('companyAssoc'))
                redirect(base_url());
            
            $hmo = array('0'=>'none');
            $hmos = $this->data_fetcher->fetch_table('hmo',array('hmoCode','asc'));
            foreach($hmos as $h)
            {
                $hmo[$h['hmoId']] = $h['hmoCode']; 
            }
         
            $this->data['hmo_list'] = $hmo;
            $this->data['hmo'] = $this->data_fetcher->getItem('patient_hmo',$id,'patientid');
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/patient_hmo",$this->data);
            $this->load->view("common/footer",$this->data); 
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url());    
        
        
    }
    
    public function submit_patient()
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7)))
        {
            $post = $this->input->post();
            $hmo['hmoid'] = $post['hmoid'];
            unset($post['hmoid']);
            $hmo['policyNo'] = $post['policyNo'];
            unset($post['policyNo']);
            $hmo['renewed'] = date("Y-m-d",strtotime($post['renewed']));
            unset($post['renewed']);
            $hmo['expires'] = date("Y-m-d",strtotime($post['expires']));
            unset($post['expires']);
        
            //print_r($post);
           // $this->data_poster->log_action('User','Added a new User: '.$post['firstname']." ".$post['lastname'],'aqua');
            $pw = $post['password'];
            $post['password'] = pw_hash($post['password'],'aqua');
            $post['birthday'] = date("Y-m-d",strtotime($post['birthday']));
            $this->data_poster->post_data('patient',$post);
            $id = $this->db->insert_id();
            $hmo['patientid'] = $this->db->insert_id();
            $this->data_poster->post_data('patient_hmo',$hmo);
            $message['strSubject'] = "AccuraXpress Patient Portal";
            $message['strMessage'] = "Welcome to AccuraXpress! ".$post['patientFirstname']." ".$post['patientLastname']."<br /><a href='".base_url()."clients/login'>Click here to login to Patient Portal</a><br />Patientid:".$post['patientid']." password: ".$pw;
            $this->user_model->send_notification_email_client($message,$post['email']);
            
            //email to client here after adding
        }
        redirect(base_url()."vitals/add_vitals/".$id);
            
    }
    
    public function edit_submit_patient()
    {
        $post = $this->input->post();
        //=print_r($post);
        
        
        $this->data_poster->deleteItem('patient_history',$post['id'],'pHpid');
        
        $all = explode(",",$post['allergies']);
        unset($post['allergies']);
        foreach($all as $a)
        {
            $data['desc'] = $a;
            $data['pHpid'] = $post['id'];
            $data['histType'] = "allergy";
                
        
            $this->data_poster->post_data('patient_history',$data);
        }
        
        
        $all = explode(",",$post['illnesses']);
        unset($post['illnesses']);
        foreach($all as $a)
        {
            $data['desc'] = $a;
            $data['pHpid'] = $post['id'];
            $data['histType'] = "illness";
                
            $this->data_poster->post_data('patient_history',$data);
        }
        
        
        
        $post['password'] = pw_hash($post['password']);
        $post['birthday'] = date("Y-m-d",strtotime($post['birthday']));
        $this->data_poster->post_data('patient',$post,$post['id'],'id');
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        
        redirect(base_url()."patient/edit_patient/".$post['id']);
            
    }
    
    public function edit_submit_patient_hmo()
    {
        $post = $this->input->post();
        $post['renewed'] = date("Y-m-d",strtotime($post['renewed']));
        $post['expires'] = date("Y-m-d",strtotime($post['expires']));
        //print_r($post);
        //die();
        if(isset($post['patienthmoId']) && $post['patienthmoId']!="") 
            $this->data_poster->post_data('patient_hmo',$post,$post['patienthmoId'],'patienthmoId');
        else
        {
            unset($post['patienthmoId']);
            $this->data_poster->post_data('patient_hmo',$post);
        }
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        
        redirect(base_url()."patient/edit_hmo/".$post['patientid']);
    }
    
    public function view_all_patients()
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7,8)))
        {
            $this->data['page'] = "view_all_patients";
            $this->data['opentree'] = "patient";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/patient_view",$this->data);
            $this->load->view("common/footer_datatables",$this->data); 
            $this->load->view("common/patient_conf",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url());  
    }
    
    
   function generate_patient_id()
    {
        
        $post = $this->input->post();
        $data['patientID'] = $this->data_fetcher->generatePatientID($post['year']);
        
        echo json_encode($data);   
    }
    
    public function check_patient_id()
    {
        $post = $this->input->post();
        $patient = $this->data_fetcher->getItem('patient',$post['id'],'patientid');
        if(empty($patient))
            $data['duplicate'] = false;
        else
            $data['duplicate'] = true;
        
        echo json_encode($data);
    }
    
    
    public function delete_patient()
    {
        $data['message'] = "failed";
        
        if($this->is_admin()){
            $post = $this->input->post();
                $this->data_poster->deleteItem('patient',$post['id'],'id');
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            
        }
        echo json_encode($data);
    }
    /*
    public function patient_history($id)
    {
        if($this->is_admin())
        { 
            $this->data['patient'] = $this->data_fetcher->getItem('patient',$id,'id');
            $this->data['uid'] = $id;
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/patient_history",$this->data);
            $this->load->view("common/footer",$this->data);
            $this->load->view("common/patient_history_conf",$this->data);
        }
        else
            redirect(base_url()); 
    }
    */
    
    
    public function logged_in()
    {
        if($this->session->userdata('admin_logged'))
            return true;
        else
            return false;
    }
    
    public function is_admin()
    {
        if($this->session->userdata('roles')=="admin")
            return true;
        else
            return false;
    }


}