<aside class="right-side">
<section class="content-header">
                    <h1>
                        Patient
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Patient</a></li>
                        <li class="active">Add Patient</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New Patient</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>patient/submit_patient" method="post" role="form">
                 <div class="box-body">
                     <div class="form-group col-xs-4">
                        <label for="patientFirstname">First Name*</label>
                        <input type="text" name="patientFirstname" class="form-control" id="patientFirstname" placeholder="Enter First Name">
                    </div>
                    <div class="form-group col-xs-4">
                        <label for="patientLastname">Last Name*</label>
                        <input type="text" name="patientLastname" class="form-control" id="patientLastname" placeholder="Enter Last Name">
                    </div>
                     <div class="form-group col-xs-4">
                        <label for="patientMiddlename">Middle Name</label>
                        <input type="text" name="patientMiddlename" class="form-control" id="patientMiddlename" placeholder="Enter Middle Name">
                    </div>
                     <div class="form-group col-xs-6">
                        <label for="patientid">Patient ID</label>
                          <input type="text" disabled name="patientid" id="patientid" class="form-control" placeholder="Enter Patient ID" value="<?php echo $gPid; ?>">
                    </div>
                     <?php echo cms_hidden('patientid',$gPid); ?>
                     <div class="form-group col-xs-6">
                        <label for="password">Password</label>
                        <input type="text" name="password" class="form-control" id="password" placeholder="Enter Password" value="<?php echo $gPwd; ?>">
                    </div>
                 <div class="form-group col-xs-6">
                        <label for="alias">Alias (Nickname)</label>
                        <input type="text" name="alias" class="form-control" id="alias" placeholder="Enter Alias">
                </div>
                 <?php  echo cms_date('birthday','Date of Birth','Date of Birth','col-xs-6',date("m/d/Y"));  ?>
                
                 <div class="form-group col-xs-6">
                    <label for="sex">Gender</label>
                    <select class="form-control" name="sex" > 
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </div> 
                <div class="form-group col-xs-6">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" > 
                        <option value="single">Single</option>
                        <option value="married">Married</option>
                        <option value="divorced">Divorced</option>
                        <option value="annulled">Annulled</option>
                        <option value="widowed">Widowed</option>
                    </select>
                </div> 
                
                <div class="form-group col-xs-12">
                    <label for="address">Address</label>
                    <textarea name="address" class="form-control" id="address" placeholder="Enter Address"></textarea>
                </div>
                <div class="form-group col-xs-6">
                        <label for="addressCity">City/Town</label>
                        <input type="text" name="addressCity" class="form-control" id="addressCity" placeholder="Enter City or Town">
                </div>
                <div class="form-group col-xs-6">
                        <label for="addressProvince">Province/Region/State</label>
                        <input type="text" name="addressProvince" class="form-control" id="addressProvince" placeholder="Enter Province Region or State">
                </div>
                <div class="form-group col-xs-8">
                        <label for="addressCountry">Country</label>
                        <input type="text" name="addressCountry" class="form-control" id="addressCountry" placeholder="Enter Country">
                </div>
                
                
                <div class="form-group col-xs-4">
                        <label for="occupation">Occupation</label>
                        <input type="text" name="occupation" class="form-control" id="occupation" placeholder="Enter Occupation">
                </div>
                <div class="form-group col-xs-6">
                        <label for="contactno">Contact Number</label>
                        <input type="text" name="contactno" class="form-control" id="contactno" placeholder="Enter Contact Number">
                </div>
                <div class="form-group col-xs-6">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email Address">
                </div>
                
                <div class="form-group col-xs-6">
                        <label for="personToNotify">Person to Notify in case of Emergency</label>
                        <input type="text" name="personToNotify" class="form-control" id="personToNotify" placeholder="Enter Full Name">
                </div>
                <div class="form-group col-xs-6">
                        <label for="personToNotifyContactNo">Emergency Contact Number</label>
                        <input type="text" name="personToNotifyContactNo" class="form-control" id="personToNotifyContactNo" placeholder="Enter Contact Number">
                </div>
                <?php echo cms_hidden('companyid',$company); ?>
                <hr /> 
                     
                <div class="col-xs-12">
                    <h4>HMO Information</h4>
                </div>
                <?php
                      
                    echo cms_dropdown('hmoid','Select HMO',$hmo_list,'col-xs-6'); 
                    echo cms_input('policyNo','Policy Number','Enter Policy Number','col-xs-6');
                    echo cms_date('renewed','Date Renewed','Date of Renewal','col-xs-6');
                    echo cms_date('expires','Date Expires','Date of Expiry','col-xs-6');
                ?>
                
                <div class="form-group col-xs-12">
                    <input type="submit" value="add" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>