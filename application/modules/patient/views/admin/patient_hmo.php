<aside class="right-side">
<section class="content-header">
                    <h1>
                        Patient
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Patient</a></li>
                        <li class="active">HMO Information</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Patient's HMO Information</h3>
                <hr />
                <a class="btn btn-app" href="<?php echo base_url()."patient/edit_patient/".$item['id']; ?>">
                    <i class="fa fa-wheelchair"></i>
                    Personal information
                </a>
                <hr />
        </div>
        <div class="box-body row">
             <form id="validate-hmo" action="<?php echo base_url(); ?>patient/edit_submit_patient_hmo" method="post" role="form">
             <?php  
                    $phid = isset($hmo['patienthmoId'])?$hmo['patienthmoId']:null;
                    $limit = isset($hmo['hmoLimit'])?$hmo['hmoLimit']:null;
                    $hmo_id = isset($hmo['hmoid'])?$hmo['hmoid']:null;
                    $policy_number = isset($hmo['policyNo'])?$hmo['policyNo']:null;
                    $renewed = isset($hmo['renewed'])?date("m/d/Y",strtotime($hmo['renewed'])):date('m/d/Y');
                    $expires = isset($hmo['expires'])?date("m/d/Y",strtotime($hmo['expires'])):date('m/d/Y');
            
                    
                    echo cms_hidden('patienthmoId',$phid);
                    echo cms_hidden('patientid',$item['id']);
                    echo cms_dropdown('hmoid','Select HMO',$hmo_list,'col-xs-6',$hmo_id); 
                    echo cms_input('policyNo','Policy Number','Enter Policy Number','col-xs-6',$policy_number);
                    echo cms_number('hmoLimit','HMO Limit','Enter Limit','col-xs-6',$limit);
                    echo cms_date('renewed','Date Renewed','Date of Renewal','col-xs-6',$renewed);
                    echo cms_date('expires','Date Expires','Date of Expiry','col-xs-6',$expires);
             ?>
                  <div class="form-group col-xs-12">
                    <input type="submit" value="update" class="btn btn-default  btn-flat">
                </div>
            </form>
        </div>
    </div>
</div>
</aside>