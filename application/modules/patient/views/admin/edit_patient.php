<aside class="right-side">
<section class="content-header">
                    <h1>
                        Patient
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Patient</a></li>
                        <li class="active">Edit Patient</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Edit Patient</h3>
                <hr />
                <a class="btn btn-app" href="<?php echo base_url()."patient/edit_hmo/".$item['id']; ?>">
                    <i class="fa fa-credit-card"></i>
                    HMO information
                </a>
                <hr />
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>patient/edit_submit_patient" method="post" role="form">
                <input type="hidden" value="<?php echo $item['id']; ?>" name="id" />
                 <div class="box-body">
                     <div class="form-group col-xs-4">
                        <label for="patientFirstname">First Name*</label>
                        <input type="text" name="patientFirstname" class="form-control" id="patientFirstname" placeholder="Enter First Name" value="<?php echo $item['patientFirstname']; ?>">
                    </div>
                    <div class="form-group col-xs-4">
                        <label for="patientLastname">Last Name*</label>
                        <input type="text" name="patientLastname" class="form-control" id="patientLastname" placeholder="Enter Last Name" value="<?php echo $item['patientLastname']; ?>">
                    </div>
                     <div class="form-group col-xs-4">
                        <label for="patientMiddlename">Middle Name</label>
                        <input type="text" name="patientMiddlename" class="form-control" id="patientMiddlename" placeholder="Enter Middle Name" value="<?php echo $item['patientMiddlename']; ?>">
                    </div>
                     <div class="form-group col-xs-6">
                        <label for="patientid">Patient ID</label>
                        <input disabled type="text" name="patientid" id="patientid" class="form-control" placeholder="Enter Patient ID" value="<?php echo $item['patientid']; ?>">  
                    </div>
                     <div class="form-group col-xs-6">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password" value="<?php echo pw_unhash($item['password']); ?>">
                    </div>
                 <div class="form-group col-xs-6">
                        <label for="alias">Alias (Nickname)</label>
                        <input type="text" name="alias" class="form-control" id="alias" placeholder="Enter Alias" value="<?php echo $item['alias']; ?>">
                </div>
                 <?php  echo cms_date('birthday','Date of Birth','Date of Birth','col-xs-6',date("m/d/Y",strtotime($item['birthday'])));  ?>
                
                 <div class="form-group col-xs-6">
                    <label for="sex">Gender</label>
                    <select class="form-control" name="sex" > 
                        <option <?php echo ($item['sex']=="male")?'selected':''; ?> value="male">Male</option>
                        <option <?php echo ($item['sex']=="female")?'selected':''; ?> value="female">Female</option>
                    </select>
                </div> 
                <div class="form-group col-xs-6">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" > 
                        <option <?php echo ($item['status']=="single")?'selected':''; ?> value="single">Single</option>
                        <option <?php echo ($item['status']=="married")?'selected':''; ?> value="married">Married</option>
                        <option <?php echo ($item['status']=="divorced")?'selected':''; ?> value="divorced">Divorced</option>
                        <option <?php echo ($item['status']=="annulled")?'selected':''; ?> value="annulled">Annulled</option>
                        <option <?php echo ($item['status']=="widowed")?'selected':''; ?> value="widowed">Widowed</option>
                    </select>
                </div>  
                <div class="form-group col-xs-12">
                    <label for="address">Address</label>
                    <textarea name="address" class="form-control" id="address" placeholder="Enter Address"><?php echo $item['address']; ?></textarea>
                </div>
                <div class="form-group col-xs-6">
                        <label for="addressCity">City/Town</label>
                        <input value="<?php echo $item['addressCity']; ?>" type="text" name="addressCity" class="form-control" id="addressCity" placeholder="Enter City or Town">
                </div>
                <div class="form-group col-xs-6">
                        <label for="addressProvince">Province/Region/State</label>
                        <input value="<?php echo $item['addressProvince']; ?>" type="text" name="addressProvince" class="form-control" id="addressProvince" placeholder="Enter Province Region or State">
                </div>
                <div class="form-group col-xs-8">
                        <label for="addressCountry">Country</label>
                        <input value="<?php echo $item['addressCountry']; ?>" type="text" name="addressCountry" class="form-control" id="addressCountry" placeholder="Enter Country">
                </div>
                <div class="form-group col-xs-4">
                        <label for="occupation">Occupation</label>
                        <input type="text" name="occupation" class="form-control" id="occupation" placeholder="Enter Occupation" value="<?php echo $item['occupation']; ?>">
                </div>
                <div class="form-group col-xs-6">
                        <label for="contactno">Contact Number</label>
                        <input type="text" name="contactno" class="form-control" id="contactno" placeholder="Enter Contact Number" value="<?php echo $item['contactno']; ?>">
                </div>
                <div class="form-group col-xs-6">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email Address" value="<?php echo $item['email']; ?>">
                </div>
                
                <div class="form-group col-xs-6">
                        <label for="personToNotify">Person to Notify in case of Emergency</label>
                        <input type="text" name="personToNotify" class="form-control" id="personToNotify" placeholder="Enter Full Name" value="<?php echo $item['personToNotify']; ?>">
                </div>
                <div class="form-group col-xs-6">
                        <label for="personToNotifyContactNo">Emergency Contact Number</label>
                        <input type="text" name="personToNotifyContactNo" class="form-control" id="personToNotifyContactNo" placeholder="Enter Contact Number" value="<?php echo $item['personToNotifyContactNo']; ?>">
                </div>
                <?php 
                     if($role != "company hr")
                     echo cms_dropdown('companyid','Select Company',$companies,'col-xs-6',$item['companyid']); 
                     
                ?>
                <div class="col-sm-12">
                    <h3>Medical History</h3>     
                </div>
                <div class="col-sm-12">
                    <label>Allergies</label><br />
                <input type="text" multiple class="allergies" data-url="<?php echo base_url(); ?>allergies.json"  value="<?php echo $all_val ?>" data-initial-value='<?php echo $all_arr; ?>' data-load-once="true" data-user-option-allowed="true" name="allergies" />

                </div>
                <div class="col-sm-12">
                    <label>Illnesses</label><br />
                <input type="text" multiple class="illnesses" data-url="<?php echo base_url(); ?>illnesses.json" value="<?php echo $ill_val ?>" data-initial-value='<?php echo $ill_arr; ?>' data-load-once="true" data-user-option-allowed="true" name="illnesses" />

                </div>
               
                <div class="form-group col-xs-12">
                     <hr />
                    <input type="submit" value="update" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>