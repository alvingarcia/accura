<aside style="background-color: #ecf0f5;" class="right-side">
<section class="content-header">
                     <h1>
                         
                        <img src="<?php echo $img_dir; ?>ax-logo.png" style="max-width:300px;" class="img-responsive" />
                        <small>
                            <a href="<?php echo base_url(); ?>patient/view_all_patients" class="btn btn-app">
                            <i class="fa fa-arrow-left"></i>
                            Back to Patients
                            </a>
                            <a href="<?php echo base_url(); ?>vitals/add_vitals/<?php echo $patient['id']; ?>" class="btn btn-app">
                            <i class="fa fa-plus"></i>
                            New Vitals
                            </a>
                         
                            </small>
                    </h1>   
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Transactions</a></li>
                        <li class="active"><?php echo $ptest['testTypeName']." Results"; ?></li>
                    </ol>
                </section>
    <div class="content">
            <div class="alert alert-danger" style="display:none;">
                <i class="fa fa-ban"></i>
                <b>Alert!</b> Can not delete that user
            </div>
            <div class="box">
                <div class="box-header">
                    <h4><?php echo $patient['patientLastname'].", ".$patient['patientFirstname']; ?> Age: <?php echo calculate_age($patient['birthday']); ?></h4>
                    <hr />
                    <ul class="nav nav-tabs" role="tablist">
                         <?php foreach($test_type as $tt): ?>
                        <li class="<?php echo (isset($page) && $page==$tt['testTypeName']." Results")?'active':''; ?>"><a href="<?php echo base_url() ?>patient/transactions/<?php echo $pid; ?>/<?php echo $tt['testTypeId'] ?>"><span><?php echo $tt['testTypeName']; ?> Results</span></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="box-body">
                    <form target="_blank" id="form-pdf" method="post" action="<?php echo base_url().$pdf_action."/".$pid; ?>">
                    <input type="hidden" name="type" value="<?php echo $type; ?>" />
                    <div class="table-responsive">
                    <table id="faculty-table" class="table">
                        <thead><tr>
                            <th>id</th>
                            <th>Order Date</th>
                            <th>Patient ID</th>
                            <th>Patient Name</th>
                            <th>Gender</th>
                            <th>Birthdate/Age</th>
                            <th>Test</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    </div>
                    </form>
                    <hr />
                    <a href="#" id="view-pdf" class="btn btn-primary">Print Results</a>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <h3>Vitals History</h3>
                </div>
                <div class="box-body">
                    <table id="vitals-table" class="table">
                        <thead><tr>
                            <th>Date</th>
                            <th>Height</th>
                            <th>Weight</th>
                            <th>BP</th>
                            <th>BMI</th>
                            <th>ECG</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach($vitals as $vital): ?>
                            <tr>
                                <td><?php echo date("M j, Y h:ia",strtotime($vital['vitalsDate'])); ?></td>
                                <td><?php echo $vital['vitalsHeight']; ?> cm</td>
                                <td><?php echo $vital['vitalsWeight']; ?> kg</td>
                                <td><?php echo $vital['vitalsBp']; ?></td>
                                <td><?php echo round($vital['vitalsWeight']/(($vital['vitalsHeight']/100)^2),2); ?></td>
                                <td><?php echo $vital['vitalsECG']; ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
</aside>