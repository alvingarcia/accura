<script type="text/javascript">
    $(document).ready(function(){
        
        $('.allergies').fastselect();
        $('.illnesses').fastselect();
        
        var dup = false;
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='patientFirstname']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='patientFirstname']").html("<i class='fa fa-times-circle-o'></i> Firstname field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='patientFirstname']").html("Firstname*");
            }
        
        });
        $("input[name='patientid']").blur(function(){
            field = $(this);
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='patientid']").html("<i class='fa fa-times-circle-o'></i> Patient ID field is required");
            }
            else
            {
                $.ajax({
                'url':'<?php echo base_url(); ?>patient/check_patient_id',
                'method':'post',
                'data':{'id':$("#patientid").val()},
                'dataType':'json',
                'success':function(ret){
                    dup = ret.duplicate;
                    
                    if(dup)
                    {
                         field.parent().addClass('has-error');
                        $("label[for='patientid']").html("<i class='fa fa-times-circle-o'></i> Duplicate Value");
                    }
                    else
                    {
                        field.parent().removeClass('has-error');
                         $("label[for='patientid']").html("Patient ID*");
                    }
                    
                }

            });
            }
            
            
            
        });
                                          
        $("input[name='patientLastname']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='patientLastname']").html("<i class='fa fa-times-circle-o'></i> Lastname field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='patientLastname']").html("Lastname*");
            }
        });
        
        $("input[name='password']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='password']").html("<i class='fa fa-times-circle-o'></i> Password field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='password']").html("Password*");
            }
        });
        
        $("input[name='email']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='email']").html("<i class='fa fa-times-circle-o'></i> Email field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='email']").html("Email*");
            }
        });
        $("#validate-faculty").submit(function(e){
            
            
            if($("input[name='patientFirstname']").val() == ""){
                $("input[name='patientFirstname']").parent().addClass('has-error');
                $("label[for='patientFirstname']").html("<i class='fa fa-times-circle-o'></i> Firstname field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='patientFirstname']").parent().removeClass('has-error');
                $("label[for='patientFirstname']").html("Firstname*");
            }
            if($("input[name='patientLastname']").val() == ""){
                $("input[name='patientLastname']").parent().addClass('has-error');
                 $("label[for='patientLastname']").html("<i class='fa fa-times-circle-o'></i> Lastname field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='patientLastname']").parent().removeClass('has-error');
                $("label[for='patientLastname']").html("Lastname*");
                
            }
            if($("input[name='email']").val() == ""){
                $("input[name='email']").parent().addClass('has-error');
                 $("label[for='email']").html("<i class='fa fa-times-circle-o'></i> Email field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='email']").parent().removeClass('has-error');
                $("label[for='email']").html("Email*");
                
            }
            
            if($("input[name='password']").val() == ""){
                $("input[name='password']").parent().addClass('has-error');
                 $("label[for='password']").html("<i class='fa fa-times-circle-o'></i> Password field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='password']").parent().removeClass('has-error');
                $("label[for='password']").html("Password*");
                
            }
            
            if($("input[name='patientid']").val() == "" )
            {
                $("input[name='patientid']").parent().addClass('has-error');
                $("label[for='patientid']").html("<i class='fa fa-times-circle-o'></i> Patient ID field is required");
            }
            else
            {
                $.ajax({
                'url':'<?php echo base_url(); ?>patient/check_patient_id',
                'method':'post',
                'data':{'id':$("#patientid").val()},
                'dataType':'json',
                'success':function(ret){
                    dup = ret.duplicate;
                    
                    if(dup)
                    {
                         $("input[name='patientid']").parent().addClass('has-error');
                        $("label[for='patientid']").html("<i class='fa fa-times-circle-o'></i> Duplicate Value");
                    }
                    else
                    {
                        $("input[name='patientid']").parent().removeClass('has-error');
                         $("label[for='patientid']").html("Patient ID*");
                    }
                    
                }

            });
            }
            
            
            
           
            
            
            if(dup)
            {
                e.preventDefault();
            }
            
            return;
        });
        
        
        $("#generatepid").click(function(){
           
            $.ajax({
                'url':'<?php echo base_url(); ?>patient/generate_patient_id',
                'method':'post',
                'data':{'year':$(this).attr("rel")},
                'dataType':'json',
                'success':function(ret){
                    $("#patientid").val(ret.patientID);
                }
            
            });
        });
    });
</script>