<aside class="right-side">
<section class="content-header">
                    <h1>
                        Edit Template
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Edit Template</a></li>
                        <li class="active">Edit Template</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Edit Template</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>rx/edit_submit_rx" method="post" role="form">
                 <div class="box-body row">
                        <?php echo cms_hidden('rxId',$item['rxId']); ?>
                        <div class="col-sm-4">
                            <label for="templateName">Template Name</label>
                            <input value="<?php echo $item['templateName']; ?>" type="text" name="templateName" class="form-control" />
                        </div>
                        <div class="col-sm-4">
                            <label for="rxMedicine">Medicine</label>
                            <input value="<?php echo $item['rxMedicine']; ?>" type="text" name="rxMedicine" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <label for="rxDosage">Dosage</label>
                            <input value="<?php echo $item['rxDosage']; ?>" type="number" name="rxDosage" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <label for="rxUnit">Unit</label>
                            <input value="<?php echo $item['rxUnit']; ?>" type="text" name="rxUnit" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <label for="rxTimes">Freq</label>
                            <input value="<?php echo $item['rxTimes']; ?>" type="number" name="rxTimes" class="form-control" />
                        </div>
                        <div class="col-sm-2">
                            <label for="rxDays">Days</label>
                            <input value="<?php echo $item['rxDays']; ?>" type="number" name="rxDays" class="form-control" />
                        </div>
                        <div class="col-sm-6">
                            <label for="rxSchedule">Schedule</label>
                            <input value="<?php echo $item['rxSchedule']; ?>" placeholder="Before Meal, After Meal, 10:00am 1:00pm, etc..." type="text" name="rxSchedule" class="form-control" />
                        </div> 
                <div class="form-group col-xs-12">
                    <hr />
                    <input type="submit" value="update" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>