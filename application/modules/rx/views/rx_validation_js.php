<script type="text/javascript">
    $(document).ready(function(){
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='templateName']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='templateName']").html("<i class='fa fa-times-circle-o'></i> Name field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='templateName']").html("Template Name*");
            }
        
        });
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='rxMedicine']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='rxMedicine']").html("<i class='fa fa-times-circle-o'></i> Medicine field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='rxMedicine']").html("Medicine*");
            }
        
        });
        
       
        $("#validate-faculty").submit(function(e){
            
            
            if($("input[name='templateName']").val() == ""){
                $("input[name='templateName']").parent().addClass('has-error');
                $("label[for='templateName']").html("<i class='fa fa-times-circle-o'></i> Name field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='templateName']").parent().removeClass('has-error');
                $("label[for='templateName']").html("Template Name*");
            }
            if($("input[name='rxMedicine']").val() == ""){
                $("input[name='rxMedicine']").parent().addClass('has-error');
                $("label[for='rxMedicine']").html("<i class='fa fa-times-circle-o'></i> Medicine field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='rxMedicine']").parent().removeClass('has-error');
                $("label[for='rxMedicine']").html("Medicine*");
            }
            
            
            
            return;
        });
        
        
       
    });
</script>