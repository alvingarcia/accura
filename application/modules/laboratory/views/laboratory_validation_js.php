<script type="text/javascript">
    $(document).ready(function(){
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='laboratoryBranch']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='laboratoryBranch']").html("<i class='fa fa-times-circle-o'></i> Branch Name field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='laboratoryBranch']").html("Branch Name*");
            }
        
        });
        
        $("#validate-faculty").submit(function(e){
            
            
            if($("input[name='laboratoryBranch']").val() == ""){
                $("input[name='laboratoryBranch']").parent().addClass('has-error');
                $("label[for='laboratoryBranch']").html("<i class='fa fa-times-circle-o'></i> Branch Name field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='laboratoryBranch']").parent().removeClass('has-error');
                $("label[for='laboratoryBranch']").html("Branch Name*");
            }
            
            
            return;
        });
        
        
        $("#generate-doctor-id").click(function(){
           
            $.ajax({
                'url':'<?php echo base_url(); ?>doctor/generate_doctor_id',
                'method':'post',
                'data':{'year':$(this).attr("rel")},
                'dataType':'json',
                'success':function(ret){
                    $("#patientid").val(ret.patientID);
                }
            
            });
        });
    });
</script>