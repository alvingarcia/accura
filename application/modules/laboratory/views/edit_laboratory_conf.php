<script type="text/javascript">
    $(document).ready(function(){
        
        $("#save-procedure").click(function(e){
            e.preventDefault();
            var procedure = $("#procedure-selector").val();
            var price = $("#price").val();
            
            
            data = {'procedure':procedure,'laboratoryid':'<?php echo $lab_id; ?>','price':price};
            
            
            $.ajax({
                'url':'<?php echo base_url(); ?>laboratory/submit_procedure/',
                'method':'post',
                'data':data,
                'dataType':'json',
                'success':function(ret){
                   document.location = "<?php echo current_url(); ?>";
                }
            });
            
        });
    
        $(".delete-procedure").click(function(e){
            e.preventDefault();
            var labproc = $(this).attr('rel');
            
            data = {'labProcId':labproc};
            
            
            $.ajax({
                'url':'<?php echo base_url(); ?>laboratory/delete_lab_proc/',
                'method':'post',
                'data':data,
                'dataType':'json',
                'success':function(ret){
                   document.location = "<?php echo current_url(); ?>";
                }
            });
            
        });
        
        
        
        
    });
    
</script>