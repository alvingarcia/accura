<aside class="right-side">
<section class="content-header">
                    <h1>
                        Laboratory Collection Center
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Laboratory Collection Center</a></li>
                        <li class="active">Add Collection Center</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New Collection Center</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>laboratory/submit_laboratory" method="post" role="form">
                 <div class="box-body row">
                <?php  
                    echo cms_input('laboratoryBranch','Branch Name','Enter Name','col-sm-12');
                    echo cms_input('laboratoryNumber','Laboratory Number','Enter Number','col-sm-12');
                    echo cms_textarea('laboratoryAddress','Address','Enter Description','col-sm-12');
                    
                    ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="add" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>