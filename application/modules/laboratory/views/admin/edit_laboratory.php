<aside class="right-side">
<section class="content-header">
                    <h1>
                        Laboratory Collection Center
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Laboratory Collection Center</a></li>
                        <li class="active">Add Collection Center</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Edit Collection Center</h3>
        </div>
       
            
            
             <div class="box-body">
                    <div class="row">
                         <form id="validate-faculty" action="<?php echo base_url(); ?>laboratory/edit_submit_laboratory" method="post" role="form">
                    <?php  
                        echo cms_hidden('laboratoryId',$item['laboratoryId']);
                        echo cms_input('laboratoryBranch','Branch Name','Enter Name','col-sm-12',$item['laboratoryBranch']);
                        echo cms_input('laboratoryNumber','Laboratory Number','Enter Number','col-sm-12',$item['laboratoryNumber']);
                        echo cms_textarea('laboratoryAddress','Address','Enter Description','col-sm-12',$item['laboratoryAddress']);

                        ?>
                    <div class="form-group col-xs-12">
                        <input type="submit" value="update" class="btn btn-default  btn-flat">
                    </div>
                    </form>
                    </div>
                <?php if(!empty($proc)): ?>
                <hr />
                 <h3>Add Tests</h3>
                <div class="row">
                <div class="col-md-5">
                    <h4>Tests</h4>
                    <select class="form-control" id="procedure-selector">
                        <?php foreach($proc as $l): ?>
                            <option value="<?php echo $l['procedureId']; ?>"><?php echo $l['procedureName']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                
                <div class="col-md-5">
                    <h4>Price</h4>
                    <input type="text" class="form-control" id="price" />
                </div>
                <div class="col-md-2">
                    <br /><br />
                    <a href="#" id="save-procedure" class="btn btn-default  btn-flat btn-block">Save</a>

                </div>
                </div>
                <?php endif; ?>
                <hr />

                <?php foreach($proc_selected as $l): ?>
                    <div class="row">     
                    <div class="col-md-5">
                        <p><?php echo $l['procedureName']; ?></p>
                    </div>

                    <div class="col-md-5">
                        <p>P<?php echo ($l['labCcPrice']>0)?$l['labCcPrice']:$l['procedurePrice']." (standard price)"; ?></p>
                    </div>
                    <div class="col-md-2">
                        <a href="#" rel="<?php echo $l['labprocId']; ?>" class="btn btn-default delete-procedure  btn-flat btn-block btn-sm">Remove</a>

                    </div>
                    </div>
                    <hr />
                <?php endforeach; ?>
                 
                 
            
                
            
       
        </div>
</aside>