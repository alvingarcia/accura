<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laboratory extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');		
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");	
		$this->load->model("user_model");
        $this->config->load('courses');
        $this->data['user'] = $this->session->all_userdata();
        $this->data['page'] = "adminusers";
    }
    
    
    public function add_laboratory()
    {
        if($this->is_admin())
        {
            $users = array();
            
            $use = $this->data_fetcher->getUsersNotInDoctor();
            foreach($use as $u)
            {
                $users[$u['id']] = $u['lastname']." ".$u['firstname']; 
            }
          
            $this->data['users'] = $users;
            
            $this->data['page'] = "add_laboratory";
            $this->data['opentree'] = "laboratory";
            $this->load->view("common/header",$this->data);
            if(empty($use))
                $this->load->view("admin/add_laboratory_full",$this->data);
            else
                $this->load->view("admin/add_laboratory",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("laboratory_validation_js",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function edit_laboratory($id)
    {
        
        if($this->is_admin())
        {
           
            $this->data['proc'] = $this->data_fetcher->getProcedureNotSelected($id);
            $this->data['proc_selected'] = $this->data_fetcher->getProcedureSelected($id);
            
            $this->data['lab_id'] = $id;
            
            $this->data['item'] = $this->data_fetcher->getItem('laboratories',$id,'laboratoryId');
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/edit_laboratory",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("laboratory_validation_js",$this->data); 
            $this->load->view("edit_laboratory_conf",$this->data); 
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url());    
        
        
    }
    
    public function submit_procedure()
    {
        $post = $this->input->post();
        $lab = $post['laboratoryid'];
        $proc = $post['procedure'];
        $price = $post['price'];
        
        //print_r($post);
                    
        $data_subject['procedureid'] = $proc;
        $data_subject['laboratoryid'] = $lab;
        $data_subject['labCcPrice'] = $price;
        $this->data_poster->post_data('laboratory_procedure',$data_subject);  
        
        $data['message'] = "Success";
        
        echo json_encode($data);
    }
    
    public function delete_lab_proc()
    {
        $post = $this->input->post();
        $labproc = $post['labProcId'];
        
        $this->data_poster->deleteItem('laboratory_procedure',$labproc,'labProcId');
        
        $data['message'] = "Success";
        
        echo json_encode($data);
    }
    
   
    
    public function submit_laboratory()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            //print_r($post);
           // $this->data_poster->log_action('User','Added a new User: '.$post['firstname']." ".$post['lastname'],'aqua');
            $this->data_poster->post_data('laboratories',$post);
        }
        redirect(base_url()."laboratory/view_all_laboratories");
            
    }
    
    public function edit_submit_laboratory()
    {
        $post = $this->input->post();
        //print_r($post);
        $this->data_poster->post_data('laboratories',$post,$post['laboratoryId'],'laboratoryId');
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        
        redirect(base_url()."laboratory/edit_laboratory/".$post['laboratoryId']);
            
    }
    
   
    public function view_all_laboratories()
    {
        if($this->is_admin())
        {
            $this->data['page'] = "view_all_laboratories";
            $this->data['opentree'] = "laboratory";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/laboratory_view",$this->data);
            $this->load->view("common/footer_datatables",$this->data); 
            $this->load->view("common/laboratory_conf",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url());  
    }
    
    
   function generate_patient_id()
    {
        
        $post = $this->input->post();
        $data['patientID'] = $this->data_fetcher->generatePatientID($post['year']);
        
        echo json_encode($data);   
    }
    
    
    public function delete_laboratory()
    {
        $data['message'] = "failed";
        
        if($this->is_admin()){
            $post = $this->input->post();
                $this->data_poster->deleteItem('laboratories',$post['id'],'laboratoryId');
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            
        }
        echo json_encode($data);
    }
    
    public function patient_viewer($id, $sem = null)
    {
        if($this->is_admin())
        { 

            $this->load->view("common/header",$this->data);
            $this->load->view("admin/faculty_viewer",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("common/faculty_viewer_conf",$this->data); 
        }
        else
            redirect(base_url()); 
    }
    
    
    
    public function logged_in()
    {
        if($this->session->userdata('admin_logged'))
            return true;
        else
            return false;
    }
    
    public function is_admin()
    {
        if($this->session->userdata('roles')=="admin")
            return true;
        else
            return false;
    }


}