<aside class="right-side">
    <section class="content-header">
        <h1>
            Barcode
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Barcode</a></li>
            <li class="active">Barcode Test</li>
        </ol>
    </section>
    <div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Barcode</h3>
        </div>
        <div class="box-body">
            
            <div>
            <iframe style="border:0;height:100px;" src="<?php echo base_url()."barcode/printBarcodeTest/".$id; ?>" name="frame"></iframe>
            </div>
            <a href="#" class="btn btn-default" onclick="frames['frame'].print()"><i class="fa fa-print"></i> Print</a>
            
        </div>
            
            
       
    </div>
    </div>
</aside>