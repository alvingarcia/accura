<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include('src/BarcodeGenerator.php');
include('src/BarcodeGeneratorPNG.php');
include('src/BarcodeGeneratorSVG.php');
include('src/BarcodeGeneratorJPG.php');
include('src/BarcodeGeneratorHTML.php');

class Barcode extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');		
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");	
		$this->load->model("user_model");
        $this->config->load('courses');
        $this->data['user'] = $this->session->all_userdata();
        $this->data['page'] = "adminusers";
    }
    
    
    public function generateBarcodeTest($id)
    {
        
        $test = $this->data_fetcher->getItem('appointment_test',$id,'appointmentTestId');
        $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
        $this->data['code'] = $test['testRequestCode'];
        $this->data['id'] = $id;
        $this->data['generated_barcode'] =  $generator->getBarcode($test['testRequestCode'], $generator::TYPE_CODE_128);
        
        $this->load->view("common/header",$this->data);
        $this->load->view("admin/barcode_test",$this->data);
        $this->load->view("common/footer",$this->data); 
    }
    
    public function printBarcodeTest($id)
    {
        
        $test = $this->data_fetcher->getItem('appointment_test',$id,'appointmentTestId');
        $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
        $this->data['code'] = $test['testRequestCode'];
        $this->data['test'] = $test;
        $this->data['generated_barcode'] =  '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode('081231723897', $generator::TYPE_CODE_128)) . '">';
        
        $this->load->view("common/header_frame",$this->data);
        $this->load->view("admin/barcode_print",$this->data);
    }
    
    
    
    public function logged_in()
    {
        if($this->session->userdata('admin_logged'))
            return true;
        else
            return false;
    }
    
    public function is_admin()
    {
        if($this->session->userdata('roles')=="admin")
            return true;
        else
            return false;
    }


}