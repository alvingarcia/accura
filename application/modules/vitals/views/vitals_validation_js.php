<script type="text/javascript">
    $(document).ready(function(){
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='vitalsHeight']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='vitalsHeight']").html("<i class='fa fa-times-circle-o'></i> height field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='vitalsHeight']").html("Height*");
            }
        
        });
        
        $("input[name='vitalsWeight']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='vitalsWeight']").html("<i class='fa fa-times-circle-o'></i> weight field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='vitalsWeight']").html("Weight*");
            }
        
        });
        
        $("input[name='vitalsBp']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='vitalsBp']").html("<i class='fa fa-times-circle-o'></i> bp field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='vitalsBp']").html("Blood Pressure*");
            }
        
        });
        
        $("#validate-faculty").submit(function(e){
            
            
            if($("input[name='vitalsHeight']").val() == ""){
                $("input[name='vitalsHeight']").parent().addClass('has-error');
                $("label[for='vitalsHeight']").html("<i class='fa fa-times-circle-o'></i> height field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='vitalsHeight']").parent().removeClass('has-error');
                $("label[for='vitalsHeight']").html("Height*");
            }
            if($("input[name='vitalsWeight']").val() == ""){
                $("input[name='vitalsWeight']").parent().addClass('has-error');
                $("label[for='vitalsWeight']").html("<i class='fa fa-times-circle-o'></i> weight field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='vitalsWeight']").parent().removeClass('has-error');
                $("label[for='vitalsWeight']").html("Blood Pressure*");
            }
            if($("input[name='vitalsBp']").val() == ""){
                $("input[name='vitalsBp']").parent().addClass('has-error');
                $("label[for='vitalsBp']").html("<i class='fa fa-times-circle-o'></i> bp field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='vitalsBp']").parent().removeClass('has-error');
                $("label[for='vitalsBp']").html("Blood Pressure*");
            }
            
            
            return;
        });
        
        
       
    });
</script>