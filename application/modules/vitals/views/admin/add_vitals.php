<aside class="right-side">
<section class="content-header">
                    <h1>
                        Vitals
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Vitals</a></li>
                        <li class="active">Add Vitals</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New Vitals</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>vitals/submit_vitals" method="post" role="form">
                 <div class="box-body row">
                <?php  
                     echo cms_dropdown2('vitalsPatientID','Select Patient',$patients,'col-xs-6',$pid);
                    echo cms_input('vitalsHeight','Height','Enter Height','col-sm-6');
                    echo cms_input('vitalsWeight','Weight','Enter Weight','col-sm-6');
                    echo cms_input('vitalsBp','Blood Pressure','Enter Weight','col-sm-6');
                    //echo cms_input('vitalsBMI','BMI','Enter BMI','col-sm-6');
                    echo cms_textarea('vitalsECG','ECG','Enter ECG Results','col-sm-6');
                    echo cms_hidden('vitalsDate',date("Y-m-d h:i:s"));
                    ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="add" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>