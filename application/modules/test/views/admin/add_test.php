<aside class="right-side">
<section class="content-header">
                    <h1>
                        Tests
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Tests</a></li>
                        <li class="active">Add Test</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New Test</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>test/submit_procedure" method="post" role="form">
                 <div class="box-body row">
                <?php  
                    echo cms_input('procedureCode','Code','Enter Name','col-sm-6');
                    echo cms_input('procedureName','Name','Enter Name','col-sm-6');
                    echo cms_dropdown('procedureType','Type of Test',$types,'col-xs-6'); 
                    echo cms_input('procedureFormalName','Formal Name','Enter Name','col-sm-6');
                    echo cms_input('procedureShortName','Short Name','Enter Name','col-sm-6');
                     echo cms_input('procedureSpecimenRequirement','Specimen Requirement','Enter Name','col-sm-6');
                    echo cms_number('procedureWeight','Average time in minutes','Enter Average time in minutes','col-sm-6');
                    echo cms_number('procedurePrice','SRP','Enter SRP','col-sm-6');
                    echo cms_dropdown('procedureSection','Select Area',$areas,'col-xs-6');
                    echo cms_textarea('procedureDescription','Description','Enter Description','col-sm-12');
                    ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="add" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>