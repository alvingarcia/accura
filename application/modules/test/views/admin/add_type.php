<aside class="right-side">
<section class="content-header">
                    <h1>
                        Section
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Tests</a></li>
                        <li class="active">Add Section</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New Section</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>test/submit_test_type" method="post" role="form">
                 <div class="box-body row">
                <?php  
                    
                    echo cms_input('testTypeName','Name','Enter Name','col-sm-6');
                    echo cms_textarea('testTypeDescription','Description','Enter Description','col-sm-6');
                    ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="add" class="btn btn-default  btn-flat">
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>