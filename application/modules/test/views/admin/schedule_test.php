<aside class="right-side">
<section class="content-header">
                    <h1>
                        Tests
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Tests</a></li>
                        <li class="active">Schedule Tests</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Test Request</h3>
        </div>
         <div class="box-body">
             <div class="col-sm-6 form-group">
                <label>Type</label>
                <select class="form-control select2">
                    <option value="scheduled">Scheduled</option>
                    <option value="walk-in">Walk In</option>
                </select> 
            </div>
             <div class="col-sm-6 form-group">
                <label>Select Tests</label>
                <select multiple class="form-control select2">
                    <?php foreach($proc as $pr): ?>
                    <option value="<?php echo $pr['procedureId']; ?>"><?php echo $pr['procedureName']; ?></option>
                    <?php endforeach; ?>
                </select> 
            </div>
                   
        </div>
</aside>