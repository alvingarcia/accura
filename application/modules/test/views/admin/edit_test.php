<aside class="right-side">
<section class="content-header">
                    <h1>
                        Tests
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Tests</a></li>
                        <li class="active">Edit Test</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Edit Test</h3>
        </div>
            
         <div class="box-body">
             <div class="row">
                 <form id="validate-faculty" action="<?php echo base_url(); ?>test/edit_submit_procedure" method="post" role="form">
        <?php  
            echo cms_hidden('procedureId',$item['procedureId']);
            echo cms_input('procedureCode','Code','Enter Name','col-sm-6',$item['procedureCode']);
            echo cms_input('procedureName','Name','Enter Name','col-sm-6',$item['procedureName']);
            echo cms_dropdown('procedureType','Type of Test',$types,'col-xs-6',$item['procedureType']); 
            echo cms_input('procedureFormalName','Formal Name','Enter Name','col-sm-6',$item['procedureFormalName']);
            echo cms_input('procedureShortName','Short Name','Enter Name','col-sm-6',$item['procedureShortName']);
            echo cms_input('procedureSpecimenRequirement','Specimen Requirement','Enter Name','col-sm-6',$item['procedureSpecimenRequirement']);
            echo cms_number('procedureWeight','Average time in minutes','Enter Average time in minutes','col-sm-6',$item['procedureWeight']);
            echo cms_number('procedurePrice','SRP','Enter SRP','col-sm-6',$item['procedurePrice']);
            echo cms_dropdown('procedureSection','Select Area',$areas,'col-xs-6',$item['procedureSection']);
            echo cms_textarea('procedureDescription','Description','Enter Description','col-sm-12',$item['procedureDescription']);
            ?>
        <div class="form-group col-xs-12">
            <input type="submit" value="update" class="btn btn-default  btn-flat">
        </div>
    </form>
        </div>
             
    </div>
</aside>
    
<!-- Button trigger modal -->
