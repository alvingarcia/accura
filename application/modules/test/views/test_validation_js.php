<script type="text/javascript">
    $(document).ready(function(){
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='procedureName']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='procedureName']").html("<i class='fa fa-times-circle-o'></i> name field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='procedureName']").html("Procedure Name*");
            }
        
        });
        
        $("#validate-faculty").submit(function(e){
            
            
            if($("input[name='procedureName']").val() == ""){
                $("input[name='procedureName']").parent().addClass('has-error');
                $("label[for='procedureName']").html("<i class='fa fa-times-circle-o'></i> name field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='procedureName']").parent().removeClass('has-error');
                $("label[for='procedureName']").html("Procedure Name*");
            }
            
            
            return;
        });
        
        $(".remove-item").click(function(e){
            e.preventDefault();
            var conf = confirm("are you sure you want to delete?");
            if(conf){
                container = $(this).parent().parent();
                id  = $(this).attr('rel'); 
                data = {'id':id};
                 $.ajax({
                        'url':'<?php echo base_url(); ?>test/delete_reference/',
                        'method':'post',
                        'data':data,
                        'dataType':'json',
                        'success':function(ret){
                            container.remove();
                        }
                    });
            }
        });
        $("#rSubmit").click(function(e){
            
            data = {};
            valid = true;
            // build key-values
            $.each($(".rform"), function(){
              if($(this).val() == "" && $(this).attr('name')!="criteria")
                  valid = false;
              
                data [$(this).attr('name')] = $(this).val();
                
            }); 
           <?php if(isset($item['procedureId'])): ?>
           if(valid)
               $.ajax({
                    'url':'<?php echo base_url(); ?>test/add_reference_range/<?php echo $item['procedureId']; ?>',
                    'method':'post',
                    'data':data,
                    'dataType':'json',
                    'success':function(ret){
                        alert("added"); 
                        ht = "<tr class='refItem'><td>"+ret.newRef.criteria+"</td><td>"+ret.newRef.rfrom+"</td><td> "+ret.newRef.rto+"</td><td>"+ret.newRef.factor+"</td><td>"+ret.newRef.siUnit+"</td><td>"+ret.newRef.convUnit+"</td><td><a rel='"+ret.referenceRangeId+"' class='remove-item' href='#'>remove</a></td></tr>";
                        $('#newRange').modal('hide');
                        $("input.rform").val('');
                        $("#refContainer").append(ht);
                        
                        
                        $(".remove-item").click(function(e){
                            e.preventDefault();
                            var conf = confirm("are you sure you want to delete?");
                            if(conf){
                                container = $(this).parent().parent();
                                id  = $(this).attr('rel'); 
                                data = {'id':id};
                                 $.ajax({
                                        'url':'<?php echo base_url(); ?>test/delete_reference/',
                                        'method':'post',
                                        'data':data,
                                        'dataType':'json',
                                        'success':function(ret){
                                            container.remove();
                                        }
                                    });
                            }
                            
                        });
                    }
                });
           else
               alert("fill up all fields");
            
            <?php endif; ?>
           
       });
    });
</script>