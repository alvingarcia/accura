<script type="text/javascript">
    $(document).ready(function(){
        
        $("#save-branches").click(function(e){
            e.preventDefault();
            var branches = Array();
            $('#branches-selected option').each(function(i, selected){ 
                branches[i] = $(selected).val();
            });
            
            
            data = {'branches':branches,'procedureid':<?php echo $proc_id; ?>};
            
            
            $.ajax({
                'url':'<?php echo base_url(); ?>test/submit_branches/',
                'method':'post',
                'data':data,
                'dataType':'json',
                'success':function(ret){
                   alert("saved"); 
                }
            });
            
        });
        
        $("#load-branches").click(function(e){ 
             e.preventDefault();
                $('#branches-selector :selected').each(function(i, selected){ 
                    itemVal = $(selected).val(); 
                    itemText = $(selected).text();
                    $("#branches-selected").append($('<option>', { 
                                    value: itemVal,
                                    text : itemText
                                }));
                        $("#branches-selector option[value='"+itemVal+"']").remove();
                    
                });
                
                
        });
        
        $("#unload-branches").click(function(e){ 
             e.preventDefault();
                $('#branches-selected :selected').each(function(i, selected){ 
                    itemVal = $(selected).val(); 
                    itemText = $(selected).text();
                    $("#branches-selector").prepend($('<option>', { 
                                    value: itemVal,
                                    text : itemText
                                }));
                        $("#branches-selected option[value='"+itemVal+"']").remove();
                    
                });
                
                
        });
        
        
    });
    
</script>