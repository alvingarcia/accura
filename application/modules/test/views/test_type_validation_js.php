<script type="text/javascript">
    $(document).ready(function(){
        
        //FORM VALIDATION FOR STUDENT
        $("input[name='testTypeName']").blur(function(){
            if($(this).val() == "" )
            {
                $(this).parent().addClass('has-error');
                $("label[for='testTypeName']").html("<i class='fa fa-times-circle-o'></i> name field is required");
            }
            else
            {
                 $(this).parent().removeClass('has-error');
                 $("label[for='testTypeName']").html("Name*");
            }
        
        });
        
        $("#validate-faculty").submit(function(e){
            
            
            if($("input[name='testTypeName']").val() == ""){
                $("input[name='testTypeName']").parent().addClass('has-error');
                $("label[for='testTypeName']").html("<i class='fa fa-times-circle-o'></i> name field is required");
                e.preventDefault();
            }
            else
            {
                $("input[name='testTypeName']").parent().removeClass('has-error');
                $("label[for='testTypeName']").html("Name*");
            }
            
            
            return;
        });
        
           
       });
</script>