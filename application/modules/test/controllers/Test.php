<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');		
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");	
		$this->load->model("user_model");
        $this->config->load('courses');
        $this->data['user'] = $this->session->all_userdata();
        $this->data['page'] = "adminusers";
    }
    
    
    public function add_procedure()
    {
        if($this->is_admin())
        {
            $areas = array();
            $ty = array();
            $ar = $this->data_fetcher->fetch_table('area',array('areaName','asc'),null,array('laboratoryid'=>$this->session->userdata('laboratoryid')));
            
            $types = $this->data_fetcher->fetch_table('test_type',array('testTypeName','asc'),null);
            
            foreach($ar as $u)
            {
                $areas[$u['areaId']] = $u['areaName']; 
            }
            
            foreach($types as $t)
            {
                $ty[$t['testTypeId']] = $t['testTypeName']; 
            }
          
            
            $this->config->load('courses');		
            $this->data['types'] = $ty;
            $this->data['areas'] = $areas;
            
            $this->data['page'] = "add_procedure";
            $this->data['opentree'] = "procedure";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/add_test",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("test_validation_js",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function add_test_type()
    {
        if($this->is_admin())
        {
            
            $this->data['page'] = "add_test_type";
            $this->data['opentree'] = "area";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/add_type",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("test_type_validation_js",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function schedule_procedure()
    {
        if($this->is_admin())
        {
            $this->data['proc'] = $this->data_fetcher->fetch_table('procedures',array('procedureName','asc'));
            
            $this->data['page'] = "schedule_procedure";
            $this->data['opentree'] = "procedure";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/schedule_test",$this->data);
            $this->load->view("common/footer",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function submit_branches()
    {
        $post = $this->input->post();
        $proc = $post['procedureid'];
        $this->data_poster->deleteItems('laboratory_procedure',array('procedureid'=>$proc));
        if(isset($post['branches']))
        {
            //check per subject

            foreach($post['branches'] as $branch)
            {
                    
                $data_subject['procedureid'] = $proc;
                $data_subject['laboratoryid'] = $branch;
                $this->data_poster->post_data('laboratory_procedure',$data_subject);  

            }
        }
        
        $data['message'] = "Success";
        
        echo json_encode($data);
    }
    
    public function add_reference_range($procid)
    {
        $post = $this->input->post();
        $post['procedureid'] = $procid;
        $this->data_poster->post_data('reference_range',$post);
        $data['referenceRangeId'] = $this->db->insert_id();
        $data['newRef'] = $post;
        echo json_encode($data);
    }
    
    public function delete_reference()
    {
        $post = $this->input->post();
        $id = $post['id'];
        $this->data_poster->deleteItem('reference_range',$id,'referenceRangeId');
        $data['message'] = "success";
        echo json_encode($data);
    }
    public function edit_procedure($id)
    {
        
        if($this->is_admin())
        {
            $areas = array();
            $ty = array();
            $ar = $this->data_fetcher->fetch_table('area',array('areaName','asc'),null,array('laboratoryid'=>$this->session->userdata('laboratoryid')));
            
            $types = $this->data_fetcher->fetch_table('test_type',array('testTypeName','asc'),null);
            
            foreach($ar as $u)
            {
                $areas[$u['areaId']] = $u['areaName']; 
            }
            
             foreach($types as $t)
            {
                $ty[$t['testTypeId']] = $t['testTypeName']; 
            }
          
            
            $this->config->load('courses');		
            $this->data['types'] = $ty;
            
            $this->data['si_units'] = $this->config->item('si_units');
            $this->data['conv_units'] = $this->config->item('conv_units');
            
            $this->data['areas'] = $areas;
           
            $this->data['lcc'] = $this->data_fetcher->getLaboratoryNotSelected($id);
            $this->data['lcc_selected'] = $this->data_fetcher->getLaboratorySelected($id);
            
            $this->data['ref_range'] = $this->data_fetcher->fetch_table('reference_range',null,null,array('procedureid'=>$id));
            
            $this->data['proc_id'] = $id;
            $this->data['opentree']="procedure";
            $this->data['item'] = $this->data_fetcher->getItem('procedures',$id,'procedureId');
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/edit_test",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("test_validation_js",$this->data);
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url());    
        
        
    }
    
    public function edit_type($id)
    {
        
        if($this->is_admin())
        {
           
             
            $this->data['test_type_id'] = $id;
            $this->data['opentree']="area";
            $this->data['item'] = $this->data_fetcher->getItem('test_type',$id,'testTypeId');
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/edit_type",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("test_type_validation_js",$this->data);
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url());    
        
        
    }
    
   
    
    public function submit_procedure()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            //print_r($post);
           // $this->data_poster->log_action('User','Added a new User: '.$post['firstname']." ".$post['lastname'],'aqua');
            $this->data_poster->post_data('procedures',$post);
        }
        redirect(base_url()."test/edit_procedure/".$this->db->insert_id());
            
    }
    
    public function submit_test_type()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            //print_r($post);
           // $this->data_poster->log_action('User','Added a new User: '.$post['firstname']." ".$post['lastname'],'aqua');
            $this->data_poster->post_data('test_type',$post);
        }
        redirect(base_url()."test/edit_type/".$this->db->insert_id());
    }
    
    public function edit_submit_procedure()
    {
        $post = $this->input->post();
        //print_r($post);
        $this->data_poster->post_data('procedures',$post,$post['procedureId'],'procedureId');
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        
        redirect(base_url()."test/edit_procedure/".$post['procedureId']);
            
    }
    
    public function edit_submit_test_type()
    {
        $post = $this->input->post();
        //print_r($post);
        $this->data_poster->post_data('test_type',$post,$post['testTypeId'],'testTypeId');
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        
        redirect(base_url()."test/edit_type/".$post['testTypeId']);
            
    }
    
   
    public function view_all_procedures()
    {
        if($this->is_admin())
        {
            $this->data['page'] = "view_all_procedures";
            $this->data['opentree'] = "procedure";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/test_view",$this->data);
            $this->load->view("common/footer_datatables",$this->data); 
            $this->load->view("common/test_conf",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url());  
    }
    
    public function view_all_test_types()
    {
        if($this->is_admin())
        {
            $this->data['page'] = "view_all_test_types";
            $this->data['opentree'] = "area";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/test_type_view",$this->data);
            $this->load->view("common/footer_datatables",$this->data); 
            $this->load->view("common/test_type_conf",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url());  
    }
    
    
   function generate_patient_id()
    {
        
        $post = $this->input->post();
        $data['patientID'] = $this->data_fetcher->generatePatientID($post['year']);
        
        echo json_encode($data);   
    }
    
    
    public function delete_procedure()
    {
        $data['message'] = "failed";
        
        if($this->is_admin()){
            $post = $this->input->post();
                $this->data_poster->deleteItem('procedures',$post['id'],'procedureId');
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            
        }
        echo json_encode($data);
    }
    
    public function delete_test_type()
    {
         $data['message'] = "failed";
        
        if($this->is_admin()){
            $post = $this->input->post();
            $items = $this->data_fetcher->getProceduresLab($this->session->userdata('apLabId'),$post['id']);
            if(empty($items))
            {
                $this->data_poster->deleteItem('test_type',$post['id'],'testTypeId');
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            }
            else
            {
                $data['message'] = "Cannot delete test type is in use by test";
            }
        }
        echo json_encode($data);
    }
    
    public function procedure_viewer($id, $sem = null)
    {
        if($this->is_admin())
        { 

            $this->load->view("common/header",$this->data);
            $this->load->view("admin/faculty_viewer",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("common/faculty_viewer_conf",$this->data); 
        }
        else
            redirect(base_url()); 
    }
    
    
    
    public function logged_in()
    {
        if($this->session->userdata('admin_logged'))
            return true;
        else
            return false;
    }
    
    public function is_admin()
    {
        if($this->session->userdata('roles')=="admin")
            return true;
        else
            return false;
    }


}