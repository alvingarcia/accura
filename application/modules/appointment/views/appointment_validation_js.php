<script type="text/javascript">
    $(document).ready(function(){
        <?php if(isset($tests) && !empty($tests)):  ?>
             $("#step2-submit").removeAttr('disabled');
        <?php else: ?>
            $("#step2-submit").attr('disabled','disabled');
        <?php endif; ?>
       
        
        $("#step1-submit").click(function(e){
           $("#step1").submit(); 
        });
        
        $("#step2-submit").click(function(e){
           $("#step2").submit(); 
        });
        
        $("#step3-submit").click(function(e){
           $("#step3").submit(); 
        });
        
        $("#step1-back").click(function(e){
           $.ajax({
            'url':'<?php echo base_url(); ?>appointment/resetData/',
            'method':'post',
            'data':{},
            'dataType':'json',
            'success':function(ret){
              document.location ="<?php echo current_url(); ?>";
            }
            }); 
        });
        
        $("#step2-back").click(function(e){
           document.location = "<?php echo base_url(); ?>appointment/new_appointment"; 
        });
        
         $("#step3-back").click(function(e){
           document.location = "<?php echo base_url(); ?>appointment/new_appointment2"; 
        });
        
        
        $("#add-test").click(function(e){
            e.preventDefault();
            var proc = $("#procedure-select option[value='"+$("#procedure-select").val()+"']").text();
            var medtech = $("#medtech-select option[value='"+$("#medtech-select").val()+"']").text();
            var doctor = $("#doctor-select option[value='"+$("#doctor-select").val()+"']").text();
            var procid = $("#procedure-select").val();
            var val = $("#procedure-select").val()+';'+$("#medtech-select").val()+';'+$("#doctor-select").val();
            
            html = "<input type='hidden' name='test[]' value='"+val+"' rel='"+$("#procedure-select").val()+"' >";
            $("#form-container").append(html);
            $("#procedure-select option[value='"+$("#procedure-select").val()+"']").remove();
            
            html2 = "<div class='row' style='border-bottom:1px solid #f2f2f2;padding-bottom:1rem;'><div id='proc-text' class='col-md-4'>"+proc+"</div><div class='col-md-3'>"+medtech+"</div><div class='col-md-3'>"+doctor+"</div><div class='col-md-2'><a rel='"+procid+"' class='remove-test btn btn-default btn-flat btn-block'>Remove</a></div></div>";
            $("#tests-container").append(html2);
            
            $("#medtech-container").hide();
            $("#doctor-container").hide();
            $("#add-test-container").hide();
            $("#step2-submit").removeAttr('disabled');
            
            $(".remove-test").click(function(e){
                e.preventDefault();

                var id = $(this).attr("rel");
                var text = $(this).parent().parent().find("#proc-text").html();
                $("#procedure-select").append($("<option />").val(id).text(text));
                $("input[rel='"+id+"']").remove();
                $(this).parent().parent().remove();
                if($("#form-container").is(':empty'))
                    $("#step2-submit").attr('disabled','disabled');

            });
        
        });
        
        
        
    });
        
        
</script>