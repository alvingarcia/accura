<aside class="right-side">
<section class="content-header">
                    <h1>
                        Transaction
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Transaction</a></li>
                        <li class="active">New Transaction</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Set Transaction</h3>
        </div>
       
            
            <form id="step1" action="<?php echo base_url(); ?>appointment/new_appointment2" method="post" role="form">
                 
                 <?php 
                    echo cms_dropdown2('patientid','Select Patient',$patients,'col-xs-6',$this->session->userdata('apPatientId'));
                    echo cms_dropdown2('patientType','Select Patient Type',$patient_types,'col-xs-6',$this->session->userdata('apPatientType'));
                    echo cms_dropdown2('laboratoryid','Select Laboratory',$laboratories,'col-xs-6',$this->session->userdata('apLabId'));
                    echo cms_hidden('appointmentDate',($this->session->userdata('apDate')!==null)?$this->session->userdata('apDate'):date("m/d/Y h:i a"));  ?>
                <?php /* 
                <div class="col-sm-6 form-group">
                    <label>Select Tests</label>
                    <select multiple class="form-control select2">
                        <?php foreach($proc as $pr): ?>
                        <option value="<?php echo $pr['procedureId']; ?>"><?php echo $pr['procedureName']; ?></option>
                        <?php endforeach; ?>
                    </select> 
                </div>
               */ ?>
                <div class="form-group col-xs-12">
                     <a href="#" id="step1-back" class="btn btn-default  btn-flat"><i class="fa fa-times"></i> Cancel/Reset</a>
                    <a href="#" id="step1-submit" class="btn btn-default  btn-flat">Next <i class="fa fa-arrow-right"></i></a>
                </div>
                <div style="clear:both"></div>
            </form>
       
        </div>
</aside>