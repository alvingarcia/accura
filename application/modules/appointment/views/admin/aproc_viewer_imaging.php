<aside class="right-side">
<section class="content-header">
                    <h1>
                        Appointment
                        <small>
                       
                        <a class="btn btn-app" href="<?php echo base_url()."appointment/view_appointment/".$appointment['appointmentId']; ?>">
                                <i class="fa fa-arrow-left"></i>Back to Appointment</a> 
                        </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Appointments</a></li>
                        <li class="active">View Test</li>
                    </ol>
                </section>
    <div class="content">
            <div class="alert alert-danger" style="display:none;">
                <i class="fa fa-ban"></i>
                <b>Alert!</b> Can not delete that user
            </div>
            <hr />
            <p><strong>Date:</strong> <?php echo date("D M j,Y",strtotime($appointment['appointmentDate'])); ?></p>
            <h3><?php echo $patient['patientLastname'].", ".$patient['patientFirstname']; ?></h3>
            <hr />
            <div class="box box-primary">
                <div class="box-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Test ID</th>
                                <th>Test Code</th>
                                <th>Test</th>
                                <th>Specimen ID</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th><?php echo $test['appointmentTestId']; ?></th>
                            <th><?php echo $test['procedureCode']; ?></th>
                            <th><?php echo $test['procedureName']; ?></th>
                            <th><?php echo $test['testSpecimenId']; ?></th>
                        </tr>
                        </tbody>
                    </table>
                    <hr />
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Result</th>
                                <th>conv</th>
                                <th>SI</th>
                                <th>Flag</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($results as $result): ?>
                        <tr>
                            <th><?php echo $result['rname']; ?></th>
                            <th><?php echo $result['rvalueconv']." ".$result['rr_conv']; ?></th>
                            <th><?php echo $result['rvaluesi']." ".$result['rr_si']; ?></th>
                            <th><?php echo $result['flag']; ?></th>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    
                </div>
        </div>
    </div>
</aside>

