<aside class="right-side">
<section class="content-header">
                    <h1>
                        Appointment
                        <small>
                       
                        <a class="btn btn-app" href="<?php echo base_url()."appointment/view_appointment/".$appointment['appointmentId']; ?>">
                                <i class="fa fa-arrow-left"></i>Back to Appointment</a> 
                        </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Appointments</a></li>
                        <li class="active">View Test</li>
                    </ol>
                </section>
    <div class="content">
            <div class="alert alert-danger" style="display:none;">
                <i class="fa fa-ban"></i>
                <b>Alert!</b> Can not delete that user
            </div>
            <hr />
            <p><strong>Date:</strong> <?php echo date("D M j,Y",strtotime($appointment['appointmentDate'])); ?></p>
            <h3><?php echo $patient['patientLastname'].", ".$patient['patientFirstname']; ?></h3>
            <hr />
            <div class="box box-primary">
                <div class="box-body">
                    <h4>Consultation</h4>
                    <hr />
                    <h5>Findings:</h5>
                    <p><?php echo $test['findings']; ?></p>
                    <hr />
                    <h5>Recommended Tests:</h5>
                    <p><?php echo $test['testRecommendations']; ?></p>
                    <hr />
                    <h5>Prescription:</h5>
                    <p><?php echo $test['perscription']; ?></p>
                    <a target="_blank" href="<?php echo base_url()."pdf/prescription/".$test['appointmentTestId']; ?>" id="print-prescription" class="btn btn-success"><i class="fa fa-print"></i> Print Prescription</a>
                </div>
        </div>
    </div>
</aside>

