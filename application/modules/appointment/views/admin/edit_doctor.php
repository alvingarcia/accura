<aside class="right-side">
<section class="content-header">
                    <h1>
                        Doctor
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Doctor</a></li>
                        <li class="active">Add Doctor</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">New Doctor</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>doctor/edit_submit_doctor" method="post" role="form">
                 <div class="box-body">
                     <h4><?php echo $profile['lastname'].", ".$profile['firstname']; ?></h4>
                     <hr />
                     <div class="row">
                <?php 
                    echo cms_hidden('doctorId',$item['doctorId']);
                    echo cms_input('prcid','PRC ID','Enter ID','col-sm-6',$item['prcid']);
                     echo cms_dropdown('qualification','Role',array("doctor"=>"Doctor","med tech"=>"Med Tech","laboratory admin"=>"Laboratory Admin"),'col-xs-6',$item['qualification']); 
                    echo cms_input('specialization','Specialization','Enter Specialization','col-sm-6',$item['specialization']);
                    echo cms_input('subspecialization','Sub Specialization','Enter Sub Specialization','col-sm-6',$item['subspecialization']);
                    ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="update" class="btn btn-default  btn-flat">
                </div>
                </div>
                
            </form>
       
        </div>
</aside>