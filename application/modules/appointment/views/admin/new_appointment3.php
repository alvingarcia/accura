<aside class="right-side">
<section class="content-header">
                    <h1>
                        Transaction
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Transaction</a></li>
                        <li class="active">New Transaction</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Confirm Transaction</h3>
        </div>
       
           <div class="box-body">
               <form id="step3" action="<?php echo base_url(); ?>appointment/submit_appointment" method="post" role="form">
                <?php 
                   echo cms_hidden('patientid',$patient['id']); 
                   echo cms_hidden('laboratoryid',$laboratoryid);
                ?>
                
                <p><?php echo $patient['patientLastname'].", ".$patient['patientFirstname']; ?></p>
                
                <p>HMO: <?php echo isset($hmo['hmoName'])?$hmo['hmoName']:'N/A'; ?></p>
                <p>Expires: <?php echo isset($hmo_detail['expires'])?$hmo_detail['expires']:'N/A'; ?></p>
                 <div class="row">
                            <?php
                             echo cms_date_time('appointmentDate','Appointment Date','Enter Date and Time','col-sm-6',date("m/d/Y h:i:s"));
                            ?>
                        </div>
                <hr />
                
                 <h4>Tests</h4>
                  
               <?php 
                   $total = 0;
                   foreach($test_arr as $test): 
                   if($laboratoryid == 0)
                       $price = $test['proc']['procedurePrice'];
                   else
                       $price = $test['proc']['labCcPrice'];
                   
                   $total+=$price;
                    ?>
                    <?php 
                       echo cms_hidden('procedureid[]',$test['proc']['procedureId']); 
                    ?>
                    <div class="procedure-cont">
                        <p><?php echo $test['proc']['procedureName']; ?></p>
                        <p><strong>Price:</strong> P<?php echo $price; ?></p>
                    </div>
                    <hr />
                <?php endforeach; ?>
                <p><strong>Total:</strong> P<?php echo $total; ?></p>
                <hr />
                 <?php 
                  echo cms_dropdown('appointmentStatus','Status',array("active"=>"Active","on hold"=>"Inactive"),'col-xs-6'); ?>
                <?php echo cms_dropdown('paid','Payment',array("0"=>"Cash","1"=>"Charge Account","2"=>"Company","3"=>"HMO"),'col-xs-6'); ?>
               <div class="form-group col-xs-12">
                    <a href="#" id="step3-back" class="btn btn-default  btn-flat"><i class="fa fa-arrow-left"></i> Back</a>
                    <a href="#" id="step3-submit" class="btn btn-default  btn-flat">Confirm and Save <i class="fa fa-arrow-right"></i></a>
                </div>
                </div>
                </form>
            </div>
            </form>
       
        </div>
</aside>