<aside class="right-side">
<section class="content-header">
                    <h1>
                        Transactions
                        <small>
                        <a class="btn btn-app" href="<?php echo base_url()."appointment/view_all_appointments/"; ?>">
                                <i class="fa fa-arrow-left"></i>View Transaction</a> 
                        </small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Transactions</a></li>
                        <li class="active">View Transaction</li>
                    </ol>
                </section>
    <div class="content">
           
            <div class="box box-solid box-success">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $patient['patientLastname'].", ".$patient['patientFirstname']; ?></h3>
                    <div class="box-tools">

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                   
                    <p><strong>Branch:</strong> <?php echo (isset($laboratory['laboratoryBranch']))?$laboratory['laboratoryBranch']:'Main'; ?></p>
                    <p><strong>Transaction ID:</strong><?php echo sprintf('%08d',$appointment['appointmentId']); ?></p>
                    <p><strong>Patient Type:</strong> <?php echo strtoupper($appointment['appointmentType']); ?></p>
                    <form method="post" action="<?php echo base_url(); ?>appointment/update_transaction_status">
                        <div class="row">
                            <?php echo cms_date_time('appointmentDate','Appointment Date','Enter Date and Time','col-sm-6',date("m/d/Y",strtotime($appointment['appointmentDate']))." ".date("H:i:s",strtotime($appointment['appointmentTime']))); 
                            
                            echo cms_hidden("appointmentId",$appointment['appointmentId']); ?>
                            
                            <div class="col-sm-6">
                                <label>Status</label>
                                <select name="appointmentStatus" class="form-control" rel="">
                                    <option value="active" <?php echo $appointment['appointmentStatus'] == 'active'?'selected':''; ?>>Active</option>
                                    <option value="on hold" <?php echo $appointment['appointmentStatus'] == 'on hold'?'selected':''; ?>>Inactive</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                 <div class="alert alert-info" style="display:none;">
                                    <i class="fa fa-ban"></i>
                                    <b>Alert!</b> Error!
                                </div>
                            </div>
                        </div>
                        <input type="submit" value="update" class="btn btn-primary" />
                    </form>
                    <hr />
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Test</th><th>Price</th><th>Status</th><th></th>
                            </tr>
                        </thead>
                        <tbody>
                    <?php 
                    $total = 0;
                    foreach($tests as $test): 
                    $total += $proc[$test['appointmentTestId']]['price'];
                    ?>
                    <tr>
                        <td><?php echo $test['procedureName']; ?></td>
                        <td>P <?php echo $proc[$test['appointmentTestId']]['price']; ?></td>
                        <td><span class='text-green'><?php echo $test['appointmentTestStatus']; ?></td>
                        <td>
                            <a href="<?php echo base_url()."appointment/aproc_view/".$test['appointmentTestId']; ?>" class="btn btn-default btn-flat">Details</a>
                            <?php if($test['appointmentTestStatus'] == "waiting" || $test['appointmentTestStatus'] == "in queue"): ?>
                            <a href="<?php echo base_url()."appointment/hold_test/".$test['appointmentTestId']."/".$test['intOnHold']."/".$appointment['appointmentId']; ?>" class="btn btn-default btn-flat">
                                <?php echo ($test['intOnHold'] == 1)?'Resume Testing':'Put on Hold';  ?>
                            </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                    </table>
                    <hr />
                    <p><strong>Total:</strong> P<?php echo $total; ?></p>
                    <hr />
                     <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#newTest">
                        Add Test
                     </button>
                     <a target="_blank" href="<?php echo base_url(); ?>pdf/print_receipt/<?php echo $appointment['appointmentId']; ?>" class="btn btn-primary btn-md">
                        <i class="fa fa-print"></i> Print Receipt 
                     </a>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
    </div>
</aside>


<!-- Modal -->
<div class="modal fade" id="newTest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" style="width: 80%;" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Tests</h4>
      </div>
        <form method="post" action="<?php echo base_url()."appointment/additional_test"; ?>" >
            <?php echo cms_hidden('appointmentId',$appointment['appointmentId']);
                  echo cms_hidden('laboratoryid',$appointment['laboratoryid']);
            ?>
      <div class="modal-body">
        <div class="form-group">
            <label>Select Test</label>
             <?php foreach($procedure_types as $proc): ?>

            <?php if(!empty($proc)): ?>
            <h5 style="font-weight:700"><?php echo strtoupper($proc[0]['testTypeName']); ?></h5>
            <div class="row">
                 <?php 
                    foreach($proc as $pr): 

                ?>
              <div class="col-sm-4">
                  <div class="checkbox">
                    <label>
                      <input class="cb" <?php echo in_array($pr['procedureId'],$tests)?'checked':''; ?> name="test[]" value="<?php echo $pr['procedureId']; ?>" type="checkbox">
                      <?php echo $pr['procedureName']; ?>
                    </label>
                  </div>
             </div>

                <?php 
                endforeach; ?>

             </div>
            <hr />
            <?php endif; ?>
             <?php endforeach; ?>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" value="Add" class="btn btn-primary">
      </div>
        </form>
    </div>
  </div>
</div>
