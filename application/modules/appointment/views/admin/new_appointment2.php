<aside class="right-side">
<section class="content-header">
                    <h1>
                        Appointment
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Appointment</a></li>
                        <li class="active">New Appointment</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Select Tests</h3>
        </div>
       
        <div class="box-body"> 
            <form id="step2" action="<?php echo base_url(); ?>appointment/new_appointment_confirm" method="post" role="form">
                <p><?php echo $patient['patientLastname'].", ".$patient['patientFirstname']; ?></p>
                <p><?php echo date("l M j, Y - h:i a",strtotime($date)); ?></p>
                <hr />
                    
                     <?php 
                       echo cms_hidden('patientid',$patient['id']);
                      echo cms_hidden('appointmentDate',$date);
                    ?>
                    <div id="form-container"></div>
            
                <div>
                   
                    <table id="faculty-table" class="table">
                        <thead><tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Formal Name</th>
                            <th>Short Name</th>
                            <th>Specimen Requirement</th>
                            <th>Type</th>
                            <th>Area</th>
                            <th>Cost</th>
                        </tr>
                        </thead>
                        <tbody>

                    </tbody></table>
                    <div class="input-group">
                    
                    </div>
                </div>
                
            </form>
                <hr />
                <div>
                    <div class="form-group">
                        <a href="#" id="step2-back" class="btn btn-default  btn-flat"><i class="fa fa-arrow-left"></i> Back</a>
                        <a href="#" id="step2-submit" class="btn btn-default  btn-flat">Save and Proceed <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            
        </div>
       
        </div>
</aside>