<aside class="right-side">
<section class="content-header">
                    <h1>
                        Update Test
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Update Test</a></li>
                        <li class="active">Update Test</li>
                    </ol>
                </section>
<div class="content">
    <div class="span10 box box-primary">
        <div class="box-header">
                <h3 class="box-title">Update Test</h3>
        </div>
       
            
            <form id="validate-faculty" action="<?php echo base_url(); ?>appointment/update_results" method="post" role="form">
                 <div class="box-body">
                    
                     <hr />
                     <div class="row">
                <?php 
                    echo cms_hidden('accessKey','a@4cCur@a4xp7755');
                    echo cms_input('tid','Test ID','Enter ID','col-sm-6');
                    echo cms_input('rname','Result Name','Resultname','col-sm-6');
                    echo cms_input('rvalueconv','Result Conv','','col-sm-6');
                    echo cms_input('rvaluesi','Result SI','','col-sm-6');
                    echo cms_input('flag','Flag','','col-sm-6');
                    echo cms_input('rr_conv','Reference Range Conv','','col-sm-6');
                    echo cms_input('rr_si','Reference Range SI','','col-sm-6');
                    echo cms_input('si_unit','Unit SI','','col-sm-6');
                    echo cms_input('conv_unit','Unit Conv','','col-sm-6');
                    echo cms_textarea('comment','Comments','','col-sm-6');
                    ?>
                <div class="form-group col-xs-12">
                    <input type="submit" value="update" class="btn btn-default  btn-flat">
                </div>
                </div>
                
            </form>
       
        </div>
</aside>