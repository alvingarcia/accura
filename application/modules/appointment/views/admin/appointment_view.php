<aside class="right-side">
<section class="content-header">
                    <h1>
                        Transactions
                        <small></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Transactions</a></li>
                        <li class="active">View Transactions</li>
                    </ol>
                </section>
    <div class="content">
            <div class="alert alert-danger" style="display:none;">
                <i class="fa fa-ban"></i>
                <b>Alert!</b> Can not delete that user
            </div>
            <div class="box box-solid box-success">
                <div class="box-header">
                    <h3 class="box-title">Transactions</h3>
                    <div class="box-tools">

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="faculty-table" class="table">
                        <thead><tr>
                            <th>Transaction ID</th>
                            <th>Patient</th>
                            <th>Date</th>
                            <th>Select Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                    </tbody></table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
    </div>
</aside>