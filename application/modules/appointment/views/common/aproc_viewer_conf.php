<script type="text/javascript">
    $(document).ready(function(){
       
        $("#finalze-results").click(function(e){
            e.preventDefault(); 
            conf = confirm("Are you sure you want to finalize?");
            if(conf)
            {
                $("#form").attr("action","<?php echo base_url()."appointment/submit_results/1" ?>");
                $("#form").submit();
            }
        });
        
        $(".add-new-result").click(function(e){
            e.preventDefault();
            var id = $(this).attr('rel');
            //alert(id);
            var html = $(this).attr('data-html');
            //alert(html);
            $("#test-"+id).append("<div class='row'>"+html+"</div>");
        });
        
        $(".delete-result").click(function(e){
            e.preventDefault();
            id = $(this).attr('rel');
            
            conf = confirm("Are you sure you want to delete?");
            if(conf)
            {
                var data = {'id':id};
                $.ajax({
                    'url':'<?php echo base_url(); ?>index.php/appointment/delete_result',
                    'method':'post',
                    'data':data,
                    'dataType':'json',
                    'success':function(ret){
                        if(ret.message == "failed"){
                            alert("failed");
                        }
                        else
                            document.location = "<?php echo current_url(); ?>";
                        
                }
            });
            }
        });
        
    });
</script>