<script type="text/javascript">
    
    $(document).ready(function(){
    $('#faculty-table').dataTable( {
            "aLengthMenu":  [250, 500, 750, 1000],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo base_url(); ?>index.php/datatables/data_tables_transaction_test/<?php echo $lid; ?>",
            "aoColumnDefs":[
                {
                    "aTargets":[0],
                    "bVisible": false 
                }
            ],
            "aaSorting": [[1,'asc']],
            "fnDrawCallback": function () {  
                $(".cb").click(function(e){
                    var chk = $('.cb:checked').length;
                    if (chk){ 
                        $("#step2-submit").removeAttr('disabled');
                    }
                    else{
                       $("#step2-submit").attr('disabled','disabled');
                    }

                      //;
                });
                
                $('.cb').change(function() {
                    if($(this).is(":checked")) 
                        type = "add";
                    else
                        type = "delete";
                        //alert("checked");
                        id = $(this).val();
                        var data = {'id':id};
                        $.ajax({
                            'url':'<?php echo base_url(); ?>index.php/appointment/update_session/'+type,
                            'method':'post',
                            'data':data,
                            'dataType':'json',
                            'success':function(ret){
                                if(ret.message == "success"){
                                    
                                }

                        }
                    });
                    
                    
                });
                
            },
        } );
        
    });

</script>