<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Appointment extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->config->load('themes');		
		$theme = $this->config->item('unity');
		if($theme == "" || !isset($theme))
			$theme = $this->config->item('global_theme');
		
        
        $this->data['img_dir'] = base_url()."assets/themes/".$theme."/images/";	
        $this->data['student_pics'] = base_url()."assets/photos/";
        $this->data['css_dir'] = base_url()."assets/themes/".$theme."/css/";
        $this->data['js_dir'] = base_url()."assets/themes/".$theme."/js/";
        $this->data['title'] = "CCT Unity";
        $this->load->library("email");	
        $this->load->helper("cms_form");	
		$this->load->model("user_model");
        $this->config->load('courses');
        $this->data['user'] = $this->session->all_userdata();
        $this->data['page'] = "adminusers";
    }
    
    
    public function hl7_parser_test()
    {
        $message = "MSH|^~\&|LMSRISUS|LIFETRACK|ORIV|RIV|20180425064658.000||ORU^R01|032296|P|2.3.1||||
PID|1||201492||TEST^TEST^^^||20160721|O|||
ORC|RE|3683||
OBR|1|3683||3683^|||20180425 06:46:58|||||||||||3683||||201804250645||US|F|||||||Admin|
OBX|1|FT|^SR Instance UID||1.2.840.10008.114051.218485.1095065602.384427952.1655973847||||||F||||||
OBX|2|FT|^Study Instance UID||1.2.840.10008.114051.218485.1095065602.384427952.1655973847||||||F||||||
OBX|3|FT|||Technique: Ultrasound of the pelvis was performed. ~~Comparison: None. ~~FINDINGS:  ~  Adnexa: No adnexal masses are seen. ~  Lymph nodes: No pathologically enlarged nodes. ~  Peritoneal Cavity: Unremarkable. ~~~Impression: ~   1. No adnexal masses are seen. ~   2. No pathologically enlarged nodes. ~   3. Unremarkable. ~||||||F||||||
";
        
        
        //$msg = new HL7\Message($message);
    }
    
    public function new_appointment()
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7)))
        {
            
            $this->data['patients'] = $this->data_fetcher->getDropdown('patient','id',array('patientLastname','patientFirstname'),array('patientLastname','asc'));
            $this->data['patient_types'] = array("walk in"=>"Walk in","referral"=>"Referral","charge account"=>"Charge Account","hmo member"=>"HMO Member","charge to employee/company/family"=>"Charge to employee/company/family");
            if($this->is_admin()){
            $this->data['laboratories'] = $this->data_fetcher->getDropdown('laboratories','laboratoryId',array('laboratoryBranch'),array('laboratoryBranch','asc'));
            $this->data['laboratories'][0]  = "Main";
            }
            else
            {
                 $lab = $this->data_fetcher->getItem('laboratories',$this->session->userdata('laboratoryid'),'laboratoryId');
                
                $this->data['laboratories'] = array($lab['laboratoryId']=>$lab['laboratoryBranch']);
            }
            $this->data['page'] = "new_appointment";
            $this->data['opentree'] = "appointment";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/new_appointment",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("appointment_validation_js",$this->data); 
        }
        else
            redirect(base_url()); 
        
    }
    
    public function resetData()
    {
        $array_items = array('apPatientId', 'apDate','test','apLabId','apPatientType');
        $this->session->unset_userdata($array_items);
        $data['message'] = "success";
        echo json_encode($data);
        
        
    }
    
    public function new_appointment2()
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7)))
        {
            $post = $this->input->post();
            if(!empty($post)){
                
                if($post['laboratoryid'] != $this->session->userdata('apLabId'))
                   $this->session->unset_userdata('test');
                
                $this->session->set_userdata('apPatientId',$post['patientid']);
                $this->session->set_userdata('apPatientType',$post['patientType']);
                $this->session->set_userdata('apDate',$post['appointmentDate']);
                $this->session->set_userdata('apLabId',$post['laboratoryid']);
                
                
            }
            
            $this->config->load('courses');		
            $this->data['types'] = $this->data_fetcher->fetch_table('test_type',array('testTypeName','asc'),null);
            
            
            $this->data['tests'] = (is_array($this->session->userdata('test')))?$this->session->userdata('test'):array();
            
            
           foreach($this->data['types'] as $type){
                $this->data['procedure_types'][] = $this->data_fetcher->getProceduresLab($this->session->userdata('apLabId'),$type['testTypeId']);
           }
            //$this->data['proc'] = $this->data_fetcher->fetch_table('procedures',array('procedureName','asc'));
            
            $this->data['patient'] = $this->data_fetcher->getItem('patient',$this->session->userdata('apPatientId'),'id');
            
            $this->data['lid'] = $this->session->userdata('apLabId');
            
            $this->data['date'] = $this->session->userdata('apDate');
            
            $this->data['page'] = "new_appointment";
            $this->data['opentree'] = "appointment";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/new_appointment2",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("appointment_validation_js",$this->data); 
            $this->load->view("common/new_appointment2_conf",$this->data);
        }
        else
            redirect(base_url()); 
        
    }
    
    public function new_appointment_confirm()
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7)))
        {
            $post = $this->input->post();
            
            $tests = $this->session->userdata('test');
            $this->data['laboratoryid'] = $this->session->userdata('apLabId');
         
            $this->data['patient'] = $this->data_fetcher->getItem('patient',$this->session->userdata('apPatientId'),'id');
         
            $this->data['hmo_detail'] = $this->data_fetcher->getItem('patient_hmo',$this->data['patient']['id'],'patientid');
            if(!empty($this->data['hmo_detail']))
                $this->data['hmo'] = $this->data_fetcher->getItem('hmo',$this->data['hmo_detail']['hmoid'],'hmoId');
             else
                 $this->data['hmo'] = array();
         
            $this->data['date'] = $this->session->userdata('apDate');
            
            $test_arr = array();
            $i = 0;
            foreach($tests as $test)
            {
                if($this->data['laboratoryid'] == 0){
                    $test_arr[$i]['proc'] = $this->data_fetcher->getItem('procedures',$test,'procedureId');
                }
                else{
                     $test_arr[$i]['proc'] = $this->data_fetcher->getProcedureJoinLab($test,$this->data['laboratoryid']);
                }
                $i++;
            }
            
            $this->data['doctors'] = $this->data_fetcher->getAvailableDoctorsDropdown($this->data['date']);
         
            $this->data['test_arr'] = $test_arr;
            $this->data['post'] = $post;
            
            $this->data['page'] = "new_appointment";
            $this->data['opentree'] = "appointment";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/new_appointment3",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("appointment_validation_js",$this->data); 
        }
        else
            redirect(base_url());    
    }
    
    public function submit_appointment()
    {
        $post = $this->input->post();
        $data['pid'] = $post['patientid'];
        $data['paid'] = $post['paid'];
        $data['laboratoryid'] = $post['laboratoryid'];
        $data['appointmentDate'] = date("Y-m-d",strtotime($post['appointmentDate']));
        $data['appointmentTime'] = date("H:i:s",strtotime($post['appointmentDate']));
        $data['appointmentType'] = $this->session->userdata('apPatientType');
        $data['appointmentStatus'] = $post['appointmentStatus'];
        $this->data_poster->post_data('appointment',$data);
        unset($data);
        $data['aid'] = $this->db->insert_id();
        $areaIds = array();
        $waitingTime = array();
        
        for($i=0;$i<count($post['procedureid']);$i++)
        {
            $areaId = $this->data_fetcher->getAreaByProcedure($post['procedureid'][$i]);
            if(!in_array($areaId,$areaIds))
                $areaIds[] = $areaId;
                
            $data['testRequestCode'] = date('ymdHis').$post['laboratoryid'].$i;
            $data['procedureid'] = $post['procedureid'][$i];
            $data['appointmentTestStatus'] = 'waiting';
            //$data['testRequestCode'] = $this->data_fetcher->generateTestRequestCode();
            $this->data_poster->post_data('appointment_test',$data);
        }
       
        foreach($areaIds as $aid)
        {
            $time = $this->data_fetcher->getEstimatedTime($aid);
            $waitingTime[$aid] = $time;
        }
        
        $area_priority = get_priority($waitingTime);
        $this->data_poster->updateQueue($data['aid'],$area_priority);
        
        $array_items = array('apPatientId', 'apDate','test','apLabId','apPatientType');
        $this->session->unset_userdata($array_items);
        
        redirect(base_url()."appointment/view_appointment/".$data['aid']);
    }
    
   
    
    public function edit_doctor($id)
    {
        
        if($this->is_admin())
        {
           
            $this->data['opentree'] = "doctor";
            $this->data['item'] = $this->data_fetcher->getItem('doctors',$id,'doctorId');
            $this->data['profile'] = $this->data_fetcher->getItem('user',$this->data['item']['userid'],'id');
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/edit_doctor",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("doctor_validation_js",$this->data); 
           // print_r($this->data['classlists']);
            
        }
        else
            redirect(base_url());    
        
        
    }
    
    public function get_staff()
    {
        $post = $this->input->post();
        
        $data['medtech'] = $this->data_fetcher->getMedTech($post['procedure_id']);
        $data['doctors'] = $this->data_fetcher->getDoctor($post['procedure_id']);
        
        echo json_encode($data);
    }
    
   
    
    public function submit_doctor()
    {
        if($this->is_admin())
        {
            $post = $this->input->post();
            //print_r($post);
           // $this->data_poster->log_action('User','Added a new User: '.$post['firstname']." ".$post['lastname'],'aqua');
            $this->data_poster->post_data('doctors',$post);
        }
        redirect(base_url()."doctor/view_all_doctors");
            
    }
    
    public function edit_submit_doctor()
    {
        $post = $this->input->post();
        //print_r($post);
        $this->data_poster->post_data('doctors',$post,$post['doctorId'],'doctorId');
        //$this->data_poster->log_action('Faculty','Updated Faculty Info: '.$post['strFirstname']." ".$post['strLastname'],'aqua');
        
        
        redirect(base_url()."doctor/edit_doctor/".$post['doctorId']);
            
    }
    
   
    public function view_all_appointments()
    {
        if($this->is_admin() || $this->is_lccadmin())
        {
            $this->data['page'] = "view_all_appointments";
            $this->data['opentree'] = "appointment";
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/appointment_view",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("common/appointment_conf",$this->data); 
            //print_r($this->data['classlist']);
            
        }
        else
            redirect(base_url());  
    }
    
    
   function generate_patient_id()
    {
        
        $post = $this->input->post();
        $data['patientID'] = $this->data_fetcher->generatePatientID($post['year']);
        
        echo json_encode($data);   
    }
    
    
    public function delete_doctor()
    {
        $data['message'] = "failed";
        
        if($this->is_admin()){
            $post = $this->input->post();
                $this->data_poster->deleteItem('doctors',$post['id'],'doctorId');
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            
        }
        echo json_encode($data);
    }
    
    public function update_transaction_status()
    {
        if($this->is_admin()){
            $post = $this->input->post();
            $post['appointmentTime'] = date("H:i:s",strtotime($post['appointmentDate']));
            $post['appointmentDate'] = date("Y-m-d",strtotime($post['appointmentDate']));
            $this->data_poster->post_data('appointment',$post,$post['appointmentId'],'appointmentId');
            
            $d['appointmentTestStatus'] = "in queue";
            $this->data_poster->post_data('appointment_test',$d,$post['appointmentId'],'aid',array('appointmentTestStatus'=>'in progress'));
            
           
        }
        redirect(base_url()."appointment/view_appointment/".$post['appointmentId']);
    }
    
    public function delete_result()
    {
        $data['message'] = "failed";
        
        if($this->is_admin()){
            $post = $this->input->post();
                $this->data_poster->deleteItem('appointment_test_results',$post['id'],'appontmentTestResultId');
                //$this->data_poster->log_action('User','Deleted a User '.$post['fname'].' '.$post['lname'],'red');
                $data['message'] = "success";
            
        }
        echo json_encode($data);
    }
    
    public function view_appointment($id)
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7)))
        { 

            $this->data['appointment'] = $this->data_fetcher->getItem('appointment',$id,'appointmentId');
            $ret_proc = array();
            $this->data['tests'] = $this->data_fetcher->getProceduresAppointment($id);
            
            foreach($this->data['tests'] as $t)
            {
                if($this->data['appointment']['laboratoryid'] == 0)
                {
                    $r = $this->data_fetcher->getItem('procedures',$t['procedureid'],'procedureId');
                    $r['price'] = $r['procedurePrice'];
                    $ret_proc[$t['appointmentTestId']] = $r;
                }
                     
                else{
                    $r = $this->data_fetcher->getProcedureJoinLab($t['procedureid'],$this->data['appointment']['laboratoryid']);
                    $r['price'] = $r['labCcPrice'];
                    $ret_proc[$t['appointmentTestId']] = $r; 
                }
            }
            
            $this->config->load('courses');	
            
            $this->data['types'] = $this->data_fetcher->getItems('test_type');
            
            foreach($this->data['types'] as $type){
                $this->data['procedure_types'][] = $this->data_fetcher->getProceduresLabNotSelected($this->data['appointment']['laboratoryid'],$type['testTypeId'],$id);
           }
            
            
            
            $this->data['proc'] = $ret_proc;
            
            $this->data['patient'] = $this->data_fetcher->getItem('patient',$this->data['appointment']['pid'],'id');
            $this->data['laboratory'] = $this->data_fetcher->getItem('laboratories',$this->data['appointment']['laboratoryid'],'laboratoryId');
            
            $this->load->view("common/header",$this->data);
            $this->load->view("admin/appointment_viewer",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("common/appointment_viewer_conf",$this->data); 
        }
        else
            redirect(base_url()); 
    }
    
    public function update_session($type="add")
    {
        $post = $this->input->post();
        
        $tests = $this->session->userdata('test');
        $id = $post['id'];
        
        if($type == "add")
            $tests[] = $id;
        else
            if (($key = array_search($id, $tests)) !== false) {
                unset($tests[$key]);
            }

        $this->session->set_userdata('test',$tests);
        
        echo json_encode(array("message"=>"success"));
        
    }
    
    public function hold_test($id,$i,$aid)
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7)))
        { 
            $data['intOnHold'] = ($i == 0)?1:0;
            $data['appointmentTestStatus'] = "waiting";
            $this->data_poster->post_data('appointment_test',$data,$id,'appointmentTestId');
            $d = $this->data_fetcher->fetch_table('appointment_test',null,null,array('appointmentTestStatus'=>'in queue','aid'=>$aid));
            if($i == 0){
                if(empty($d))
                    $this->data_poster->queueNextTest($aid,$id);
            }
            else
            {
                if(empty($d))
                    $this->data_poster->queueNextTest($aid);
            }
            redirect(base_url()."appointment/view_appointment/".$aid);
        }
        else
           redirect(base_url()); 
    }
    public function additional_test()
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7)))
        { 
            $post = $this->input->post();
            $d['aid'] = $post['appointmentId'];
            $inqueue = $this->data_fetcher->count_table_contents('appointment_test',null,array('aid'=>$d['aid'],'appointmentTestStatus'=>'in queue'));
            if(isset($post['test'])){
                $i = 0;
                foreach($post['test'] as $test)
                {
                    $inqueue = $this->data_fetcher->count_table_contents('appointment_test',null,array('aid'=>$d['aid'],'appointmentTestStatus'=>'in queue'));
                    $d['procedureid'] = $test;
                    $d['testRequestCode'] = date('ymdHis').$post['laboratoryid'].$i;
                    if($inqueue == 0){
                        $d['appointmentTestStatus'] = "in queue";
                        $d['testQueueCode'] = date('ymdHis');
                    }
                    else
                    {
                        $d['appointmentTestStatus'] = "waiting";
                    }
                    $this->data_poster->post_data('appointment_test',$d);
                    $i++;
                }
                
            }
            
            redirect(base_url()."appointment/view_appointment/".$d['aid']);
        }
        else
            redirect(base_url()); 
    }
    
    public function aproc_view($id)
    {
        if($this->data_fetcher->is_specific(array(0,1,2,3,4,6,7)))
        { 
            $ret = array();
            $tests = $this->data_fetcher->getTestById($id);
            $this->data['results'] = $this->data_fetcher->getResultsById($id);
            $this->data['test'] = $tests;
            $this->data['id'] = $id;
            
            $areaName = $this->data_fetcher->getAreaName($tests['procedureid']);
            
            $this->data['appointment'] = $this->data_fetcher->getItem('appointment',$tests['aid'],'appointmentId');
            
            $this->data['patient'] = $this->data_fetcher->getItem('patient',$this->data['appointment']['pid'],'id');
            $this->data['laboratory'] = $this->data_fetcher->getItem('laboratories',$this->data['appointment']['laboratoryid'],'laboratoryId');
           
            
            
            
            $this->load->view("common/header",$this->data);
            if($areaName == "Consultation"){
                $this->data['test'] = $this->data_fetcher->getTestConsultationById($id);
                $this->load->view("admin/aproc_viewer_cons",$this->data);
            }
            else if($areaName != "Imaging")
                $this->load->view("admin/aproc_viewer",$this->data);
            else
                $this->load->view("admin/aproc_viewer_imaging",$this->data);
            $this->load->view("common/footer",$this->data); 
            $this->load->view("common/aproc_viewer_conf",$this->data);
        }
        else
            redirect(base_url()); 
    }
    
    public function submit_order($id,$area=0)
    {
        if($this->is_admin())
        {
            $items = array();
            $data = array();
            $tests = $this->data_fetcher->getTestsByArea($id,$area);
           
            foreach($tests as $test){
                
                $items[] = array('test_code'=>$test['procedureCode'],'name'=>$test['procedureName'],'specimenID'=>$test['testSpecimenId']);     
            }
            
            $appointment = $this->data_fetcher->getItem('appointment',$id,'appointmentId');
            $patient = $this->data_fetcher->getItem('patient',$appointment['pid'],'id');
            
            $data = array(
                    "transactionID"=>$appointment['appointmentId'],
                    "patientID"=>$patient['patientid'],
                    "patientName"=>array(
                        "lastname"=>$patient['patientLastname'],
                        "firstname"=>$patient['patientFirstname'],
                        "middlename"=>$patient['patientMiddlename']),
                    "birthdate"=>$patient['birthday'],
                    "gender"=>$patient['sex'],
                    "age"=>calculate_age($patient['birthday']),
                    "address"=>$patient['address']." ".$patient['addressCity']." ".$patient['addressProvince']." ".$patient['addressCountry'],
                    "email"=>$patient['email'],
                    "contact_number"=>$patient['contactno'],
                    "tests"=>$items);
            //API URL
            
            
            
          
            foreach($tests as $test){
                $d['appointmentTestStatus'] = 'waiting results';
                //$data['testRequestCode'] = $this->data_fetcher->generateTestRequestCode();
                $this->data_poster->post_data('appointment_test',$d,$test['appointmentTestId'],'appointmentTestId');
            }

            redirect(base_url()."admin/area/".$area."/3");
            
            
        }
        else
            redirect(base_url());
    }
    
    public function get_orders()
    {
        $items = array();
            $data = array();
            $tests = $this->data_fetcher->getTestsWaiting();
            foreach($tests as $test){
            
            $appointment = $this->data_fetcher->getItem('appointment',$test['aid'],'appointmentId');
            $patient = $this->data_fetcher->getItem('patient',$appointment['pid'],'id');
            $gender = ($patient['sex'] == 'male')?'M':'F';
            
                if(!isset($data[$test['aid']])){
            $data[$test['aid']] = array(
                    
                    "patientID"=>$patient['patientid'],
                    "patientName"=>array(
                        "lastname"=>$patient['patientLastname'],
                        "firstname"=>$patient['patientFirstname'],
                        "middlename"=>$patient['patientMiddlename']),
                    "birthdate"=>$patient['birthday'],
                    "gender"=>$gender,
                    "dateTimeRequested"=>date("Y-m-d H:i:s"),
                    "age"=>calculate_age($patient['birthday']),
                    "address"=>$patient['address']." ".$patient['addressCity']." ".$patient['addressProvince']." ".$patient['addressCountry'],
                    "email"=>$patient['email'],
                    "contact_number"=>$patient['contactno']
                    );
            }
                
                $data[$test['aid']]['tests'][] = array(
                    "test_id"=>$test['appointmentTestId'],
                    "name"=>$test['procedureName'],
                    "specimenID"=>$test['testSpecimenId']);
                
            }
        
        echo json_encode($data);
    }
    
    public function update_order($aid)
    {
        
        $key = "a@4cCur@a4xp7755";
        $get = $this->input->get();
        if($get['accessKey'] == $key)
        {
            $tests = $this->data_fetcher->getTestsWaitingPerTransaction($aid);
            foreach($tests as $test){
                $d['appointmentTestStatus'] = 'order sent';
                //$data['testRequestCode'] = $this->data_fetcher->generateTestRequestCode();
                $this->data_poster->post_data('appointment_test',$d,$test['appointmentTestId'],'appointmentTestId');
            }
            echo "success";
        }
        else
            echo "invalid key";
    }
    
    public function update_test_mock()
    {
         $this->load->view("common/header",$this->data);
         $this->load->view("admin/update_test_mock",$this->data);
         $this->load->view("common/footer",$this->data); 
         
    }
    
    public function update_results()
    {
        $key = "a@4cCur@a4xp7755";
        $post = $this->input->post();
        if($post['accessKey'] == $key)
        {
            $data['appointmentTestIdfk'] = $post['tid'];
            $data['rname'] = $post['rname'];
            $data['rvalueconv'] = $post['rvalueconv'];
            $data['rvaluesi'] = $post['rvaluesi'];
            $data['flag'] = $post['flag'];
            $data['rr_conv'] = $post['rr_conv'];
            $data['rr_si'] = $post['rr_si'];
            $data['si_unit'] = $post['si_unit'];
            $data['conv_unit'] = $post['conv_unit'];
            $data['comment'] = $post['comment'];
            
            $trans = $this->data_fetcher->getResultsTransaction($post['tid'],$data['rname']);
            if(empty($trans))
                $this->data_poster->post_data('test_results',$data);
            else
                $this->data_poster->post_data('test_results',$data,$trans['appointmentTestIdfk'],'appointmentTestIdfk');
            
            $d['appointmentTestStatus'] = 'received results';
                //$data['testRequestCode'] = $this->data_fetcher->generateTestRequestCode();
            $this->data_poster->post_data('appointment_test',$d,$post['tid'],'appointmentTestId');
            
            echo "success";
        }
        else
            echo "invalid key";
    }
    
    public function update_results2()
    {
        $key = "a@4cCur@a4xp7755";
        $post = $this->input->post();
        if($post['accessKey'] == $key)
        {
            //Update Comment
            $d['resultComments'] = $post['tests']['comments'];
            $this->data_poster->post_data('appointment_test',$d,$post['tests']['tid'],'appointmentTestId');
            
            $data['appointmentTestIdfk'] = $post['tests']['tid']; //Request ID
            foreach($post['tests'] as $test){
                
                
                $data['rname'] = $test['rname'];
                $data['rvalueconv'] = $test['rvalueconv'];
                $data['rvaluesi'] = $test['rvaluesi'];
                $data['flag'] = $test['flag'];
                $data['rr_conv'] = $test['rr_conv'];
                $data['rr_si'] = $test['rr_si'];
                $data['si_unit'] = $test['si_unit'];
                $data['conv_unit'] = $test['conv_unit'];
                

                $trans = $this->data_fetcher->getResultsTransaction($test['tid'],$data['rname']);
                if(empty($trans))
                    $this->data_poster->post_data('test_results',$data);
                else
                    $this->data_poster->post_data('test_results',$data,$trans['appointmentTestIdfk'],'appointmentTestIdfk');

                 $d['appointmentTestStatus'] = 'received results';
                //$data['testRequestCode'] = $this->data_fetcher->generateTestRequestCode();
                $this->data_poster->post_data('appointment_test',$d,$test['tid'],'appointmentTestId');
            
            }
            
           
            
            echo "success";
        }
        else
            echo "invalid key";
    }
    
    public function get_appointment_alert($lab)
    {
        if($this->logged_in())
        {
        
            $appointment = $this->data_fetcher->getAlert($lab);

            $ret['appointment'] = $appointment;
            $ret['count'] = count($ret['appointment']);

            echo json_encode($ret);
        }
    }
    
    
    public function logged_in()
    {
        if($this->session->userdata('admin_logged'))
            return true;
        else
            return false;
    }
    
    public function is_admin()
    {
        if($this->session->userdata('roles')=="admin")
            return true;
        else
            return false;
    }
    
    public function is_lccadmin()
    {
        if($this->session->userdata('roles')=="lcc_admin")
            return true;
        else
            return false;
    }


}