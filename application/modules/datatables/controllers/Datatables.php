<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('src/facebook.php');

class Datatables extends CI_Controller {

	public function __construct()
	{
		
        parent::__construct();
		
        
        //$this->load->model("user_model");
        //$this->config->load('courses');
        $this->data["user"] = $this->session->all_userdata();
        $this->load->helper("cms_form");
		
	}
    
    public function sentItemsData()
    {
        
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * TABLE CONFIG
         */

        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        
		
        $aColumns = array('intMessageUserID','strFirstName','strLastName','strSubject', 'dteDate','intRead');
        $sIndexColumn = "intMessageUserID";
        $sTable = 'tb_mas_message_user';
        $table = $sTable;
        $user = $this->session->userdata('intID');
        
        /* 
         * Paging
         */
        $sLimit = "";
        $sOrder = "";
        
        
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayStart']).", ".
                mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayLength'] );
        }


        /*
         * Ordering
         */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                        ".mysqli_real_escape_string($this->db->conn_id,$_GET['sSortDir_'.$i] ) .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                $sOrder = "";
            }
        }


        /* 
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "WHERE $table.intFacultyIDSender LIKE '".$user."' AND intTrash = 0 ";
        
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            
                $sWhere .= "AND (";
            
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        /* Individual column filtering */
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = "WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch_'.$i])."%' ";
            }
        }

        
        $join = "JOIN tb_mas_faculty ON tb_mas_faculty.intID = tb_mas_message_user.intFacultyID ";
        $join .= "JOIN tb_mas_system_message ON tb_mas_system_message.intID = tb_mas_message_user.intMessageID ";
        
        $groupBy = "GROUP BY intMessageID ";

        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM $sTable
            $join
            $sWhere
            $groupBy
            $sOrder
            $sLimit
            
        ";
        $rResult = $this->db->query($sQuery);

        /* Data set length after filtering */
        $sQuery = "
            SELECT FOUND_ROWS() as frows
        ";
        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result();
        $iFilteredTotal = $aResultFilterTotal[0]->frows;

        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as cnt
            FROM   $sTable
        ";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result();
        $iTotal = $aResultTotal[0]->cnt;


        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ($rResult->result() as $aRow)
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                if ( $aColumns[$i] == "strFirstName" && $table == 'tb_mas_message_user')
                {
                    /* Special output formatting for 'version' column */
                    $row[] = '<label><input rel="'.$aRow->$aColumns[0].'" type="checkbox" class="minimal message-check"/></label> '.$aRow->$aColumns[$i]." ".$aRow->$aColumns[$i+1];
                }
                else if ( $aColumns[$i] == "strLastName" && $table == 'tb_mas_message_user')
                {
                    
                }
                else if($aColumns[$i] == 'dteDate' && $table == 'tb_mas_message_user')
                {
                    $row[] = time_lapsed($aRow->$aColumns[$i]);
                }
                else if($aColumns[$i] == 'strSubject' && $table == 'tb_mas_message_user')
                {
                    $row[] = "<a href='".base_url()."messages/view_message/".$aRow->$aColumns[0]."'>".$aRow->$aColumns[$i]."</a>";
                }
                else if(substr($aColumns[$i], 0, 3) == 'dte')
                {
                    $row[] = date("M j, Y",strtotime($aRow->$aColumns[$i]));
                }
                else if ( $aColumns[$i] != ' ' )
                {
                    /* General output */
                    $row[] = $aRow->$aColumns[$i];
                }
                
            }
            $output['aaData'][] = $row;
        }
	   echo json_encode( $output );
    }
        
    
    public function data_tables_transactions($userid,$type=1)
    {
        
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * TABLE CONFIG
         */

        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */		
		
        $aColumns = array("appointmentTestId","appointmentDate","patientid","patientLastname","patientFirstname","sex","birthday","procedureName","appointmentTestStatus");
        //$aColumns = array("intID","strFullName","strCourse","strSection");
        $sIndexColumn = "appointmentId";
        $sTable = "appointment";
        
       
        
        /* 
         * Paging
         */
        $sLimit = "";
        $sOrder = "";
        
        
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayStart'] ).", ".
                mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayLength']);
        }


        /*
         * Ordering
         */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                        ".mysqli_real_escape_string($this->db->conn_id,$_GET['sSortDir_'.$i] ) .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                $sOrder = "";
            }
        }


        /* 
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "WHERE pid = '".$userid."' AND testTypeId = ".$type;
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
           
            
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        /* Individual column filtering */
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = "WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch_'.$i])."%' ";
            }
        }
            $join = "";
        
            $join .= "JOIN patient ON appointment.pid = patient.id ";
            $join .= "JOIN appointment_test ON appointment.appointmentId = appointment_test.aid ";
            $join .= "JOIN procedures ON appointment_test.procedureid = procedures.procedureId ";
            //$join .= "JOIN area ON area.areaId = procedures.procedureType ";
            $join .= "JOIN test_type ON test_type.testTypeId = procedures.procedureType ";
        
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM $sTable
            $join
            $sWhere
            $sOrder
            $sLimit
            
        ";
        $rResult = $this->db->query($sQuery);

        /* Data set length after filtering */
        $sQuery = "
            SELECT FOUND_ROWS() as frows
        ";
        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result();
        $iFilteredTotal = $aResultFilterTotal[0]->frows;

        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as cnt
            FROM   $sTable
        ";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result();
        $iTotal = $aResultTotal[0]->cnt;


        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ($rResult->result() as $aRow)
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                if ( $aColumns[$i] == "patientLastname")
                {
                    /* Special output formatting for 'version' column */
                    $row[] = $aRow->$aColumns[$i]."  ".$aRow->$aColumns[$i+1];
                }
                else if ( $aColumns[$i] == "patientFirstname" || $aColumns[$i] == "patientMiddlename")
                {
                    
                }
                else if($aColumns[$i] == 'dteDate' && $table == 'tb_mas_message_user')
                {
                    $row[] = time_lapsed($aRow->$aColumns[$i]);
                }
                else if($aColumns[$i] == 'strSubject' && $table == 'tb_mas_message_user')
                {
                    $row[] = "<a href='".base_url()."messages/view_message/".$aRow->$aColumns[0]."'>".$aRow->$aColumns[$i]."</a>";
                }
                elseif($aColumns[$i] == "birthday")
                {
                    $row[] = date("M j, Y",strtotime($aRow->$aColumns[$i]))." / ".calculate_age($aRow->$aColumns[$i],$aRow->appointmentDate);
                }
                else if($aColumns[$i]== 'appointmentDate')
                {
                    $row[] = "<label><input name='test[]' value=".$aRow->$aColumns[0]." type='checkbox' class='minimal message-check'/></label> ".date("M j, Y",strtotime($aRow->$aColumns[$i]));
                }
                else if($aColumns[$i] == 'laboratoryNumber')
                {
                    $row[] = "<label><input rel=".$aRow->$aColumns[0]." type='checkbox' class='minimal message-check'/></label> ".$aRow->$aColumns[$i];
                }
                else if ( $aColumns[$i] != ' ' )
                {
                    /* General output */
                    $row[] = $aRow->$aColumns[$i];
                }
                
            }
            $output['aaData'][] = $row;
        }
	   echo json_encode( $output );
    }
    
    public function data_tables_transactions_user($userid)
    {
        
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * TABLE CONFIG
         */

        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */		
		
        $aColumns = array("appointmentTestId","appointmentDate","birthday","procedureName","appointmentTestStatus");
        //$aColumns = array("intID","strFullName","strCourse","strSection");
        $sIndexColumn = "appointmentId";
        $sTable = "appointment";
        
       
        
        /* 
         * Paging
         */
        $sLimit = "";
        $sOrder = "";
        
        
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayStart'] ).", ".
                mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayLength']);
        }


        /*
         * Ordering
         */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                        ".mysqli_real_escape_string($this->db->conn_id,$_GET['sSortDir_'.$i] ) .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                $sOrder = "";
            }
        }


        /* 
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "WHERE pid = '".$userid."' ";
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
                $sWhere = "WHERE (";
           
            
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        /* Individual column filtering */
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = "WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch_'.$i])."%' ";
            }
        }
            $join = "";
        
            $join .= "JOIN patient ON appointment.pid = patient.id ";
            $join .= "JOIN appointment_test ON appointment.appointmentId = appointment_test.aid ";
            $join .= "JOIN procedures ON appointment_test.procedureid = procedures.procedureId ";
            $join .= "JOIN area ON area.areaId = procedures.procedureType ";
        
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM $sTable
            $join
            $sWhere
            $sOrder
            $sLimit
            
        ";
        $rResult = $this->db->query($sQuery);

        /* Data set length after filtering */
        $sQuery = "
            SELECT FOUND_ROWS() as frows
        ";
        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result();
        $iFilteredTotal = $aResultFilterTotal[0]->frows;

        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as cnt
            FROM   $sTable
        ";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result();
        $iTotal = $aResultTotal[0]->cnt;


        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ($rResult->result() as $aRow)
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                if($aColumns[$i] == 'dteDate' && $table == 'tb_mas_message_user')
                {
                    $row[] = time_lapsed($aRow->$aColumns[$i]);
                }
                else if($aColumns[$i] == 'strSubject' && $table == 'tb_mas_message_user')
                {
                    $row[] = "<a href='".base_url()."messages/view_message/".$aRow->$aColumns[0]."'>".$aRow->$aColumns[$i]."</a>";
                }
                elseif($aColumns[$i] == "birthday")
                {
                    $row[] = date("M j, Y",strtotime($aRow->$aColumns[$i]))." / ".calculate_age($aRow->$aColumns[$i],$aRow->appointmentDate);
                }
                else if($aColumns[$i]== 'appointmentDate')
                {
                    $row[] = date("M j, Y",strtotime($aRow->$aColumns[$i]));
                }
                else if($aColumns[$i] == 'laboratoryNumber')
                {
                    $row[] = "<label><input rel=".$aRow->$aColumns[0]." type='checkbox' class='minimal message-check'/></label> ".$aRow->$aColumns[$i];
                }
                else if ( $aColumns[$i] != ' ' )
                {
                    /* General output */
                    $row[] = $aRow->$aColumns[$i];
                }
                
            }
            $output['aaData'][] = $row;
        }
	   echo json_encode( $output );
    }
    
    public function data_tables_ajax($table,$user=null,$trashed=null)
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * TABLE CONFIG
         */

        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        $this->config->load('data-tables');		
		
        $aColumns = $this->config->item($table.'_columns');
        $sIndexColumn = $this->config->item($table.'_index');
        $sTable = $table;
        
       
        
        /* 
         * Paging
         */
        $sLimit = "";
        $sOrder = "";
        
        
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayStart']).", ".
                mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayLength']);
        }


        /*
         * Ordering
         */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                        ".mysqli_real_escape_string($this->db->conn_id,$_GET['sSortDir_'.$i]) .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                $sOrder = "";
            }
        }


        /* 
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "";
        if($user == "lcc_admin" && $table == "appointment")
        {
            $role = $this->session->userdata('laboratoryid');
            $sWhere = "WHERE appointment.laboratoryid = ".$role." ";
        }
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            if($sWhere == "")
                $sWhere = "WHERE (";
            else
                $sWhere .= "AND (";
            
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                if(($aColumns[$i] == "firstname" || $aColumns[$i] == "lastname") && ($table == 'doctors' || $table == 'user' ) )
                    $sWhere .= "CONCAT_WS(' ',firstname,lastname) LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'])."%' OR ";
                elseif(($aColumns[$i] == "patientFirstname" || $aColumns[$i] == "patientLastname") && ($table == 'patient' ))
                    $sWhere .= "CONCAT_WS(' ',patientFirstname,patientLastname) LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'])."%' OR ";
                    
                else
                    $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'] )."%' OR ";
                    
                
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
            
            
            
        }
        
        /* Individual column filtering */
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = "WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
               
                
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch_'.$i])."%' ";
        
                
                
            }
        }

        $join = "";
        if($table == "area")
            $join .= "JOIN test_type ON area.areaType = test_type.testTypeId ";
        
        if($table == "doctors")
            $join .= "JOIN user ON doctors.userid = user.id ";
        
        if($table == "appointment")
            $join .= "JOIN patient ON appointment.pid = patient.id ";
        
        if($table == "procedures"){
            $join .= "JOIN area ON procedures.procedureSection = area.areaId ";
            $join .= "JOIN test_type ON procedures.procedureType = test_type.testTypeId ";
        }
        
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns)). 
            " FROM $sTable
            $join
            $sWhere
            $sOrder
            $sLimit
            
        ";
        
        
        $rResult = $this->db->query($sQuery);

        /* Data set length after filtering */
        $sQuery = "
            SELECT FOUND_ROWS() as frows
        ";
        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result();
        $iFilteredTotal = $aResultFilterTotal[0]->frows;

        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as cnt
            FROM   $sTable
        ";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result();
        $iTotal = $aResultTotal[0]->cnt;

        

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ($rResult->result() as $aRow)
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                if ( $aColumns[$i] == "patientLastname" && $table == 'appointment')
                {
                    /* Special output formatting for 'version' column */
                    $row[] = $aRow->$aColumns[$i].",  ".$aRow->$aColumns[$i+1];
                }
                else if ( ($aColumns[$i] == "appointmentId") && $table == 'appointment')
                {
                    $row[] = sprintf('%08d',$aRow->$aColumns[$i]); 
                }
                else if ( ($aColumns[$i] == "patientFirstname") && $table == 'appointment')
                {
                    
                }
                else if ( $aColumns[$i] == "strFirstName" && $table == 'tb_mas_message_user')
                {
                    /* Special output formatting for 'version' column */
                    $row[] = '<label><input rel="'.$aRow->$aColumns[0].'" type="checkbox" class="minimal message-check"/></label> '.$aRow->$aColumns[$i]." ".$aRow->$aColumns[$i+1];
                }
                else if ( $aColumns[$i] == "strLastName" && $table == 'tb_mas_message_user')
                {
                    
                }
                else if ( $aColumns[$i] == "intUserLevel" && $table == 'tb_mas_faculty')
                {
                    $row[] = switch_user_level($aRow->$aColumns[$i]); 
                }
                else if($aColumns[$i] == 'dteDate' && $table == 'tb_mas_message_user')
                {
                    $row[] = time_lapsed($aRow->$aColumns[$i]);
                }
                else if($aColumns[$i] == 'strSubject' && $table == 'tb_mas_message_user')
                {
                    $row[] = "<a href='".base_url()."messages/view_message/".$aRow->$aColumns[0]."'>".$aRow->$aColumns[$i]."</a>";
                }
                else if($aColumns[$i] == 'birthday')
                {
                    $row[] = date("M j, Y",strtotime($aRow->$aColumns[$i]));
                }
                else if(substr($aColumns[$i], -4) == 'Date')
                {
                    $row[] = date("M j, Y",strtotime($aRow->$aColumns[$i]));
                }
                else if ( $aColumns[$i] != ' ' )
                {
                    /* General output */
                    $row[] = $aRow->$aColumns[$i];
                }
                
            }
            $output['aaData'][] = $row;
        }
	   echo json_encode( $output );
    }
    
    public function data_tables_transaction_test($id=0)
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * TABLE CONFIG
         */

        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        $this->config->load('data-tables');		
		$table = "procedures";
        if($id==0)
        $aColumns = array("procedureId","procedureName","procedureFormalName","procedureShortName","procedureSpecimenRequirement","testTypeName","areaName","procedurePrice");
        else
            $aColumns = array("procedures.procedureId as procedureId","procedureName","procedureFormalName","procedureShortName","procedureSpecimenRequirement","testTypeName","areaName","labCcPrice");
        
$sIndexColumn = "procedureId";
        $sTable = $table;
        
        /* 
         * Paging
         */
        $sLimit = "";
        $sOrder = "";
        
        
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayStart']).", ".
                mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayLength']);
        }


        /*
         * Ordering
         */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                        ".mysqli_real_escape_string($this->db->conn_id,$_GET['sSortDir_'.$i]) .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                $sOrder = "";
            }
        }


        /* 
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "";
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            if($sWhere == "")
                $sWhere = "WHERE (";
            else
                $sWhere .= "AND (";
            
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                if(($aColumns[$i] == "firstname" || $aColumns[$i] == "lastname") && ($table == 'doctors' || $table == 'user' ) )
                    $sWhere .= "CONCAT_WS(' ',firstname,lastname) LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'])."%' OR ";
                elseif(($aColumns[$i] == "patientFirstname" || $aColumns[$i] == "patientLastname") && ($table == 'patient' ))
                    $sWhere .= "CONCAT_WS(' ',patientFirstname,patientLastname) LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'])."%' OR ";
                    
                else
                    $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'] )."%' OR ";
                    
                
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
            
            
            
        }
        
        /* Individual column filtering */
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = "WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
               
                
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch_'.$i])."%' ";
        
                
                
            }
        }

        
        $join = "JOIN area ON procedures.procedureSection = area.areaId ";
        $join .= "JOIN test_type ON procedures.procedureType = test_type.testTypeId ";
        
        if($id!=0)
            $join .= "JOIN laboratory_procedure ON procedures.procedureId = laboratory_procedure.procedureid ";
        
        
        
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns)). 
            " FROM $sTable
            $join
            $sWhere
            $sOrder
            $sLimit
            
        ";
        
        
        $rResult = $this->db->query($sQuery);

        /* Data set length after filtering */
        $sQuery = "
            SELECT FOUND_ROWS() as frows
        ";
        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result();
        $iFilteredTotal = $aResultFilterTotal[0]->frows;

        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as cnt
            FROM   $sTable
        ";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result();
        $iTotal = $aResultTotal[0]->cnt;

        

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        
        $tests = (is_array($this->session->userdata('test')))?$this->session->userdata('test'):array();

        foreach ($rResult->result() as $aRow)
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
              
                if($aColumns[$i] == "procedures.procedureId as procedureId")
                {
                        $row[] = $aRow->procedureId;
                }
                else if($aColumns[$i] == 'procedureName')
                {
                    $checked = in_array($aRow->procedureId,$tests)?'checked':'';
                    $row[] = '<input class="cb" '.$checked.' name="test[]" value="'.$aRow->procedureId.'" type="checkbox">';
                }
                else if(substr($aColumns[$i], -4) == 'Date')
                {
                    $row[] = date("M j, Y",strtotime($aRow->$aColumns[$i]));
                }
                else if ( $aColumns[$i] != ' ' )
                {
                        $row[] = $aRow->$aColumns[$i];
                }
                
            }
            $output['aaData'][] = $row;
        }
	   echo json_encode( $output );
    }
    
    public function data_tables_ajax_patienthr($company=0)
    {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * TABLE CONFIG
         */

        /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */		
		
        $aColumns = array("id","patientLastname","patientFirstname","sex","birthday");;
        $sIndexColumn = "id";
        $sTable = 'patient';
        
       
        
        /* 
         * Paging
         */
        $sLimit = "";
        $sOrder = "";
        
        
        
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayStart']).", ".
                mysqli_real_escape_string($this->db->conn_id,$_GET['iDisplayLength']);
        }


        /*
         * Ordering
         */
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                        ".mysqli_real_escape_string($this->db->conn_id,$_GET['sSortDir_'.$i]) .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                $sOrder = "";
            }
        }


        /* 
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        $sWhere = "WHERE companyid = ".$company." ";
        
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            if($sWhere == "")
                $sWhere = "WHERE (";
            else
                $sWhere .= "AND (";
            
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                if(($aColumns[$i] == "firstname" || $aColumns[$i] == "lastname") && ($table == 'doctors' || $table == 'user' ) )
                    $sWhere .= "CONCAT_WS(' ',firstname,lastname) LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'])."%' OR ";
                elseif(($aColumns[$i] == "patientFirstname" || $aColumns[$i] == "patientLastname") && ($table == 'patient' ))
                    $sWhere .= "CONCAT_WS(' ',patientFirstname,patientLastname) LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'])."%' OR ";
                    
                else
                    $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch'] )."%' OR ";
                    
                
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
            
            
            
        }
        
        /* Individual column filtering */
        for ( $i=0 ; $i<count($aColumns) ; $i++ )
        {
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
            {
                if ( $sWhere == "" )
                {
                    $sWhere = "WHERE ";
                }
                else
                {
                    $sWhere .= " AND ";
                }
               
                
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($this->db->conn_id,$_GET['sSearch_'.$i])."%' ";
        
                
                
            }
        }

        $join = "";
        
        /*
         * SQL queries
         * Get data to display
         */
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns)). 
            " FROM $sTable
            $join
            $sWhere
            $sOrder
            $sLimit
            
        ";
        
        
        $rResult = $this->db->query($sQuery);

        /* Data set length after filtering */
        $sQuery = "
            SELECT FOUND_ROWS() as frows
        ";
        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->result();
        $iFilteredTotal = $aResultFilterTotal[0]->frows;

        /* Total data set length */
        $sQuery = "
            SELECT COUNT(".$sIndexColumn.") as cnt
            FROM   $sTable
        ";
        $rResultTotal = $this->db->query($sQuery);
        $aResultTotal = $rResultTotal->result();
        $iTotal = $aResultTotal[0]->cnt;

        

        /*
         * Output
         */
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );

        foreach ($rResult->result() as $aRow)
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                if($aColumns[$i] == 'birthday')
                {
                    $row[] = date("M j, Y",strtotime($aRow->$aColumns[$i]));
                }
                else if ( $aColumns[$i] != ' ' )
                {
                    /* General output */
                    $row[] = $aRow->$aColumns[$i];
                }
                
            }
            $output['aaData'][] = $row;
        }
	   echo json_encode( $output );
    }
    
    
    
    
    
}