<?php

$config['user_columns'] = array("id","firstname","lastname","roles","username");
$config['user_index'] = "id";

$config['patient_columns'] = array("id","patientid","patientLastname","patientFirstname","birthday");
$config['patient_index'] = "id";

$config['doctors_columns'] = array("doctorId","lastname","firstname","qualification","specialization","schedule");
$config['doctors_index'] = "doctorId";

$config['procedures_columns'] = array("procedureId","procedureCode","procedureName","procedureFormalName","procedureShortName","procedureSpecimenRequirement","testTypeName","areaName","procedurePrice");
$config['procedures_index'] = "procedureId";

$config['laboratories_columns'] = array("laboratoryId","laboratoryBranch","laboratoryAddress");
$config['laboratories_index'] = "laboratoryId";

$config['hmo_columns'] = array("hmoId","hmoName","hmoCode","insuranceProvider");
$config['hmo_index'] = "hmoId";

$config['companies_columns'] = array("companyId","companyName","companyAddress","companyContactNumber","companyStatus");
$config['companies_index'] = "companyId";

$config['area_columns'] = array("areaId","areaName","roomNumber","testTypeName");
$config['area_index'] = "areaId";

$config['appointment_columns'] = array("appointmentId","patientLastname","patientFirstname","appointmentDate");
$config['appointment_index'] = "appointmentId";

$config['test_type_columns'] = array("testTypeId","testTypeName","testTypeDescription");
$config['test_type_index'] = "testTypeId";

$config['rx_template_columns'] = array("rxId","templateName","rxMedicine");
$config['rx_template_index'] = "rxId";

$config['tb_mas_content_columns'] = array('intID', 'strTitle', 'strSecondary');
$config['tb_mas_content_index'] = "intID";

$config['tb_mas_content_category_columns'] = array('intID', 'strTitle', 'dteStart','dteEnd');
$config['tb_mas_content_category_index'] = "intCategoryID";

$config['tb_mas_category_columns'] = array('intID', 'strName');
$config['tb_mas_category_index'] = "intID";

?>