
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/bootstrap.min.js"></script>
<!--DATA TABLES--->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/AdminLTE/app.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/plugins/flot/excanvas.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/plugins/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/plugins/flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/lib/adminlte/js/plugins/flot/jquery.flot.categories.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo $js_dir; ?>fastselect.standalone.min.js"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/lib/fancybox/jquery.fancybox.js';?>"></script>

<script>

    
    
    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
    
    $(document).ready(function(){
        
        //get_appointments();
        //setInterval("get_appointments()",5000);
        
        $('.date').datetimepicker({
            pickTime: false
         });
        
        $('.date-time').datetimepicker({
            pickTime: true
         });
        
         $(".select2").select2();
        
        
    
        $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                'Last 7 Days': [moment().subtract('days', 6), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month',1).endOf('month')]
                            },
                            startDate: moment().subtract('days', 29),
                            endDate: moment()
                        },
                function(start, end) {
                   var daterange = start.format('YYYY-MM-D') + '/' + end.format('YYYY-MM-D');
                   document.location="<?php echo base_url(); ?>unity/logs/"+daterange;
                }
                );
        $("#s1").change(function(e){
            var str = $(this).val();
            $(".filter-year :nth-child(2)").each(function(){
                if($(this).html().trim() != str)
                    $(this).parent().hide();
                else
                    $(this).parent().show();
            });
        });
        
        
        $('#date-filter-button').click(function(e){
            
            var filter = $("#date-filter").val();
            alert(filter);
            if(filter == "")
                document.location="<?php echo base_url(); ?>";
            else
                document.location="<?php echo base_url(); ?>unity/faculty_dashboard/"+filter;
                
            
            
            
        });
        
       
        $("#transcrossSelect").change(function(e){
            if($(this).val() == 'transferee' || $(this).val() == 'cross')
                $('#transcrossText').prop("disabled", false);
            else
                $('#transcrossText').prop("disabled", true);
        });
        
    });
    
    function get_appointments() { 
        $.ajax({
           type: "GET",
           url: "<?php echo base_url(); ?>appointment/get_appointment_alert/<?php echo $this->session->userdata('laboratoryid'); ?>",
           dataType:'json',
           success: function(data){

                if(data.count>parseInt($(".unread-message-alert").html())){
                    $(".unread-message-alert").removeClass('hide');
                    document.getElementById('ping').play();
                }
                if(data.count == 0)
                    $(".unread-message-alert").addClass('hide');

                $("#message-list").html(''); 
                for(i in data.appointment)
                {
                    htappend = '<li><a href="<?php echo base_url()."admin/dashboard/" ?>'+data.appointment[i].appointmentTestId+'"><div class="pull-left"></div><h4>'+data.appointment[i].testRequestCode+'<small><i class="fa fa-clock-o"></i> '+data.appointment[i].appointmentDate+'</small></h4><p>'+data.appointment[i].patientLastname+' '+data.appointment[i].patientFirstname+'</p></a></li><!-- end message -->';

                    $("#message-list").append(htappend);
                }


                $(".unread-message-alert").html(data.count);
                $(".unread-message-text").html(data.count);
            }
        });
    }
    
   
</script>
