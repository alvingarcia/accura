<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>Accura</title>
<!-----CSS----------------------------------------------->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/adminlte/css/jQueryUI/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/adminlte/css/bootstrap.min.css">    
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/adminlte/css/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/adminlte/css/font-awesome.min.css"> 
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/adminlte/css/select2/select2.min.css"> 
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css">  
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/adminlte/css/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/adminlte/css/ionicons.min.css">  
<link href="<?php echo base_url(); ?>assets/lib/adminlte/css/iCheck/all.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/adminlte/css/AdminLTE.min.css">
<?php 
        $skin = 'skin-black-light';
?>  
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/adminlte/css/skins/<?php echo $skin; ?>.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/adminlte/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" />
<link rel="stylesheet" href="<?php echo $css_dir; ?>fastselect.min.css">
    
<!-- Include Editor style. -->
<link href='<?php echo base_url(); ?>assets/lib/fwysiwig/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">

    <link href="<?php echo base_url(); ?>assets/lib/fwysiwig/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<link href='<?php echo base_url(); ?>assets/lib/fwysiwig/css/froala_style.min.css' rel='stylesheet' type='text/css' />


        
<link rel="stylesheet" type="text/css" href="<?php echo $css_dir; ?>full_calendar.css" />
<link rel="stylesheet" href="<?php echo $css_dir; ?>style.css">


<!-----END CSS------------------------------------------->
 <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    <audio id="ping" src="<?php echo base_url(); ?>assets/ping.mp3" preload="auto"></audio>
    
    
    
    
</head>
    <body class="sidebar-mini <?php echo $skin; ?>">
        <header class="main-header">
            <!-- Logo -->
            <a href="<?php echo base_url(); ?>admin" class="logo">
              <!-- mini logo for sidebar mini 50x50 pixels -->
              <span class="logo-mini"><b>ACX</b></span>
              <!-- logo for regular state and mobile devices -->
              <span class="logo-lg"><b>Accura</b>Xpress</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
              <!-- Sidebar toggle button-->
              <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
              </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown messages-menu hidden-xs">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-exclamation-triangle"></i>
                              <span class="label label-success unread-message-alert"></span>
                            </a>
                            <ul class="dropdown-menu">
                              <li class="header"></li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu" id="message-list">
                                        
                                    </ul>
                                </li>
                              <li class="footer"><a href="<?php echo base_url(); ?>admin/dashboard">Refresh Dashboard</a></li>
                            </ul>
                      </li>
                         <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-pencil-square-o"></i>
                                <span>Content <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url(); ?>admin/add_content">Add Content</a></li>
                                 <li><a href="<?php echo base_url(); ?>admin/view_all_content">View Content</a></li>
                             </ul>
                        </li>
                        
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $user['username']; ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?php echo ($user['picture']=="")?$img_dir."default_image.jpg":base_url().IMAGE_UPLOAD_DIR.$user['picture']; ?>" class="img-circle" alt="User Image">
                                    <p>
                                        <a style="color:#fff;" href="<?php echo base_url(); ?>faculty/my_profile"><?php echo $user['firstname']." ".$user['lastname']; ?></a>
                                        <small><?php echo $user['roles']; ?></small>
                                    </p>
                                    
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo base_url(); ?>faculty/edit_profile" class="btn btn-default btn-flat">Edit Profile</a>
                                    </div>
                                  
                                    <div class="pull-right">
                                        <a href="<?php echo base_url(); ?>users/logout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
          <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">                 
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo ($user['picture']=="")?$img_dir."default_image.jpg":base_url().IMAGE_UPLOAD_DIR.$user['picture']; ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $user['firstname']; ?></p>
                            <i class="fa fa-users text-green"></i> <?php echo $user['roles']; ?>
                        </div>
                    </div>
                    
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">Main Menu</li>
                        <li class="<?php echo (isset($page) && $page=="dashboard")?'active':''; ?>"><a href="<?php echo base_url() ?>admin"><i class="fa fa-home text-green"></i> <span>Dashboard</span></a></li>
                        <li class="<?php echo (isset($page) && $page=="dashboard")?'active':''; ?>"><a href="<?php echo base_url() ?>" target="_blank"><span>Patient Portal</span></a></li>
                         <?php if($user['roles'] == "admin" || $user['roles'] == "lcc_admin"): ?>
                        <li class="treeview <?php echo (isset($opentree) && $opentree=="appointment")?'active':''; ?>">
                            <a href="#">
                                <i class="fa fa-calendar"></i> <span>Transactions</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                 <li class="<?php echo (isset($page) && $page=="new_appointment")?'active':''; ?>"><a href="<?php echo base_url(); ?>appointment/new_appointment" style="margin-left: 10px;"><i class="ion ion-android-person-add"></i> New Transaction</a></li>
                                <li class="<?php echo (isset($page) && $page=="view_all_appointments")?'active':''; ?>"><a href="<?php echo base_url(); ?>appointment/view_all_appointments" style="margin-left: 10px;"><i class="ion ion-eye"></i> View Transactions</a></li>
                                
                            </ul>
                        </li>
                        <?php endif; ?>
                        <?php if($user['roles'] == "admin"): ?>
                        <li class="treeview <?php echo (isset($opentree) && $opentree=="admin")?'active':''; ?>">
                            <a href="#">
                                <i class="fa fa-lock"></i> <span>Admin</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                 <li class="<?php echo (isset($page) && $page=="add_user")?'active':''; ?>"><a href="<?php echo base_url(); ?>adminusers/add_user" style="margin-left: 10px;"><i class="ion ion-android-person-add"></i> Add User</a></li>
                                <li class="<?php echo (isset($page) && $page=="view_all_users")?'active':''; ?>"><a href="<?php echo base_url(); ?>adminusers/view_all_users" style="margin-left: 10px;"><i class="ion ion-eye"></i> View Users</a></li>
                                
                            </ul>
                        </li>
                        <?php endif; ?>
                        <?php if($user['roles'] == "admin" || $user['roles'] == "clinic staff" || $user['roles'] == "receptionist"): ?>
                        <li class="treeview <?php echo (isset($opentree) && $opentree=="patient")?'active':''; ?>">
                            <a href="#">
                                <i class="fa fa-wheelchair"></i> <span>Patient</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                 <li class="<?php echo (isset($page) && $page=="add_patient")?'active':''; ?>"><a href="<?php echo base_url(); ?>patient/add_patient" style="margin-left: 10px;"><i class="ion ion-android-person-add"></i> Add Patient</a></li>
                                <li class="<?php echo (isset($page) && $page=="view_all_patients")?'active':''; ?>"><a href="<?php echo base_url(); ?>patient/view_all_patients" style="margin-left: 10px;"><i class="ion ion-eye"></i> View Patients</a></li>
                                <li class="<?php echo (isset($page) && $page=="add_vitals")?'active':''; ?>"><a href="<?php echo base_url(); ?>vitals/add_vitals" style="margin-left: 10px;"><i class="ion ion-plus"></i> New Vitals</a></li>
                                
                            </ul>
                        </li>
                        <?php endif; ?>
                        
                        <?php if($user['roles'] == "admin"): ?>
                        <li class="treeview <?php echo (isset($opentree) && $opentree=="doctor")?'active':''; ?>">
                            <a href="#">
                                <i class="fa fa-user-md"></i> <span>Doctor/Medical Staff</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                 <li class="<?php echo (isset($page) && $page=="add_doctor")?'active':''; ?>"><a href="<?php echo base_url(); ?>doctor/add_doctor" style="margin-left: 10px;"><i class="ion ion-android-person-add"></i> Add Doctor/Staff</a></li>
                                <li class="<?php echo (isset($page) && $page=="view_all_doctors")?'active':''; ?>"><a href="<?php echo base_url(); ?>doctor/view_all_doctors" style="margin-left: 10px;"><i class="ion ion-eye"></i> View Doctors/Staff</a></li>
                                <li class="<?php echo (isset($page) && $page=="add_rx")?'active':''; ?>"><a href="<?php echo base_url(); ?>rx/add_rx" style="margin-left: 10px;"><i class="fas fa-plus"></i> Add Rx</a></li>
                                <li class="<?php echo (isset($page) && $page=="view_all_rx")?'active':''; ?>"><a href="<?php echo base_url(); ?>rx/view_all_rx" style="margin-left: 10px;"><i class="ion ion-eye"></i> View Rx Templates</a></li>
                                
                            </ul>
                        </li>
                        <?php endif; ?>
                        
                        <?php if($user['roles'] == "admin"): ?>
                        <li class="treeview <?php echo (isset($opentree) && $opentree=="area")?'active':''; ?>">
                            <a href="#">
                                <i class="fa fa-cogs"></i> <span>Area/Section</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                 <li class="<?php echo (isset($page) && $page=="add_area")?'active':''; ?>"><a href="<?php echo base_url(); ?>area/add_area" style="margin-left: 10px;"><i class="fa fa-plus"></i> Add Area</a></li>
                                <li class="<?php echo (isset($page) && $page=="view_all_areas")?'active':''; ?>"><a href="<?php echo base_url(); ?>area/view_all_areas" style="margin-left: 10px;"><i class="ion ion-eye"></i> View Area</a></li>
                                <li class="<?php echo (isset($page) && $page=="add_test_type")?'active':''; ?>"><a href="<?php echo base_url(); ?>test/add_test_type" style="margin-left: 10px;"><i class="fa fa-plus"></i> Add Section</a></li>
                                <li class="<?php echo (isset($page) && $page=="view_all_test_types")?'active':''; ?>"><a href="<?php echo base_url(); ?>test/view_all_test_types" style="margin-left: 10px;"><i class="ion ion-eye"></i> View Sections</a></li>
                            </ul>
                        </li>
                        <?php endif; ?>
                        
                        <?php if($user['roles'] == "admin"): ?>
                        <li class="treeview <?php echo (isset($opentree) && $opentree=="procedure")?'active':''; ?>">
                            <a href="#">
                                <i class="fa fa-medkit"></i> <span>Tests</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                 <li class="<?php echo (isset($page) && $page=="add_procedure")?'active':''; ?>"><a href="<?php echo base_url(); ?>test/add_procedure" style="margin-left: 10px;"><i class="fa fa-plus"></i> Add Test</a></li>
                                <li class="<?php echo (isset($page) && $page=="view_all_procedures")?'active':''; ?>"><a href="<?php echo base_url(); ?>test/view_all_procedures" style="margin-left: 10px;"><i class="ion ion-eye"></i> View Tests</a></li>
                                
                            </ul>
                        </li>
                        <?php endif; ?>
                        
                        <?php if($user['roles'] == "admin"): ?>
                        <li class="treeview <?php echo (isset($opentree) && $opentree=="laboratory")?'active':''; ?>">
                            <a href="#">
                                <i class="fa fa-hospital"></i> <span>Lab Collection Centers</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                 <li class="<?php echo (isset($page) && $page=="add_laboratory")?'active':''; ?>"><a href="<?php echo base_url(); ?>laboratory/add_laboratory" style="margin-left: 10px;"><i class="ion ion-android-person-add"></i> Add Collection Center</a></li>
                                <li class="<?php echo (isset($page) && $page=="view_all_laboratories")?'active':''; ?>"><a href="<?php echo base_url(); ?>laboratory/view_all_laboratories" style="margin-left: 10px;"><i class="ion ion-eye"></i> View Collection Centers</a></li>
                                
                            </ul>
                        </li>
                        <?php endif; ?>
                        <?php if($user['roles'] == "admin"): ?>
                        <li class="treeview <?php echo (isset($opentree) && $opentree=="hmo")?'active':''; ?>">
                            <a href="#">
                                <i class="fa fa-credit-card"></i> <span>HMO</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                 <li class="<?php echo (isset($page) && $page=="add_hmo")?'active':''; ?>"><a href="<?php echo base_url(); ?>hmo/add_hmo" style="margin-left: 10px;"><i class="ion ion-android-person-add"></i> Add HMO</a></li>
                                <li class="<?php echo (isset($page) && $page=="view_all_hmos")?'active':''; ?>"><a href="<?php echo base_url(); ?>hmo/view_all_hmos" style="margin-left: 10px;"><i class="ion ion-eye"></i> View HMOs</a></li>
                                
                            </ul>
                        </li>
                        <?php endif; ?>
                        <?php if($user['roles'] == "admin"): ?>
                        <li class="treeview <?php echo (isset($opentree) && $opentree=="companies")?'active':''; ?>">
                            <a href="#">
                                <i class="fa fa-users"></i> <span>Companies</span>
                                <i class="fa pull-right fa-angle-left"></i>
                            </a>
                            <ul class="treeview-menu">
                                
                                 <li class="<?php echo (isset($page) && $page=="add_company")?'active':''; ?>"><a href="<?php echo base_url(); ?>companies/add_company" style="margin-left: 10px;"><i class="ion ion-android-person-add"></i> Add Company</a></li>
                                <li class="<?php echo (isset($page) && $page=="view_all_companies")?'active':''; ?>"><a href="<?php echo base_url(); ?>companies/view_all_companies" style="margin-left: 10px;"><i class="ion ion-eye"></i> View Companies</a></li>
                                
                            </ul>
                        </li>
                        <?php endif; ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

        