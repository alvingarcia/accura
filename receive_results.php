<?php 

$arr = array();

$tests = array();


$results = array();
$results[] = array("rname"=>"color","rvalue"=>"light","flag"=>"normal","rr_conv"=>"","rr_si"=>"");
$tests[] = array("test_code"=>"443","name"=>"urinalysis","specimenID"=>"30","results"=>$results);
$results = array();
$results[] = array("rname"=>"WBC","rvalue"=>"102","flag"=>"low","rr_conv"=>"50-60hg/g","rr_si"=>"44-22ss/sd");
$results[] = array("rname"=>"RBC","rvalue"=>"500","flag"=>"high","rr_conv"=>"50-60hg/g","rr_si"=>"44-22ss/sd");
$results[] = array("rname"=>"Hbg","rvalue"=>"22","flag"=>"normal","rr_conv"=>"50-60hg/g","rr_si"=>"44-22ss/sd");
$tests[] = array("test_code"=>"224","name"=>"CBC","specimenID"=>"22","results"=>$results);

$order = array("transactionID"=>"123456","patientID"=>"8876642","patientAge"=>"45","patientName"=>"Juan Dela Cruz","tests"=>$tests);


echo json_encode($order); 

?>